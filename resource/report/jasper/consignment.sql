select 

  con.id 'conId' 
, deaCusAttr.name 'dealerName'
, total.amount 'conAmount'
, total.price 'conPrice'
from Consignment con

left join Dealer dea on dea.id = con.dealerId
inner join Customer deaCus on deaCus.id = dea.customerId
inner join PersonAttr deaCusAttr on deaCusAttr.id = deaCus.id 
inner join 
( select 
  ord.id orderId
, sum(if(flow.flowType in (101, 103), flow.amount, 0) - if(flow.flowType in (102, 104), flow.amount, 0)) amount
, sum(if(flow.flowType in (101, 103), flow.amount, 0) - if(flow.flowType in (102, 104), flow.amount, 0)
      * ps.price) price
    from ProductFlow flow
    inner join OrderItemProductFlow oiFlowConnector on oiFlowConnector.productFlowId = flow.id
    inner join OrderItem oi on oi.id = oiFlowConnector.orderItemId
    inner join ProductSnapshot ps on ps.id = oi.productSnapshotId
    inner join `Order` ord on ord.id = oi.orderId) total on total.orderId = con.orderId