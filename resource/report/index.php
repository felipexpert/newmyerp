<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once('class/tcpdf/tcpdf.php');
include_once('class/PHPJasperXML.inc.php');
include_once('../../application/config/database.php');

$PHPJasperXML = new PHPJasperXML();
//$PHPJasperXML->debugsql=true;
$parameters = [];
forEach($_GET as $k => $v) if($k != 'report') $parameters[$k] = $v;
$PHPJasperXML->arrayParameter = $parameters;
$PHPJasperXML->load_xml_file('jasper/'.$_GET['report'].'/'.$_GET['report'].'.jrxml');

$PHPJasperXML->transferDBtoArray(
  $db['default']['hostname']
, $db['default']['username']
, $db['default']['password']
, $db['default']['database']);
$PHPJasperXML->outpage("I");    //page output method I:standard output  D:Download file


?>
