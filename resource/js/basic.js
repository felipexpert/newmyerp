"use strict";
define(['util', 'language', 'formatter', 'numberInputFormatter', 'enterAsTab', 'positionHelper', 'toast'], 
    function(Util, lang, Formatter, NumberInputFormatter, EnterAsTab, PositionHelper, Toast) {
  var Basic = Object.create(null);

  //alert(lang.get('test', 'Felipe', '20'));
  if(typeof Promise === 'undefined' || !/firefox|chrome/i.test(navigator.userAgent)) 
    alert(lang.get('unsupportedBrowser')); 

  Basic.jsonForm = function(form, url, contentObject, success) {
    var method = Util.get;
    if(contentObject) method = Util.json;
    document.body.style.cursor = "progress";
    Basic.serverLoggerCommunication(method, [url, contentObject], lang.get('search'), function(result) { 
      if(result._status != 'success') alert(result._message || lang.get('wrongData'));
      for(var key in result) {
        if(key.slice(0, 1) == '_') continue;
        form[key].value = result[key];
      }
      Basic.doNF(form);
      success && success(result);
      document.body.style.cursor = "";
    }, function(error) {
        console.log(error);
      document.body.style.cursor = "";
    }, true);
  };

  Basic.submitForm = function(form, success, error, complete, formDataFile) {
    var formData = new FormData(form),
        xhr = new XMLHttpRequest();
    xhr.open(form.method, form.action, true);
    xhr.send(formData);
    xhr.onreadystatechange = function() { //Call a function when the state changes.
      if(xhr.readyState == 4) {
        if(xhr.status < 400) success && success(JSON.parse(xhr.responseText));
        else error && error();
        complete && complete();
      }
    }
  };
  /*Basic.submitForm = function(form, success, error, complete, formDataFile) {
    var formData = new FormData(form);
    formDataFile && formData.append(formDataFile.getName(), formDataFile.getFile());
    $.ajax({
      url: form.action,
      type: form.method,
      data: formData,
      processData: false,
      contentType: false,
      success: function(data, textStatus, jqXHR) {
        if(typeof data.error === 'undefined') {
          // Success
          success && success(data);
        } else {
          // Server error
          error && error(data.error);
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        // Ajax problem
        error && error(textStatus);
      },
      complete: function(){
        // Done
        complete && complete();
      } 
    });
  };*/

  Basic.submitLogForm = function(event, form, success, fail, formDataFile) {
    event.preventDefault();
    var submitButton = Util.queryOver(form, 'input[type="submit"]');
    var node;
    Util.queryOver(form, 'div[log]', true).forEach(function(e) {
      e.parentNode.removeChild(e);
    });
    Util.insertAfter(node = Util.elt("div"), form.firstChild);
    node.setAttribute("log", "");
    if(submitButton) submitButton.disabled = true;
    var log = Util.queryOver(form, "[log]");
    document.body.style.cursor = "progress";
    Basic.submitForm(form, function(result) {
      try {
        var succ = result._status == 'success';
        succ && !form.classList.contains('noReset') && form.reset();
        if(log) {
          if(succ) {
            log.setAttribute("log", "success");
            log.innerHTML = result.message || lang.get('success');
          } else {
            log.setAttribute("log", "fail");
            log.innerHTML = result.message || lang.get('fail');
          }
        }
        success && success(result, log);
      } catch(ex) {
        console.log("Result from server:", text);
        console.log("Exception:", ex);
      }
    }, function(error) {
      console.log("Error:", error);
      fail && fail(error);
    }, function() {
      document.body.style.cursor = "";
      if(submitButton) submitButton.disabled = false;
    }, formDataFile);
  };

  Basic.numberToMoney = function(number) {
    return Util.currencyFormat(number, lang.get('currency'), lang.get('c'), lang.get('.'), lang.get(','));
  };

  Basic.numberToM = function(number) {
    return Util.currencyFormat(number, '', lang.get('c'), lang.get('.'), lang.get(','));
  };

  Basic.verifyPlaces = function() {
    var currUrl = location.href;
    var urls = Util.toRealArray(arguments).map(function(place) {
      return baseUrl + place;
    });
    return urls.some(function(url) { return currUrl.indexOf(url) != -1; });
  };
 
  Basic.NumberFormatGetter = function(name, form) {
    this.name = name;
    this.form = form;
  };
  Basic.NumberFormatGetter.prototype.facade = function() {
    return Util.queryOver(this.form || document.body, '[data-name="' + this.name + '"]');
  };
  Basic.NumberFormatGetter.prototype.real = function() {
    return Util.queryOver(this.form || document.body, '[name="' + this.name + '"]')
  };
  
  Util.query('span.numberToMoney', true).forEach(function(span) {
    span.textContent =  Basic.numberToMoney(span.textContent);
  });

  Basic.formatters = Object.create(null);

  Basic.doNF = function(form) {
    Util.queryOver(form, 'input.nf', true).forEach(Basic.syncNF);
  };

  Basic.syncNF = function(input) {
    var formatter = Basic.formatters[input.name || input.id];
    formatter && formatter.updateFacade();
  };

  Basic.prepareNumberFormat = function(input) {
    var formatter = new NumberInputFormatter(input, input === document.activeElement, input.getAttribute('data-focus'));
    formatter.prepare();
    Basic.formatters[input.name || input.id] = formatter;
  };

  Basic.arrangeTable = function(table) {
    table.style.width = '100%';
    var tableWidth = new PositionHelper(table).bounds.width;
    var thArray = Util.queryOver(table, 'th', true);
    thArray.forEach(function(th, index) {
      var customWidth = th.getAttribute('data-width');
      if(customWidth) {
        th.style.width = customWidth / 100 * tableWidth + "px";
      } else {
        var x = tableWidth / thArray.length;
        var y = 0;
        if(table.rows[1]) {
          Util.toRealArray(table.rows[1].cells).some(function(cell, i) {
            if(index == i) {
              y = new PositionHelper(cell).bounds.width;
              return true;
            }
          });
        }
        th.style.width = Math.max(x, y) + "px";
      }
      var thBounds = new PositionHelper(th).bounds;
      var width = thBounds.width;
      Util.iterateTableRows(table, function(row) {  
        row.some(function(td, i) { 
          if(index == i) {
            td.style.width = width + "px";
            return true;
          }
        });
      });
    });
  };

  Basic.arrangeTables = function() {
    Util.query('table.scroll', true).forEach(Basic.arrangeTable);
  };
  Basic.arrangeTables();

  Basic.json = function(url, object, task, success, fail, nonSuccessMessage) {
    Basic.serverLoggerCommunication(Util.json, [url, object], task, success, fail, nonSuccessMessage);
  };

  Basic.post = Basic.json;

  Basic.get = function(url, task, success, fail, nonSuccessMessage) {
    Basic.serverLoggerCommunication(Util.get, [url], task, success, fail, nonSuccessMessage);
  };

  Basic.serverLoggerCommunication = function(method, args, task, success, fail, nonSuccessMessage) {
    var t;
    var late = setTimeout(function() { 
      t = new Toast(lang.get("toast.interactionLate", task), Toast.Type.INFO, Toast.Time.NORMAL);
      t.show();
    }, 2000);
    method.apply(null, args).then(function(result) {
      clearTimeout(late);
      success && success(Util.tryParse(result));
      if(!nonSuccessMessage) {
        t = new Toast(lang.get("toast.interactionSuccess", task), Toast.Type.SUCCESS, Toast.Time.NORMAL);
        t.show();
      }
    }, function(error) {
      clearTimeout(late);
      fail && fail(error);
      if(error instanceof Util.RequestError) {
        t = new Toast(lang.get("toast.interactionRequestError", task), Toast.Type.WARNING, Toast.Time.LONG);
        t.show();
      } else if(error instanceof Util.NetworkError) {
        t = new Toast(lang.get("toast.interactionNetworkError", task), Toast.Type.DANGER, Toast.Time.LONG);
        t.show();
      }
    });
  };

  Basic.basicResponse = function(r, succ, fail) {
   if(r._status == 'success')
     Util.evaluate(r.message && new Toast(r.message, Toast.Type.SUCCESS).show(), succ && succ());
   else
     Util.evaluate(r.message && new Toast(r.message, Toast.Type.DANGER).show(), fail && fail());
  };

  Util.query('[name]', true).forEach(function(node) {
    var name = node.getAttribute('name');
    if(name)
      node.setAttribute('id', name);
  });

  // Ajax Form Automation
  (function() {
    var forms = Util.query('form.ajax', true);
    if(!forms) return;
    forms.forEach(function(form) {
      form.addEventListener("submit", function(event) {
        Basic.submitLogForm(event, form, function(result, log) {
          if(result.success) {
            var redirectTo = Util.queryOver(form, '#redirect');
            if(redirectTo) {
              var value = redirectTo.value.split(';');
              var url = value[0], pageName = value[1];
              log.textContent += ". Você será redirecionado para " + pageName + " em 2 segundos";
              setTimeout(function() {
                window.open(url, '_self');
              }, 2500);
            }
          }
        });
      });
    }); 
  })();

  Formatter.decimals = lang.get('c');
  Formatter.fraction = lang.get('.');
  Formatter.separator = lang.get(',');

  Util.query('input.nf', true).forEach(Basic.prepareNumberFormat);

  Util.query('.enterAsTab', true).forEach(function(wrapper) {
    var inputs = Util.queryOver(wrapper, 'input, select', true).filter(function(input) {
      var isDisplayNone = input.style.display && input.style.display.toLowerCase() == 'none';
      return !isDisplayNone
          && !input.readOnly 
          && input.type != 'hidden' && input.type != 'submit'; 
    });
    var action = null,
        next = null;
    if(wrapper.nodeName == 'FORM') action = function() { Util.fireEvent(wrapper, 'submit'); };
    if(wrapper.hasAttribute('data-focus')) next = Util.query(wrapper.getAttribute('data-focus'));
    var tte = new EnterAsTab(inputs, action, next);
    tte.chain();
  });

  return Basic;
});
