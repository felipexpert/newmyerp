"use strict";
var words = Object.create(null);

// Number standarts (based on American format)
words["currency"] = "R$";
words["c"] = 2;
words["."] = ",";
words[","] = ".";

// General
words["parseDate"] = function(date) {
  var year = NaN, month = NaN, day = NaN;
  if(date.length === 10) {
    year = parseInt(date.slice(6));
    month = parseInt(date.slice(3, 5));
    day = parseInt(date.slice(0, 2));
  }
  year = isNaN(year) ? 1970 : Math.min(3000, Math.max(1970, year));
  month = isNaN(month) ? 1 : Math.min(12, Math.max(1, month))
  day = isNaN(day) ? 1 : Math.min(31, Math.max(1, day));
  var date = new Date(year, month - 1, day);
  return date.getTime();
};
(function() {
  words["date"] = function(milliseconds) {
    var date = new Date(milliseconds);
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    return pad(0, 2, day) + "/" + pad(0, 2, monthIndex + 1) + "/" + pad(0, 4, year);
  };
  words["dateFull"] = function(milliseconds) {
    var date = new Date(milliseconds);
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    return pad(0, 2, day) + "/" + pad(0, 2, monthIndex + 1) + "/" + pad(0, 4, year) 
      + " " + pad(0, 2, hours) + ":" + pad(0, 2, minutes) + ":" + pad(0, 2, seconds);
  };
  function pad(symbol, length, text) {
    text = String(text);
    symbol = String(symbol);
    while(text.length < length) text = symbol + text;
    return text;
  }
})();
words["activated"] = "Ativado";
words["code"] = "Código";
words["deactivated"] = "Desativado";
words["deletePos"] = "Insira a posição a ser excluída";
words["dynamicSearch"] = "Busca Dinâmica";
words["fail"] = "Falha";
words["hashcode"] = "Hashcode";
words["invalidCode"] = "Código Inválido";
words["name"] = "Nome";
words["newDefinitions"] = "Novas definições foram obtidas do servidor";
words["noCode"] = "Código em branco";
words["redirectMessage"] = function(info, pageName) {
  return info + ". Você será redirecionado para " + pageName + " em 2 segundos";
};
words["search"] = "Procura";
words["subtotal"] = "Subtotal";
words["success"] = "Sucesso";
words["total"] = "Total";
words["unsupportedBrowser"] = "Por favor, use o navegador Firefox ou Google Chrome (atualizados preferencialmente)";
words["update"] = "Update";
words["wrongData"] = "Dados incoerentes";
words["zeroError"] = "A quantidade não pode ser zero";

// Sort alphabetically the following!

// Billing
words["billing.aggregates"] = "Agregados";
words["billing.discount"] = "Desconto";
words["billing.byCash"] = "À vista";
words["billing.change"] = function(formatedAmount) {
  return "Troco " + formatedAmount;
};
words["billing.enterCPF"] = "Digite o CPF"; 

// Consignment
words["consign.actionOnItemAlreadyDone"] = function(action, item, row) {
  return "Uma " + action + " já foi efetuada no item " + item + " da linha " + row + ", deseja continuar?";
};
words["consign.boundWith"] = "Vinculado com";
words["consign.cancelQuestion"] = "Deseja cancelar este consignado?";
words["consign.closingDate"] = "Data de fechamento";
words["consign.consignorName"] = "Nome do consignante";
words["consign.creationDate"] = "Data de criação";
words["consign.description"] = "Descrição";
words["consign.in"] = "Entrada";
words["consign.makeSaleQuestion"] = "Deseja realmente finalizar este consignado e transformar em venda?";
words["consign.notBound"] = "Sem vínculo";
words["consign.out"] = "Saída";
words["consign.previsionClosingDate"] = "Previsão de fechamento";
words["consign.totalOut"] = "Total de saída";

// FastSelling
words["fastSelling.addProduct"] = "Adicionar Produto";
words["fastSelling.alreadyInUseIdentifier"] = function(identifier) {
  return "O identificador \"" + identifier + "\" já está em uso.";
};
words["fastSelling.cancelQuestion"] = "Deseja cancelar esta venda?";
words["fastSelling.createSale"] = "Criação da Venda";
words["fastSelling.identifier"] = "Identificador";
words["fastSelling.removeOrderItem"] = "Remover Item";
words["fastSelling.removeQuestion"] = "Deseja realmente excluir?";
words["fastSelling.updateOrderItem"] = "Atualizar Item";
words["fastSelling.newSaleIdentifier"] = "Identificador da nova venda";
words["fastSelling.notCreatedSale"] = "A Venda atual ainda não foi criada";

// FlowType
words["flowType.OUT_CONSIGNED"] = "Consignado saída";
words["flowType.OUT_CONSIGNED_OFFSET"] = "Consignado saída reajuste";
words["flowType.IN_CONSIGNED"] = "Consignado entrada";
words["flowType.IN_CONSIGNED_OFFSET"] = "Consignado entrada reajuste";

// OrderItem
words["orderItem.deleteQuestion"] = (name) => "Tem certeza que quer excluir o item \"" + name + "\"?";

// Personal
words["person.name"] = "Nome";
words["person.fone"] = "Telefone";
words["person.address"] = "Endereço";
words["person.cpf"] = "CPF";
words["person.rg"] = "RG";

// PortionBucket
words["portionBucket.paymentTypeUndefined"] = "Tipo de pagamento nao definido";

// ProdCategory
words["prodCategory.printerName"] = "Nome da impressora";
words["prodCategory.printerPath"] = "Path";

// Product
words["product.strangeQuantity"] = (quantity) => "Tem certeza que deseja prosseguir com a quantidade " + quantity + "?";
words["product.empty"] = "Vazio";
words["product.name"] = "Nome";
words["product.price"] = "Preço";
words["product.characteristicName"] = "Insira o nome da característica";
words["product.characteristicInstanceName"] = function(characteristicName) {
  return "Insira um(a) " + characteristicName;
};
words["product.productHasntBeenLoaded"] = "O produto não foi carregado";

// Product Characteristic
words["pChar.cannotDeleteItem"] = (name) => "Item " + name + " não pode ser deletado!";

// Tables
words["tables.update"] = "Atualizar Mesas"; 

// Toast messages
words["toast.interactionLate"] = function(task) { 
  return "Tarefa " + (task ? task + " " : "") + "está ocorrendo"; 
};
words["toast.interactionNetworkError"] = function(task) { 
  return "Por causa da rede a tarefa " + (task ? task + " " : "") + "falhou"; 
};
words["toast.interactionRequestError"] = function(task) { 
  return "Tarefa " + (task ? task + " " : "") + "falhou no servidor"; 
};
words["toast.interactionSuccess"] = function(task) { 
  return "Tarefa " + (task ? task + " " : "") + "concluída"; 
};
