"use strict";
define(["util", "toast", "language"], function(Util, Toast, lang) {
  var Request = Object.create(null);
  
  Request.METHODS = Object.create(null);
  Request.METHODS.GET = "GET";
  Request.METHODS.POST = "POST";
  Request.METHODS.PUT = "PUT";
  Request.METHODS.DELETE = "DELETE";

  Request.request = function(method, url, content) {
    return new Promise(function(succeed, fail) {
      var req = new XMLHttpRequest();
      req.open(method, url, true);
      req.addEventListener("load", function() {
        succeed && succeed({code: req.status, body: req.responseText, type: req.getResponseHeader("Content-Type")});
      });
      req.addEventListener("error", function() {
        fail && fail(new Request.NetworkError("Network Error!"));
      });
      req.setRequestHeader("Origin", "http://localhost:3000");
      req.setRequestHeader("Access-Control-Request-Method", "GET");
      req.setRequestHeader("Access-Control-Request-Headers", "X-Custom-Header");
      req.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
      req.send(content);
    });
  };

  Request.toastRequest = function(method, url, content, task, nonSuccessMessage) {
    return Request.request(method, url, content).then(function(response) {
      if(response.code >= 400) {
        var t = new Toast(lang.get("toast.interactionRequestError", task), Toast.Type.WARNING, Toast.Time.LONG);
        t.show();
        throw new Request.RequestError("Request Error!");
      } else if(!nonSuccessMessage) {
        var t = new Toast(lang.get("toast.interactionSuccess", task), Toast.Type.SUCCESS, Toast.Time.NORMAL);
        t.show();
      }
      return response;
    }, function(error) {
      var t = new Toast(lang.get("toast.interactionNetworkError", task), Toast.Type.DANGER, Toast.Time.LONG);
      t.show();
      throw error;
    });
  };

  Util.createException('NetworkError', Request);
  Util.createException('RequestError', Request);

  return Request;
});
