"use strict";

define(['optional'], (Optional) => {
  let Misc = Object.create(null);
  Misc.queryStringValue = (key) => {
    let matches = new RegExp(key + "=(.+?)(&|$)").exec(location.search);
    return Optional.fromNullable(matches).map((matches) => matches[1]);
  };
  Misc.clone = (obj) => {
    if(null === obj || undefined === obj || "object" !== typeof obj) return obj;
    if(obj instanceof Date) return Mist.cloneDate(obj);
    if(obj instanceof Array) return obj.map(Misc.clone);
    if(obj instanceof Object) {
      let copy = {};
      for(let attr in obj) if(obj.hasOwnProperty(attr)) 
        copy[attr] = Misc.clone(obj[attr]);
      return copy;
    }
    throw new Error("Unable to copy obj! Its type \"" + (typeof obj) + "\" isn't supported.");
  };
  Misc.cloneDate = (date) => {
    let copy = new Date();
    copy.setTime(date.getTime());
    return copy;
  };
  return Misc;
});
