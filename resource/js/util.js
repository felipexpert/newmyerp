"use strict";
define(function() {
  var Util = Object.create(null);

  Util.AssertionFailed = function(message) {
    this.message = message;
    this.stack = (new Error()).stack;
  }
  Util.AssertionFailed.prototype = Object.create(Error.prototype);

  Util.assert = function(test, message) {
    if(!test) throw new Util.AssertionFailed(message || 'Assertion failed!');
  }

  Util.toRealArray = function(array) {
    return Array.prototype.slice.call(array, 0);
  };

  Util.objToArray = function(obj, f) {
    var array = [];
    if(f) for(var k in obj)
      array.push(f(obj[k], k));
    else for(var k in obj)
      array.push(obj[k]);
    return array;
  }

  Util.woIndex = function(array, index) {
    return array.slice(0, index).concat(array.slice(index + 1));
  }

  Util.queryOver = function(element, tag, all) {
    if(all)
      return Util.toRealArray(element.querySelectorAll(tag));
    return element.querySelector(tag);
  };

  Util.query = function(tag, all) {
    return Util.queryOver(document, tag, all);
  };

  Util.elt = function(name, attributes) {
    var node = document.createElement(name);
    if(attributes) {
      for(var attrib in attributes)
        if(attributes.hasOwnProperty(attrib))
          node.setAttribute(attrib, attributes[attrib]);
    }
    var args = Util.toRealArray(arguments).slice(2);
    args.forEach(function(e) {
      Util.assert(e && e.nodeType || typeof e == 'string' || typeof e == 'number', 
          "elt: Invalid element type " + (typeof e) + ", when creating a " + name);
      if(typeof e == "string" || typeof e == "number")
        e = document.createTextNode(e);
      node.appendChild(e);
    });
    return node;
  }

  Util.createException = function(exceptionName, wrapper) {
    wrapper = wrapper || window;
    wrapper[exceptionName] = function(message) {
      this.message = message;
      this.stack = (new Error()).stack;
    };
    wrapper[exceptionName].prototype = Object.create(Error.prototype);
    wrapper[exceptionName].prototype.name = exceptionName;
  };
  
  Util.createException('NetworkError', Util);
  Util.createException('RequestError', Util);

  Util.get = function(url) {
    return new Promise(function(succeed, fail) {
      var req = new XMLHttpRequest();
      req.open("GET", url, true);
      req.addEventListener("load", function() {
        if (req.status < 400)
          succeed && succeed(req.responseText);
        else
          fail && fail(new Util.RequestError("Request failed: " + req.statusText));
      });
      req.addEventListener("error", function() {
        fail && fail(new Util.NetworkError("Network error"));
      });
      req.send(null);
    });
  };

  Util.json = function(url, object) {
    var content = JSON.stringify(object);
    return new Promise(function(succeed, fail) {
      var req = new XMLHttpRequest();
      req.open("POST", url, true);
      req.addEventListener("load", function() {
        if(req.status < 400)
          succeed && succeed(req.responseText);
        else
          fail && fail(new Util.RequestError("Request failed: " + req.statusText));
      });
      req.addEventListener("error", function() {
        fail && fail(new Util.NetworkError("Network error"));
      });
      req.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
      req.send(content);
    });
  };

  Util.post = Util.json;

  Util.tryAgain = function(method, url, content, run) {
    method(url, content).then(run, function(error) {
      console.log(error);
      tryAgain(method, url, run, content);
    });
  };

  Util.insertAfter = function(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
  };

  Util.insertBefore = function(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode);
  };

  Util.fireEvent = function(element, event) {
    if (document.createEvent) {
      // dispatch for firefox + others
      var evt = document.createEvent("HTMLEvents");
      evt.initEvent(event, true, true ); // event type,bubbling,cancelable
      return !element.dispatchEvent(evt);
    } else {
      // dispatch for IE
      var evt = document.createEventObject();
      return element.fireEvent('on'+event,evt)
    }
  };

  Util.capFirstLetter = function(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  Util.ucFirst = Util.capFirstLetter;

  Util.dateToText = function(date, format) {
    format = format.replace("YYYY", date.getFullYear())
    .replace("MM", padWith(date.getMonth() + 1, '0', 2))
    .replace("DD", padWith(date.getDate(), '0', 2))
    .replace("hh", padWith(date.getHours(), '0', 2))
    .replace("mm", padWith(date.getMinutes(), '0', 2))
    .replace("ss", padWith(date.getSeconds(), '0', 2))
    return format;
  };

  Util.padWith = function(text, character, length) {
    text = text + "";
    for(var pos = text.length; pos < length; pos++)
      text = character + text;
    return text;
  };

  /* inspired by https://gist.github.com/1129031 */
  /*global document, DOMParser*/
  (function(DOMParser) {
    var proto = DOMParser.prototype, 
        nativeParse = proto.parseFromString;

    // Firefox/Opera/IE throw errors on unsupported types
    try {
      // WebKit returns null on unsupported types
      if ((new DOMParser()).parseFromString("", "text/html")) {
        // text/html parsing is natively supported
        return;
      }
    } catch (ex) {}

    proto.parseFromString = function(markup, type) {
      if (/^\s*text\/html\s*(?:;|$)/i.test(type)) {
        var doc = document.implementation.createHTMLDocument("");
        if (markup.toLowerCase().indexOf('<!doctype') > -1) {
          doc.documentElement.innerHTML = markup;
        } else {
          doc.body.innerHTML = markup;
        }
        return doc;
      } else {
        return nativeParse.apply(this, arguments);
      }
    };
  }(DOMParser));
  
  Util.currencyToNumber = function(currency, d) {
    d = d == undefined ? "," : d;
    var number = "";
    for(var i = 0; i < currency.lengh; i++) {
      var c = currency[i];
      if(/\d/.test(c)) number += c;
      else if (c == d) number += '.';
    }
    return Number(number);
  };

  Util.currencyFormat = function(n, prefix, c, d, t) {
    prefix = prefix == undefined ? "$" : prefix,
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t; 
    var s = n < 0 ? "-" : "", 
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
        j = (j = i.length) > 3 ? j % 3 : 0;
    return prefix + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) 
         + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
  };

  Util.iterateTableRows = function(table, func, offset) {
   if(offset === undefined) offset = 1;
   for (var i = 0 + offset, row; row = table.rows[i]; i++) {
     //iterate through rows
     //rows would be accessed using the "row" variable assigned in the for loop
     var array = [];
     for (var j = 0, col; col = row.cells[j]; j++) {
       //iterate through columns
       //columns would be accessed using the "col" variable assigned in the for loop
       array.push(col);
     }
     func(array, i);  
   } 
  };

  Util.sortTableByColumn = function(table, columnIndex, criteria) {
    var rows = Util.queryOver(table, 'tbody tr', true);
    if(!rows.length) rows = Util.queryOver(table, 'tr', true);
    var parent = rows[0].parentNode;
    rows.forEach(function(r) {
      parent.removeChild(r);
    });
    rows.sort(function(a,b) {
      var columnA = a.cells[columnIndex].textContent,
          columnB = b.cells[columnIndex].textContent;
      if(criteria) return criteria(columnA, columnB);
      return columnA.localeCompare(columnB)
    });
    rows.forEach(function(r) {
      parent.appendChild(r);
    }); 
  };
  Util.getViewportHeight = function() {
    return Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  };
  Util.getViewportWidth = function() {
    return Math.max(document.documentElement.clientHeight, window.innerHeight || 0);   
  };

  Util.getHeight = function() {
    var body = document.body,
        html = document.documentElement;
    return Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, 
        html.offsetHeight);
  };
  Util.getWidth = function() {
    var body = document.body,
        html = document.documentElement;
    return Math.max(body.scrollWidth, body.offsetWidth, html.clientWidth, html.scrollWidth, 
        html.offsetWidth);
  };
  Util.killCtrl = function(action) {
    return function(event) {
      if(event.ctrlKey) {
        action && action(event);
        if(!Util.intersection([72, 76, 82], [event.keyCode]).length) {
          event.preventDefault();
        }
      } 
    };
  };
  Util.clamp = function(number, min, max) {
    return Math.max(min, Math.min(number, max));
  };
  Util.repeat = function(x, f, oneBased) {
    if(oneBased) for(var i = 1; i <= x; i++)
      f(i);
    else for(var i = 0; i < x; i++)
      f(i);
  };
  Util.produce = function(x, f, oneBased, array) {
    if(array) array = array.slice(0);
    else array = [];
    if(oneBased) for(var i = 1; i <= x; i++)
      array.push(f(i));
    else for(var i = 0; i < x; i++)
      array.push(f(i));
    return array;
  };
  Util.push = function() {
    var xs = arguments[0].slice(0);
    if(arguments.length == 2)
     xs.push(arguments[1]);
    else if(arguments.length == 3)
      xs[arguments[1]] = arguments[2];
    return xs;
  };
  Util.getUrlId = function() {
    var href = window.location.href;
    return href.slice(href.lastIndexOf('/') + 1).replace(/^d+/, "$&");
  };
  Util.eq = function(a, b) {
    // 1 / 128 -> EPSILON
    return Math.abs(a - b) < 0.0078125;
  };
  Util.le = function(a, b) {
    return Util.eq(a, b) || a < b;
  };
  Util.lt = function(a, b) {
    return !Util.eq(a, b) && a < b;
  }
  Util.ge = function(a, b) {
    return Util.eq(a, b) || a > b;
  };
  Util.gt = function(a, b) {
    return !Util.eq(a, b) && a > b;
  }
  Util.zero = function(a) {
    return Util.eq(a, 0) ? 0 : a;
  };
  Util.intersection = function(a, b) {
    return a.filter(function(e) {
      return b.indexOf(e) != -1;
    });
  };
  Util.executeFunctionByName = function(functionName, context) {
    var args = Array.prototype.slice.call(arguments, 2);
    var namespaces = functionName.split(".");
    var func = namespaces.pop();
    for (var i = 0; i < namespaces.length; i++) {
      context = context[namespaces[i]];
    }
    return context[func].apply(context, args);  
  };
  Util.evaluate = function() { return arguments[arguments.length - 1]; };
  Util.tryParse = function(text) {
    try {
      return JSON.parse(text);
    } catch(_) {
      console.log("Could not parse the following:", text);
      return null;
    }
  };
  Util.isSet = function(e) {
    return !(e === undefined || e === null);
  };
  Util.openInNewTab = function(url) {
    var win = window.open(url, "_blank");
    win.focus();
  };
  return Util;
});
