define(['util', 'positionHelper'], function(Util, PositionHelper) {
  Toast.Type = Object.create(null);
  Toast.Type.SUCCESS = 'success';
  Toast.Type.INFO = 'info';
  Toast.Type.WARNING = 'warning';
  Toast.Type.DANGER = 'danger';

  Toast.Time = Object.create(null);
  Toast.Time.LONG = 5000;
  Toast.Time.NORMAL = 2500;

  Toast.Z_INDEX = 300;

  function Toast(text, type, time) {
    this.text = text;
    this.type = type || Toast.Type.INFO;
    this.time = time || Toast.Time.NORMAL;
  }

  Toast.prototype.show = function() {
    var toast = Util.elt('div', {class: this._getClass()}, this.text);
    toast.style.position = 'fixed';
    toast.style.zIndex = Toast.Z_INDEX;
    document.body.appendChild(toast);
    var helper = new PositionHelper(toast);
    var bounds = helper.bounds;
    var width = document.body.clientWidth;
    toast.style.left = width / 2 - bounds.width / 2 + "px";
    setTimeout(function() { 
      toast.parentNode.removeChild(toast); 
      Toast._toasts = Util.woIndex(Toast._toasts, Toast._toasts.indexOf(this));
      Toast._arrangeItems();
    }.bind(this), this.time);
    this.toast = toast;
    this.toastHeight = bounds.height;
    Toast._toasts.push(this);
    Toast._arrangeItems();
  };

  Toast._toasts = [];
  Toast._arrangeItems = function() {
    var height = document.body.clientHeight,
        offset = 0,
        margin = height / 64;
    Toast._toasts.forEach(function(toast) {
      toast.toast.style.bottom = offset + margin + "px";
      offset += toast.toastHeight + margin;
    });
  };
  Toast.prototype._getClass = function() {
    return 'alert alert-' + this.type;
  };
  
  return Toast;
});
