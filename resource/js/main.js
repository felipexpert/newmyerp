"use strict";

require.config({
  scriptType: "application/javascript;version=1.8",
  urlArgs: "bust=" + 27,
  //urlArgs: "bust=" + (new Date()).getTime(),
  shim: {
    angular: {
      exports: "angular"
    }
  },
  // Add this map config in addition to any baseUrl or
  // paths config you may already have in the project.
  map: {
    // '*' means all modules will get 'jquery-private'
    // for their 'jquery' dependency.
    '*': { 'jquery': 'jquery-private' },

    // 'jquery-private' wants the real jQuery module
    // though. If this line was not here, there would
    // be an unresolvable cyclic dependency.
    'jquery-private': { 'jquery': 'jquery' }
  } 
});
require(["switch"]);
