"use strict";

define(['basic', 'language'], function(Basic, lang) {
  function HashcodeChecker(hashcodeUrl, hashcodeFunction, updateUrl, updateFunction, delay) {
    this.hashcodeUrl = hashcodeUrl;
    this.hashcodeFunction = hashcodeFunction;
    this.updateUrl = updateUrl;
    this.updateFunction = updateFunction;
    this.delay = delay;
  }
  HashcodeChecker.prototype.registerPeriodicEvent = function() {
    setTimeout(function() {
      Basic.get(this.hashcodeUrl, lang.get('hashcode'), function(result) {
        var found = this.hashcodeFunction(result)
        if(found) Basic.get(this.updateUrl, lang.get('update'), function(result) {
          this.updateFunction(result);
          new Basic.Toast(lang.get('newDefinitions'), 'info', Basic.Toast.LONG).show();
          this.registerPeriodicEvent();
        }.bind(this), null, true);
        found || this.registerPeriodicEvent();
      }.bind(this), this.registerPeriodicEvent.bind(this), true);
    }.bind(this), this.delay); 
  };
  HashcodeChecker.LONG = 5000;
  HashcodeChecker.FAST = 2000;
  return HashcodeChecker;
});
