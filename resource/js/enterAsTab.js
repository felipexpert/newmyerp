"use strict";
define(['util'], function(Util) {
  function EnterAsTab(focusables, action, next) {
    this.focusables = focusables;
    this.action = action;
    this.next = next;
    this.cleanBucket = [];
    this.chained = false;
  }

  EnterAsTab.prototype.chain = function() {
    if(this.chained) throw new EnterAsTab.ChainedError();
    this.focusables.forEach(function(f, i) {
      if(i + 1 < this.focusables.length) 
        this._setupEvent(f, this.focusables[i + 1]);
      else 
        this._setupEvent(f, this.next ? this.next : this.focusables[0], this.action); 
    }.bind(this));
    this.chained = true;
  };

  EnterAsTab.prototype.unchain = function() {
    if(!this.chained) throw new EnterAsTab.UnchainedError();
    this.cleanBucket.forEach(function(obj) {
      obj.element.removeEventListener('keypress', obj.event);
    });
    this.cleanBucket = [];
    this.chained = false;
  };

  EnterAsTab.prototype._setupEvent = function(from, to, action) {
    var keypressEvent = function(event) {
      if(event.which == 13) {
        event.preventDefault();
        to.select ? to.select() : to.focus();
        //to.tagName == 'SELECT' ? to.focus() : to.select();
        action && action();
      }
    };
    from.addEventListener('keypress', keypressEvent);
    this.cleanBucket.push({element: from, event: keypressEvent});
  };

  Util.createException('ChainedError', EnterAsTab);
  Util.createException('UnchainedError', EnterAsTab);

  return EnterAsTab;
});
