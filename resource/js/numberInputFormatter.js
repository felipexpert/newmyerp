"use strict";
define(['util', 'formatter'], function(Util, Formatter) {

  function NumberInputFormatter(input, hasFocus, focusValue) {
    this.input = input;
    this.hasFocus = hasFocus;
    this.focusValue = focusValue;
    this.prepared = false;
  }

  NumberInputFormatter.prototype.prepare = function() {
    if(this.prepared) throw new NumberInputFormatter.PreparedError();
    this.input.style.display = 'none';
    this.wrapper = Util.elt('span');
    Util.insertBefore(this.wrapper, this.input);
    this.wrapper.appendChild(this.input);
    this.facade = Util.elt('input', {type: 'text', 
      'data-name': this.input.name, 
      class: this.input.className});
    this.facade.readOnly = this.input.readOnly;
    this.facade.autofocus = this.input.autofocus;
    this.wrapper.insertBefore(this.facade, this.input);
    this.updateFacade();
    this.facade.addEventListener('keyup', function() {
      var cleaned = Formatter.clean(this.facade.value);
      this.facade.value = cleaned;
      this.input.value = Formatter.formatCleaned(cleaned);
    }.bind(this));
    this.facade.autofocus && this.facade.focus();
    (this.focusValue || this.focusValue === "") && this.facade.addEventListener('focus', function() {
      setTimeout(function() {
        this.input.value = this.focusValue;
        this.updateFacade();
        this.facade.select(); 
      }.bind(this), 150);
    }.bind(this));
    this.prepared = true;
  };

  NumberInputFormatter.prototype.updateFacade = function() {
    if(this.input.value)
      this.facade.value = Util.currencyFormat(this.input.value, 
          '', 
          Formatter.decimals, 
          Formatter.fraction, 
          Formatter.separator);
    else
      this.facade.value = "";
  };

  NumberInputFormatter.prototype.updateValue = function(newValue) {
    this.input.value = newValue;
    this.updateFacade();
  };

  Util.createException('PreparedError', NumberInputFormatter);

  return NumberInputFormatter;
});
