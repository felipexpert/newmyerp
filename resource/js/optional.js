"use strict";

define(["util"], (Util) => {
  function Optional() {}
  Optional.absent = () => Absent._INSTANCE;
  Optional.of = (reference) => new Present(checkNotNull(reference));
  Optional.fromNullable = (reference) => Util.isSet(reference) ? new Present(reference) : Absent._INSTANCE;

  function Absent() {}
  Absent.prototype = Object.create(Optional.prototype);
  Absent._INSTANCE = new Absent();
  Absent.prototype.ifPresent = () => {};
  Absent.prototype.isPresent = () => false;
  Absent.prototype.get = () => { throw new Optional.IllegalStateError("Cannot call get on Absent instance"); };
  Absent.prototype.getOrElse = (defaultValue) => {
    let message = "use Optional.orNull() instead of Optional.getOrElse(null)";
    return checkNotNull(defaultValue, message);
  };
  Absent.prototype.getOrNull = () => null;
  Absent.prototype.map = function() { return this };
  Absent.prototype.equals = function(object) { return object === this; }; 

  function Present(reference) {
    this.reference = reference;
  }
  Present.prototype = Object.create(Optional.prototype);
  Present.prototype.ifPresent = function(func) { func(this.reference); };
  Present.prototype.isPresent = () => true;
  Present.prototype.get = function() { return this.reference; };
  Present.prototype.getOrElse = function(defaultValue) {
    checkNotNull(defaultValue, "use Optional.orNull() instead of Optional.getOrElse(null)");
    return this.reference;
  };
  Present.prototype.getOrNull = Present.prototype.get;
  Present.prototype.map = function(mapper) { return Optional.fromNullable(mapper(this.reference)); };
  Present.prototype.equals = function(object) {
    if(object instanceof Present) return this.reference === object.get();
    return false;
  };


  function checkNotNull(reference, message = "Unallowed null in reference found.") {
    if(!Util.isSet(reference)) throw new Optional.NullPointerError(message);
    return reference;
  }
  Util.createException("NullPointerError", Optional);
  Util.createException("IllegalStateError", Optional);

  return Optional;
});
