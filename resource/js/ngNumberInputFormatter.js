"use strict";

define(['app', 'util', 'basic', 'numberInputFormatter'], function(app, util, basic, NumberInputFormatter) {
  app.directive('numberInput', function() {
    return {
      restrict: 'E',
      controllerAs: 'ctrl', 
      template: '<input name="{{ctrl.name}}">',
      controller: function() {
        this.name = 'name',
        /*var input = Util.query('input');      
        var formatter = new NumberInputFormatter(input);
        formatter.prepare();*/
      }
    }
  });
})
