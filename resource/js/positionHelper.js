"use strict";
define(function() {
  function PositionHelper (element, isRelativeToWholeDocument) {
    this._element = element;
    this._isRelativeToWholeDocument = isRelativeToWholeDocument;
    this._bounds = null;
  }

  Object.defineProperty(PositionHelper.prototype, 'element', {
    get: function() { return this._element; }
  });

  Object.defineProperty(PositionHelper.prototype, 'isRelativeToWholeDocument', {
    get: function() { return this._isRelativeToWholeDocument; }
  });

  Object.defineProperty(PositionHelper.prototype, 'bounds', {
    get: function() {
      if(this._bounds === null) {
        var bds = this.element.getBoundingClientRect();
        this._bounds = { top: bds.top, right: bds.right, 
              bottom: bds.bottom, left: bds.left,
              height: bds.bottom - bds.top, width: bds.right - bds.left };
        if(this.isRelativeToWholeDocument) {
          this._bounds.top += pageYOffset;
          this._bounds.right += pageXOffset;
          this._bounds.bottom += pageYOffset;
          this._bounds.left += pageXOffset;
        }
      }
      return this._bounds;
    }
  });
  
  return PositionHelper;
});
