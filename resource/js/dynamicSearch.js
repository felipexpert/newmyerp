"use strict";

define(['util', 'basic', 'language', 'tableSelector'], function(Util, Basic, lang, TableSelector) {
  Util.query('.dynamicSearch', true).forEach(function(div) {
    var form = Util.queryOver(div, 'form');
    var tableTarget = Util.queryOver(div, '.tableTarget');
    var table = Util.elt('table', {class: 'scroll table table-bordered table-striped table-hover'});
    tableTarget.appendChild(table);
    var timeoutCode = null;
    var tableSelector = new TableSelector(table, form);
    tableSelector.enable();
    function formKeyup(event) {
      if(event.keyCode == 40 || event.keyCode == 38) return;
      if(timeoutCode)
        clearTimeout(timeoutCode);
      timeoutCode = setTimeout(function() {
        Basic.json(form.action, {filter: form.filter.value, data: form.data.value}, lang.get('dynamicSearch'), 
            function(result) {
          if(result._status != 'success') {
            alert(result._message || lang.get('wrongData'));
          } else {
            var newIH = '<thead>';
            result.data.forEach(function(row, indexRow) {
              if(indexRow == 1) newIH += '</thead><tbody>';
              newIH += '<tr>';
              row.forEach(function(cell, indexCell) {
                var array = cell.split('<meta>'),
                    cell = array[0],
                    meta = array[1];
                switch(meta) {
                  case 'currency':
                    cell = '<div style="text-align: right">' + Basic.numberToM(cell) + '</div>';
                    break;
                }
                if(indexRow)
                  newIH += '<td>' + cell + '</td>';
                else
                  newIH += '<th>' + cell + '</th>';
              });
              newIH += '</tr>';
            });
            newIH += '</tbody>';
            table.innerHTML = newIH;
            Basic.arrangeTables();
            //tableSelector.disable();
            //tableSelector.enable();
            tableSelector.update();
          }
        }, null, true);
      }, 250)
    }
    form.data.addEventListener('keyup', formKeyup);
    form.addEventListener('submit', function(event) {
      event.preventDefault();
      if(tableSelector.hasSelectedRow()) {
        var selectedItem = tableSelector.getSelectedRow()[0]; 
        var input = Util.query('#' + div.getAttribute('data-target-input'));
        input.value = selectedItem;
        input.focus();
      }
    });
    addEventListener('keydown', Util.killCtrl(function(event) {
      switch(event.keyCode) {
        case 70: // F -> filter by
          form.filter.focus();
          break;
        case 73: // I -> filter
          form.data.focus();
      }
    }));
  });
});
