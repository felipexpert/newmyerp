"use strict";

define(["util", "basic", "modal", "restrict/models/sale", "language"], function(Util, Basic, Modal, Sale, lang) {
  var saleByIdUrl = baseUrl + "vwSale/",
      tablesAjaxUrl = baseUrl + "selling/tablesAjax",
      createSaleUrl = baseUrl + "selling/createAjax",
      identifierUrl = baseUrl + "selling/identifierAjax/",
      canUseIdentifierUrl = baseUrl + "selling/canUseIdentifierAjax/",
      go = Util.query("#go"),
      tables = Util.query("#tables"),
      singletons = Util.query("#singletons"),
      shared = Util.query("#shared"),
      sharedModal = new Modal(shared, null, go.number.focus.bind(go.number)),
      newForm = Util.query("#new form"),
      newModal = new Modal(newForm.parentNode, newForm.identifier.focus.bind(newForm.identifier));
  sharedModal.prepare();
  newModal.prepare();
  go.addEventListener("submit", function(event) {
    event.preventDefault();
    var node = Util.queryOver(tables, "[data-orderNumber='" + go.number.value + "']");
    node.click();
  });
  newButton.addEventListener("click", newModal.display.bind(newModal));
  newForm.addEventListener("submit", function(event) {
    event.preventDefault();
    if(!newForm.identifier.value) return;
    Basic.json(canUseIdentifierUrl, {identifier: newForm.identifier.value}, lang.get("fastSelling.createSale"), 
        function(r) {
      if(r.canUseIdentifier)
        Basic.json(createSaleUrl, {identifier: newForm.identifier.value}, lang.get("fastSelling.createSale"), 
             function(result) {
          location.href = saleByIdUrl + result.id;
        }, null, true);
      else alert(lang.get("fastSelling.alreadyInUseIdentifier", newForm.identifier.value)); 
    }, null, true)
  });
  update();
  function update() {
    Basic.get(tablesAjaxUrl, lang.get("tables.update"), function(result) {
      tables.innerHTML = singletons.innerHTML = "";
      Util.produce(result.maxTables, function(num) {
        var ft = result.tables.filter(function(table) { return table.orderNumber == num; });
        if(ft.length == 0) {
           tables.appendChild(Sale.newNode(num));
        } else {
          Util.assert(ft.length == 1, "Access - update - invalid tables length: " + ft.length);
          var table = ft[0];
          if(table.sales.length == 1) {
            var sale = table.sales[0];
            tables.appendChild(new Sale(sale.id, sale.orderNumber, sale.identifier).makeNode(sale.orderNumber));
          } else {
            var sn = Sale.sharedNode(num);
            tables.appendChild(sn);
            sn.addEventListener("click", function() {
              shared.innerHTML = "";
              var sortedSales = Util.toRealArray(table.sales).sort(function(sa, sb) { 
                var saText = sa.identifier ? String(sa.identifier) : String(sa.id);
                var sbText = sb.identifier ? String(sb.identifier) : String(sb.id);
                return saText.toUpperCase().localeCompare(sbText.toUpperCase()); 
              });
              sortedSales.forEach(function(s) {
                shared.appendChild(new Sale(s.id, s.orderNumber, s.identifier).makeNode(s.identifier));
              }); 
              sharedModal.display();
              shared.firstChild.focus();
            });
          }
        }
      }, true); 
      result.looseSales.forEach(function(s) {
        singletons.appendChild(new Sale(s.id, null, s.identifier).makeNode(s.identifier));
      });
      setTimeout(update, 5000);
    }, update, true);
  }
});
