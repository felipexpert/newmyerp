"use strict";

define(['util', 'basic', 'app', 'language', 'notifier'], function(Util, Basic, app, lang) {
  app.controller('DiscountController', ['$scope', 'notifier', function($scope, notifier) {
    this.save = function() {
      Basic.json(baseUrl + 'billing/discountAjax/' + this.saleId, {discount: this.discount}, null, function(r) {
        this.sync(r);
        notifier.billingObservers.forEach(function(f) { f(); });
      }.bind(this));
    };
    $scope.init = function(saleId) {
      this.saleId = saleId;
      Basic.get(baseUrl + 'billing/discountAjax/' + this.saleId, null, this.sync.bind(this));
    }.bind(this);
    this.sync = function(r) {
      $scope.$apply(function() {
        this.discount = r.discount;
        this.canChange = r.canChange;
      }.bind(this));
    };
  }]);
});
