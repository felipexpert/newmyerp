"use strict";

define(["util", "basic", "language", "restrict/models/orderItem", "modal", "tableSelector"], 
    function(Util, Basic, lang, OrderItem, Modal, TableSelector) {
  var getProdUrl = baseUrl + "prod/getProductAjax",
      createSaleUrl = baseUrl + "selling/createAjax",
      addOrderItemUrl = baseUrl + "selling/addOrderItemAjax/",
      rmProdUrl = baseUrl + "selling/rmProdFromOrderAjax",
      updateOrderItemUrl = baseUrl + "selling/updateOrderItemAjax",
      identifierUrl = baseUrl + "selling/identifierAjax/",
      canUseIdentifiersUrl = baseUrl + "selling/canUseIdentifiersAjax/",
      cancelSaleUrl = baseUrl + "selling/cancelSale/",
      billingUrl = baseUrl + "billing/receipt/",
      fsUrl = baseUrl + "selling/fastSelling",
      form =  Util.query("form#getProd"),
      amountFacade = Util.queryOver(form, "input[data-name='amount']"),
      table = Util.query("table"),
      total = Util.query("span#orderTotal"),
      orderNumberNode = Util.query("span#orderNumber"),
      orderNumber = orderNumberNode && orderNumberNode.textContent ? Number(orderNumberNode.textContent) : null, 
      saleIdNode = Util.query("span#saleId"),
      back = Util.query("#back"),
      removeProd = Util.query("button#removeProd"),
      searchProd = Util.query("button#searchProd"),
      detailButton = Util.query("button#detailButton"),
      print = Util.query("button#print"),
      printRedo = Util.query("button#printRedo"),
      dynamicSearchNode = Util.query(".dynamicSearch"),
      dynamicSearchForm = Util.queryOver(dynamicSearchNode, "form"),
      searchModal = new Modal(dynamicSearchNode, 
          dynamicSearchForm.reset.bind(dynamicSearchForm), 
          function() { getProd(); amountFacade.focus(); }),
      tableSelector = new TableSelector(table, form),
      detailModal = new Modal(Util.query("#detailModal"), null, form.code.focus.bind(form.code)),
      billing = Util.query("#billing"),
      detailForm = Util.query("#detail"),
      identifier = Util.query("#identifier"),
      btnIdentifier = Util.query("#btnIdentifier"), 
      newForm = Util.query("#new form"),
      newModal = new Modal(newForm.parentNode, function() {
        newForm.currIdentifier.value = identifier.value;
        newForm.currIdentifier.focus();
      }),
      newButton = Util.query("#newButton"),
      ancCancel = Util.query("#ancCancel");
  dynamicSearchForm.addEventListener("submit", searchModal.close.bind(searchModal));
  searchModal.prepare();
  tableSelector.enable();
  detailModal.prepare();
  newModal.prepare();
  OrderItem.constructOrderItems(table);
  updateTotal(total);
  OrderItem.putSubTotals(table);
  OrderItem.beautifyTable(table);
  form.code.addEventListener("blur", getProd);

  form.addEventListener("submit", function(event) {
    event.preventDefault();
    if(!form.code.value) {
      alert(lang.get("noCode"));
      return;
    } else if(form.amount.value == 0) {
      alert(lang.get("zeroError"));
      return;
    }
    Util.queryOver(form, "input[type='submit']").disabled = true;
    document.body.style.cursor = "progress";
    createSale(function() {
      Basic.json(addOrderItemUrl + saleIdNode.textContent, {code: form.code.value, amount: form.amount.value}, 
          lang.get("fastSelling.addProduct"), 
        function(result) {
        var orderItem = new OrderItem(result.displayOrder, result.code, result.amount, 
            Number(result.price), result.name);
        orderItem.insertTo(table);
        OrderItem.orderItems.push(orderItem);
        updateTotal(total);
        tableSelector.update();
        Util.queryOver(form, "input[type='submit']").disabled = false;
        //resetAmount();
        document.body.style.cursor = "";
        form.reset();
      });
    });
  });
  searchProd.addEventListener("click", searchModal.display.bind(searchModal));
  detailButton.addEventListener("click", function() {
    detailModal.display();
    var ni = Util.queryOver(detailForm, "[data-name='amount']");
    ni.select();
  });
  removeProd.addEventListener("click", function() {
    var selectedPos = tableSelector.getSelectedRow()[0]; 
    deletePos(selectedPos);
  });
  print.addEventListener("click", function() { 
    Util.get(baseUrl + "printer/printSale?saleId=" + saleIdNode.textContent, null, function() {
      alert(lang.get("success"));
    }).then();
  });
  printRedo.addEventListener("click", function() { 
    Util.get(baseUrl + "printer/printSale?saleId=" + saleIdNode.textContent + "&redo=true", null, function() {
      alert(lang.get("success"));
    }).then();
  });
  addEventListener("keydown", Util.killCtrl(function(event) {
    switch(event.keyCode) {
      case 66: //B -> search prod
        searchProd.click();
        break;
      case 80: //P -> print order
        print.click();
        break;
      case 69: //E -> remove prod
        removeProd.click();
        break;
      case 46: //DEL -> quick remove prod
        var selectedPos = tableSelector.getSelectedRow()[0]; 
        deletePos(selectedPos);
        break;
      case 86: //V -> back
        back.click();
        break;
      case 67: //C -> focus code
        OrderItem.orderItems = Util.woIndex(OrderItem.orderItems);
        OrderItem.orderItems = Util.woIndex(OrderItem.orderItems);
        form.code.focus();
        break;
      case 68: //D -> detail item
        var pos = tableSelector.getSelectedRow()[0];        
        var orderItem = OrderItem.orderItems.filter(function(oi) { return oi.displayOrder == pos })[0];
        detailForm.displayOrder.value = orderItem.displayOrder;
        detailForm.name.value = orderItem.name;
        detailForm.amount.value = orderItem.amount;
        Basic.syncNF(detailForm.amount);
        detailButton.click();
        break;
      case 81: //Q -> focus amount
        amountFacade.focus();
        break;
      case 52: //$ -> receipt
        billing.click();
    }
  })); 
  detailForm.addEventListener("submit", function(event) {
    event.preventDefault();
    var pos = tableSelector.getSelectedRow()[0];        
    var orderItem = OrderItem.orderItems.filter(function(oi) { return oi.displayOrder == pos })[0];
    orderItem.saleId = parseInt(saleIdNode.textContent);
    orderItem.amount = parseFloat(detailForm.amount.value);
    Basic.json(updateOrderItemUrl, orderItem, lang.get("fastSelling.updateOrderItem"), function(json) {
      if(json._status == "success") {
        orderItem.updateIn(table);
        updateTotal(total);
        detailModal.close();
      }
    });
  });
  amountFacade.addEventListener("keyup", updateItemSubtotal);
  billing.addEventListener("click", function() { location = billingUrl + saleIdNode.textContent; });
  btnIdentifier.addEventListener("click", function() {
    if(!identifier.value) return;
    updateIdentifier(identifier.value);
  });
  newButton && newButton.addEventListener("click", newModal.display.bind(newModal));
  newForm.addEventListener("submit", function(event) {
    event.preventDefault();
    if(!newForm.currIdentifier.value || !newForm.newIdentifier.value) return;
    createSale(function() {
      Basic.json(canUseIdentifiersUrl + saleIdNode.textContent, {identifier1: newForm.currIdentifier.value, 
            identifier2: newForm.newIdentifier.value}, null, function(result) {
        if(!result.canUseIdentifier1) 
          alert(lang.get("fastSelling.alreadyInUseIdentifier", newForm.currIdentifier.value)); 
        else if(!result.canUseIdentifier2) 
          alert(lang.get("fastSelling.alreadyInUseIdentifier", newForm.newIdentifier.value)); 
        else
          updateIdentifier(newForm.currIdentifier.value, function() {
            Basic.json(createSaleUrl, {orderNumber: orderNumber}, null, function(r) {
              Basic.json(identifierUrl +  r.id, {identifier: newForm.newIdentifier.value}, null, function() {
                location.href = fsUrl + "?orderNumber=" + orderNumber + "&saleId=" + r.id; 
              });
            });
          });  
      });
    });
  });
  ancCancel && ancCancel.addEventListener("click", function(event){
    if(!confirm("Deseja realmente cancelar a venda?")) event.preventDefault();
  });
  function updateItemSubtotal() {
    form.subtotal.value = Basic.numberToM(form.amount.value * form.price.value);
  }
  function updateIdentifier(identifier, callBack) {
    createSale(function() {
      Basic.json(identifierUrl + saleIdNode.textContent, {identifier: identifier}, 
          lang.get("fastSelling.identifier"), function(result) {
        if(result._status == "success") callBack && callBack();
        else alert(result.message);    
      });
    });
  }
  function updateTotal(span) {
    span.textContent = lang.get("total") + ": " + Basic.numberToMoney(OrderItem.getTotal());
  }
  function deletePos(pos) {
    if(!confirm(lang.get("fastSelling.removeQuestion"))) return;
    if(pos <= 0 || !OrderItem.orderItems.filter(function(oi) { return oi.displayOrder == pos; }).length) return;
    var jsonContent = {saleId: form.saleId.value, displayOrder: pos};
    Basic.json(rmProdUrl, jsonContent, lang.get("fastSelling.removeOrderItem"), function(json) {
      if(json._status == "success") {
        var filtered = OrderItem.orderItems.filter(function(oi) { return oi.displayOrder == pos });
        if(filtered.length) {
          filtered[0].removeFrom(table);
          updateTotal(total);
          tableSelector.update(true);
        }
      }
    });
  }
  function createSale(then) {
    if(!saleIdNode.textContent) {
        Basic.json(createSaleUrl, {orderNumber: orderNumber}, lang.get("fastSelling.createSale"), 
          function(result) {
        form.saleId.value = saleIdNode.textContent = result.id;
        //The url may be incomplete at this point, so lets ensure it
        ancCancel.href = cancelSaleUrl + result.id;
        ancCancel[result.canCancel ? "removeAttribute" : "setAttribute"]("disabled", "");
        then();
      });  
    } else {
      then();
    }
  }
  function getProd() {
    if(form.code.value)
      Basic.jsonForm(form, getProdUrl, {code: form.code.value}, function(result) {
        if(result._status == "success") {
          Basic.syncNF(form.amount);
          updateItemSubtotal();
        } else {
          form.code.value = "";
          form.code.focus(); 
        }
      });
  }
});
