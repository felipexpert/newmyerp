
"use strict";

define(['util', 'basic', 'language', 'restrict/models/orderItem', 'restrict/models/orderHashcoder',
    'hashcodeChecker'], function(Util, Basic, lang, OrderItem, OrderHashcoder, HashcodeChecker) {
  var progressByOrderNumberUrl = baseUrl + "sale/progressByOrderNumberAjax/",
      orderNumberNode = Util.query('span#orderNumber'),
      orderNumber = orderNumberNode.textContent ? Number(orderNumberNode.textContent) : null,
      saleIdNode = Util.query('span#saleId'),
      form =  Util.query('form#getProd'),
      table = Util.query('table'),
      total = Util.query('span#orderTotal');
  new HashcodeChecker(baseUrl + "sale/fsHashcodeAjax/" + orderNumber, function(result) {
    var result = JSON.parse(result);
    if(result._status == 'success') {
      var hashcoder = new OrderHashcoder(orderNumber, saleIdNode.textContent);
      var hashcode = hashcoder.generateForFastSelling();
      console.log(result.hashcode, hashcode);
      return result.hashcode != hashcode;
    }
  }, progressByOrderNumberUrl + orderNumber, function(result) {
    var result = JSON.parse(result);
    if(result._status = 'success') {
      if(result.saleId) {
        form.saleId.value = saleIdNode.textContent = result.saleId;
        OrderItem.orderItems = [];
        result.orderItems.forEach(function(oi) {
          OrderItem.orderItems.push(new OrderItem(oi.displayOrder, oi.code, oi.amount, oi.price, oi.description));
        });
        OrderItem.constructTable(table);
        updateTotal(total);
      } else {
        form.saleId.value = saleIdNode.textContent = '';
        OrderItem.orderItems = [];
        OrderItem.constructTable(table);
      }
    }
  }, HashcodeChecker.FAST).registerPeriodicEvent();
  function updateTotal(span) {
    span.textContent = lang.get('total') + ': ' + Basic.numberToMoney(OrderItem.getTotal());
  }
});
        
