"use strict"
require(['util', 'basic', 'app', 'language', 'notifier'], function(Util, Basic, app, lang) {
  app.controller('CashReceiptController', ['notifier', function(notifier) {
    this.payer = null;
    this.slots = [];
    notifier.receiptObservers.push(function(payer, paymentTypes) {
      this.slots = Util.toRealArray(paymentTypes).reduce(function(acc, pt) {
        acc.push({paymentType: pt, amount: 0});
        return acc;
      }, []); 
      this.payer = payer; 
    }.bind(this));
    this.change = function() {
      var portions = this.payer.portions.reduce(function(acc, p) { return acc + p; }, 0);
      var amount = this.slots.reduce(function(acc, a) { return acc + a.amount; }, 0);
      return portions + amount - this.payer.amount;
    };
    this.confirm = function() {
      var slots = this.slots.filter(function(s) { return Util.gt(s.amount, 0); })
        .map(function(s) { return {paymentTypeId: s.paymentType.id, amount: s.amount}; });
      console.log(slots);
      Basic.json(baseUrl + 'billing/addAndPayPortionAjax/' + this.payer.id, slots, null, function(r) {
        if(r) notifier.billingObservers.forEach(function(f) { f(); }); //notify
        else alert(lang.get('paySale.thereIsNoDifferenceToPay'));
      });
    };
  }]);
});
