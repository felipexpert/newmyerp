"use strict";

define(['util', 'basic', 'app', 'language', 'notifier'], function(Util, Basic, app, lang) {
  app.controller('AggregateController', ['$scope', '$http', 'notifier', function($scope, $http, notifier) {
    this.aggregates = [];
    this.total = function(aggregate) {
      return aggregate.subtotal * aggregate.amount;
    };
    this.save = function() {
      Basic.json(baseUrl + 'billing/setAggregatesAjax/', this.aggregates, lang.get('paySale.updateAggr'), function() {
        notifier.billingObservers.forEach(function(f) { f(); });
      });
    };
    $scope.init = function(saleId) {
      this.saleId = saleId;
      $http.get(baseUrl + 'billing/getAggregatesAjax/' + saleId).success(function(r) {
        this.aggregates = r;
      }.bind(this));
    }.bind(this);
  }]);
});
