"use strict";
define(['util', 'basic', 'language'], function(Util, Basic, lang) {
  function OrderItem(displayOrder, code, amount, price, name) {
    this.displayOrder = displayOrder;
    this.code = code;
    this.amount = amount;
    this.price = price;
    this.name = name;
  }
  OrderItem.prototype.insertTo = function(table) {
    var tbody;
    if(!(tbody = Util.queryOver(table, 'tbody'))) tbody = table.appendChild(Util.elt('tbody'));
    var row = tbody.insertRow(-1);
    var pos = row.insertCell(0);
    pos.textContent = this.displayOrder;
    row.insertCell(1).textContent = this.code;
    row.insertCell(2).textContent = this.name; 
    var amount = row.insertCell(3);
    var price = row.insertCell(4);
    var subTotal = row.insertCell(5);
    amount.textContent = Util.currencyFormat(this.amount, "", 3, lang.get('.'), lang.get(',')) ;
    price.textContent = Basic.numberToM(this.price);
    subTotal.textContent = Basic.numberToM(this.amount * this.price);
    pos.style.textAlign = amount.style.textAlign = price.style.textAlign = subTotal.style.textAlign = 'right';
    Basic.arrangeTables();
  };
  OrderItem.prototype.removeFrom = function(table) {
    OrderItem.orderItems = OrderItem.orderItems.filter(function(oi) { 
      return oi.displayOrder !=  this.displayOrder;
    }.bind(this));
    OrderItem.orderItems.filter(function(oi) {
      return oi.displayOrder > this.displayOrder;
    }.bind(this)).forEach(function(oi) {
      oi.displayOrder--;
    });
    Util.iterateTableRows(table, function(row, index) {
      if(row[0].textContent == this.displayOrder) table.deleteRow(index);
    }.bind(this));
    Util.iterateTableRows(table, function(row) {
      if(row[0].textContent > this.displayOrder) row[0].textContent--;
    }.bind(this));
  };
  OrderItem.prototype.updateIn = function(table) {
    Util.iterateTableRows(table, function(row) {
      if(row[0].textContent == this.displayOrder) {
        row[1].textContent = this.code;
        row[2].textContent = this.name;
        row[3].textContent = Util.currencyFormat(this.amount, "", 3, lang.get('.'), lang.get(',')) ;
        row[4].textContent = Basic.numberToM(this.price);
        row[5].textContent = Basic.numberToM(this.amount * this.price);
      }
    }.bind(this));
  }
  OrderItem.orderItems = [];
  OrderItem.getTotal = function() {
    return OrderItem.orderItems.map(function(orderItem) { 
      return orderItem.amount * orderItem.price;  
    }).reduce(function(done, curr) { 
      return done + curr; 
    }, 0);
  };
  OrderItem.constructOrderItems = function(table) {
    OrderItem.orderItems = [];
    Util.iterateTableRows(table, function(row) {
      var orderItem = new OrderItem(Number(row[0].textContent), row[1].textContent, 
          Number(row[3].textContent), Number(row[4].textContent), row[2].textContent);
      OrderItem.orderItems.push(orderItem);
    }); 
    OrderItem.sortItems(table);
  };
  OrderItem.constructTable = function(table) {
    var tbody = Util.queryOver(table, 'tbody');
    if(tbody) tbody.innerHTML = '';
    OrderItem.orderItems.forEach(function(oi) {
      oi.insertTo(table);
    });
    OrderItem.sortItems(table);
  }
  OrderItem.putSubTotals = function(table) {
    Util.iterateTableRows(table, function(row) {
      var totalPrice = Number(row[3].textContent) * Number(row[4].textContent);
      row[5].textContent = totalPrice;
    });
  };
  OrderItem.beautifyTable = function(table) {
    Util.iterateTableRows(table, function(row) {
      row[3].textContent = Util.currencyFormat(row[3].textContent, "", 3, lang.get('.'), lang.get(','));
      row[4].textContent = Basic.numberToM(row[4].textContent); 
      row[5].textContent = Basic.numberToM(row[5].textContent);
      row[0].style.textAlign = row[3].style.textAlign = row[4].style.textAlign = row[5].style.textAlign = "right";
    });
  };
  OrderItem.sortItems = function(table) {
    OrderItem.orderItems.sort(function(a, b) { return a.displayOrder - b.displayOrder; });
    Util.sortTableByColumn(table, 0, function(a, b) {return parseInt(a) - parseInt(b); });
  };
  return OrderItem;
});
