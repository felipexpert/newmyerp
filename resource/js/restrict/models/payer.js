"use strict";
define(['util', 'basic', 'language', 'restrict/models/payment'], function(Util, Basic, lang, Payment) {
  var updatePayerUrl = baseUrl + 'billing/updatePayerAjax/',
      removePayerUrl = baseUrl + 'billing/removePayerByIdAjax/';
  function Payer(id, description, paymentAmount, payments, customer) {
    this._id = id;
    this.description = description;
    this._paymentAmount = paymentAmount;
    this._payments = payments;
    this.customer = customer;
  }
  Object.defineProperty(Payer.prototype, 'id', {
    get: function() { return Number(this._id); },
    set: function(id) { this._id = id; }
  });
  Object.defineProperty(Payer.prototype, 'paymentAmount', {
    get: function() { return Number(this._paymentAmount); },
    set: function(paymentAmount) { this._paymentAmount = paymentAmount; }
  });
  Object.defineProperty(Payer.prototype, 'payments', {
    get: function() {
      this._payments.forEach(function(p) {
        if(!(p instanceof Payment)) throw new Error('One item from payments isn\'t of type Payment');
      });
      return this._payments;
    }, set: function(payments) { this._payments = payments; }
  });
  Payer.prototype.appendTo = function(payersDiv, receiptsOl, update, dataFocus) {
    Util.assert(payersDiv && payersDiv.nodeType, "Payer : appendTo - payersDiv is not valid");
    Util.assert(receiptsOl && receiptsOl.nodeType, "Payer : appendTo - receiptsOl is not valid");
    var paymentAmountInput = Util.elt('input', {type: 'text', class: 'nf', name: 'paymentAmount', 
        value: this.paymentAmount});
    this.marker = Util.elt('span', {class: 'marker glyphicon glyphicon-unchecked'}); 
    var form = Util.elt('form', { style: 'display: inline', 
        class: 'enterAsTab' + (Util.le(this.paymentAmount - this.paidAmount(), 0) ? '' : ' next'), 
        'data-focus': dataFocus}, 
      Util.elt('input', {type: 'text', name: 'description', value: this.description}),
      paymentAmountInput,
      Util.elt('button', {type: 'submit', class: 'btn-primary', value: lang.get('paySale.focus')}, 
          this.marker));
    Util.assert(payersDiv && payersDiv.nodeType, "Payer : appendTo - payersDiv isn't a div at all!");
    var closeButton = Util.elt('span', {style: 'color: #b86', class: 'glyphicon glyphicon-remove-sign'});
    payersDiv.appendChild(Util.elt('div', {class: 'payerInfo'}, closeButton, form));
    Basic.prepareNumberFormat(paymentAmountInput);
    Util.enterAsTab(form);
    form.addEventListener('submit', function(event) {
      event.preventDefault();
      Payer.curr = this;
      if(Util.eq(this.paymentAmount, form.paymentAmount.value) && this.description == form.description.value) {
        checkPayer(this);
      } else {
        var json = {
          id: this.id, 
          description: form.description.value,
          paymentAmount: form.paymentAmount.value
        };
        Basic.json(updatePayerUrl + Payer.saleId, json, lang.get('paySale.updatePayer'), function(text) {
          var result = JSON.parse(text);
          switch(result._status) {
            case 'success':
              update();
              break;
            case 'fail':
              form.description.value = this.description;
              form.paymentAmount.value = this.paymentAmount;
              Basic.syncNF(form.paymentAmount);
              alert(result.message);
              form.description.select();
          }
        }.bind(this));
      }
    }.bind(this));
    closeButton.addEventListener('click', function() { 
      Basic.json(removePayerUrl + this.id, null, lang.get('paySale.removePayer'), function(text) {
        var result = JSON.parse(text);
        switch(result._status) {
          case "success": update(); break;
          case "fail": alert(result.message); break;
          default : Util.assert(false, "Payer - closeButton - click: unexpected!");
        }
      });
    }.bind(this));
    var paymentUl = lang.get('paySale.didntPay'); 
    if(this.customer) {
      paymentUl = lang.get('paySale.credit'):
    } else if (this.payments.length) {
      paymentUl = this.payments.reduce(function(acc, payment) {
        acc.appendChild(Util.elt('div', null, payment.paymentType.acronym, ' - ', 
            Basic.numberToMoney(payment.amount)));
        return acc;
      }, Util.elt('div'));
      var difference = this.paymentAmount - this.paidAmount();
      if(!Util.le(difference, 0))
        paymentUl.appendChild(Util.elt('div', null, lang.get('paySale.itsMissing', 
            Basic.numberToMoney(Util.zero(difference))))); 
      else {
        paymentUl.appendChild(Util.elt('div', null, lang.get('paySale.change', 
            Basic.numberToMoney(Util.zero(-difference))))); 
        this.marker.style.display = 'none';
        this.marker.parentNode.classList.remove('btn-primary');
        this.marker.parentNode.classList.add('btn-success');
      }
    }
    var receipts  = Util.elt('li', null, Util.elt('ul', null, Util.elt('strong', null, this.description), 
        Util.elt('div', null, paymentUl)));
    receiptsOl.appendChild(receipts);
  };
  Payer.prototype.paidAmount = function() {
    return this.payments.reduce(function(acc, p) {
      return acc + p.amount;
    }, 0);
  }
  Payer.prototype.addPayment = function(payment) {
    this.payments.push(payment);
  };
  Payer.curr = null;
  Payer.saleId = null;
  Payer.payers = [];
  Payer.has = function(id) {
    return Payer.payers.filter(function(p) { return p.id == id; }).length > 0;
  };
  Payer.find = function(id) {
    var payer = Payer.payers.filter(function(p) { return p.id == id; });
    if(!payer.length) throw new Error('Payer.find - payer not found!');
    return payer[0];
  };
  Payer.clear = function() { Payer.payers = []; };
  Payer.add = function(id, description, paymentAmount, payments) {
    if(!payments) payments = [];
    Payer.payers.push(new Payer(id, description, paymentAmount, payments));
  };
  Payer.update = function(payersDiv, receiptsOl, update, dataFocus) {
    receiptsOl.innerHTML = payersDiv.innerHTML = '';
    Payer.payers.sort(function(a, b) { return a.id - b.id; });
    Payer.payers.forEach(function(payer) { payer.appendTo(payersDiv, receiptsOl, update, dataFocus); });
    if(Payer.curr && Payer.has(Payer.curr.id)) {
      Payer.curr = Payer.find(Payer.curr.id);
      checkPayer(Payer.curr);
    }
  };
  Payer.everybodyPaidAll = function(saleTotal) {
    return Util.ge(Payer.total(), saleTotal) && Payer.payers.every(function(payer) { 
      return Util.ge(payer.paidAmount(), payer.paymentAmount); 
    });
  };
  Payer.total = function() {
    return Payer.payers.reduce(function(acc, payer) { return acc + payer.paymentAmount; }, 0);
  };
  Payer.paidAmount = function() {
    return Payer.payers.reduce(function(acc, payer) { return acc + payer.paidAmount(); }, 0);
  };
  Payer.liquidPaidAmount = function() {
    return Payer.payers.reduce(function(acc, payer) { 
      return acc + Math.min(payer.paymentAmount, payer.paidAmount());
    }, 0);
  }; 
  Payer.selectNext = function() {
    cleanMarker();
    var target = Util.query('#payers .next input[type="text"]');
    if(target) target.focus();
  };
  function checkPayer(payer) {
    cleanMarker();
    payer.marker.classList.remove('glyphicon-unchecked');
    payer.marker.classList.add('glyphicon-check');
  }
  function cleanMarker() {
    Util.query('.marker', true).forEach(function(marker) { 
      marker.classList.remove('glyphicon-check');
      marker.classList.add('glyphicon-unchecked');
    });
  }
  return Payer;
});
