"use strict";
define(['util'], function(Util) {
  function PaymentType(id, acronym) {
    this._id = id;
    this.acronym = acronym;
  }
  Object.defineProperty(PaymentType.prototype, 'id', {
    get: function() { return Number(this._id); },
    set: function(id) { this._id = id; } 
  });
  PaymentType.types = [];
  PaymentType.add = function(id, acronym) {
    PaymentType.types.push(new PaymentType(id, acronym));
  };
  PaymentType.find = function(id) {
    var types = PaymentType.types.filter(function(pt) {
      return pt.id == id;
    });
    Util.assert(types.length > 0, 'PaymentTypeModel - find: didn\'t find the given id!');
    return types[0];
  }
  return PaymentType;
});
