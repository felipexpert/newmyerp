"use strict";
define(["util", "basic", "language"], function(Util, Basic, lang) {
  var saleUrl = baseUrl + "vwSale/",
      saleOrderNumberUrl = baseUrl + "vwSale/orderNumber/";
  function Sale(id, orderNumber, identifier) {
    this.id = id;
    this.orderNumber = orderNumber;
    this.identifier = identifier;
  }
  Sale.prototype.makeNode = function(show) {
    var url = saleUrl + this.id;
    return Util.elt("a", {class: "btn btn-primary raised singleton", href: url, 
        "data-orderNumber": this.orderNumber}, Util.elt("i",{class:"fa fa-user"})," ",show);
  };
  Sale.newNode = function(orderNumber) {
    return Util.elt("a", {class: "btn btn-default raised btn-dark empty", 
        href: saleOrderNumberUrl + orderNumber, 
        "data-orderNumber": orderNumber},orderNumber);
  };
  Sale.sharedNode = function(orderNumber) {
    return Util.elt("button", {class: "btn btn-primary raised shared", "data-orderNumber": orderNumber}, Util.elt("i",{class:"fa fa-share-alt"})," ",orderNumber);
  };
  return Sale;
});
