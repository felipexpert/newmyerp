"use strict";
define(['util', 'restrict/models/paymentType'], function(Util, PaymentType) {
  function Payment(id, paymentType, amount) {
    this._id = id;
    this._paymentType = paymentType;
    this._amount = amount;
  }
  Object.defineProperty(Payment.prototype, 'id', {
    get: function() { return Number(this._id); },
    set: function(id) { this._id = id; }
  });
  Object.defineProperty(Payment.prototype, 'paymentType', {
    get: function() { 
      if(!(this._paymentType instanceof PaymentType))
        throw new Error('paymentType isn\t of type PaymentType');
      return this._paymentType; 
    }, set: function(paymentType) { this._paymentType = paymentType; }
  });
  Object.defineProperty(Payment.prototype, 'amount', {
    get: function() { return Number(this._amount); },
    set: function(amount) { this._amount = amount; }
  });
  return Payment;
});
