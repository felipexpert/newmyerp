"use strict";
define(['util', 'basic', 'restrict/models/orderItem'], function(Util, Basic, OrderItem) {
  var DELIMITER = ':', SUB_DELIMITER = ',';
  function OrderHashcoder(orderNumber, orderId) {
    this.orderNumber = orderNumber;
    this.orderId = orderId;
  }
  OrderHashcoder.prototype.generateForFastSelling = function() {
    var hashcode = this.orderNumber; 
    if(!this.orderId) {
      hashcode += DELIMITER + "-1";
    } else {
      hashcode += DELIMITER + this.orderId;
      if(OrderItem.orderItems.length) {
        hashcode += DELIMITER;
        OrderItem.orderItems.forEach(function(oi) {
          hashcode += '(' + oi.displayOrder + DELIMITER + oi.code + DELIMITER + Number(oi.amount) + ')' + SUB_DELIMITER; 
        });
      }
    }
    return hashcode;
  }
  return OrderHashcoder;
});
