"use strict";

define(['util', 'basic', 'language', 'restrict/models/payer', 'restrict/models/paymentType', 
    'restrict/models/payment'], function(Util, Basic, lang, Payer, PaymentType, Payment) {
  var updateAggrStatusUrl = baseUrl + 'billing/updateAggrStatusAjax/',
      saleId = Util.getUrlId(),
      addPayerUrl = baseUrl + 'billing/addPayerAjax/' + saleId,
      removePayerUrl = baseUrl + 'billing/removePayerAjax/' + saleId,
      progressUrl = baseUrl + 'billing/progressAjax/' + saleId,
      closeUrl = baseUrl + 'billing/closeAjax/' + saleId,
      payUrl = baseUrl + 'billing/payAjax/',
      tablesUrl = baseUrl + 'sale/tables',
      newSellingUrl = baseUrl + 'sale/fastSelling',
      orderNumberNode = Util.query('#orderNumber'),
      orderNumber = orderNumberNode.textContent.trim(),
      discountForm = Util.query('#discountForm'),
      confirmButton = Util.query('#confirm'),
      printButton = Util.query('#print'),
      printECFButton = Util.query('#printECF'),
      closing = Util.query('#closing'),
      totalItems = Util.query('#totalItems'),
      aggrs = Util.query('[data-aggr]', true), 
      total = Util.query('#total'),
      payersNode = Util.query('#payers'),
      addPayer = Util.query('#addPayer'),
      removePayer = Util.query('#removePayer'),
      difference = Util.query('#difference'),
      paymentForm = Util.query('#payment'),
      paymentTypes = Util.queryOver(paymentForm, 'input[name^="pt"]', true),
      paymentTypesFacade = Util.queryOver(paymentForm, 'input[data-name^="pt"]', true),
      change = Util.query('#change'),
      receiptsOl = Util.query('#receipts'),
      paymentDataFocus = '#payment input[type="text"]',
      backOrderNumbers = Util.query('#backOrderNumbers');
  Payer.saleId = saleId;
  updateTotal();
  progress();
  aggrs.forEach(function(node) {
    node.addEventListener('click', function() {
      Basic.json(updateAggrStatusUrl + node.getAttribute('data-aggr'),
          {'disabled': !node.checked}, lang.get('paySale.updateAggr'), function(text) {
        var result = JSON.parse(text);
        if(result._status == 'success') updateTotal();
      }); 
    });
  });
  discountForm.addEventListener('submit', function(event) {
    Basic.submitLogForm(event, discountForm, progress);
  });
  addPayer.addEventListener('click', changeProgress.bind(null, addPayerUrl, lang.get('paySale.addPayer')));
  removePayer.addEventListener('click', changeProgress.bind(null, removePayerUrl, lang.get('paySale.removePayer')));
  confirmButton.addEventListener('click', function() {
    changeProgress(closeUrl, lang.get('paySale.close'));
    confirmButton.classList.add('executed');
  });
  printButton.addEventListener('click', function() { 
    Util.get(outerUrl + 'pedido_imprimir_nfiscal.php?id=' + saleId).then();
  });
  printECFButton.addEventListener('click', function() { 
    var documento = prompt("Digite o CPF:");
    Util.get(outerUrl + 'modal/ecf_imprimir.php?id='+saleId+'&documento='+encodeURIComponent(documento)).then();
  });
  paymentTypes.forEach(function(pt) {
    PaymentType.add(idOf(pt), pt.getAttribute('data-acronym'));
  });
  paymentTypesFacade.forEach(function(ptf) {
    ptf.addEventListener('keyup', updateChange); 
  });
  paymentForm.addEventListener('submit', function(event) {
    event.preventDefault();
    if(!Payer.curr) {
      alert(lang.get('paySale.noPayerSelected'));
      return;
    }
    Basic.json(payUrl + Payer.curr.id, payments(), lang.get('paySale.pay'), function(text) {
      var result = JSON.parse(text);
      switch(result._status) {
        case 'success':
          progress();
          paymentTypes.forEach(function(typeNode) {
            typeNode.value = 0;
            Basic.syncNF(typeNode);
          });
          // it doesn' work without timeout!
          setTimeout(function() { 
            updateChange();
            Payer.selectNext();
          }, 500);
          break;
        case 'fail':
          alert(result.message);
          break
        default:
          Util.assert(false, 'PaymentForm: submit - unexpected server response status!');
      }
    });
  });
  addEventListener('keydown', Util.killCtrl(function(event) {
    switch(event.keyCode) {
      case 73: // I
        confirmButton.click();
        break; 
      case 80: // P
        printButton.click();
        break;
      case 77: // M
      printECFButton.click();
    }
  }));
  function calcTotal() {
    return Number(totalItems.getAttribute('data-totalItems'))
        + aggrs.reduce(function(acc, node) {
      return acc + (node.checked ? Number(node.getAttribute('data-amount')) : 0);   
    }, 0) - discountForm.discount.value;
  }
  function progress() {
    Basic.get(progressUrl, lang.get('paySale.getPayers'), function(text) {
      var result = JSON.parse(text);
      if(result._status == 'success') {
        Payer.clear();
        result.payers.forEach(function(payer) { 
          var payments = payer.payments.reduce(function(acc, p) {
            var type = PaymentType.find(p.paymentType.id);
            Util.assert(type && (type instanceof PaymentType), 'paySale: progress - invalid type!')
            acc.push(new Payment(p.id, type, p.amount));
            return acc;
          }, []);
          Payer.add(payer.id, payer.description, payer.paymentAmount, payments); 
        });
        Payer.update(payersNode, receiptsOl, progress, paymentDataFocus);
        if(result.closing) {
          closing.textContent = result.closing;        
          confirmButton.style.display = 'none';
          if(confirmButton.classList.contains('executed')) {
            var seconds = 3;
            closing.textContent += " - " + lang.get('paySale.redirect', seconds);  
            var url = orderNumber ? tablesUrl : newSellingUrl;
            setTimeout(function() { location.href = url; }, seconds * 1000);
          }
        } else {
          closing.textContent = '';
          confirmButton.style.display = '';
          if(Payer.everybodyPaidAll(calcTotal())) {
            confirmButton.classList.remove('btn-warning');
            confirmButton.classList.add('btn-success');
          } else {
            confirmButton.classList.remove('btn-success');
            confirmButton.classList.add('btn-warning');
          }
        }
        updateTotal();
      }
    }, null, true);
  }
  function calcDifference() {
    return calcTotal() - Payer.liquidPaidAmount();
  }
  function updateTotal() {
    total.textContent = Basic.numberToMoney(calcTotal());
    updateDifference();
  }
  function updateDifference() {
    difference.textContent = Basic.numberToMoney(Util.zero(calcDifference())); 
  }
  function payments() {
    return paymentTypes.reduce(function(acc, input) {
      var value = Number(input.value);
      if(value)
        acc.push({paymentTypeId: idOf(input), amount: value});
      return acc;
    }, []);
  }
  function idOf(pt) { return pt.name.replace(/^pt(\d+)$/, '$1'); }
  function updateChange() {
    if(!Payer.curr) return;
    var paymentsTotal = payments().reduce(function(acc, p) {
      return acc + p.amount;
    }, 0);
    if(!Util.eq(paymentsTotal, 0)) {
      var c = Util.zero(Payer.curr.paidAmount() + paymentsTotal - Payer.curr.paymentAmount);
      var neg = c < 0;
      change.textContent = neg ? lang.get('paySale.itsMissing', Basic.numberToM(-c)) : Basic.numberToM(c);
      change.style.color = neg ? 'red' : ''; 
    } else {
      change.style.color = change.textContent = '';
    }
  }
  function changeProgress(url, taskName) {
    Basic.json(url, null, taskName, function(text) {
      var result = JSON.parse(text);
      switch(result._status) {
        case "success": progress(); break;
        case "fail": alert(result.message); break;
        default : Util.assert(false, "addPayer - click: unexpected!");
      }
    });
  }
});
