"use strict";

define(['util', 'basic', 'loadang'], function(Util, Basic, Loadang) {
  //Angular defauts
  let loadang = new Loadang("ng/services", "ng/filters", "ng/directives", "ng/controllers");
  loadang
    .addServices( "ShortcutFactory"
                , "AjaxFactory"
                , "RequestFactory")
    .addDirectives( "autofocus"
                  , "xpEnterAsTab"
                  , "xpNumber"
                  , "xpDate"
                  , "xpPercentage"
                  , "xpTableSelector");

  //addEventListener('keydown', Util.killCtrl(function() { console.log('veio'); }));
  if(Basic.verifyPlaces('selling/fastSelling')) {
    var requirements = ['restrict/sale', 'dynamicSearch'];
    // now each orderNumber may have more than one sale
    //if(location.href.indexOf('orderNumber') != -1) requirements.push('restrict/saleHashcoder');
    require(requirements);
  } 
  else if(Basic.verifyPlaces('selling/tables')) { require(['restrict/access']); }
  else if(Basic.verifyPlaces('billing/test')) { require(['restrict/test']); }
  else {
    if(Basic.verifyPlaces('vwProduct')) loadang
      .addServices( "ProductInstanceFactory"
                  , "ProductFactory"
                  , "ProductCategoryFactory"
                  , "ProductBrandFactory"
                  , "ProductTaxRateFactory"
                  , "FeedProductCategoriesFactory"
                  , "CharacteristicFactory"
                  , "CharacteristicInstanceFactory"
                  , "ConfFactory")
      .addDirectives( "xpProductCharacteristic")
      .addControllers("product");
    else if(Basic.verifyPlaces("vwSale") || Basic.verifyPlaces("billing/receipt")) loadang
      .addServices( "SaleFactory"
                  , "OrderFactory"
                  , "PortionBucketFactory"
                  , "PortionFactory"
                  , "PredefinedParcellingFactory"
                  , "PaymentTypeFactory"
                  , "PayerFactory"
                  , "PrinterFactory"
                  , "CustomerFactory"
                  , "CharacteristicInstanceFactory"
                  , "ProductInstanceFactory"
                  , "ProductSnapshotFactory"
                  , "FeedCustomersFactory"
                  , "FeedProductsFactory")
      .addDirectives( "xpPortionBucket"
                    , "xpProductHelper"
                    , "xpAggregates"
                    , "xpDiscount"
                    , "xpPayerStatus"
                    , "xpPayer")
      .addControllers("sale", "billing");
    else if(Basic.verifyPlaces("vwConsignment/access")) loadang
      .addServices( "ConsignmentFactory"
                  , "FeedConsignmentsFactory"
                  , "CharacteristicInstanceFactory"
                  , "ProductInstanceFactory"
                  , "FeedProductsFactory"
                  , "DealerFactory"
                  , "FeedDealersFactory")
      .addControllers("consignmentAccess");
    else if(Basic.verifyPlaces("vwConsignment")) loadang
      .addServices( "CharacteristicInstanceFactory"
                  , "ProductInstanceFactory"
                  , "ProductSnapshotFactory"
                  , "OrderFactory"
                  , "OrderItemFactory"
                  , "ConsignmentFactory"
                  , "CustomerFactory"
                  , "FeedProductsFactory"
                  , "FeedCustomersFactory")
      .addDirectives( "xpProductHelper"
                    , "xpConsignedOrderItem")
      .addControllers("consignment");
    else if(Basic.verifyPlaces("vwStock")) loadang
      .addControllers("stock");
    require(loadang.load());
  }
});
