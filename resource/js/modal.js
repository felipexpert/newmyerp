define(['util', 'positionHelper'], function(Util, PositionHelper) {
  addEventListener('keydown', function(event) {
    if(event.keyCode == 27) {
      if(Modal.lastOpenedModals.length) 
        Modal.lastOpenedModals[Modal.lastOpenedModals.length - 1].close();
    }
  });
  function Modal(modal, openCallback, closeCallback) {
    this.modal = modal;
    this.openCallback = openCallback;
    this.closeCallback = closeCallback;
  } 
  Modal.startingZIndex = 100;
  Modal.prototype.display = function() {
    var height = Util.getHeight(); 
    var width = Util.getWidth(); 
    this.modal.classList.remove('modalOff');
    this.modal.classList.add('modalOn');
    var modalHelper = new PositionHelper(this.modal);
    var modalBounds = modalHelper.bounds;
    this.modal.style.top = height * .02 + 'px';
    this.modal.style.maxHeight = (height - height * .04) + 'px';
    this.modal.style.left = (width - modalBounds.width) / 2 + 'px';
    //this._bodyHeightStyle = document.body.style.height;
    //this.modal.style.height = height * .96 + 'px';
    var closeButton = Util.queryOver(this.modal, '.close');
    if(!closeButton) {
      var closeButton = Util.elt('div');
      closeButton.className = "closeButton";
      this.modal.appendChild(closeButton);
      closeButton.addEventListener('click', this.close.bind(this));
    }
    var inputToFocus = Util.queryOver(this.modal, '[data-focus],[autofocus]');
    Modal.openModal(this);
    if(inputToFocus) inputToFocus.focus();
    this.openCallback && this.openCallback(this.modal);
  };
  Modal.prototype.close = function() {
    this.modal.classList.remove('modalOn');
    this.modal.classList.add('modalOff');
    //document.body.style.height = this._bodyHeightStyle;
    this.closeCallback && this.closeCallback();
    Modal.closeModal(this);
  };
  Modal.prototype.prepare = function() {
    this.modal.classList.remove('modalOn');
    this.modal.classList.add('modalOff');
  };
  Modal.openModal = function(modal) {
    if(!Modal.lastOpenedModals.length) {
      modal.modal.style.zIndex = Modal.startingZIndex;
      Modal.lastOpenedModals.push(modal);
    } else if(Modal.lastOpenedModals[Modal.lastOpenedModals.length - 1] != this) {
      modal.modal.style.zIndex = Modal.lastOpenedModals[Modal.lastOpenedModals.length - 1].modal.style.zIndex + 1;
      Modal.lastOpenedModals.push(modal);
    }
  };
  Modal.closeModal = function(modal) {
    var index;
    if((index = Modal.lastOpenedModals.indexOf(modal)) > -1)
      Modal.lastOpenedModals = Util.woIndex(Modal.lastOpenedModals, index);
  };
  Modal.lastOpenedModals = [];
  return Modal;
});
