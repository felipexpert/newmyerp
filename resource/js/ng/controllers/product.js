"use strict";

require(["util", "arrays", "misc", "optional", "modal", "ng/app"], function(Util, Arrays, Misc, Optional, Modal, app) {
  app.controller("ProductController", 
  function($scope, $timeout, FeedProductCategories, ProductCategory, ProductBrand, ProductTaxRate, ProductInstance, 
           Product, Characteristic, Conf) {
    let node = Util.query("#product-controller"),
        categoryModal = new Modal(Util.queryOver(node, "#category-modal"));
    categoryModal.prepare();
    $scope.conf = $scope.productInfo = null;
    $scope.product = { id: null, category: null, instances: [], brand: null
                     , taxRate: null, name: "", codePrefix: "", defaultPrice: 0
                     , defaultBuyingPrice: 0, defaultProfitPercentage: 0, productType: null
                     , obs: "", desabled: false };
    $scope.selected = [];
    $scope.fixedInstances = [];
    $scope.characteristics = [];
    $scope.brands = [];
    $scope.taxRates = [];
    $scope.feedProductCategories = FeedProductCategories;
    $scope.proportional = false;
    // label stuff start
    $scope.labelType = null;
    $scope.labelAmount = 0;
    $scope.labelTypes = [ {id: "pequeno_amaxima", name: "Pequeno (Amaxima)"}
                        , {id: "grande_amaxima", name: "Grande (Amaxima)"}];
    // label stuff end
    $scope.bindProductCategory = (row) => {
      categoryModal.close();
      ProductCategory.get(row[0]).then((resp) => $scope.$apply(() => $scope.product.category = resp.obj));
    };
    $scope.showCategoryModal = () => categoryModal.display();
    //$scope.$watch("selected", updateInstances, true);
    $scope.updateInstances = () => {
      let produced = ProductInstance.createInstances(Optional.fromNullable($scope.characteristics), 
                                                                 $scope.selected, 
                                                                 $scope.product.codePrefix,
                                                                 $scope.conf.barCodeLength,
                                                                 $scope.product.defaultPrice, 
                                                                 $scope.product.defaultBuyingPrice),
          fixedApplied = ProductInstance.applyFixed(produced, $scope.fixedInstances);
      $scope.product.instances = fixedApplied;
      console.log("update instances (interface)", $scope.selected);
    }
    $scope.defaultPriceUpdated = () => {
      Product.updateDefaultProfitPercentage($scope.product);
      $scope.product.instances.forEach((i) => {
        i.price = $scope.product.defaultPrice;
        ProductInstance.updateProfitPercentage(i);
      });
    };
    $scope.defaultProfitPercentageUpdated = () => {
      Product.updateDefaultPrice($scope.product);
      $scope.product.instances.forEach((i) => {
        i.profitPercentage = $scope.product.defaultProfitPercentage;
        ProductInstance.updatePrice(i);
      });
    };
    $scope.defaultBuyingPriceUpdated = () => {
      Product.updateDefaultPrice($scope.product);
      $scope.product.instances.forEach((i) => {
        i.buyingPrice = $scope.product.defaultBuyingPrice;
        ProductInstance.updatePrice(i);
      });
    };
    $scope.proportionalChanged = () => {
      console.log($scope.proportional);
      $scope.product.instances.forEach((i) => i.proportional = $scope.proportional);
    };
    $scope.prodInstName = (instance) => $scope.product.name 
      + " " 
      + instance.characteristicInstances
          .slice()
          .sort((ia, ib) => ia.displayOrder - ib.displayOrder)
          .map((c) => c.name)
          .join(", ");
    $scope.save = sendToServer.bind(this, "create");
    $scope.update = sendToServer.bind(this, "update");
    $scope.updatePrice = ProductInstance.updatePrice;
    $scope.updateProfitPercentage = ProductInstance.updateProfitPercentage;
    $scope.init = (id) => {
      let promise = Conf.get()
        .then((resp) => {
          $scope.$apply(() => $scope.conf = resp.obj);
          return Product.info();
        })
        .then((resp) => {
          $scope.$apply(() => $scope.productInfo = resp.obj);
          return ProductBrand.getAll();
        })
        .then((resp) => {
          $scope.$apply(() => $scope.brands = resp.obj);
          return ProductTaxRate.getAll();
        })
        .then((resp) => {
          $scope.$apply(() => {
            $scope.taxRates = resp.obj;
            if($scope.conf.productTaxRate)
              $scope.product.taxRate = Arrays.find((tr) => tr.id == $scope.conf.productTaxRate.id, $scope.taxRates).get();
          });
          return Characteristic.getAll();
        });
      if(id) promise
        .then((resp) => {
          $scope.$apply(() => $scope.characteristics = resp.obj);
          return Product.getById(id);
        })
        .then((resp) => {
          $scope.$apply(() => setupProduct(resp.obj));
        });
      else promise.then((resp) => $scope.$apply(() => {
        $scope.characteristics = resp.obj
        let codePrefix = $scope.productInfo.lastId 
                       ? $scope.productInfo.lastId + 1
                       : 1;
        $scope.product.codePrefix = codePrefix;
      }));
    };
    function sendToServer(method) {
      let product = Misc.clone($scope.product);
      product.instances.forEach((i) => i.code = $scope.product.codePrefix + i.codeSuffix);
      return Product[method](product).then((resp) => {
        if(resp.obj.success) $scope.$apply(() => setupProduct(resp.obj.product));
        else alert(resp.obj.message);
      });
    }
    function setupProduct(serverProduct) {
      setupSelected(serverProduct);
      serverProduct.instances.forEach((i) => i.codeSuffix = i.code.slice(serverProduct.codePrefix.length));
      serverProduct.instances.forEach(ProductInstance.updateProfitPercentage);
      $scope.fixedInstances = serverProduct.instances;
      serverProduct.instances = serverProduct.instances.filter((i) => {
        return ProductInstance.isSelected(i, $scope.selected, $scope.characteristics); 
      });
      $scope.product = serverProduct;
      Product.updateDefaultProfitPercentage($scope.product);
      $scope.product.brand 
        && ($scope.product.brand = Arrays.find((b) => b.id == $scope.product.brand.id, $scope.brands).get());
      $scope.product.taxRate
        && ($scope.product.taxRate = Arrays.find((t) => t.id == $scope.product.taxRate.id, $scope.taxRates).get());
      setupSelected(serverProduct);
      console.log("$scope.product has been setted!");
    }
    function setupSelected(serverProduct) {
      let charss = serverProduct.instances
            .filter((i) => !i.disabled)
            .map((i) => i.characteristicInstances), 
          chars = Arrays.flat(charss),
          charIds = chars.map((c) => c.id),
          uniqueIds = Arrays.nub(charIds);
      $scope.selected = uniqueIds;
      console.log("$scope.selected has been setted!");
    }
  });
});
