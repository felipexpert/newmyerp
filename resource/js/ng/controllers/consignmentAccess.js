"use strict";

require(["ng/app", "misc", "util", "modal"], function(app, Misc, Util, Modal) {
  app.controller("ConsignmentAccessController", function($scope, FeedConsignments, FeedProducts, FeedDealers) {
    let productsModal = new Modal(Util.query("#productsModal")),
        dealersModal = new Modal(Util.query("#dealersModal"));
    $scope.baseUrl = baseUrl;
    $scope.productName = $scope.dealerName = null;
    $scope.optional = { onlyOpened: Misc.queryStringValue("onlyOpened").map((value) => {
                          return value == "true";
                        }).getOrElse(false)
                      , productInstanceId: Misc.queryStringValue("productInstanceId").getOrNull()
                      , dealerId : Misc.queryStringValue("dealerId").getOrNull() };
    console.log($scope.optional);
    $scope.feedConsignments = FeedConsignments;
    $scope.accessConsignment = function(row) {
      window.location = baseUrl + "vwConsignment/" + row[0];
    };
    $scope.feedProducts = FeedProducts;
    $scope.cleanProductFilter = function() {
      $scope.optional.productInstanceId = $scope.productName = null;
    };
    $scope.bindProduct = function(row) {
      productsModal.close();
      $scope.productName = row[2];
      $scope.optional.productInstanceId = row[1];
    };
    $scope.showProductsModal = productsModal.display.bind(productsModal);
    $scope.feedDealers = FeedDealers;
    $scope.cleanDealerFilter = function() {
      $scope.optional.dealerId = $scope.dealerName = null;
    };
    $scope.bindDealer = function(row) {
      dealersModal.close();
      $scope.dealerName = row[1];
      $scope.optional.dealerId = row[0]; 
    };
    $scope.showDealersModal = dealersModal.display.bind(dealersModal);
  });
});
