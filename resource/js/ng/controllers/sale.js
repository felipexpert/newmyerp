"use strict";

require(["util", "arrays", "ng/app", "language"], function(Util, arrays, app, lang) {
  app.controller("SaleController", function($scope, $http, Sale, Order, ProductSnapshot, Printer, Shortcut) {
    var tablesUrl = baseUrl + "selling/tables",
        fastSellingUrl = outerUrl + "index.php?p=pedido", 
        saleUrl = baseUrl + "vwSale";
    $scope.sale = null;
    $scope.orderNumber = null;
    $scope.identifier = "";
    $scope.exchanges = null;
    $scope.portionBucketRefresh = 0;
    $scope.backUrl = fastSellingUrl;
    $scope.init = function(id, orderNumber) {
      $scope.orderNumber = orderNumber;
      id && updateSale(id); 
      id && $http.get(finalUrl + "sales/" + id + "/exchanges?included=orders").then((resp) => {
        $scope.exchanges = resp.data;
      });
    };
    $scope.isExchangeEnabled = (exchange) => {
      let order = arrays.find( (o) => o.id == exchange.relationships.order.data.id
                             , $scope.exchanges.included.orders).get();
      return !order.attributes.disabled;
    };
    $scope.ensure = function() {
      return new Promise(function(succeed, failed) {
        if(!$scope.sale) Sale.create($scope.orderNumber, $scope.identifier.length ? $scope.identifier : null)
          .then(function(resp) {
            if(resp.obj.success) {
              $scope.sale = resp.obj.sale;
              succeed();
            } else {
              alert(resp.obj.message);
              setupIdentifier();
              failed();
            }
          }, failed);
        else succeed();
      });
    };
    $scope.updateIdentifier = function() {
      if(!$scope.identifier.length) return;
      if($scope.sale) Sale.updateIdentifier($scope.sale.id, $scope.identifier).then(function(resp) {
        if(resp.obj.success) $scope.sale.identifier = $scope.identifier;
        else {
          alert(resp.obj.message);   
          setupIdentifier();
        }
      }); 
      else $scope.ensure();
    };
    $scope.addSale = function() {
      if(!$scope.sale) {
        alert(lang.get("fastSelling.notCreatedSale"));
        return;
      }
      var newSaleName = prompt(lang.get("fastSelling.newSaleIdentifier"));
      if(newSaleName) {
        Sale.create($scope.orderNumber, newSaleName).then(function(resp) {
          if(resp.obj.success) location.href = baseUrl + "vwSale/" + resp.obj.sale.id;  
          else alert(resp.obj.message);
        });
      } 
    };
    $scope.addProd = function(code, amount) {
      $scope.ensure()
        .then(function() { return Sale.addProd($scope.sale.id, code, amount); })
        .then(function(resp) { 
          if(resp.obj.validCode) {
            $scope.$apply(function() {
              $scope.sale.order.orderItems.push(resp.obj.orderItem);
            });
          } else {
            alert(lang.get("invalidCode"));
          }
          return initPayers();
        });
    };
    $scope.orderItemAmount = function(orderItem) {
      return orderItem.productFlows
        .filter(function(flow) { return flow.flowTypeName == "OUT_SOLD"; })
        .reduce(function(acc, flow) { return acc + flow.amount; }, 0); 
    };
    $scope.updateOrderItemAmount = function(orderItem) {
      Sale.updateOrderItemAmount($scope.sale.id, orderItem.id, orderItem.amount)
        .then(function(resp) {
          $scope.$apply(function() {
            var old = $scope.sale.order.orderItems.filter(function(oi) { return oi.id == resp.obj.id })[0];
            $scope.sale.order.orderItems[$scope.sale.order.orderItems.indexOf(old)] = resp.obj;
          });
          return initPayers();
        });
    };
    $scope.total = function() {
      return $scope.sale
        ? $scope.sale.order.orderItems.reduce(function(acc, oi) { 
            return acc + $scope.orderItemAmount(oi) * oi.productSnapshot.price; 
          }, 0)
        : 0;
    };
    $scope.updateSnapshot = function(productSnapshot) {
      ProductSnapshot.update(productSnapshot).then(function() {
        return initPayers();
      });
    };
    $scope.updateSale = updateSale;
    $scope.setPortionBucket = function(portionBucket) {
      $scope.sale.payers[0].portionBucket = portionBucket;
    };
    $scope.canClose = function() {
      if($scope.sale && $scope.sale.payers.length) {
        var payer = $scope.sale.payers[0];
        return !$scope.sale.closing 
          && Util.le(payer.portionBucket.amount, payer.portionBucket.portions.reduce(function(acc, portion) {
            return acc + portion.total;
          }, 0));
      } else {
        return false;
      }
    };
    $scope.close = function() {
      Sale.close($scope.sale.id).then(function() {
        if($scope.orderNumber) Util.query("#ancBack").click();
        else location.href = saleUrl;
      });
    };
    $scope.deleteItem = function(oi) {
      confirm(lang.get("orderItem.deleteQuestion", oi.productSnapshot.name)) &&
      Order.deleteItem($scope.sale.order.id, oi.displayOrder)
        .then(function() {
          return updateSale();
        }).then(function() {
          return initPayers();
        });
    };
    $scope.printInvoice = function() {
      $scope.sale && Printer.printInvoice($scope.sale.id); 
    };
    $scope.printECF = function() {
      if($scope.sale) {
        var document  = prompt(lang.get("billing.enterCPF"));
        document !== null && Printer.printECF($scope.sale.id, document);
      }
    };
    $scope.printSale = function(redo) {
      if($scope.sale) Printer.printSale($scope.sale.id, redo);
    };
    $scope.cancel = () => {
      confirm(lang.get("fastSelling.cancelQuestion")) &&
      Sale.cancel($scope.sale.id).then((resp) => {
        if(resp.obj.success) Util.query("#ancBack").click();
        else alert(resp.obj.message); 
      });
    };
    $scope.newExchangeUrl = (saleId) => finalUrl + "sales/" + saleId + "/exchange";
    $scope.exchangeUrl = (exchangeId) => finalUrl + "exchanges/" + exchangeId;
    Shortcut({
      86: "#ancBack",
      74: "#ancHome",
      52: "#ancReceipt",
      80: $scope.printSale.bind(null, false),
      78: $scope.printSale.bind(null, true),
      69: $scope.cancel 
    });
    function initPayers() {
      return Sale.initPayers($scope.sale.id).then(function(resp) {
        $scope.$apply(function() {
          $scope.sale.payers = resp.obj;
          $scope.portionBucketRefresh++;
        }); 
      });
    }
    function updateSale(id) {
      return Sale.get(id || ($scope.sale && $scope.sale.id), "orderItems-payers").then(function(resp) {
        $scope.$apply(function() {
          $scope.sale = resp.obj;
          $scope.orderNumber = $scope.sale.orderNumber;
          $scope.identifier = $scope.sale.identifier;
          $scope.backUrl = $scope.orderNumber ? tablesUrl : fastSellingUrl; 
          return initPayers();
        });
      });
    }
    function setupIdentifier() {
      $scope.$apply(function() {
        $scope.identifier = $scope.sale && $scope.sale.identifier ? $scope.sale.identifier : "";
      });
    }
  });
});
