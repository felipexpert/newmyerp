"use strict";

require(["util", "ng/app", "language"], function(Util, app, lang) {
  app.controller("BillingController", function($scope, Sale, Payer, Printer, Shortcut) { 
    var ctrl = this;
    this.saleId = null;
    this.sale = null;
    this.pending = 0;
    this.init = function(saleId) {
      ctrl.saleId = saleId;
      updateSale();
    };
    this.updateSale = updateSale;
    this.calcPending = function() {
      // I had to use ctrl instead of this because it will be a callback function, 
      // which will be called without the this into its context 
      ctrl.pending = calcPending(ctrl.sale); 
    };
    this.addPayer = function() {
      Sale.addPayer(ctrl.sale.id).then(updateSale);
    };
    this.updatePayer = function(payer) {
      Payer.update(payer.id, payer).then(updateSale); 
    };
    this.removePayer = function(payer) {
      Payer.delete(payer.id).then(updateSale);
    };
    this.canClose = function() {
      return ctrl.sale && !ctrl.sale.closing && ctrl.ok && ctrl.sale.payers.every(function(payer) {
        return Util.le(payer.portionBucket.amount, payer.portionBucket.portions.reduce(function(acc, portion) {
          return acc + portion.total;
        }, 0));
      });
    };
    this.close = function() {
      Sale.close(ctrl.sale.id).then(updateSale);
    };
    this.printInvoice = function() {
      Printer.printInvoice(ctrl.saleId); 
    };
    this.printECF = function() {
      var document  = prompt(lang.get("billing.enterCPF"));
      document !== null && Printer.printECF(ctrl.saleId, document);
    };
    Shortcut({
      86: "#ancBack", // V -> Go back
      84: "#ancTables", // T -> Tables
      69: ctrl.printInvoice, // E -> Print Invoice
      73: ctrl.printECF, // I -> Print ECF
      70: function() { // F -> Close
        ctrl.canClose() && ctrl.close();
      },
      74: "#ancHome" // J -> home
    });
    function updateSale() {
      Sale.get(ctrl.saleId, "payers").then(function(resp) {
        var data = resp.obj;
        $scope.$apply(function() {
          ctrl.sale = data;
          ctrl.pending = calcPending(data);
          ctrl.ok = Util.eq(ctrl.sale.total, calcPayersTotal(data));
        });
      });
    }
    function calcPayersTotal(sale) {
      return sale.payers.reduce(function(acc, p) {
        return acc + p.portionBucket.amount;
      }, 0);
    }
    function calcPending(sale) {
      return sale.total - sale.payers.reduce(function(acc, p) {
        var totalPortions = p.portionBucket.portions.reduce(function(acc, p) {
          return acc + p.total;
        }, 0);
        return acc + Math.min(p.portionBucket.amount, totalPortions);
      }, 0);
    }
  });
});
