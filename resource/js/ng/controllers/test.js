"use strict";

require(["util", "ng/app", "language"], function(Util, app, lang) {
  app.controller("TestController", function($scope) { 
    $scope.myValue = "First Arg";
    $scope.logMessage = function (arg1, message) {
      console.log(this, arg1, message);
    };
  });
  app.directive("xpPhone", function() {
    return {
      restrict: "E",
      scope: {
        dial: "&"
      },
      template: "<input type='text' ng-model='value'><button ng-click='func()'>Message!</button>",
      link: function(scope, element, attrs) {
        scope.value = "inicio";
        scope.func = function() {
          scope.dial({message: scope.value});
        };
      },
    };
  })
});
