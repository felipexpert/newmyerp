"use strict";

require(["util", "basic", "ng/app", "language", "modal", "enterAsTab"], function(Util, Basic, app, lang, Modal, EnterAsTab) {
  app.controller("ConsignmentController", 
      function($scope, Order, OrderItem, Consignment, ProductSnapshot, FeedCustomers, Shortcut, $timeout) { 
    var customersModal = null;
    $scope.baseUrl = baseUrl;
    $scope.consign = null;
    $scope.activeOrderItems = null;
    $scope.targetProduct = null;
    $scope.selectedOrderItem = null;
    $scope.previsionClosing = null;
    $scope.init = function(id) {
      id && Consignment.getExtra(id, "orderItems").then(setup); 
    };
    $scope.ensure = function() {
      return new Promise(function(succeed, failed) {
        if(!$scope.consign) Consignment.create().then(function(resp) {
          setup(resp);
          succeed();
        }, failed);
        else succeed();
      });
    };
    $scope.addProd = function(code, amount) {
      /*var xs = $scope.consign ? $scope.consign.orderItems.filter(function(oi) { 
        return oi.productSnapshot.product.code == code && 
        (   ($scope.coeficient == 1  && oi.summary.positiveFlows.amount)
         || ($scope.coeficient == -1 && oi.summary.negativeFlows.amount)); 
      }) : [];
      (  !xs.length 
      || confirm(lang.get(
             "consign.actionOnItemAlreadyDone"
           ,  lang.get($scope.coeficient == 1 ? "consign.out" : "consign.in")
           , xs[0].productSnapshot.name
           , $scope.consign.orderItems.indexOf(xs[0]) + 1
         ))
      ) && */
      $scope.ensure()
        .then(function() {
          return Consignment.addProd($scope.consign.id, code, $scope.coeficient * amount);
        })
        .then(function(resp) {
          if(resp.obj.validCode) {
            $scope.$apply(function() {
              setupOrderItem(resp.obj.orderItem);
            });
          } else {
            alert(lang.get("invalidCode"));
          }
        });
    };
    $scope.openItem = function(orderItem) {
      $scope.selectedOrderItem = orderItem;
      var modal = new Modal(Util.query("#selectedOrderItemModal"));
      modal.prepare();
      modal.display();
    };
    $scope.openCustomersModal = function() {
      customersModal = new Modal(Util.query("#customersModal"));
      customersModal.display();
    };
    $scope.feedCustomers = FeedCustomers;
    $scope.bindCustomer = function(row) {
      customersModal.close();
      $scope.ensure()
        .then(function() {
          return Consignment.bindCustomer($scope.consign.id, row[0]);
        })
        .then(function(resp) {
          $scope.$apply(function() {
            $scope.consign.dealer = resp.obj;
          });
        });
    };
    $scope.isSuccess = function(oi) { 
      return oi.summary.positiveFlows.amount == oi.summary.negativeFlows.amount; 
    };
    $scope.isWarning = function(oi) { 
      return oi.summary.negativeFlows.amount != 0 && oi.summary.positiveFlows.amount > oi.summary.negativeFlows.amount; 
    };
    $scope.isError = function(oi) { 
      return oi.summary.positiveFlows.amount < oi.summary.negativeFlows.amount; 
    };
    $scope.updatePositiveFlow = function(oi) {
      if(oi.outOffset == oi.summary.positiveFlows.amount) return;
      Consignment.updateOrderItemAmount($scope.consign.id, oi.id, 103, oi.outOffset - oi.summary.negativeFlows.amount)
        .then(setupOrderItemAndApply);
    };
    $scope.updateNegativeFlow = function(oi) {
      if(oi.inOffset == oi.summary.negativeFlows.amount) return;
      Consignment.updateOrderItemAmount($scope.consign.id, oi.id, 104, oi.summary.positiveFlows.amount - oi.inOffset)
        .then(setupOrderItemAndApply);
    };
    $scope.updatePrevisionClosing = function() {
      var previsionClosing = $scope.previsionClosing;
      // $scope.ensure() may create a consignment and set $scope.previsionClosing to null
      $scope.ensure()
        .then(function() {
          $scope.previsionClosing = previsionClosing;
          return Consignment.updatePrevisionClosing($scope.consign.id, $scope.previsionClosing);
        })
        .then(function() {
          $scope.$apply(function() {
            $scope.consign.previsionClosing = $scope.previsionClosing;
          });
        });
    };
    $scope.report = function() {
      Util.openInNewTab(baseUrl + "vwConsignment/report/" + $scope.consign.id);
      Util.openInNewTab(baseUrl + "vwConsignment/reportNP/" + $scope.consign.id);
    };
    $scope.updateSnapshot = function(productSnapshot) {
      ProductSnapshot.update(productSnapshot).then();
    };
    $scope.negativeAmount = () => OrderItem.negativeAmount($scope.activeOrderItems);
    $scope.positiveAmount = () => OrderItem.positiveAmount($scope.activeOrderItems);
    $scope.negativeTotal = () => OrderItem.negativeTotal($scope.activeOrderItems);
    $scope.positiveTotal = () => OrderItem.positiveTotal($scope.activeOrderItems);
    $scope.costTotal = () => OrderItem.costTotal($scope.activeOrderItems);
    $scope.amount = () => OrderItem.amount($scope.activeOrderItems);
    $scope.total = () => OrderItem.total($scope.activeOrderItems);
    $scope.deleteItem = (oi) => {
      confirm(lang.get("orderItem.deleteQuestion", oi.productSnapshot.name)) &&
      Order.deleteItem($scope.consign.order.id, oi.displayOrder)
        .then(() => $scope.init($scope.consign.id));
    };
    $scope.cancel = () => {
      confirm(lang.get("consign.cancelQuestion")) &&
      Consignment.cancel($scope.consign.id).then((resp) => {
        if(resp.obj.success) Util.query("#ancBack").click();
        else alert(resp.obj.message);
      });
    };
    $scope.makeSale = () => {
      console.log("vai");
      ($scope.consign.hasSale || confirm(lang.get("consign.makeSaleQuestion"))) &&
      (location.href = baseUrl + "vwConsignment/makeSale/" + $scope.consign.id);
    };
    Shortcut({
      86: "#ancBack", // V -> Go back
      84: $scope.openCustomersModal, // T
      80: $scope.makeSale, // P
      70: $scope.report,
      69: $scope.cancel
    });

    function setup(resp) {
      $scope.$apply(function() {
        $scope.consign = resp.obj;
        $scope.previsionClosing = $scope.consign.previsionClosing;
        sortOrderItems($scope.consign.orderItems);
        $scope.activeOrderItems = $scope.consign.orderItems.filter((oi) => !oi.disabled);
      });
    }
    function sortOrderItems(orderItems) {
      orderItems.sort(function(oiA, oiB) {
        return oiB.productFlows[oiB.productFlows.length - 1].id -
               oiA.productFlows[oiA.productFlows.length - 1].id;
      });
    }
    function setupOrderItemAndApply(resp) {
      $scope.$apply(function() { setupOrderItem(resp.obj); });
    }
    function setupOrderItem(orderItem) {
      $scope.consign.orderItems = $scope.consign.orderItems.filter(function(oi) {
        return oi.id != orderItem.id;
      });
      $scope.consign.orderItems.push(orderItem);
      sortOrderItems($scope.consign.orderItems);
      $scope.activeOrderItems = $scope.consign.orderItems.filter((oi) => !oi.disabled);
    }
  });
});
