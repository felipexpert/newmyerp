"use strict";

require(["ng/app", "util", "basic", "language"], function(app, Util, Basic, lang) {
  app.directive("xpDate", ["$timeout", function($timeout) {
    return {
      restrict: "E",
      scope:  {
        date: "=ngModel",
        blur: "&"
      },
      templateUrl: baseUrl + "resource/templates/directives/xp-date.html",
      link: function(scope, iElement, iAttrs) {
        scope.$watch("date", function() {
          scope.date !== null && (scope.facade = lang.get("date", scope.date));
        });
        scope.onKeyup = function($event) {
          if(!Util.intersection([$event.keyCode], [8, 46]).length 
              && Util.intersection([scope.facade.length], [2, 5]).length)
            scope.facade += "/";
          if(scope.facade.length > 10) scope.facade = scope.facade.slice(0, 10);
        };
        scope.onBlur = function($event) {
          scope.date = lang.get('parseDate', scope.facade);
          $timeout(function() {
            scope.blur && scope.blur({$event: $event, date: scope.date});
          });
        };
      }
    };
  }]);
});
