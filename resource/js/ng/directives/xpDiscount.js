"use strict";

require(["ng/app", "util", "basic"], function(app, Util, Basic) {
  app.directive("xpDiscount", ["Sale", function(Sale) {
    return {
      restrict: "E",
      replace: true,
      scope: {
        saleId: "@",
        update: "&"
      },
      templateUrl: baseUrl + "resource/templates/directives/xp-discount.html",
      link: function(scope, element, attrs) {
        scope.sale = null;
        updateSale();
        scope.save = function() {
          Sale.update(scope.saleId, scope.sale)
            .then(() => Sale.initPayers(scope.sale.id))
            .then(scope.update);
        };
        scope.reload = updateSale;
        function updateSale() {
          return Sale.get(scope.saleId).then(function(resp) {
            scope.$apply(() => scope.sale = resp.obj);
          });
        }
      }
    }
  }]);
})
