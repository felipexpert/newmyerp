"use strict";

require(["util", "ng/app"], function(Util, app) {
  app.directive("xpPercentage", function() {
    return {
      restrict: "E",
      replace: true,
      scope: {
        isPercentage: "=",
        referenceNumber: "=", // 1
        number: "=",
        blur: "&",
        focus: "&"
      },
      templateUrl: baseUrl + "resource/templates/directives/xp-percentage.html",
      link: function(scope, element, attrs) {
        element = element[0]; // DOM element
        var button = Util.queryOver(element, "button");
        scope.$watch("isPercentage", updateFacade);
        scope.$watch("referenceNumber", updateFacade);
        scope.$watch("number", updateFacade);
        scope.toggle = function() {
          scope.isPercentage = !scope.isPercentage;
        };
        scope.update = function() {
          scope.number = scope.isPercentage
            ? (scope.facade / 100) * scope.referenceNumber
            : scope.facade;
        };
        scope.onBlur = function($event) {
          scope.blur({$event: $event, number: scope.number});
        };
        scope.onFocus = function($event) {
          scope.focus({$event: $event, number: scope.number});
        };
        function updateFacade() {
          if(scope.isPercentage) {
            button.textContent =  "%"; 
            scope.facade = (scope.number / scope.referenceNumber) * 100;
          } else {
            button.textContent =  "N"; 
            scope.facade = scope.number;
          }
        }
      }
    };
  });
});
