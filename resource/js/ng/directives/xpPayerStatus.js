"use strict";
require(["basic", "ng/app", "language"], function(Basic, app, lang) {
  app.directive("xpPayerStatus", function() {
    return {
      restrict: "E", 
      replace: true, 
      scope: {
        payer: "="
      },
      templateUrl: baseUrl + "resource/templates/directives/xp-payer-status.html",
      link: function(scope, element, attrs) {
        scope.portionHasReversals = (portion) => {
          return portion.payments.filter((p) => p.hasReversal).length > 0;
        };
        scope.paymentStatus = function(portion) {
          var paymentTotal = portion.payments
            .filter((p) => !p.hasReversal)
            .reduce((acc, p) => acc + p.amount, 0);
          var diff = paymentTotal - portion.total;
          return lang.get("billing.change", Basic.numberToMoney(Math.abs(diff)));
        };
        scope.validPayments = (payments) => payments.filter((p) => !p.hasReversal); 
      }
    };
  });
});
