"use strict";

require(["ng/app"], function(app) {
  app.directive("xpConsignedOrderItem", function() {
    return {
      restrict: "E",
      replace: true,
      scope: {
        orderItem: "="
      },
      templateUrl: baseUrl + "resource/templates/directives/xp-consigned-order-item.html"
    };
  });
});
