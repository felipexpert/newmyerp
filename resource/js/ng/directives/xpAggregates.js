"use strict";

require(["ng/app", "util", "basic"], function(app, Util, Basic) {
  app.directive("xpAggregates", ["Sale", function(Sale) {
    return {
      restrict: "E",
      replace: true,
      scope: {
        saleId: "@",
        update: "&"
      },
      templateUrl: baseUrl + "resource/templates/directives/xp-aggregates.html",
      link: function(scope, element, attrs) {
        scope.aggregates = [];
        scope.total = function(aggregate) { 
          return aggregate.subtotal * aggregate.amount; 
        };
        Sale.get(scope.saleId).then(function(resp) {
          scope.aggregates = resp.obj.saleAggregates;
        });
        scope.save = function() {
          var sale = {aggregates: scope.aggregates};
          Sale.update(scope.saleId, sale).then(scope.update);
        };
      }
    }; 
  }]);
});
