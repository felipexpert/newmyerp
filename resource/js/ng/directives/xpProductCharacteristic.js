"use strict";

require(["ng/app", "util", "arrays", "optional", "language"], function(app, Util, Arrays, Optional, lang) {
  app.directive("xpProductCharacteristic", function($timeout, Characteristic, CharacteristicInstance) {
    return {
      restrict: "E",
      replace: true,
      scope: {
        selected: "=",
        characteristics: "=",
        notifyChange: "&"
      },
      templateUrl: baseUrl + "resource/templates/directives/xp-product-characteristic.html",
      link: (scope, element, attrs) => {
        scope.createCharacteristic = () => {
          var name = prompt(lang.get("product.characteristicName"));
          name && Characteristic.create(name).then(function(resp) {
            scope.$apply(function() {
              scope.characteristics.push(resp.obj);
            });
          })
        };
        scope.createInstance = (c) => {
          var name = prompt(lang.get("product.characteristicInstanceName", c.name));
          name && Characteristic.createInstance(c.id, name).then(function(resp) {
            scope.$apply(function() {
              c.instances.push(resp.obj);
              c.instances.sort(function(a, b) {
                return a.name.localeCompare(b.name);
              });
            });
          });
        };
        scope.toggle = (i) => {
          let index = scope.selected.indexOf(i.id);
          if(index != -1) scope.selected = Util.woIndex(scope.selected, index);
          else scope.selected = Util.push(scope.selected, i.id);
          notifyChange();
          console.log("toggle");
        };
        scope.isSelected = (i) => scope.selected.indexOf(i.id) != -1;
        scope.save = (c) => Characteristic.update(c);
        scope.deleteChar = (c) => {
          return init().then(() => {
            let c2 = Arrays.find((c2) => c2.id == c.id, scope.characteristics).get();
            if(c2.disableable) return Characteristic.delete(c.id)
                .then(init)
                .then(() => {
                  scope.selected = scope.selected.filter((id) => !c.instances.some((i) => id == i.id))
                  notifyChange();
                });
            alert(lang.get("pChar.cannotDeleteItem", c.name));  
          });
        };
        scope.deleteInst = (i) => {
          return init().then(() => {
            let instancesNonFlat = scope.characteristics.map((c) => c.instances),
                instances = Arrays.flat(instancesNonFlat),
                i2 = Arrays.find((i2) => i2.id == i.id, instances).get();
            if(i2.disableable) return CharacteristicInstance.delete(i.id)
                .then(init)
                .then(() => {
                  scope.selected = scope.selected.filter((id) => id != i.id)
                  notifyChange();
                });
            alert(lang.get("pChar.cannotDeleteItem", i.name));  
          });
        };
        scope.up = (c) => Characteristic.up(c.id).then(init);
        scope.down = (c) => Characteristic.down(c.id).then(init);
        function notifyChange() { $timeout(scope.notifyChange); }

        function init() {
          return Characteristic.getAll().then(function(resp) {
            scope.$apply(function() {
              scope.characteristics = resp.obj;
              scope.setCharacteristics({characteristics: scope.characteristics});
            });
          });
        } 
      }
    };
  });
});
