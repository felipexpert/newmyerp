"use strict";

require(["util", "ng/app"], function(Util, app) {
  app.directive('autofocus', function($timeout) {
    return function(scope, element) {
      $timeout(function() {
        var maybeInput = Util.queryOver(element[0], "input");
        maybeInput 
          ? maybeInput.select 
            ? maybeInput.select()
            : maybeInput.focus() 
          : element[0].focus();
      });
    }
  });
});
