"use static";

require(["util", "modal", "ng/app", "language"], function(Util, Modal, app, lang) {
  app.directive("xpProductHelper", function($timeout, ProductInstance, FeedProducts, Shortcut) {
    return {
      restrict: "E"
    , replace: true
    , scope: {
      notifyProd: "="
    }
    , templateUrl: baseUrl + "resource/templates/directives/xp-product-helper.html"
    , link: function(scope, element, attrs) {
      var node = element[0],
          productsModal = null;
      scope.baseUrl = baseUrl;
      scope.codeOrBarCode = "";
      scope.amount = 1;
      scope.targetProduct = null;
      scope.fastMode = false;
      scope.loadProdData = function() {
        !scope.fastMode && ProductInstance.getByCodeOrBarCode(scope.codeOrBarCode).then((resp) => {
          scope.$apply(function() {
            scope.targetProduct = resp.obj;
            scope.updateSubtotal();
          });
        });
      };
      scope.updateSubtotal = function() {
        scope.targetProduct && (scope.targetProduct.subtotal = scope.targetProduct.price * scope.amount);
      };
      scope.confirm = function() {
        return scope.amount < 100 && scope.amount > 0
          ? send()
          : confirm(lang.get("product.strangeQuantity", scope.amount))
            ? send()
            : null;
        function send() {
          return ProductInstance.getByCodeOrBarCode(scope.codeOrBarCode).then((resp) => {
            let product = resp.obj;
            if(!product) {
              alert(lang.get("product.productHasntBeenLoaded"));
            } else {
              scope.notifyProd(product.code, scope.amount);
              scope.codeOrBarCode = "";
              scope.targetProduct = null;
              scope.fastMode && $timeout(() => {
                scope.amount = 1;
                Util.queryOver(node, "#code").select();
              });
            }
          });
        }
      };
      scope.feedProducts = FeedProducts;
      scope.bindProduct = function(row) {
        productsModal.close();
        scope.codeOrBarCode = row[0];
        scope.targetProduct = null;
        $timeout(function() {
          scope.loadProdData().then(function() {
            scope.confirm();
          });
        });
      };
      scope.openProductsModal = function() {
        productsModal = new Modal(Util.query("#productsModal"));
        productsModal.display();
      };
      Shortcut({
        66: scope.openProductsModal // B
      });
    }
    };
  });
});
