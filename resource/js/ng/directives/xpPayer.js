"use strict";

require(["util", "basic", "modal", "ng/app", "language"], function(Util, Basic, Modal, app, lang) {
  app.directive("xpPayer", function(Sale, Customer, Payer, FeedCustomers) {
    return {
      restrict: "E",
      scope: {
        payer: "=",
        update: "="
      },
      templateUrl: baseUrl + "resource/templates/directives/xp-payer.html",
      link: function(scope, element, attrs) {
        var node = element[0],
            portionBucketNode = Util.queryOver(node, ".xp-portion-bucket"),
            portionBucketModal = new Modal(portionBucketNode),
            tableSelectorNode = Util.queryOver(node, ".xp-table-selector"),
            tableSelectorModal = new Modal(tableSelectorNode); 
        portionBucketModal.prepare();
        tableSelectorModal.prepare();
        scope.baseUrl = baseUrl;
        scope.setPortionBucket = function(portionBucket) {
          scope.payer.portionBucket = portionBucket;
          scope.update && scope.update();
        };
        scope.accessPortionBucket = function() {
          portionBucketModal.display();
        };
        scope.showCustomerModal = function() {
          tableSelectorModal.display();
        };
        scope.feedCustomers = FeedCustomers;
        scope.bindCustomer = function(row) {
          var payer = {customer: {id: row[0]}};
          Payer.update(scope.payer.id, payer).then(scope.changePayer);
        };
        scope.changePayer = function() {
          scope.$apply(() => {
            tableSelectorModal.close();
            location.reload(true);
          });
        };
      } 
    };
  });
});
