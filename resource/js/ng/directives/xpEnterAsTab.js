"use strict";

require(["util", "enterAsTab", "ng/app"], function(Util, EnterAsTab, app) {
  app.directive("xpEnterAsTab", function($timeout) {
    return {
      scope: {
        then: "&"
      },
      link: function(scope, element, attrs) {
        var element = element[0],
            focusables = Util.queryOver(element, "[data-index]", true);
        focusables.sort((a, b) => a.getAttribute("data-index") - b.getAttribute("data-index"));
        var enterAsTab = new EnterAsTab(focusables, () => scope.then && scope.then());
        enterAsTab.chain();
      }
    }
  });
});
