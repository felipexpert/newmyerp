"use strict";

require(["ng/app", "util", "basic", "modal", "enterAsTab", "language"], function(app, Util, Basic, Modal, EnterAsTab, lang) {
  app.directive("xpPortionBucket", 
    function(PortionBucket, Portion, PredefinedParcelling, PaymentType, Payer, FeedCustomers, $timeout) {
    return {
      restrict: "E",
      replace: true,
      scope: { id: "@"
             , refresh: "="
             , setPortionBucket: "&"
             , setPayer: "&" },
      templateUrl: baseUrl + "resource/templates/directives/xp-portion-bucket.html",
      link: function(scope, element, attrs) {
        var node = element[0],
            tableSelectorNode = Util.queryOver(node, ".xp-table-selector"),
            tableSelectorModal = new Modal(tableSelectorNode),
            enterAsTab = null; 
        tableSelectorModal.prepare();
        scope.portionBucket = null;
        scope.predefinedParcellings = [];
        scope.selectedPredefinedParcelling = null;
        scope.portionsAmount = 1;
        scope.paymentTypes = [];
        scope.slots = [];
        scope.hasDownPayment = false;
        scope.downPaymentType = null;
        scope.downPayment = 0;
        scope.$watch("hasDownPayment", console.log);
        scope.insertPredefinedParcelling = function() {
          PortionBucket.insertPredefinedParcelling(scope.id, scope.selectedPredefinedParcelling.id)
            .then(updatePortionBucket);
        };
        scope.dateStatus = function(portion) {
          return portion.validUntil ? lang.get("date", portion.validUntil) : lang.get("billing.byCash");
        };
        scope.paid = function(portion) {
          let paid = portion.payments
            .filter((payment) => !payment.hasReversal)
            .reduce((acc, payment) => acc + payment.amount, 0);
          return paid;
        };
        scope.updatePortionValidUntil = function(portion) {
          Portion.update(portion.id, portion).then(updatePortionBucket);
        };
        scope.updatePortionTotal = function(portion) {
          Portion.update(portion.id, portion).then(updatePortionBucket);
        };
        scope.removePortion = function(portion) {
          Portion.delete(portion.id).then(updatePortionBucket);
        };
        scope.createPortions = function() {
          if(scope.hasDownPayment) {
            if(!scope.downPaymentType) {
              alert(lang.get("portionBucket.paymentTypeUndefined"));
              return;
            }
            let payment = {amount: scope.downPayment, paymentTypeId: scope.downPaymentType.id};
            PortionBucket.createAndPayPortion(scope.id, [payment])
              .then(createPortions)
              .then(updatePortionBucket);
          } else createPortions().then(updatePortionBucket);
          function createPortions() { return PortionBucket.createPortions(scope.id, scope.portionsAmount); };
        };
        scope.createAndPayPortion = function() {
          var payments = scope.slots.filter(function(slot) {
            return Util.gt(slot.amount, 0);
          }).map(function(slot) { 
            return {amount: slot.amount, paymentTypeId: slot.paymentType.id}; 
          });
          payments.length && PortionBucket.createAndPayPortion(scope.id, payments).then(updatePortionBucket);
        }
        scope.portionChange = function(portion) {
          var paymentTotal = portion.payments
            .filter((payment) => !payment.hasReversal)
            .reduce((acc, payment) => acc + payment.amount, 0);
          return paymentTotal - portion.total;
        };
        scope.total = function() {
          if(!scope.portionBucket) return 0; 
          var totalPortions = scope.portionBucket.portions.reduce((acc, p) => { 
            let reversed = p.payments
              .filter((payment) => payment.hasReversal)
              .reduce((acc, payment) => acc + payment.amount, 0);
            return acc + p.total - Math.min(reversed, p.total); 
          }, 0);
          return scope.portionBucket.amount - totalPortions;
        };
        scope.totalPayments = () => scope.slots.reduce((acc, s) => acc + s.amount, 0);
        scope.change = function() {
          if(!scope.portionBucket) return 0; 
          return scope.totalPayments() - scope.total();
        };
        scope.showCustomerModal = function() {
          tableSelectorModal.display();
        };
        scope.feedCustomers = FeedCustomers;
        scope.bindCustomer = function(row) {
          var payer = {customer: {id: row[0]}};
          scope.portionBucket.payer &&
          Payer.update(scope.portionBucket.payer.id, payer)
            .then(updatePortionBucket)
            .then(() => {
              tableSelectorModal.close();
              scope.setPayer({payer: scope.portionBucket.payer});
            });
        };
        PaymentType.get().then((resp) => {
          scope.paymentTypes = resp.obj;
          scope.slots = resp.obj.map((paymentType) => ({ amount: 0, paymentType: paymentType }));
        });
        updatePortionBucket();
        scope.$watch("refresh", function(n, o) {
          updatePortionBucket();
        });
        PredefinedParcelling.get().then(function(resp) {
          scope.predefinedParcellings = resp.obj;
          scope.selectedPredefinedParcelling = resp.obj[0];
        });

        function updatePortionBucket() {
          scope.slots.forEach(function(slot) { slot.amount = 0; });
          return PortionBucket.getExtra(scope.id, "customer-payer").then(function(req) {
            scope.$apply(function() {
              scope.portionBucket = req.obj;
              scope.setPortionBucket && scope.setPortionBucket({portionBucket: req.obj});
              $timeout(() => {
                enterAsTab && enterAsTab.unchain();
                var inputs = Util.queryOver(node, "[data-index]", true);
                inputs.sort((a, b) => a.getAttribute("data-index") - b.getAttribute("data-index"));
                enterAsTab = new EnterAsTab(inputs, scope.createAndPayPortion);
                enterAsTab.chain();
              });
            });
          });
        }
      }
    }; 
  });
});
