"use strict";

require(["util", "basic", "tableSelector", "ng/app"], function(Util, Basic, TableSelector, app) {
  app.directive("xpTableSelector", ["$timeout", function($timeout) {
    return {
      restrict: "E",
      replace: true,
      templateUrl: baseUrl + "resource/templates/directives/xp-table-selector.html",
      scope: {
        link: "@",
        getData: "&",
        optional: "=",
        select: "&"
        //eager: "@"
      },
      link: function(scope, e, attrs) {
        var elem = e[0],
            table = Util.queryOver(elem, "table"),
            input = Util.queryOver(elem, ".filter"),
            tableSelector = null;
        scope.headings = [];
        scope.items = 100;
        scope.filter = "";
        scope.$watch("optional", constructTable, true);
        scope.selectRow = function() {
          scope.filter = "";
          scope.select && scope.select({selectedRow: tableSelector.getSelectedRow()});
        };
        scope.onKeyup = function($event) {
          if($event.keyCode == 13 && tableSelector && tableSelector.hasSelectedRow())
            scope.selectRow();
          else if(!Util.intersection([40, 38], [$event.keyCode]).length) 
            constructTable();
        };
        function constructTable() {
          var promise = null;
          var optional = typeof scope.optional == "object" ? scope.optional : null;
          scope.getData 
          && scope.items  
          && (promise = scope.getData({items: scope.items, filter: scope.filter, optional: optional})).then 
          && promise.then(function(data) {
            scope.headings = data.headings;
            var innerHTML = "<thead><tr>";
            innerHTML += data.headings.reduce(function(acc, heading) {
              return acc + "<th" + (heading.width ? " data-width=" + heading.width : "") + ">" + heading.name + "</th>";
            }, ""); 
            innerHTML += "</tr></thead><tbody>";
            innerHTML += data.rows.reduce(function(acc, row) {
              return acc + "<tr>" + row.reduce(function(acc, column) {
                return acc + "<td>" + column + "</td>";
              }, "") + "</tr>";
            }, "");
            innerHTML += "</tbody>";
            table.innerHTML = innerHTML;
            tableSelector && tableSelector.disable();
            tableSelector = new TableSelector(table, input);
            tableSelector.enable();
            Basic.arrangeTable(table);
          });
        };
      }
    };
  }]);
});
