"use strict";

require(["ng/app", "util", "formatter", "basic"], function(app, Util, Formatter, Basic) {
  app.directive("xpNumber", function($timeout) {
    return {
      restrict: "E",
      scope:  {
        number: "=ngModel",
        decimals: "@",
        blur: "&",
        focus: "&",
        keyup: "&"
      },
      templateUrl: baseUrl + "resource/templates/directives/xp-number.html",
      link: function(scope, iElement, iAttrs) {
        //console.log(iElement[0]);
        //iElement[0].style.border="3px solid red";
        var element = iElement[0],
            input = Util.queryOver(element, "input");
        element.select = function() { input.select(); };
        switch(scope.decimals) {
          case "money": scope.decimals = sys.MONEY_PRECISION; break;
          case "generic": scope.decimals = sys.GENERIC_PRECISION; break;
        }
        scope.facade = null;
        scope.$watch("number", function() {
          if(scope.number != 0 || scope.facade != "") {
            scope.facade = Formatter.numStr(scope.number);
            if(document.activeElement != input)
              applyDecimals();
          }
        });
        scope.updateNumber = function($event) {
          scope.number = Formatter.strNum(scope.facade);
          scope.keyup && $timeout(function() {
            scope.keyup({$event: $event, number: scope.number});
          });
        };
        scope.onBlur = function($event) {
          applyDecimals();
          scope.blur && scope.blur({$event: $event, number: scope.number});
        };
        scope.onFocus = function($event) {
          scope.focus && scope.focus({$event: $event, number: scope.number});
        };
        function applyDecimals() {
          if(scope.decimals === undefined) return;
          let decimals = Number(scope.decimals);
          if(decimals > 0) scope.facade = Formatter.enforceDecimals(scope.facade, decimals);
        }
      }
    };
  });
});
