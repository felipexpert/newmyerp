"use strict";
require(["util", "ng/app"], function(Util, app) {
  app.factory("Shortcut", function ShortcutFactory($document) {
    return function(shortcuts) {
      $document.on("keydown", Util.killCtrl());
      $document.on("keyup", Util.killCtrl(function(event) {
        for(var attr in shortcuts) if(shortcuts.hasOwnProperty(attr) && event.keyCode == attr) {
          if(typeof shortcuts[attr] == "function")    shortcuts[attr]();
          else                                        Util.query(shortcuts[attr]).click();
          break;
        }
      }));
    };
  });
});
