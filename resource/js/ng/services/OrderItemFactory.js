"use strict";

require(["ng/app"], function(app) {
  app.factory("OrderItem", function OrderItemFactory() {
    return {
      negativeAmount: function(orderItems) {
        return orderItems.reduce(function(acc, oi) {
          return acc + oi.summary.negativeFlows.amount;
        }, 0);
      }, 
      positiveAmount: function(orderItems) {
        return orderItems.reduce(function(acc, oi) {
          return acc + oi.summary.positiveFlows.amount;
        }, 0);
      },
      negativeTotal: function(orderItems) {
        return orderItems.reduce(function(acc, oi) {
          return acc + oi.summary.negativeFlows.price;
        }, 0);
      },
      positiveTotal: function(orderItems) {
        return orderItems.reduce(function(acc, oi) {
          return acc + oi.summary.positiveFlows.price;
        }, 0);
      },
      amount: function(orderItems) {
        return orderItems.reduce(function(acc, oi) {
          return acc + oi.summary.amount;
        }, 0);
      },
      total: function(orderItems) {
        return orderItems.reduce(function(acc, oi) {
          return acc + oi.summary.price;
        }, 0);
      },
      costTotal: function(orderItems) {
        return orderItems.reduce((acc, oi) => acc + oi.summary.amount * oi.productSnapshot.buyingPrice, 0);
      }
    };
  });
});
