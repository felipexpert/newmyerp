"use strict";

require(["ng/app", "language"], function(app, lang) {
  app.factory("FeedCustomers", function FeedCustomersFactory(Customer) {
    return function(items, filter) {
      return new Promise(function(succeed, failed) {
        Customer.get(items, filter).then(function(resp) {
          var headings = [ {name: 'id'}
                         , {name: lang.get('person.name')}
                         , {name: lang.get('person.fone')}
                         , {name: lang.get('person.address')}
                         , {name: lang.get('person.cpf')}
                         , {name: lang.get('person.rg')}
                         ],
              rows = resp.obj.map(function(c) {
                var a = c.personAttr;
                return [c.id, a.name, a.fone, a.address, a.cpf, a.rg];
              });
          succeed({headings: headings, rows: rows});
        }, failed);
      });
    };
  });
});
