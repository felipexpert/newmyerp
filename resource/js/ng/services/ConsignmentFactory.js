"use strict";

require(["ng/app"], function(app) {
  app.factory("Consignment", function ConsignmentFactory(Request) {
    return {
      getExtra: function(id, extra) {
        return Request.getExtra(baseUrl + "consignment/" + id, extra, null, true);
      },
      getByFilter: function(extra, items, filter, optional) {
        return Request.getExtraFilterOptional(baseUrl + "consignment", extra, items, filter, optional, null, true);
      },
      create: function() {
        return Request.post(baseUrl + "consignment/create"); 
      },
      addProd: function(id, code, amount) {
        return Request.post(baseUrl + "consignment/" + id + "/addProd", {code: code, amount: amount}, null, true);
      },
      updateOrderItemAmount: function(id, orderItemId, flowType, amount) {
        return Request.post(baseUrl + "consignment/" + id + "/updateOrderItemAmount", 
          {orderItemId: orderItemId, flowType: flowType, amount: amount});
      },
      updatePrevisionClosing: function(id, previsionClosing) {
        return Request.post(baseUrl + "consignment/" + id + "/updatePrevisionClosing", 
          {previsionClosing: previsionClosing});
      },
      bindCustomer: function(consignId, customerId) {
        return Request.post(baseUrl + "consignment/" + consignId + "/bindCustomer", {customerId: customerId});
      },
      cancel: (consignId) => Request.post(baseUrl + "consignment/" + consignId + "/cancel", null, null, true)
    };
  });
});
