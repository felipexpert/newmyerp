"use strict";

require(["util", "basic", "request", "ng/app"], function(Util, Basic, Request, app) {
  app.factory("Request", function RequestFactory() {
    return {
      get: function(url, task, noLog) {
        return Request.toastRequest(Request.METHODS.GET, url, null, task, noLog).then(respond);        
      },
      getExtra: function(url, extra, task, noLog) {
        extra && (url += "?extra=" + extra);
        return this.get(url, task, noLog);
      },
      getFilter: function(url, items, filter, task, noLog) {
        if(items && typeof filter == "string") url += "?items=" + items + "&filter=" + encodeURIComponent(filter);
        return this.get(url, task, noLog);
      },
      getFilterOptional: function(url, items, filter, optional, task, noLog) {
        let json = JSON.stringify(optional);
        if(items && typeof filter == "string") 
          url += "?items=" + items + "&filter=" + encodeURIComponent(filter) + "&optional=" + encodeURIComponent(json);
        return this.get(url, task, noLog);
      },
      getExtraFilter: function(url, extra, items, filter, task, noLog) {
        url += "?";
        if(extra) url += "extra=" + extra + "&";
        if(items && typeof filter == "string") url += "items=" + items + "&filter=" + encodeURIComponent(filter);
        return this.get(url, task, noLog); 
      },
      getExtraFilterOptional: function(url, extra, items, filter, optional, task, noLog) { 
        let json = JSON.stringify(optional);
        url += "?";
        if(extra) url += "extra=" + extra + "&";
        if(items && typeof filter == "string") 
          url += "items=" + items + "&filter=" + encodeURIComponent(filter) + "&optional=" + encodeURIComponent(json);
        return this.get(url, task, noLog);
      },
      put: function(url, object, task, noLog) {
        return Request.toastRequest(Request.METHODS.PUT, url, JSON.stringify(object), task, noLog).then(respond);        
      },
      delete: function(url, task, noLog) {
        return Request.toastRequest(Request.METHODS.DELETE, url, null, task, noLog).then(respond);        
      },
      post: function(url, object, task, noLog) {
        return Request.toastRequest(Request.METHODS.POST, url, JSON.stringify(object), task, noLog).then(respond);        
      }
    };
  }); 
  function respond(response) {
    if(response.type == "application/json;UTF-8") response.obj = Util.tryParse(response.body);
    return response;
  };
});
