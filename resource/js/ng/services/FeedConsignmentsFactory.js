"use strict";

require(["basic", "ng/app", "language"], function(Basic, app, lang) {
  app.factory("FeedConsignments", function FeedConsignmentsFactory(Consignment) {
    return function(items, filter, optional) {
      return new Promise(function(succeed, failed) {
        Consignment.getByFilter("", items, filter, optional).then(function(resp) {
          var headings = [ {name: "Id"}
                         , {name: lang.get("consign.creationDate")}
                         , {name: lang.get("consign.boundWith")}
                         , {name: "CPF"}
                         , {name: lang.get("consign.previsionClosingDate")}
                         , {name: lang.get("consign.closingDate") }
                         ],
              rows = resp.obj.map(function(c) {
                return [ c.id
                       , lang.get("dateFull", c.order.creation)
                       , c.dealer ? c.dealer.customer.personAttr.name : lang.get("consign.notBound")
                       , c.dealer ? c.dealer.customer.personAttr.cpf : lang.get("consign.notBound")
                       , c.previsionClosing ? lang.get("date", c.previsionClosing) : "---"
                       , c.closing ? lang.get("dateFull", c.closing) : "---"
                       ];
              });
          succeed({headings: headings, rows: rows});
        }, failed);
      });
    };
  });
});
