"use strict";

require(["ng/app"], function(app) {
  app.factory("Printer", function PrinterFactory(Request) {
    return {
      printInvoice: function(saleId) {
        return Request.post(outerUrl + "pedido_imprimir_nfiscal.php?id=" + saleId);
      },
      printECF: function(saleId, document) {
        document = document ? "&documento=" + encodeURIComponent(document) : "";
        return Request.post(outerUrl + "modal/ecf_imprimir.php?id=" + saleId + document);
      },
      printSale: function(saleId, redo) {
        return Request.post(baseUrl + "printer/saleItems/" + saleId, {redo: redo});
      }
    };
  });
});
