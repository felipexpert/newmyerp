"use strict";

require(['ng/app'], function(app) {
  app.factory('Portion', function PortionFactory(Request) {
    return {
      update: function(portionId, portion) {
        return Request.put(baseUrl + "portion/" + portionId, portion);
      },
      delete: function(portionId) {
        return Request.delete(baseUrl + "portion/" + portionId);
      }
    };
  }); 
});
