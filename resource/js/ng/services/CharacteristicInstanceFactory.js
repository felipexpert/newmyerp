"use strict";

require(["ng/app"], function(app) {
  app.factory("CharacteristicInstance", function CharacteristicInstanceFactory(Request) {
    return {
      delete: (id) => Request.delete(baseUrl + "characteristicInstance/" + id),
      sameCharacteristics(charsA, charsB) {
        return charsA.length == charsB.length && 
               charsA.every((a) => charsB.some((b) => b.id == a.id));
      }
    }; 
  });
});
