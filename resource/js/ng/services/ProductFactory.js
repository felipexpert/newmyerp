"use strict";

require(["util", "numbers", "arrays", "ng/app"], (Util, Numbers, Arrays, app) => {
  app.factory("Product", function ProductFactory(Request) {
    return {
      getById: (id) => Request.get(baseUrl + "product/" + id),
      info: () => Request.get(baseUrl + "product/info"),
      create: (product) => Request.post(baseUrl + "product", product),
      update: (product) => Request.put(baseUrl + "product", product),
      updateDefaultPrice(p) {
        p.defaultPrice = Numbers.ceilDecimals(p.defaultBuyingPrice * (1 + p.defaultProfitPercentage / 100), 2);
      },
      updateDefaultProfitPercentage(p) {
        if(p.defaultBuyingPrice == 0) p.defaultProfitPercentage = 0;
        else p.defaultProfitPercentage = 
          Numbers.ceilDecimals((p.defaultPrice - p.defaultBuyingPrice) / p.defaultBuyingPrice * 100, 2);
      }
    }; 
  });
});
