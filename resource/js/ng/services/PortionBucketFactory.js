"use strict";

require(['ng/app'], function(app) {
  app.factory('PortionBucket', function PortionBucketFactory(Request) {
    return {
      get: function(pbId) {
        return Request.get(baseUrl + "portionBucket/" + pbId, null, true);
      },
      getExtra: function(pbId, extra) {
        return Request.getExtra(baseUrl + "portionBucket/" + pbId, extra, null, true);
      },
      insertPredefinedParcelling: function(pbId, ppId) {
        return Request.post(baseUrl + "portionBucket/insertPredefinedParcelling/" + pbId,
          {predefinedParcellingId: ppId});
      },
      createPortions: function(pbId, portions) {
        return Request.post(baseUrl + "portionBucket/createPortions/" + pbId,
          {portions: portions}, null, true);
      },
      createAndPayPortion: function(pbId, payments) {
        return Request.post(baseUrl + "portionBucket/createAndPayPortion/" + pbId, payments, null, true);
      }
    };
  }); 
});
