"use strict";

require(['ng/app'], function(app) {
  app.factory('Payer', function PayerFactory(Request) {
    return {
      get: function(payerId) {
        return Request.get(baseUrl + 'payer/' + payerId, null, true);
      },
      update: function(payerId, content) {
        return Request.put(baseUrl + 'payer/' + payerId, content); 
      },
      delete: function(payerId) {
        return Request.delete(baseUrl + 'payer/' + payerId); 
      }
    }; 
  });
});
