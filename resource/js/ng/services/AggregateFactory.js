"use strict";

require(['util', 'basic', 'ng/app'], function(Util, Basic, app) {
  app.factory('Aggregate', ['Ajax', function AggregateFactory(Ajax) {
    return {
      get: function(scope, saleId, success) {
        Ajax.get(scope, baseUrl + 'billing/getAggregatesAjax/' + saleId, null, success, null, true);
      },
      update: function(scope, aggregates, success) {
        Ajax.post(scope, baseUrl + 'billing/updateAggregatesAjax/', aggregates, null, success, null, true);
      }
    };
  }]); 
});
