"use strict";

require(["ng/app"], function(app) {
  app.factory("OldMyerp", function OldMyerpFactory(Ajax) {
    return {
      printInvoice: function(saleId) {
        Ajax.post(null, outerUrl + 'pedido_imprimir_nfiscal.php?id=' + saleId);
      },
      printECF: function(saleId, document) {
        document = document ? "&documento=" + encodeURIComponent(document): "";
        Ajax.post(null, outerUrl + 'modal/ecf_imprimir.php?id=' + saleId + document);
      }
    };
  });
});
