"use strict";

require(["ng/app"], function(app) {
  app.factory("Order", function OrderFactory(Request) {
    return {
      deleteItem: function(orderId, displayOrder) {
        return Request.post(baseUrl + "order/" + orderId + "/deleteItem", {displayOrder: displayOrder});
      }
    };
  });
});
