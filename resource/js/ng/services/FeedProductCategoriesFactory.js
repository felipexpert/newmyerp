"use strict";

require(["basic", "ng/app", "language"], function(Basic, app, lang) {
  app.factory("FeedProductCategories", function FeedProductCategoriesFactory(ProductCategory) {
    return (items, filter) => {
      return ProductCategory.getByFilter(items, filter).then((resp) => {
        let headings = [ {name: "Id"}
                       , {name: lang.get("name")}
                       , {name: lang.get("prodCategory.printerName")}
                       , {name: lang.get("prodCategory.printerPath")}],
            rows = resp.obj.map((c) =>
              [ c.id
              , c.name
              , c.printer ? c.printer.name : "---"
              , c.printer ? c.printer.path : "---"]);
        return { headings: headings, rows: rows };
      });
    };
  });
});
