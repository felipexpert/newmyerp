"use strict";

require(["util", "basic", "request", "ng/app"], function(Util, Basic, Request, app) {
  app.factory("Ajax", function AjaxFactory() {
    return {
      get: function(scope, url, task, success, fail, noLog) {
        Request.toastRequest(Request.METHODS.GET, url, null, task, noLog).then(function(response) {
          var data = response.type == "application/json;UTF-8" ? Util.tryParse(response.body) : response.body; 
          if(scope) scope.$apply(function() { success && success(data); });
          else success && success(data);
        }, function(error) {
          if(scope) scope.$apply(function() { fail && fail(error); });
          else fail && fail(error);
        });        
      },
      getExtra: function(scope, url, extra, task, success, fail, noLog) {
        if(extra) url += "?extra=" + extra;
        this.get(scope, url, task, success, fail, noLog);
      },
      getFilter: function(scope, url, items, filter, task, success, fail, noLog) {
        if(items && typeof filter == "string") url += "?items=" + items + "&filter=" + encodeURIComponent(filter);
        this.get(scope, url, task, success, fail, noLog);
      },
      getExtraFilter: function(scope, url, extra, items, filter, task, success, fail, noLog) {
        url += "?";
        if(extra) url += "extra=" + extra + "&";
        if(items && typeof filter == "string") url += "items=" + items + "&filter=" + encodeURIComponent(filter);
        this.get(scope, url, task, success, fail, noLog);
      },
      post: function(scope, url, content, task, success, fail, noLog) {
        Request.toastRequest(Request.METHODS.POST, url, JSON.stringify(content), task, noLog).then(function(response) {
          var data = response.type == "application/json;UTF-8" ? Util.tryParse(response.body) : response.body; 
          if(scope) scope.$apply(function() { success && success(data); });
          else success && success(data);
        }, function(error) {
          if(scope) scope.$apply(function() { fail && fail(error); });
          else fail && fail(error);
        });        
      } 
    };
  }); 
});
