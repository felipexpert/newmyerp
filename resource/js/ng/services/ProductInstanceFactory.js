"use strict";

require(["util", "numbers", "arrays", "misc", "ng/app"], function(Util, Numbers, Arrays, Misc, app) {
  app.factory("ProductInstance", function ProductInstanceFactory(Request, CharacteristicInstance) {
    return {
      getByCodeOrBarCode: (codeOrBarCode) => 
        Request.get(baseUrl + "productInstance?codeOrBarCode=" + encodeURIComponent(codeOrBarCode), null, true),
      get: (items, filter) => Request.getFilter(baseUrl + "productInstance", items, filter, null, true),
      info: () => Request.get(baseUrl + "productInstance/info"),
      isSelected(i, selected, characteristics) {
        let selectedChars = chars(selected),
            selectedCharIdsRaw = selectedChars.map(getId),
            selectedCharIds = Arrays.nub(selectedCharIdsRaw),
            instanceChars = chars(i.characteristicInstances.map((ci) => ci.id)),
            instanceCharIds = instanceChars.map(getId);
        return selectedCharIds.length == instanceCharIds.length
            && selectedCharIds.every((cid) => Arrays.find((cid2) => cid == cid2, instanceCharIds).isPresent());
        function chars(selected) {
          return selected.map((instanceId) => {
            return Arrays.find((c) => {
              return Arrays.find((ci) => ci.id == instanceId, c.instances).isPresent();
            }, characteristics).get();
          });
        }
        function getId(o) { return o.id; }
      },
      applyFixed(target, fixed) {
        let newTarget = fixed.reduce((acc, pi) => {
          let p = (x) => CharacteristicInstance.sameCharacteristics(x.characteristicInstances, pi.characteristicInstances),
              maybePIindex = Arrays.findIndex(p, acc);
          maybePIindex.ifPresent((index) => {
            acc[index].id = pi.id;
          });
          return acc;
        }, target.slice());
        return newTarget;
      },
      updatePrice(i) {
        i.price = Numbers.ceilDecimals(i.buyingPrice * (1 + i.profitPercentage / 100), 2);
      },
      updateProfitPercentage(i) {
        if(i.buyingPrice == 0) i.profitPercentage = 0;
        else i.profitPercentage = Numbers.ceilDecimals((i.price - i.buyingPrice) / i.buyingPrice * 100, 2);
      },
      createInstances(maybeCharacteristics, selected, codePrefix, barCodeLength, price, buyingPrice) {
        if(!maybeCharacteristics.isPresent() || !selected.length) return [];
        let characteristics = maybeCharacteristics.get(),
            instances = characteristics
                  .slice()
                  .sort((a, b) => a.displayOrder - b.displayOrder)
                  .map((c) => c.instances)
                  .map((instances) => instances.filter((instance) => selected.some((id) => instance.id == id)))
                  .filter((instances) => instances.length),
            combinations = Arrays.combineMultiple(instances),
            prodInstances = combinations.map((cis, index) => {
              return this.createInstance(cis, index + 1, codePrefix, barCodeLength, price, buyingPrice);
            });
        return prodInstances;
      },
      createInstance(characteristicInstances, codeNumber, codePrefix, barCodeLength, price, buyingPrice) {
        let codeSuffix = Util.padWith(codeNumber, "0", 3),
            instance = {
          id: null
        , characteristicInstances: characteristicInstances
        , codeSuffix: codeSuffix
        , barCode: Util.padWith(codePrefix + codeSuffix, "0", barCodeLength) 
        , price: price
        , buyingPrice: buyingPrice
        , proportional: false
        , obs: ""
        , disabled: false
        , initialStock: 0
        };
        this.updateProfitPercentage(instance);
        return instance;
      }
    }; 
  });
});
