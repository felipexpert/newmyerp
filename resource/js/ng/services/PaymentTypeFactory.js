"use strict";

require(['util', 'basic', 'ng/app'], function(Util, Basic, app) {
  app.factory('PaymentType', function PaymentTypeFactory(Request) {
    return {
      get: function() {
        return Request.get(baseUrl + "paymentType", null, true);
      }
    };
  }); 
});
