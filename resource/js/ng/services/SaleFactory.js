"use strict";

require(['util', 'basic', 'ng/app'], function(Util, Basic, app) {
  app.factory('Sale', function SaleFactory(Request) {
    return {
      create: function(orderNumber, identifier) {
        return Request.post(baseUrl + "sale/create/", {orderNumber: orderNumber, identifier: identifier});
      },
      updateIdentifier: function(saleId, identifier) {
        return Request.post(baseUrl + "sale/" + saleId + "/updateIdentifier", {identifier: identifier});
      },
      get: function(saleId, extra) {
        return Request.getExtra(baseUrl + "sale/" + saleId, extra, null, true); 
      }, 
      update: function(saleId, contents) {
        return Request.put(baseUrl + "sale/" + saleId, contents, null, true);
      }, 
      updateOrderItemAmount: function(id, orderItemId, amount) {
        return Request.post(baseUrl + "sale/" + id + "/updateOrderItemAmount", 
          {orderItemId: orderItemId, amount: amount});
      },
      addProd: function(saleId, code, amount) {
        return Request.post(baseUrl + "sale/" + saleId + "/addProd", {code: code, amount: amount}, null, true);
      },
      initPayers: function(saleId) {
        return Request.post(baseUrl + "sale/" + saleId + "/initPayers", null, null, true); 
      },
      addPayer: function(saleId) {
        return Request.post(baseUrl + "sale/" + saleId + "/addPayer"); 
      },
      close: function(saleId) {
        return Request.post(baseUrl + "sale/" + saleId + "/close");
      },
      cancel: function(saleId) {
        return Request.post(baseUrl + "sale/" + saleId + "/cancel", null, null, true);
      }
    };
  }); 
});
