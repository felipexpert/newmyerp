"use strict";

require(["ng/app"], function(app) {
  app.factory("ProductCategory", function ProductCategoryFactory(Request) {
    return {
      get: (id) => Request.get(baseUrl + "productCategory/" + id, null, true),
      getByFilter: (items, filter) => Request.getFilter(baseUrl + "productCategory", items, filter, null, true)
    }; 
  });
});
