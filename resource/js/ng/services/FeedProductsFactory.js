"use strict";

require(["ng/app", "basic", "language"], (app, Basic, lang) => {
  app.factory("FeedProducts", function FeedProductsFactory(ProductInstance) {
    return (items, filter) => ProductInstance.get(items, filter).then((resp) => {
      let headings = [ {width: 15, name: lang.get("code")}
                     , {width: 10, name: "ID"}
                     , {width: 60, name: lang.get("product.name")}
                     , {width: 15, name: lang.get("product.price")}],
          rows = resp.obj.filter((p) => !p.disabled).map((p) => [p.code, p.id, p.name, Basic.numberToM(p.price)]);
      return {headings: headings, rows: rows};
    });
  });
});
