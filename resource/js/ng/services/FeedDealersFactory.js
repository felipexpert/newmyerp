"use strict";

require(["ng/app", "language"], function(app, lang) {
  app.factory("FeedDealers", function FeedDealersFactory(Dealer) {
    return function(items, filter) {
      return new Promise(function(succeed, failed) {
        Dealer.get(items, filter).then(function(resp) {
          var headings = [ {name: 'id'}
                         , {name: lang.get('person.name')}
                         , {name: lang.get('person.fone')}
                         , {name: lang.get('person.address')}
                         , {name: lang.get('person.cpf')}
                         , {name: lang.get('person.rg')}
                         ],
              rows = resp.obj.map(function(d) {
                var a = d.customer.personAttr;
                return [d.id, a.name, a.fone, a.address, a.cpf, a.rg];
              });
          succeed({headings: headings, rows: rows});
        }, failed);
      });
    };
  });
});
