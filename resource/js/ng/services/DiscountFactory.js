"use strict";

require(['util', 'basic', 'ng/app'], function(Util, Basic, app) {
  app.factory('Discount', ['Ajax', function DiscountFactory(Ajax) {
    return {
      get: function(scope, id, success) {
        Ajax.get(scope, baseUrl + 'billing/discountAjax/' + id, null, success, null, true);
      },
      update: function(scope, id, discount, success) {
        Ajax.post(scope, baseUrl + 'billing/discountAjax/' + id, {discount: discount}, null, success, null, true);
      }
    };
  }]); 
});
