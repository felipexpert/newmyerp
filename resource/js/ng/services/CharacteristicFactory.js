"use strict";

require(["ng/app"], function(app) {
  app.factory("Characteristic", function CharacteristicFactory(Request) {
    return {
      getAll: () => Request.get(baseUrl + "characteristic", null, false),
      create: (name) => Request.post(baseUrl + "characteristic/create", {name: name}),
      createInstance: (characteristicId, name) => 
                        Request.post(baseUrl + "characteristic/" + characteristicId + "/createInstance", {name: name}),
      update: (charac) => Request.put(baseUrl + "characteristic", charac),
      up: (id) => Request.post(baseUrl + "characteristic/" + id + "/up"),
      down: (id) => Request.post(baseUrl + "characteristic/" + id + "/down"),
      delete: (id) => Request.delete(baseUrl + "characteristic/" + id)
    }; 
  });
});
