"use strict";

define([ "util"
       , "basic"
       , "formatter"
       , "language"
       , "angular"],
    function (Util, Basic, Formatter, lang, angular) {
  var app = angular.module("webapp", []);
  app.filter("sMoney", function() { return Basic.numberToM; });
  app.filter("lMoney", function() { return Basic.numberToMoney; });
  app.filter("number", function() { 
    return Formatter.numStr;
  });
  app.filter("date2", function() {
    return lang.get.bind(lang, "date");
  });
  app.filter("dateS", function() {
    return lang.get.bind(lang, "date");
  });
  app.filter("dateF", function() {
    return lang.get.bind(lang, "dateFull");
  });
  app.filter("t", function() {
    return lang.get.bind(lang);
  });
  app.filter("abs", function() {
    return Math.abs;
  });
  return app;
});
