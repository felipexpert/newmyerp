"use strict";

define(['util'], function(Util) {
 function TableSelector(table, eventTarget) {
   this.table = table;
   this.eventTarget = eventTarget || document.body;
   this.trContainer = this.trs = this.currTr = this.prevTr = null;
   this.enabled = false;
 }
 /**
  * Custom selected rows
  */
 TableSelector.BACKGROUND_COLOR = 'yellow';
 TableSelector.BORDER = '1px solid #cb3';

 TableSelector.prototype.selectTr = function(tr) {
   this.prevTr = this.currTr;
   this.currTr && apply(this.currTr, '', '');
   apply(this.currTr = tr, TableSelector.BACKGROUND_COLOR, TableSelector.BORDER); // change here
   function apply(tr, backgroundColor, border) {
     Util.toRealArray(tr.cells).forEach(function(cell) {
       cell.style.backgroundColor = backgroundColor;
       cell.style.border = border;
     });
   }
 };
 TableSelector.prototype.enable = function() {
   this.click = function(event) {
     var td = event.target;
     if(td.nodeName !== 'TD') return;
     var selectedTr = this.trs.reduce(function(acc, tr) {
       return acc || (tr.contains(td) ? tr : null);
     }.bind(this), null);
     this.selectTr(selectedTr);
   }.bind(this);
   this.keydown = function(event) {
     if(event.keyCode == 40 || event.keyCode == 38) {
       event.preventDefault();
       if(this.currTr) {
        var index = this.trs.indexOf(this.currTr),
            next = null;
        event.keyCode == 40
          ? next = this.trs[(index + 1) % this.trs.length] 
          : next = this.trs[(this.trs.length + index - 1) % this.trs.length];
        next && this.selectTr(next);
        this._adjustViewport();
       } else {
         this.trs && this.trs.length && this.selectTr(this.trs[0]);
       }
     }
   }.bind(this);
   this.eventTarget.addEventListener('keydown', this.keydown);
   this.table.addEventListener('click', this.click);
   this.update();
   this.enabled = true;
 };
 TableSelector.prototype.update = function() {
   this.trContainer = Util.queryOver(this.table, 'tbody');
   this.trContainer || (this.trContainer = this.table);
   this.trs = Util.queryOver(this.trContainer, 'tr', true);
   if(!(this.currTr && this.trs.indexOf(this.currTr) != -1)) {
     if(this.prevTr && this.trs.indexOf(this.prevTr) != -1) this.selectTr(this.prevTr);
     else this.trs.length && this.selectTr(this.trs[0]);
   }
   this.currTr && this._adjustViewport();
 };
 TableSelector.prototype._adjustViewport = function() {
  if(this.trContainer.scrollTop + this.trContainer.clientHeight < this.currTr.offsetTop) 
    this.trContainer.scrollTop = this.currTr.offsetTop - this.trContainer.clientHeight;
  else if(this.trContainer.scrollTop > this.currTr.offsetTop - this.currTr.clientHeight) 
    this.trContainer.scrollTop = this.currTr.offsetTop - this.currTr.clientHeight;
 };
 TableSelector.prototype.disable = function() {
   this.keydown && this.eventTarget.removeEventListener('keydown', this.keydown);
   this.click && this.table.removeEventListener('click', this.click);
   this.trContainer = this.trs = this.currTr = this.prevTr = this.keydown = this.click = null;
   this.enabled = false;
 };
 TableSelector.prototype.getSelectedRow = function() {
   return Util.toRealArray(this.currTr.cells).map(function(cell) {
     return cell.textContent;
   });
 };
 TableSelector.prototype.hasSelectedRow = function() {
   return this.currTr ? true : false;
 };
 return TableSelector;
});
