"use strict";
define(["arrays"], (Arrays) => {
  Loadang.ResourceType = Object.create(null);
  Loadang.ResourceType.SERVICE = "services";
  Loadang.ResourceType.FILTER = "filters";
  Loadang.ResourceType.DIRECTIVE = "directives";
  Loadang.ResourceType.CONTROLLER = "controllers";

  function Loadang(servicePath, filterPath, directivePath, controllerPath) {
    this.servicePath = servicePath;
    this.filterPath = filterPath;
    this.directivePath = directivePath;
    this.controllerPath = controllerPath;
    this.services = [];
    this.filters = [];
    this.directives = [];
    this.controllers = [];
  } 
  Loadang.prototype.addServices = function() { 
    return this.addResources(Loadang.ResourceType.SERVICE, Arrays.array(arguments));
  };
  Loadang.prototype.addFilters = function() { 
    return this.addResources(Loadang.ResourceType.FILTERS, Arrays.array(arguments));
  };
  Loadang.prototype.addDirectives = function() { 
    return this.addResources(Loadang.ResourceType.DIRECTIVE, Arrays.array(arguments));
  };
  Loadang.prototype.addControllers = function() { 
    return this.addResources(Loadang.ResourceType.CONTROLLER, Arrays.array(arguments));
  };
  Loadang.prototype.addResources = function(resourceType, resources) {
    this[resourceType] = this[resourceType].concat(resources);
    return this;
  };
  Loadang.prototype.load = function() {
    let services = this.services.map((s) => this.servicePath + "/" + s),
        filters = this.filters.map((f) => this.filterPath + "/" + f),
        directives = this.directives.map((d) => this.directivePath + "/" + d),
        controllers = this.controllers.map((c) => this.controllerPath + "/" + c);
    return services.concat(directives).concat(controllers);
  };
  return Loadang;
});
