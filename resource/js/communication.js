"use strict";
require(['util', 'basic', 'app'], function(Util, Basic, app) {
  app.factory('Comm', ['$scope', function($scope) {
    var com = function(url, content, name, success, fail, noLog) {
      Basic.json(url, content || {}, name, function(result) {
        success && $scope.$apply(function() {
          success(result);
        });
      }, function(error) {
        fail && $scope.$apply(function() {
          fail(error);
        });
      }, noLog);
    };
    return {
      test: function(success) {
        com(baseUrl + 'billing/testAjax', null, null, success);
      }
    };
    //return { func: function() { return 'lll' }};
    /*var com = function(url, content, name, success, fail, noLog) {
      Basic.json(url, content || {}, name, function(result) {
        success && $scope.$apply(function() {
          success(result);
        });
      }, function(error) {
        fail && $scope.$apply(function() {
          fail(error);
        });
      }, noLog);
    };
    return {
      test: com.bind(null, baseUrl + 'billing/testAjax', null, null);
    };*/
  }]);
});
