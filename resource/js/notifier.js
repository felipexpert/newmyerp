"use strict";
require(['util', 'app'], function(Util, app) {
  app.service('notifier', function() {
    this.receiptObservers = [];
    this.billingObservers = [];
  });
});
