"use strict";

define(['util'], function(Util) {
  function Language(words) {
    this.words = words;  
  }
  Language.prototype.get = function(word) {
    var element = this.words[word];
    if(typeof(element) === "function") {
      Array.prototype.shift.call(arguments);
      element = element.apply(this, arguments);    
    }
    return element;
  }
  var lang = new Language(words); 
  return lang;
});
