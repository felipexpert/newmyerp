"use strict";

define(["util"], function(Util) {
  var Formatter = Object.create(null);
  
  // These must be changed, to accomplish with your locale
  Formatter.decimals = 2; 
  Formatter.fraction = ".";
  Formatter.separator = ",";
  
  Formatter.strNum = function(string) {
    return Number(Formatter.formatCleaned(Formatter.clean(string)));
  };

  Formatter.numStr = function(num, decimals) {
    var text = String(num);
    if(decimals !== undefined) {
      var currDecimals = 0,
          dotIndex = text.indexOf(".");
      if(dotIndex != -1) {
        currDecimals = text.length - dotIndex - 1;
        text += Util.produce(decimals - currDecimals, function() { return "0"; }).join("");
      }
    }
    return text.replace(/\./g, Formatter.fraction);
  };

  Formatter.clean = function(string) {
    var value = "",
        regex = new RegExp("[0-9" + Formatter.separator + Formatter.fraction + "]");
    for(var i = 0; i < string.length; i++)
      if(regex.test(string[i]) && 
      !(string[i] == Formatter.fraction && string.lastIndexOf(Formatter.fraction) > i))
        value += string[i];
    return value;
  };

  Formatter.formatCleaned = function(string) {
    return string.replace(new RegExp("\\" + Formatter.separator, "g"), "")
        .replace(new RegExp("\\" + Formatter.fraction, "g"), ".");
  };

  Formatter.enforceDecimals = function(textNumber, decimals) {
    decimals = decimals || Formatter.decimals;
    let index = textNumber.indexOf(Formatter.fraction);
    if(index == -1) {
      textNumber += Formatter.fraction;
      index = textNumber.length - 1;
    }
    textNumber += Util.produce(decimals - (textNumber.length - index - 1), () => "0").join("");
    return textNumber;
  };

  return Formatter;
});
