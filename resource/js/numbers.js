"use strict";

define(() => {
  let Numbers = Object.create(null);
  Numbers.floorDecimals = (n, decimals) => {
    let d = Math.pow(10, decimals);
    return Math.floor(n * d) / d; 
  };
  Numbers.ceilDecimals = (n, decimals) => {
    let d = Math.pow(10, decimals);
    return Math.ceil(n * d) / d; 
  };
  return Numbers;
});
