"use strict";

define(["optional"], (Optional) => {
  let Arrays = Object.create(null);
  Arrays.combineMultiple = (arrays) => { // =] XD
    return combineMultiple(arrays.map((a) => a.map((i) => [i])));
    function combineMultiple(arrays) {
      if(arrays.length == 1) return Arrays.flat(arrays);
      let combination = Arrays.combine((xs,ys) => xs.concat(ys), arrays[0], arrays[1]);
      return combineMultiple(Arrays.prepend(combination, arrays.slice(2)));
    };
  };
  Arrays.combine = (f, xs, ys) => {
    let mapped = xs.map((x) => ys.map((y) => f(x,y)));
    return Arrays.flat(mapped);
  };
  Arrays.nub = (xs) => xs.reduce((acc, x) => acc.indexOf(x) == -1 ? Arrays.append(x, acc) : acc, []);
  Arrays.flat = (xxs) => Array.prototype.concat.apply([], xxs);
  Arrays.find = (predicate,  xs) => Optional.fromNullable(xs.find(predicate));
  Arrays.findIndex = (predicate, xs) => {
    let i = xs.findIndex(predicate);
    return i != -1 ? Optional.of(i) : Optional.absent();
  };
  Arrays.append = (e, xs) => {
    let xs2 = xs.slice();
    xs2.push(e);
    return xs2;
  };
  Arrays.prepend = (e, xs) => {
    let xs2 = xs.slice();
    xs2.unshift(e);
    return xs2;
  };
  Arrays.array = (array) => Array.prototype.slice.call(array, 0);
  return Arrays;
});
