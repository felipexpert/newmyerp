<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>MODAL</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- essencial -->
    <link href="modal.css" rel="stylesheet">
    <!-- essencial -->
  </head>

  <body>

    <a href="#" data-modal-target="teste">ABRIR NO MODAL</a>
    <button type="button" data-modal-target="testex">TAMBEM NO MODAL</button>
    <img src="#" data-modal-target="teste" title="eu tambem abro no modal" />

    <div>
      <? for($i=0; $i < 50; $i++) { ?>
      <p>conteudo</p>
      <? } ?>
    </div>

    <div id="teste" class="modal-target">
      <a href="#" data-modal-target="teste2">ABRIR NO MODAL</a>
      <? for($i=0; $i < 330; $i++) { ?>
      <p>sou um modal</p>
      <? } ?>
    </div>

    <div id="teste2" class="modal-target" data-width="200">
      olá
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <!-- essencial -->
    <script src="modal.js"></script>
    <!-- essencial -->
  </body>
</html>