<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Dealer extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('dealer/dealerGetterModel', 'dealerGetter');
    $this->load->model('dealer/dealerModel', 'dealer');
  }
  function index($id = null) {
    M::rest(function() use($id) {
      if(isSet($id)) return [200, $this->dealer->prepareById($id)->map()];
      $items = $this->input->get('items') ?: PHP_INT_MAX;
      $filter = $this->input->get('filter') ?: '';
      $this->dealerGetter->prepareByFilter($items, $filter);
      return [200, $this->dealerGetter->all()->map(function($d) {
        return $this->dealer->setEntity($d)->map();
      })->toArray()];
    });
  }
}
