<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\Common\Collections\ArrayCollection;
class Test extends CI_Controller {
  function t() {
    var_dump(ServerResult::elt()[ServerResult::FAIL]->getName());
  }
  function index(){
    $schemaTool = new \Doctrine\ORM\Tools\SchemaTool($this->em);
    $classes = $this->em->getMetadataFactory()->getAllMetadata();
    $schemaTool->createSchema($classes);
    $stockCreator = M::l('stock/stockCreator');
    $stockCreator->setName('A')->create();
    echo 'Tables have been created and initial state has been set!';
  }

  function cmd() {
    $array = null;
    exec('ls', $array);
    $output = '';
    forEach($array as $line) $output .= $line . "\n";
    echo '<pre>';
    echo $output;
    echo '</pre>';
  }

  function updateSchema() {
    $schemaTool = new \Doctrine\ORM\Tools\SchemaTool($this->em);
    $classes = $this->em->getMetadataFactory()->getAllMetadata();
    $schemaTool->updateSchema($classes, true);
    echo 'Tables have been updated!';
  }

  function doLog() {
    $test = new Entity\Test();
    $test->setName('It\'s working!');
    $this->em->persist($test);
    $this->em->flush();
    $tests = $this->em->createQuery('select t from Entity\Test t')->getResult();
    forEach($tests as $t)
      echo $t->getName()."<br>";
  }
  
  function jsObjects() {
    $obj = new JSObject([
      'firstname' => 'Felipe',
      'say' => function($self, $word) {
        return $word;
      }
    ]); 
    $obj->lastname = 'Miquilini';
    $obj->sayName = function($self) {
      return $self->firstname . ' ' . $self->lastname;
    };
    $this->unit->run($obj->say('haha'), 'haha');
    $this->unit->run($obj->sayName(), 'Felipe Miquilini');
    echo $this->unit->report();
  }

  function redistributors() {
    $this->load->model('calc/simpleItemModel');
    $this->load->model('calc/newTotalRedistributorModel', 'newTotal');
    $this->load->model('calc/targetedRedistributorModel', 'targeted');
    $creator1 = function() {
      return C::forceCollection(C::produce(3, function() {
        return new SimpleItemModel(4, 0);
      }));
    };
    $creator2 = function() {
      return C::forceCollection(C::produce(4, function() {
        return new SimpleItemModel(3, 0);
      }));
    };
    $items = $creator1();
    $this->newTotal->setItems($items);
    $this->unit->run($this->newTotal->canPerform(9), TRUE);
    $this->newTotal->perform(9);
    C::fe(function($i) {
      $this->unit->run($i->getAmount(), 3);
    }, $items);

    $items = $creator1();
    $this->newTotal->setItems($items);
    $items->get(0)->unchangeable = 4;
    $this->unit->run($this->newTotal->canPerform(4), TRUE, 'can perform 4');
    $this->unit->run($this->newTotal->canPerform(3.5), FALSE, 'can perform 3.5');
    $this->newTotal->perform(9);
    $this->unit->run($items->get(0)->getAmount(), 4);
    $this->unit->run($items->get(1)->getAmount(), 2.5);
    $this->unit->run($items->get(2)->getAmount(), 2.5);
    
    $items = $creator1();
    $this->newTotal->setItems($items);
    $items->get(0)->unchangeable = 3.01;
    $this->unit->run($this->newTotal->canPerform(7), TRUE);
    $this->newTotal->perform(7);
    $this->unit->run($items->get(0)->getAmount(), 3.01);
    $this->unit->run($items->get(1)->getAmount(), 1.99);
    $this->unit->run($items->get(2)->getAmount(), 2);
    
    $items = $creator2();
    $this->targeted->setItems($items);
    $items->get(2)->unchangeable = 3;
    $this->targeted->setTarget($items->get(0));
    $this->unit->run($this->targeted->canPerform(4), TRUE);
    $this->targeted->perform(4);
    $this->targeted->setTarget($items->get(1));
    $this->unit->run($this->targeted->canPerform(4), TRUE);
    $this->targeted->perform(4);
    $this->unit->run($items->get(0)->getAmount(), 4);
    $this->unit->run($items->get(1)->getAmount(), 4);
    $this->unit->run($items->get(2)->getAmount(), 3);
    $this->unit->run($items->get(3)->getAmount(), 1);

    $items = new ArrayCollection();
    $items->add(new SimpleItemModel(0, 0));
    $items->add(new SimpleItemModel(0, 0));
    $items->add(new SimpleItemModel(0, 0));
    $items->add(new SimpleItemModel(0, 0));
    $this->newTotal->setItems($items);
    $this->newTotal->perform(12);
    C::fe(function($i) {
      $this->unit->run($i->getAmount(), 3);
    }, $items);

    $items = $creator1();
    $this->targeted->setItems($items);
    $this->targeted->setTarget($items->get(1));
    $this->targeted->perform(5);
    $this->unit->run($items->get(0)->getAmount(), 4);
    $this->unit->run($items->get(1)->getAmount(), 5);
    $this->unit->run($items->get(2)->getAmount(), 3);

    $items = $creator1();
    $this->targeted->setItems($items);
    $items->get(1)->amount = 2;
    $items->get(1)->manipulable = FALSE;
    $this->targeted->setTarget($items->get(0));
    $this->targeted->perform(5);
    $this->unit->run($items->get(0)->getAmount(), 5);
    $this->unit->run($items->get(1)->getAmount(), 2);
    $this->unit->run($items->get(2)->getAmount(), 3);

    echo $this->unit->report();
  } 
  function predefined() {
    $this->load->model('predefinedParcelling/predefinedParcellingModel', 'predefined');
    $pdPar = new Entity\PredefinedParcelling();
    $intervals = C::forceCollection(C::produce(4, function($i) { 
      return (new Entity\PredefinedInterval())->setAmount(3)->setIntervalType($i); 
    }, TRUE));
    $pdPar->setPredefinedIntervals($intervals);
    $this->predefined->setEntity($pdPar);
    $dates = $this->predefined->generateDates(new DateTime('2000-01-01'));
    $ft = function ($date) { return $date->format('Y-m-d'); };
    $this->unit->run($ft($dates->get(0)), '2000-01-04');
    $this->unit->run($ft($dates->get(1)), '2000-01-22');
    $this->unit->run($ft($dates->get(2)), '2000-04-01');
    $this->unit->run($ft($dates->get(3)), '2003-01-01');

    echo $this->unit->report();
  }
  function date() {
    $date = new DateTime('now');
    echo D::millis($date);
    echo '<br>' . $date->format('d-m-Y');
  }

  function report() {
    /*print_r($this->jasper->client->serverInfo());*/
    header('Content-Type: application/pdf');
    $report = $this->jasper->rs->runReport('/reports/consignment/consignment', 'pdf', null, null, 
      ['name' => 'Felipe', 'conId' => 54]);
    echo $report;
  }
}
