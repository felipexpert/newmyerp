<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Billing extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('sale/saleModel', 'sale');
    $this->load->model('order/orderModel', 'order');
    $this->load->model('saleAggregate/saleAggregateGetterModel', 'saleAggregateGetter');
    $this->load->model('saleAggregate/saleAggregateModel', 'saleAggregate');
    $this->load->model('payer/payerGetterModel', 'payerGetter');
    $this->load->model('payer/payerModel', 'payer');
    $this->load->model('paymentType/paymentTypeModel', 'paymentType');
    $this->load->model('paymentType/paymentTypeGetterModel', 'paymentTypeGetter');
    $this->load->model('sale/payerHelperModel', 'payerHelper');
    $this->load->model('portionBucket/portionBucketGetterModel', 'portionBucketGetter');
    $this->load->model('portionBucket/portionBucketModel', 'portionBucket');
  }

  function receipt($saleId) {
    $this->payerHelper->setSaleModel($this->sale->prepareById($saleId));
    $this->payerHelper->initPayers();
    $this->viewsModel->simpleLoad(['billing/index'=> ['saleId' => $saleId]]);
  }

  function testPortionBucket($pbId) {
    $this->viewsModel->simpleLoad(['billing/testPortionBucket' => ['pbId' => $pbId]]);
  }

  function testSearchList() {
    $this->viewsModel->simpleLoad('billing/testSearchList');
  }

  function test() {
    $this->load->view('meta'); 
    $this->load->view('billing/test'); 
    $this->load->view('footer'); 
  }

  function getAggregatesAjax($saleId) {
    $this->sale->prepareById($saleId);
    $saleAggrs = $this->saleAggregateGetter->prepareBySaleId($saleId)->all();
    header('Content-Type: application/json;UTF-8');
    echo json_encode(C::foldl(function($acc, $sa) {
      $aggr = $sa->getAggregate();
      return C::push(
             [ 'id' => $sa->getId()
             , 'name' => $aggr->getName()
             , 'subtotal' => $this->sale->calcAggregateAmount($aggr)
             , 'amount' => $sa->getAmount()
             ], $acc);
    }, [], $saleAggrs));
  }

  function updateAggregatesAjax() {
    $input = json_decode(file_get_contents('php://input'));
    C::fe(function($sa) {
      $this->saleAggregate->prepareById($sa->id)->updateAmount($sa->amount);
    }, $input);
    M::result();
  }

  function discountAjax($saleId) {
    $input = json_decode(file_get_contents('php://input'));
    $this->sale->prepareById($saleId);
    if(isSet($input->discount)) $this->sale->changeDiscount($input->discount);
    header('Content-Type: application/json;UTF-8');
    echo json_encode([ 'discount' => $this->sale->entity()->getOrder()->getDiscount()
                     , 'canChange' => $this->sale->canChangeDiscount()]);
  }

  function getPaymentTypesAjax() {
    $paymentTypes = $this->paymentTypeGetter->prepareAll()->all(); 
    header('Content-Type: application/json;UTF-8');
    echo json_encode(array_values($paymentTypes->map(function($type) {
      return $this->paymentType->setEntity($type)->map();
    })->toArray()));
  }

  function getPayersAjax($saleId) {
    $this->sale->prepareById($saleId);
    $this->order->setEntity($this->sale->entity()->getOrder());
    $payers = $this->payerGetter->prepareBySaleId($saleId)->all();
    header('Content-Type: application/json;UTF-8');
    $payers = $payers->map(function($payer) {
      return $this->payer->setEntity($payer)->map();
    })->toArray();
    echo json_encode([
      'orderNumber' => $this->sale->entity()->getOrderNumber(),
      'totalItems' => $this->order->totalItems(),
      'aggregates' => $this->sale->aggregateTotal(),
      'discount' => $this->order->entity()->getDiscount(),
      'nonParceled' => $this->sale->nonParceledAmount(),
      'payers' => $payers,
      'closing' => D::millis($this->sale->entity()->getClosing()) 
    ]);
  }

  function addPayerAjax($saleId) {
    $this->payerHelperAction($saleId, function() {
      if($this->payerHelper->canAddPayer()) $this->payerHelper->addPayer();
    });
  }

  function removePayerAjax($payerId) {
    $this->payerAction($payerId, function() {
      $this->payer->maybeRemove();
    });
  }

  private function payerHelperAction($saleId, $payerAction) {
    $this->sale->prepareById($saleId);
    $this->payerHelper->setSaleModel($this->sale);
    $payerAction();
    $this->getPayersAjax($saleId);
  }

  private function payerAction($payerId, $payerAction) {
    $this->payer->prepareById($payerId);
    $saleId = $this->payer->entity()->getSale()->getId();
    $payerAction();
    $this->getPayersAjax($saleId);
  }

  function updatePayerAjax($payerId) {
    $input = json_decode(file_get_contents('php://input'));
    $this->payerAction($payerId, function() use($input) {
      $this->payer->update($input->description, $input->amount);
    });
  }

  function addAndPayPortionAjax($payerId) {
    $input = json_decode(file_get_contents('php://input'));
    header('Content-Type: application/json;UTF-8');
    $this->portionBucketGetter->prepareByPayerId($payerId);
    $this->portionBucket->prepareByEntityGetter($this->portionBucketGetter);
    if($this->portionBucket->canAddPortion()) {
      $this->portionBucket->addAndPayPortion($input);
      echo json_encode(TRUE);    
    } else {
      echo json_encode(FALSE);    
    }
  } 

  function closeAjax($saleId) {
    $this->sale->prepareById($saleId);
    if($this->sale->canClose()) $this->sale->close();
    $this->getPayersAjax($saleId);
  }

  function testAjax() {
    header('Content-Type: application/json;UTF-8');
    M::result();
  }
}
