<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mobile extends CI_Controller {
  function __construct() {
    parent::__construct();
  }
  function getWaiters() {
    $this->load->model('employee/employeeGetterModel');
    $this->employeeGetterModel->prepareAll();
    header('Content-Type: application/json;UTF-8');
    echo json_encode(C::foldl(function($acc, $e) {
      $acc['data'] = C::push(array('_id' => $e->getId(), 'name' => $e->getName()), $acc['data']);
      return $acc;
    }, array('success' => TRUE, 'data' => array()), $this->employeeGetterModel->all()));
  }
  
  function getOrders($orderId = null) {
    $this->load->model('sale/saleGetterModel');
    $this->load->model('mobile/sale/salesFormatterModel');
    if($orderId) $this->saleGetterModel->prepareByOrderId($orderId);
    else $this->saleGetterModel->prepareAll(); 
    header('Content-Type: application/json;UTF-8');
    echo json_encode(['success' => TRUE, 
      'data' => $this->salesFormatterModel->setEntities($this->saleGetterModel->all())->format()]);
  }
  function getOrdersByOrderNumber($orderNumber) {
    $this->load->model('sale/saleGetterModel');
    $this->load->model('mobile/sale/salesFormatterModel');
    $this->load->model('sale/tableUtilsModel');
    $sales = $orderNumber != -1 ? 
      $this->saleGetterModel->prepareByOrderNumber($orderNumber)->all() :
      C::forceArray($this->tableUtilsModel->getLooseSales());
    header('Content-Type: application/json;UTF-8');
    echo json_encode(['success' => TRUE, 
      'data' => $this->salesFormatterModel->setEntities($sales)->format()]);
  }
  private function mapProduct($p) {
    $this->load->model('mobile/product/productsFormatterModel');
    return $this->productsFormatterModel->setEntities([$p])->format()[0];
  }
  function getProds() {
    $this->load->model('product/productInstanceGetterModel');
    $this->productInstanceGetterModel->prepareAll();
    header('Content-Type: application/json;UTF-8');
    echo json_encode(C::foldl(function($acc, $p) {
      $acc['data'] = C::push($this->mapProduct($p), $acc['data']);
      return $acc;
    }, array('success' => TRUE, 'data' => array()), $this->productInstanceGetterModel->all()));
  }
  function syncOrder($orderId) {
    $raw = file_get_contents('php://input');
    $vo = json_decode($raw);
    header('Content-Type: application/json;UTF-8');
    $this->logger->setMessage("status", $raw)->create();
    $this->logger->setMessage("chegou -1")->create();
    // Merging process - START
    $this->logger->setMessage("vo - ", M::grabDump($vo))->create();
    $this->logger->setMessage("chegou 0.5")->create();
    $this->load->model('mobile/itemModel');
    $this->logger->setMessage("chegou 1")->create();
    $items = C::foldl(function($acc, $vi) {
      return C::push((new ItemModel())->prepare($vi), $acc);
    }, array(), $vo->items);
    $this->logger->setMessage("items - ", M::grabDump($items))->create();
    $this->logger->setMessage("chegou 2 - ", $vo->orderId)->create();
    $this->load->model('mobile/virtualOrderModel');
    $this->virtualOrderModel->setOrderNumber($vo->orderNumber)->setOrderId($orderId)
      ->setWaiterId($vo->waiterId)->setObs($vo->obs)->setItems($items);
    if(isSet($vo->identifier)) $this->virtualOrderModel->setIdentifier($vo->identifier);
    $this->logger->setMessage("chegou 3")->create();
    $this->virtualOrderModel->merge();
    $this->logger->setMessage("chegou 4")->create();
    // Merging process - END
    $this->getOrders($this->virtualOrderModel->getOrderId());
  }
  function orderHashcode($orderNumber) {
    $this->load->model('sale/saleHashcoderModel');
    $this->saleHashcoderModel->setOrderNumber($orderNumber);
    header('Content-Type: application/json;UTF-8');
    echo json_encode(array('success' => TRUE, 'hashcode' => $this->saleHashcoderModel->generateForMobile()));
  }
  function ongoingOrders() {
    $this->load->model('sale/saleGetterModel');
    $this->saleGetterModel->prepareOngoing();
    $data = $this->saleGetterModel->all()->map(function($sale) {
      return $sale->getOrderNumber();
    })->toArray();
    $data2 = C::foldl(function($acc, $n) {
      return in_array($n, $acc) ? $acc : C::push($n, $acc);
    }, [], $data);
    header('Content-Type: application/json;UTF-8');
    echo json_encode(array('_status' => 'success', 'data' => $data2));
  }
  function test() {
    header('Content-Type: application/json;UTF-8');
    echo json_encode(array('_status' => 'success'));
  }
  function printOrder($orderId) {
    $this->load->model('printer/printerHelperModel');
    $this->load->model('sale/saleGetterModel');
    $this->saleGetterModel->prepareByOrderId($orderId);
    $this->printerHelperModel->setSale($this->saleGetterModel->first())
      ->execute();
    header('Content-Type: application/json;UTF-8');
    echo json_encode(array('success' => true));
  }
  function tablesAmount() {
    header('Content-Type: application/json;UTF-8');
    echo json_encode(array('success' => true, 'tables' => 50));
  }
}
