<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class PaymentType extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('paymentType/paymentTypeModel', 'paymentType');
    $this->load->model('paymentType/paymentTypeGetterModel', 'paymentTypeGetter');
  }
  function index() {
    M::rest(function() {
      $paymentTypes = $this->paymentTypeGetter->prepareAll()->all(); 
      $obj = array_values($paymentTypes->map(function($type) {
        return $this->paymentType->setEntity($type)->map();
      })->toArray());
      return [200, $obj];
    });
  }
}
