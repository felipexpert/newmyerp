<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Doctrine\Common\Collections\ArrayCollection;
class Selling extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('sale/saleModel');
    $this->load->model('sale/saleGetterModel');
    $this->load->model('sale/saleCreatorModel');
    $this->load->model('sale/tableUtilsModel');
    $this->load->model('sale/saleIdentifierHelperModel', 'si');
    $this->load->model('sale/saleHashcoderModel');
    $this->load->model('order/orderModel');
    $this->load->model('order/orderGetterModel');
    $this->load->model('orderItem/orderItemGetterModel');
  }

  function tables() {
    $tables = $this->tableUtilsModel->getTables();
    $tables = C::produce(50, function($num) use($tables) {
      $count = $tables->filter(function($t) use($num) { 
        return $t->getOrderNumber() == $num;  
      })->count();
      return $this->load->view('fastSelling/tableButton', array(
        'href' => '#',
        'class' => $count == 0 ? 'empty' : $count == 1 ? 'singleton' : 'shared',   
        'num' => $num
      ), TRUE);
    }, TRUE);
    $data['tables'] = implode('', $tables);
    $singletons = $this->tableUtilsModel->getLooseSales();
    $singletons = $singletons->map(function($s) {
      return $this->load->view('fastSelling/tableButton', array(
        'href' => '#',
        'class' => 'singleton',
        'num' => $s->getIdentifier()
      ), TRUE);
    });
    $data['singletons'] = implode('', $singletons->toArray());
    $this->load->view('meta');
    $this->load->view('fastSelling/access', $data);
    $this->load->view('footer');
  }

  function tablesAjax() {
    $st = $this->tableUtilsModel->simplifyTables();
    $sls = $this->tableUtilsModel->simplifyLooseSales();
    header('Content-Type: application/json;UTF-8');
    echo json_encode(array('maxTables' => 50, 'tables' => $st, 'looseSales' => $sls));
  }

  function fsById($id) {
    $this->saleModel->prepareById($id);
    redirect('selling/fastSelling?saleId='.$id);
  }
                                                  
  function fsByNumber($orderNumber) {
    $this->saleGetterModel->prepareByOrderNumber($orderNumber);
    if(!$this->saleGetterModel->has())
      redirect('selling/fastSelling?orderNumber='.$orderNumber);
    elseif($this->saleGetterModel->all()->count() == 1)
      redirect('selling/fastSelling?orderNumber='.$orderNumber
          .'&saleId='.$this->saleGetterModel->first()->getId());
    // there are 2 or more
    $buttons = $this->saleGetterModel->all()->map(function($s) {
      return $this->load->view('fastSelling/tableButton', array(
        'href' => base_url().'selling/fastSelling?orderNumber='.$s->getOrderNumber()
            .'&saleId='.$s->getId(),
        'class' => 'singleton',
        'num' => $s->getIdentifier() ?: $s->getId()
      ), TRUE);
    });
    $buttons = implode('', $buttons->toArray());
    $this->load->view('meta');
    $this->load->view('fastSelling/accessTable', array('sales' => $buttons));
    $this->load->view('footer');
  }

  function fastSelling() {
    $data['saleId'] = $this->input->get('saleId') ? $this->input->get('saleId') : '';
    $data['orderNumber'] = $this->input->get('orderNumber') ? $this->input->get('orderNumber') : null;
    $this->load->view('meta'); 
    $template = array("table_open" => 
      '<table class="scroll table table-bordered table-striped table-hover" cellspacing="0">');
    $this->table->set_template($template);
    $this->table->set_heading(array('data' =>'Pos', 'data-width' => '2'), 
      array('data' => lang('code'), 'data-width' => '14'), 
      array('data' => lang('description'), 'data-width' => '57'),
      array('data' => lang('amount'), 'data-width' => '7'), 
      array('data' => lang('unitPrice'), 'data-width' => '12'), 
      array('data' => lang('subtotal'), 'data-width' => '8'));
    $data['canCancel'] = FALSE;
    if($data['saleId']) {
      $this->saleModel->prepareById($data['saleId']);
      $this->orderModel->setEntity($this->saleModel->entity()->getOrder());
      C::fe(function($orderItem) {
        $s = $orderItem->getProductSnapshot();
        $p = $s->getProductInstance();
        $this->table->add_row($orderItem->getDisplayOrder(), 
            $p->getCode(), 
            $s->getName(),  
            $this->saleModel->calcOrderItemAmount($orderItem),
            $s->getPrice(),
            ''); // Frontend concern
      
      }, $this->orderModel->getSortedItems());
      $data['identifier'] = $this->saleModel->entity()->getIdentifier();
      $data['canCancel'] = $this->saleModel->canCancel();
    }
    $data['table'] = $this->table->generate();
    $dsData['id'] = 'prodSearch';
    $dsData['title'] = lang('searchProds'); 
    $dsData['inputTarget'] = 'code';
    $dsData['action'] = base_url().'prod/filterProdsAjax';
    $options = array('name' => lang('name'));
    $dsData['filterSelect'] = form_dropdown('filter', $options, '', 'class="form-control"');
    $data['searchProds'] = $this->load->view('dynamicSearch', $dsData, TRUE);
    $data['detail'] = $this->load->view('fastSelling/detail', array(), TRUE);
    $this->load->view('fastSelling/index', $data);
    $this->load->view('footer'); 
  }

  function canUseIdentifierAjax() {
    $input = json_decode(file_get_contents('php://input'));
    $canUseIdentifier = $this->si->setIdentifier($input->identifier)->canUseIdentifier();
    header('Content-Type: application/json;UTF-8');
    echo json_encode(['_status' => 'success', 'canUseIdentifier' => $canUseIdentifier]);
  }

  function canUseIdentifiersAjax($saleId = null) {
    $input = json_decode(file_get_contents('php://input'));
    $si = new SaleIdentifierHelperModel();
    $canUseIdentifier1 = $si->setSaleId($saleId)->setIdentifier($input->identifier1)->canUseIdentifier();
    $si = new SaleIdentifierHelperModel();
    $canUseIdentifier2 = !N::equalsIgnoreCase(trim($input->identifier1), trim($input->identifier2)) 
        && $si->setIdentifier($input->identifier2)->canUseIdentifier();
    header('Content-Type: application/json;UTF-8');
    echo json_encode(['_status' => 'success', 'canUseIdentifier1' => $canUseIdentifier1,
        'canUseIdentifier2' => $canUseIdentifier2]);
  }

  function identifierAjax($saleId) {
    $input = json_decode(file_get_contents('php://input'));
    $this->si->setSaleId($saleId)->setIdentifier($input->identifier); 
    M::result_($this->si->canUseIdentifier(), function() {
      $this->si->useIdentifier();
    }, null, null, lang('fastSelling.alreadyUsedIdentifier'));
  }

  function createAjax() {
    $input = json_decode(file_get_contents('php://input'));
    $sale = $this->saleCreatorModel->setSellingType(Entity\SellingType::FAST_SELLING)
      ->setOrderNumber(M::force($input->orderNumber))
      ->create();
    if(isSet($input->identifier)) {
      $this->si->setSaleId($sale->getId())->setIdentifier($input->identifier); 
      if($this->si->canUseIdentifier()) $this->si->useIdentifier();
    }
    $data['id'] = $sale->getId();
    $data['canCancel'] = $this->saleModel->setEntity($sale)->canCancel();
    header('Content-Type: application/json;UTF-8');
    echo json_encode($data);
  }

  function addOrderItemAjax($saleId) {
    $input = json_decode(file_get_contents('php://input'));
    $this->saleModel->prepareById($saleId);
    $this->orderModel->setEntity($this->saleModel->entity()->getOrder());
    $orderItem = $this->orderModel->addOrderItem($input->code);
    $this->saleModel->updateOrderItemAmount($orderItem, $input->amount);
    $s = $orderItem->getProductSnapshot();
    $p = $s->getProductInstance();
    $data['displayOrder'] = $orderItem->getDisplayOrder();
    $data['code'] = $p->getCode();
    $data['amount'] = $this->saleModel->calcOrderItemAmount($orderItem);
    $data['price'] = $s->getPrice();
    $data['name'] = $s->getName();
    header('Content-Type: application/json;UTF-8');
    echo json_encode($data);
  }

  function rmProdFromOrderAjax() {
    $input = json_decode(file_get_contents('php://input'));
    $this->orderGetterModel->prepareBySaleId($input->saleId);
    $this->orderModel->prepareByEntityGetter($this->orderGetterModel);
    $this->orderModel->removeOrderItem($input->displayOrder);
    M::result();
  }

  function updateOrderItemAjax() {
    $input = json_decode(file_get_contents('php://input'));
    $this->saleModel->prepareById($input->saleId);
    $orderItem = $this->orderItemGetterModel->prepareByDisplayOrder($input->displayOrder, 
      $this->saleModel->entity()->getOrder()->getId())->first();
    $this->saleModel->updateOrderItemAmount($orderItem, $input->amount);
    M::result();
  }

  function fsHashcodeAjax($orderNumber) {
    $this->saleHashcoderModel->setOrderNumber($orderNumber);
    header('Content-Type: application/json;UTF-8');
    echo json_encode(array('_status' => 'success', 'hashcode' => $this->saleHashcoderModel->generateForFastSelling()));
  }

  private function progress() {
    $data = array('_status' => 'success');
    if($this->saleGetterModel->has()) {
      $sale = $this->saleGetterModel->first();
      $this->orderModel->setEntity($sale->getOrder());
      $data['saleId'] = $sale->getId();
      $items = C::foldl(function($acc, $oi) {
        $s = $oi->getProductSnapshot();
        $p = $s->getProduct();
        return C::push(array('displayOrder' => $oi->getDisplayOrder(),
            'code' => $p->getCode(),
            'amount' => $this->saleModel->calcOrderItemAmount($oi),
            'price' => $s->getPrice(),
            'description' => $s->getName()), $acc);
      }, array(), $this->orderModel->getSortedItems());
      $data['orderItems'] = $items;
      $data['obs'] = $sale->getOrder()->getObs();
    } 
    header('Content-Type: application/json;UTF-8');
    echo json_encode($data);
  }

  function progressByIdAjax($saleId) {
    $this->saleGetterModel->prepareById($saleId);
    $this->progress();
  }

  function progressByOrderNumberAjax($orderNumber) {
    $this->saleGetterModel->prepareByOrderNumber($orderNumber);
    $this->progress();
  }

  function cancelSale($saleId) {
    if($this->saleModel->prepareById($saleId)->canCancel()) $this->saleModel->cancel();
    redirect('selling/tables');    
  }
}
