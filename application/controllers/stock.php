<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Stock extends CI_Controller {
  function __construct() {
    parent::__construct();
    M::l('stock/stock');

  }
  function index($id = null, $other = null) {
    M::rest(function() {
      return [200, M::l('stock/stockGetter')->prepareAll()->all()->map(function($s) {
        return M::l('stock/stock')->setEntity($s)->map();
      })->toArray()]; 
    });
  }
  function summary() {
    
  }
}
