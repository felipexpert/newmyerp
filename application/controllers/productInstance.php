<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProductInstance extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('product/productInstanceGetterModel', 'productInstanceGetter');
    $this->load->model('product/productInstanceModel', 'product');
  }
  function info() {
    M::rest(function() {
      $pId = $this->productInstanceGetter->prepareLast()->has() ? $this->productInstanceGetter->first()->getId() : null;
      return [200, ['lastId' => $pId]];
    });
  }
  function index($id = null) {
    M::rest(function() use($id) {
      if(isSet($id)) 
        return [200, $this->product->prepareById($id)->map()];
      $codeOrBarCode = $this->input->get('codeOrBarCode');
      if($codeOrBarCode !== FALSE) {
        if($this->productInstanceGetter->prepareByCodeOrBarCode($codeOrBarCode)->has()) 
          return [200, $this->product->prepareByEntityGetter($this->productInstanceGetter)->map()];
        else
          return [200, null];
      } 
      $items = $this->input->get('items') ?: PHP_INT_MAX;
      $filter = $this->input->get('filter') ?: '';
      return [200, $this->productInstanceGetter->prepareByFilter($items, $filter)->all()->map(function($p) {
        return $this->product->setEntity($p)->map();
      })->toArray()];
    });
  }
}
