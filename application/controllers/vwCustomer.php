<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class VWCustomer extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('person/personAttrGetterModel', 'attrGetter');
    $this->load->model('person/personAttrModel', 'attr');
    $this->load->model('person/personAttrCreatorModel', 'attrCreator');
  }
  function elt($id = '') {
    $this->load->view('meta'); 
    $this->load->view('header'); 
    if($id) {
      $attr = $this->attrGetter->prepareByCustomerId($id)->first();
      $data = ['id' => $id, 'name' => $attr->getName(), 'address' => M::clsc($attr->getAddress()), 
        'fone' => M::clsc($attr->getFone()), 'cpf' => M::clsc($attr->getCpf()), 'rg' => M::clsc($attr->getRg())];
    }
    $data['action'] = base_url().'vwCustomer/eltAjax';
    $data['fields'] = $this->load->view('person/fields', $data, TRUE);
    $this->load->view('entityHeader', $data);
    $this->load->view('footer');
  }
  function eltAjax() {
    $this->form_validation->set_rules('id', 'ID', 'max_length[200]');
    $this->form_validation->set_rules('name', 'Nome', 'required');
    $this->form_validation->set_rules('address', 'Endereco', 'max_length[200]');
    $this->form_validation->set_rules('fone', 'Fone', 'max_length[200]');
    $this->form_validation->set_rules('cpf', 'CPF', 'max_length[200]');
    $this->form_validation->set_rules('rg', 'RG', 'max_length[200]');
    if($this->form_validation->run()) {
      if(set_value('id'))
        $this->attr->prepareByEntityGetter($this->attrGetter->prepareByCustomerId(set_value('id')))
          ->update(['name' => set_value('name'),
            'address' => set_value('address') ?: null,
            'fone' => set_value('fone') ?: null,
            'cpf' => set_value('cpf') ?: null,
            'rg' => set_value('rg') ?: null]);
      else $this->attrCreator->setName(set_value('name'))
        ->setAddress(set_value('address'))
        ->setFone(set_value('fone'))
        ->setCpf(set_value('cpf'))
        ->setRg(set_value('rg'))
        ->setCustomer(TRUE)->create();
      M::result();
    } else {
      M::result(validation_errors(), ServerResult::FAIL);
    }
  }
}
