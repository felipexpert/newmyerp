<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Category extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('productCategory/productCategoryModel');
    $this->load->model('productCategory/productCategoryCreatorModel');
  }
  function elt($id = '') {
    $this->load->view('meta'); 
    $this->load->view('header'); 
    $this->load->model('printer/printerGetterModel'); 
    $currPrinter = null;
    if($id) { 
      $cat = $this->productCategoryModel->prepareById($id)->entity();
      $data = ['id' => $id, 'name' => $cat->getName()];
      $currPrinter = $cat->getPrinter() ? $cat->getPrinter()->getId() : null;
    }
    $data['action'] = base_url()."category/eltAjax";
    $data['printers'] = form_dropdown('printerId', C::foldl(function($acc, $printer) {
      return C::push($printer->getId(), $printer->getName().' - '.$printer->getPath(), $acc);
    }, [], $this->printerGetterModel->prepareAll()->all()), $currPrinter, 'class="form-control"');
    $data['fields'] = $this->load->view('category/fields', $data, TRUE); 
    $this->load->view('entityHeader', $data);
    $this->load->view('footer');
  }
  function eltAjax() {
    $this->form_validation->set_rules('name', lang('name'), 'required');
    $this->form_validation->set_rules('printerId', lang('printer'), 'integer');
    if($this->form_validation->run()) {
      $id = $this->input->post('id');
      if($id) $this->productCategoryModel->prepareById($id)->update(set_value('name'), set_value('printerId'));
      else $this->productCategoryCreatorModel->setName(set_value('name'))->preparePrinterById(set_value('printerId'))->create();
      M::result();
    } else {
      M::result(validation_errors(), FALSE);
    }
  }
}
