<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class VWConsignment extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('sale/saleGetterModel', 'saleGetter');
    $this->load->model('sale/payerHelperModel', 'payerHelper');
    $this->load->model('consign/saleHelperModel', 'saleHelper');
  }
  function index($id = null) {
    $this->viewsModel->simpleLoad(['consign/index'=> ['id' => $id]]);
  }
  function access() {
    $this->viewsModel->simpleLoad('consign/access');
  }
  function makeSale($id) {
    $sale = $this->saleHelper->prepareByConsignmentId($id)->makeSale();
    redirect('vwSale/'.$sale->getId());
  }
  function report($id) {
    header('Content-Type: application/pdf');
    $report = $this->jasper->rs->runReport('/reports/consignment/consignment', 'pdf', null, null, 
      ['conId' => $id]);
    echo $report; 
  }
  function reportNP($id) {
    header('Content-Type: application/pdf');
    $report = $this->jasper->rs->runReport('/reports/consignment/np', 'pdf', null, null, 
      ['conId' => $id]);
    echo $report; 
  }
}
