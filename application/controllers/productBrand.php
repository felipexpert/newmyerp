<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Doctrine\Common\Collections\ArrayCollection;
class ProductBrand extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('productBrand/productBrandModel', 'productBrand');
    $this->load->model('productBrand/productBrandGetterModel', 'productBrandGetter');
  }
  function index($id = null) {
    M::rest(function() {
      return [200, $this->productBrandGetter->prepareAll()->all()->map(function($b) {
        return $this->productBrand->setEntity($b)->map();
      })->toArray()];
    });
  }
}
