<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class VWProduct extends CI_Controller {
  function __construct() {
    parent::__construct();
  }
  function index($id = null) {
    $this->viewsModel->simpleLoad(['product/index'=> ['id' => $id]]);
  }
}
