<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Payer extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('payer/payerModel', 'payer');
  }
  function index($payerId) {
    M::rest(function() use($payerId) {
      return [200, $this->payer->prepareById($payerId)->map()];
    }, function($req) use($payerId) {
      $input = $req->obj;
      $description = $amount = $customerId = Maybe::nothing();
      if(isSet($input->description)) $description = $input->description;
      if(isSet($input->portionBucket)) {
        $pb = $input->portionBucket;
        if(isSet($pb->amount)) $amount = $pb->amount;
      }
      if(isSet($input->customer)) {
        $c = $input->customer;
        if(isSet($c->id)) $customerId = $c->id;
      }
      $this->payer->prepareById($payerId);
      $this->payer->update($description, $amount, $customerId);
      return [204];
    }, function() use($payerId) {
      $this->payer->prepareById($payerId)->maybeRemove();
      return [204];
    });
  }
}
