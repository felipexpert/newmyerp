<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prod extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('product/productInstanceGetterModel');
    $this->load->model('product/productInstanceModel');
  }
  function getProductAjax() {
    $input = json_decode(file_get_contents('php://input'));
    $this->productInstanceGetterModel->prepareByCode($input->code);
    $data['_status'] = $data['price'] = $data['name'] = '';
    //$data['_message'] = 'Código invalido';
    if($this->productInstanceGetterModel->has()) {
      $prod = $this->productInstanceGetterModel->first();
      $this->productInstanceModel->setEntity($prod);
      $data['price'] = $prod->getPrice();
      $data['name'] = $this->productInstanceModel->name();
      $data['_status'] = 'success';
    }
    header('Content-Type: application/json;UTF-8');
    echo json_encode($data);
  }
  function filterProdsAjax() {
    $input = json_decode(file_get_contents('php://input'));
    $data['_status'] = 'success';
    $data['data'] = [];
    $data['data'][] = ['Código', 'Nome', 'Preço'];
    if(strLen($input->data)) {
      $prods = $this->productInstanceGetterModel->prepareByFilter(100, $input->data)->all()->map(function($p) {
        $this->productInstanceModel->setEntity($p);
        return [$p->getCode(), $this->productInstanceModel->name(), $p->getPrice().'<meta>currency'];
      })->toArray();
      $data['data'] = array_merge($data['data'], $prods);
    }
    header('Content-Type: application/json;UTF-8');
    echo json_encode($data);
  }
}
