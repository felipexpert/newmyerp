<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Customer extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('customer/customerGetterModel', 'customerGetter');
    $this->load->model('customer/customerModel', 'customer');
  }
  function index($id = null) {
    M::rest(function() use($id) {
      if(isSet($id)) return [200, $this->customer->prepareById($id)->map()];
      $items = $this->input->get('items') ?: PHP_INT_MAX;
      $filter = $this->input->get('filter') ?: '';
      $this->customerGetter->prepareByFilter($items, $filter);
      return [200, $this->customerGetter->all()->map(function($c) {
        return $this->customer->setEntity($c)->map();
      })->toArray()];
    });
  }
}
