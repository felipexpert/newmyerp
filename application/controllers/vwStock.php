<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class VWStock extends CI_Controller {
  function __construct() {
    parent::__construct();
  }
  function index() {
    $this->viewsModel->simpleLoad('stock/index');
  }
}
