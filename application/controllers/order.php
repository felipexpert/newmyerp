<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Order extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('order/orderModel', 'order');
    $this->load->model('sale/saleModel', 'sale');
    $this->load->model('sale/saleGetterModel', 'saleGetter');
    $this->load->model('sale/payerHelperModel', 'payerHelper');
  }
  function index($id = null, $other = null) {
    switch($other) {
    case 'deleteItem':
      M::rest(null, null, null, function($req) use($id) {
        $this->order->prepareById($id)->removeOrderItem($req->obj->displayOrder);
        return [204];
      });
      break;
    default:
      M::rest();
    }
  }
}
