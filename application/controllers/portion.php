<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Portion extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('portion/portionModel');
  }
  function index($portionId) {
    M::rest(null, function($req) use($portionId) {
      $o = $req->obj;
      $this->portionModel->prepareById($portionId);
      if(isSet($o->total))
        $this->portionModel->updateTotal($o->total);
      if(isSet($o->validUntil))
        $this->portionModel->updateValidUntil($o->validUntil);
      return [204];
    }, function() use($portionId) {
      $this->portionModel->prepareById($portionId)->maybeRemove();
      return [204];
    });
  }
}
