<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Doctrine\Common\Collections\ArrayCollection;
class Printer extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('printer/printerModel', 'printer');
    $this->load->model('printer/printerGetterModel', 'printerGetter');
  }

  function index($id = null) {
    M::rest(function() use($id) {
      if(isSet($id)) return [200, $this->printer->prepareById($id)->map()];
      return [200, $this->printerGetter->prepareAll()->all()->map(function($prod) {
        return $this->printer->setEntity($prod)->map();
      })->toArray()]; 
    });
  }

  function saleItems($id) {
    M::rest(null, null, null, function($req) use($id) {
      $this->load->model('printer/printerHelperModel');
      $this->printerHelperModel->setSaleId($id)
          ->setRedo($req->obj->redo)
          ->execute();
      return [204];
    });
  }
}
