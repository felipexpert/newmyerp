<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class PortionBucket extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('portionBucket/portionBucketModel');
  }
  function index($pbId) {
    M::rest(function() use ($pbId) {
      $extra = $this->input->get('extra') ?: '';
      return [200, $this->portionBucketModel->prepareById($pbId)->map($extra)];
    });
  }
  function insertPredefinedParcelling($pbId) {
    M::rest(null, null, null, function($req) use($pbId) {
      $input = $req->obj;
      $this->portionBucketModel->prepareById($pbId);
      if($this->portionBucketModel->canAddPortion()) 
        $this->portionBucketModel->predefinedParcelling($input->predefinedParcellingId);
      return [204];
    });
  }
  function createPortions($pbId) {
    M::rest(null, null, null, function($req) use($pbId) {
      $input = $req->obj;
      $this->portionBucketModel->prepareById($pbId);
      if($this->portionBucketModel->canAddPortion()) $this->portionBucketModel->createPortions($input->portions);
      return [204];
    });
  }
  function createAndPayPortion($pbId) {
    M::rest(null, null, null, function($req) use($pbId) {
      $input = $req->obj;
      $this->portionBucketModel->prepareById($pbId);
      if($this->portionBucketModel->canAddPortion()) $this->portionBucketModel->addAndPayPortion($input);
      return [200, [ "output" => $this->portionBucketModel->canAddPortion() ? "Can" : "cannot"
        , "paidAmount" => $this->portionBucketModel->paidAmount()
        , "amount" => $this->portionBucketModel->entity()->getAmount()]];
    });
  } 
}
