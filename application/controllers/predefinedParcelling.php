<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class PredefinedParcelling extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('predefinedParcelling/predefinedParcellingModel', 'pp');
    $this->load->model('predefinedParcelling/predefinedParcellingGetterModel', 'ppGetter');
  }
  function index() {
    M::rest(function() {
      return [200, $this->ppGetter->prepareAll()->all()->map(function($pp) {
        return $this->pp->setEntity($pp)->map();
      })->toArray()];
    });
  }
}
