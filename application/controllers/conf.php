<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Conf extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('conf/confModel', 'conf');
  }
  function index() {
    M::rest(function() {
      $this->conf->prepareById(1);
      return [200, $this->conf->map()];
    });
  }
}
