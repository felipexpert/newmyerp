<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Sale extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('sale/saleModel', 'sale');
    $this->load->model('order/orderModel', 'order');
    $this->load->model('product/productInstanceGetterModel', 'productInstanceGetter');
    $this->load->model('orderItem/orderItemModel', 'orderItem');
    $this->load->model('sale/saleGetterModel', 'saleGetter');
    $this->load->model('saleAggregate/saleAggregateModel', 'saleAggregate');
    $this->load->model('sale/payerHelperModel', 'payerHelper');
    $this->load->model('sale/saleCreatorModel', 'saleCreator');
    $this->load->model('sale/saleIdentifierHelperModel', 'saleIdentifierHelper');
    $this->load->model('payer/payerModel', 'payer');
  }
  function index($saleId = null, $other = null) {
    switch($other) {
    case null:
      M::rest(function() use($saleId) {
        $extra = $this->input->get('extra') ?: '';
        if(isSet($saleId)) return [200, $this->sale->prepareById($saleId)->map($extra)];
        return [200, $this->saleGetter->prepareAll()->all()->map(function($sale) use($extra) {
          return $this->sale->setEntity($sale)->map($extra);
        })->toArray()];
      }, function($req) use($saleId) {
        $this->sale->prepareById($saleId);
        $input = $req->obj;
        if(isSet($input->order)) {
          $o = $input->order;
          if(isSet($o->discount)) $this->sale->changeDiscount($o->discount);
          if(isSet($o->secondDiscount)) $this->sale->changeSecondDiscount($o->secondDiscount);
        }
        if(isSet($input->aggregates)) {
          $xs = $input->aggregates;
          C::fe(function($sa) {
            $this->saleAggregate->prepareById($sa->id)->updateAmount($sa->amount);
          }, $xs);
        }
        return [204];
      });
      break;
    case 'addProd':
      M::rest(null, null, null, function($req) use($saleId) {
        if($this->productInstanceGetter->prepareByCode($req->obj->code)->has()) {
          $order = $this->sale->prepareById($saleId)->entity()->getOrder();
          $orderItem = $this->order->setEntity($order)->addOrderItem($req->obj->code);
          $this->sale->updateOrderItemAmount($orderItem, $req->obj->amount);
          return [200, [ 'validCode' => TRUE
                       , 'orderItem' => $this->orderItem->setEntity($orderItem)->map()]];
        } else {
          return [200, ['validCode' => FALSE]];
        }
      }); 
      break;
    case 'updateOrderItemAmount':
      M::rest(null, null, null, function($req) use($saleId) {
        $this->sale->prepareById($saleId)
          ->updateOrderItemAmount($req->obj->orderItemId, $req->obj->amount);
        return [200, $this->orderItem->prepareById($req->obj->orderItemId)->map()];
      });
      break;
    case 'updateIdentifier':
      M::rest(null, null, null, function($req) use($saleId) {
        if($this->saleIdentifierHelper->setSaleId($saleId)->setIdentifier($req->obj->identifier)->canUseIdentifier()) {
          $this->saleIdentifierHelper->useIdentifier();
          return [200, ['success' => TRUE]];
        }
        return [200, ['success' => FALSE, 'message' => lang('fastSelling.cannotUseIdentifier')]];
      });
      break;
    case 'addPayer':
      M::rest(null, null, null, function() use($saleId) {
        $this->sale->prepareById($saleId);
        $this->payerHelper->setSaleModel($this->sale);
        if($this->payerHelper->canAddPayer()) $this->payerHelper->addPayer();
        return [204];
      });
      break;
    case 'close':
      M::rest(null, null, null, function() use($saleId) {
        $this->sale->prepareById($saleId);
        if($this->sale->canClose()) {
          $this->sale->close();
          return [200, ['hasClosed' => TRUE]];
        } else {
          return [200, ['hasClosed' => FALSE]];
        }
      });
      break;
    case 'initPayers':
      M::rest(null, null, null, function() use($saleId) {
        $this->sale->prepareById($saleId);
        $this->payerHelper->setSaleModel($this->sale)->initPayers();
        return [200, $this->sale->entity()->getPayers()->map(function($payer) {
          return $this->payer->setEntity($payer)->map();
        })->toArray()];
      });
      break;
    case 'cancel':
      M::rest(null, null, null, function() use($saleId) {
        $either = $this->sale->prepareById($saleId)->maybeCancel();
        $obj = $either->isRight() 
          ? ["success" => TRUE, "sale" => $this->sale->map()]
          : ["success" => FALSE, "message" => $either->left()]; 
        return [200, $obj];

      });
      break;
    default:
      M::rest(); 
    }
  }
  function create() {
    M::rest(null, null, null, function($req) {
      $either = $this->saleCreator->setOrderNumber($req->obj->orderNumber)
        ->setIdentifier($req->obj->identifier)->safeCreate();
      if($either->isRight()) 
        return [200, ['success' => TRUE, 'sale' => $this->sale->setEntity($either->right())->map('orderItems-payers')]];
      return [200, ['success' => FALSE, 'message' => $either->left()]];
    });
  }
}
