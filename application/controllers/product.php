<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('product/productModel', 'product');
    $this->load->model('product/productGetterModel', 'productGetter');
    $this->load->model('product/productCreatorModel', 'productCreator');
  }
  function info() {
    M::rest(function() {
      $pId = $this->productGetter->prepareLast()->has() ? $this->productGetter->first()->getId() : null;
      return [200, ['lastId' => $pId]];
    });
  }
  function index($id = null) {
    M::rest(function() use($id) {
      $extra = $this->input->get('extra') ?: '';
      if(isSet($id)) 
        return [200, $this->product->prepareById($id)->map($extra)];
      $items = $this->input->get('items') ?: PHP_INT_MAX;
      $filter = $this->input->get('filter') ?: '';
      return [200, $this->productGetter->prepareByFilter($items, $filter)->all()->map(function($p) use($extra) {
        return $this->product->setEntity($p)->map($extra);
      })->toArray()];
    }, function($req) {
      C::fe(function($i) { $i->characteristicIds = DU::indexes($i->characteristicInstances); }, $req->obj->instances);
      $req->obj->maybeCategoryId = M::maybeIndex($req->obj->category);
      $req->obj->maybeBrandId = M::maybeIndex($req->obj->brand);
      $req->obj->maybeTaxRateId = M::maybeIndex($req->obj->taxRate);
      $either = $this->product->prepareAndPutIn($req->obj);
      return $either->ifElse(function($product) {
        return [200, ['success' => TRUE, 'product' => $this->product->setEntity($product)->map()]];
      }, function($errorMessage) {
        return [200, ['success' => FALSE, 'message' => $errorMessage]];
      });
    }, null, function($req) {
      C::fe(function($i) { $i->characteristicIds = DU::indexes($i->characteristicInstances); }, $req->obj->instances);
      $either = $this->productCreator->setName($req->obj->name)
        ->setMaybeCategoryId(M::maybeIndex($req->obj->category))
        ->setMaybeBrandId(M::maybeIndex($req->obj->brand))
        ->setMaybeTaxRateId(M::maybeIndex($req->obj->taxRate))
        ->setInstances($req->obj->instances)
        ->setCodePrefix($req->obj->codePrefix)
        ->setDefaultPrice($req->obj->defaultPrice)
        ->setDefaultBuyingPrice($req->obj->defaultBuyingPrice)
        ->setObs($req->obj->obs)
        ->safeCreate();
      return $either->ifElse(function($product) {
        return [200, ['success' => TRUE, 'product' => $this->product->setEntity($product)->map()]];
      }, function($errorMessage) {
        return [200, ['success' => FALSE, 'message' => $errorMessage]];
      }); 
    });
  }
}
