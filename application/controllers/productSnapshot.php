<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProductSnapshot extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('productSnapshot/productSnapshotModel', 'productSnapshot');
  }
  function index() {
    M::rest(null, function($req) {
      $this->productSnapshot->prepareById($req->obj->id)
        ->update(['name' => $req->obj->name, 'price' => $req->obj->price, 'buyingPrice' => $req->obj->buyingPrice,
                  'discount' => $req->obj->discount]);
      return [204];
    });
  }
}
