<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class CharacteristicInstance extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('characteristic/characteristicInstanceModel', 'charInst');
  }
  function index($id = null, $other = null) {
    M::rest(null, null, function() use($id){
     $this->charInst->prepareById($id)->maybeRemove();
      return [204];
    });    
  }
}
