<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Characteristic extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('characteristic/characteristicGetterModel', 'characteristicGetter');
    $this->load->model('characteristic/characteristicModel', 'characteristic');
    $this->load->model('characteristic/characteristicInstanceModel', 'characteristicInstance');
    $this->load->model('characteristic/characteristicCreatorModel', 'characteristicCreator');
    $this->load->model('characteristic/characteristicInstanceCreatorModel', 'characteristicInstanceCreator');
    $this->load->model('characteristic/characteristicInstanceModel', 'characteristicInstance');
  }
  function index($id = null, $other = null) {
    switch($other) {
    case null:
      M::rest(function() use($id) {
        if(isSet($id)) return [200, $this->characteristic->prepareById($id)->map()];
        return [200, $this->characteristicGetter->prepareAll()->all()->map(function($c) {
          return $this->characteristic->setEntity($c)->map();
        })->toArray()];
      }, function($req) {
        $this->characteristic->prepareAndPutIn($req->obj); 
        return [204];
      }, function() use($id) {
        $this->characteristic->prepareById($id)->maybeRemove();
        return [204];
      });
      break;
    case "createInstance":
      M::rest(null, null, null, function($req) use($id) {
        $instance = $this->characteristicInstanceCreator
          ->setCharacteristic($this->characteristic->prepareById($id)->entity())
          ->setName($req->obj->name)->create();
        return [200, $this->characteristicInstance->setEntity($instance)->map()];
      });
      break;
    case "up":
      M::rest(null, null, null, function() use($id) {
        $this->characteristic->prepareById($id)->maybeUp();
        return [204];
      });
      break;
    case "down":
      M::rest(null, null, null, function() use($id) {
        $this->characteristic->prepareById($id)->maybeDown();
        return [204];
      });
      break;
    default:
      M::rest();
    }
  }
  function create() {
    M::rest(null, null, null, function($req) {
      $characteristic = $this->characteristicCreator->setName($req->obj->name)->create();
      return [200, $this->characteristic->setEntity($characteristic)->map()];
    });
  }
}
