<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class VWSale extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('sale/saleGetterModel', 'saleGetter');
  }
  function index($id = null) {
    $this->viewsModel->simpleLoad(['sale/index'=> ['id' => $id, 'orderNumber' => null]]);
  }
  function orderNumber($orderNumber) {
    $this->viewsModel->simpleLoad(['sale/index'=> ['id' => null, 'orderNumber' => $orderNumber, 
      'back' => base_url().'selling/tables']]);
  }
  function test() {
    $this->viewsModel->simpleLoad('test');
  }
}
