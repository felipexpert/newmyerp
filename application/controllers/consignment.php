<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Consignment extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('consign/consignmentModel', 'consign');
    $this->load->model('consign/consignmentGetterModel', 'consignGetter');
    $this->load->model('consign/consignmentCreatorModel', 'consignCreator');
    $this->load->model('order/orderModel', 'order');
    $this->load->model('product/productInstanceGetterModel', 'productInstanceGetter');
    $this->load->model('dealer/dealerCreatorModel', 'dealerCreator');
    $this->load->model('dealer/dealerModel', 'dealer');
  }
  function index($id = null, $other = null) {
    switch($other) {
    case null:
      M::rest(function() use($id) {
        $extra = $this->input->get('extra') ?: '';
        if(isSet($id)) return [200, $this->consign->prepareById($id)->map($extra)];
        $items = $this->input->get('items') ?: PHP_INT_MAX;
        $filter = $this->input->get('filter') ?: '';
        $optional = json_decode($this->input->get('optional'));
        return [200, $this->consignGetter->prepareByFilter($items, $filter, $optional)->all()->map(function($consignment) use($extra) {
          return $this->consign->setEntity($consignment)->map($extra);
        })->toArray()]; 
      });
      break;
    case 'addProd':
      M::rest(null, null, null, function($req) use($id) {
        if($this->productInstanceGetter->prepareByCode($req->obj->code)->has()) {
          $orderItem = $this->order->setEntity($this->consign->prepareById($id)->entity()->getOrder())
            ->addOrGetExistingOrderItem($req->obj->code);
          $flowType = N::ge($req->obj->amount, 0) ? Entity\FlowType::OUT_CONSIGNED : Entity\FlowType::IN_CONSIGNED; 
          $amount = abs($req->obj->amount);
          $this->consign->addOrderItemFlow($orderItem, $flowType, $amount);
          return [200, [ 'validCode' => TRUE
                       , 'orderItem' => $this->consign->mapOrderItem($orderItem)]];
        } else {
          return [200, ['validCode' => FALSE]];
        }
      });
      break;
    case 'updateOrderItemAmount':
      M::rest(null, null, null, function($req) use($id) {
        $this->consign->prepareById($id)
          ->updateOrderItemAmount($req->obj->orderItemId, $req->obj->flowType, $req->obj->amount);
        return [200, $this->consign->mapOrderItem($req->obj->orderItemId)];
      });
      break;
    case 'bindCustomer':
      M::rest(null, null, null, function($req) use($id) {
        $dealer = $this->consign->prepareById($id)->bindCustomer($req->obj->customerId);
        return [200, $this->dealer->setEntity($dealer)->map()];
      });
      break;
    case 'updatePrevisionClosing':
      M::rest(null, null, null, function($req) use($id) {
        $this->consign->prepareById($id)->update(['previsionClosing' => D::millisToDate($req->obj->previsionClosing)]);
        return [204]; 
      });
      break;
    case 'cancel':
      M::rest(null, null, null, function() use($id) {
        $either = $this->consign->prepareById($id)->maybeCancel();
        $obj = $either->isRight()
          ? ['success' => TRUE, 'obj' => $this->consign->map()]
          : ['success' => FALSE, 'message' => $either->left()];
        return [200, $obj];
      });
      break;
    default:
      M::rest();
    }
  }
  function create() {
    M::rest(null, null, null, function($req) {
      $c = $this->consignCreator->create();
      return [200, $this->consign->setEntity($c)->map('orderItems-totals')];
    });
  }
}
