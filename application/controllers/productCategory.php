<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Doctrine\Common\Collections\ArrayCollection;
class ProductCategory extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('productCategory/productCategoryModel', 'productCategory');
    $this->load->model('productCategory/productCategoryGetterModel', 'productCategoryGetter');
  }

  function index($id = null) {
    M::rest(function() use($id) {
      if(isSet($id)) return [200, $this->productCategory->prepareById($id)->map()];
      $items = $this->input->get('items') ?: PHP_INT_MAX;
      $filter = $this->input->get('filter') ?: '';
      return [200, $this->productCategoryGetter->prepareByFilter($items, $filter)->all()->map(function($cat) {
        return $this->productCategory->setEntity($cat)->map();
      })->toArray()]; 
    });
  }

}
