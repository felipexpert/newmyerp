<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Doctrine\Common\Collections\ArrayCollection;
class ProductTaxRate extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('productTaxRate/productTaxRateModel', 'productTaxRate');
    $this->load->model('productTaxRate/productTaxRateGetterModel', 'productTaxRateGetter');
  }
  function index($id = null) {
    M::rest(function() {
      return [200, $this->productTaxRateGetter->prepareAll()->all()->map(function($t) {
        return $this->productTaxRate->setEntity($t)->map();
      })->toArray()];
    });
  }
}
