<?php
$lang = array(

// All
'decimals'                                                      => 2, 
'fullDate'                                                      => 'd/m/Y H:i:s',
'date'                                                          => 'd/m/Y',                   
'amount'                                                        => 'Quantidade',
'back'                                                          => 'Voltar',
'category' 	                                        	=> 'Categoria',
'code' 		                                          	=> 'Código',
'currencySymbol'                                                => 'R$',
'description'                                                   => 'Descrição',
'headerMessage'                                                 => 'Ganhando tempo para você',
'id' 		                                        	=> 'ID',
'mindware'        	                                    	=> 'Mindware',
'name' 		                                        	=> 'Nome',
'printer' 	                                              	=> 'Impressora',
'notDefined' 	                                                => 'Não definido',
'position'                                                      => 'Posição',
'price' 		                                        => 'Preço',
'product' 	                                          	=> 'Produto',
'save' 		                                            	=> 'Salvar',
'searchProds'                                                   => 'Buscar produtos',
'total'                                                         => 'Total',
'subtotal'                                                      => 'Subtotal',
'success'	                                              	=> 'Sucesso',
'fail'   	                                              	=> 'Falha',
'unitPrice'                                                     => 'Valor unitário',
'go'                                                            => 'Ir',
'day'                                                           => 'Dia',
'week'                                                          => 'Semana',
'month'                                                         => 'Mês',
'year'                                                          => 'Ano',
'notAllowedRemotion'                                            => 'Remoção não permitida',

// consignment
'consign.cantCancel'                                            => 'Não foi possível cancelar porque existe uma venda '
                                                                 . 'atrelada a este consignado e a venda possui pagamentos',

// dynamicSearch
'ds.filterBy'                                                   => '<u>F</u>iltrar por',
'ds.filter'                                                     => 'F<u>i</u>ltro',
'ds.filteredData'                                               => 'Dados Filtrados',

// fastSelling
'fastSelling.addProd'	                                	=> 'Adicionar produto',
'fastSelling.amount'	                                      	=> '<u>Q</u>uantidade',
'fastSelling.backToOrder'                                     	=> '<b><u>V</u></b>oltar para comandas',
'fastSelling.cannotUseIdentifier'	                    	=> 'Não é possível usar este identificador',
'fastSelling.codeProd'	                                      	=> '<u>C</u>ódigo',
'fastSelling.description'                                     	=> 'Descrição',
'fastSelling.emptyIdentifierError'                              => 'Erro, identificador vazio',
'fastSelling.emptyIdentifierErrorOnLastSale'                    => 'Erro, identificador vazio na última venda',
'fastSelling.saleId'	                                      	=> 'Id da venda',
'fastSelling.orderNr' 	                                    	=> 'Comanda nº',
'fastSelling.orders' 	                                    	=> 'Comandas',
'fastSelling.searchProd'                                      	=> '<b><u>B</u></b>uscar produto',
'fastSelling.removeProd'                                    	=> 'R<b><u>e</u></b>mover produto',
'fastSelling.value'	                                     	=> 'Valor',
'fastSelling.billing'                                           => '<b><u>F</u></b>aturamento',
'fastSelling.receipt'                                           => 'Recebimento',
'fastSelling.ctrlV'                                             => 'CTRL+V',
'fastSelling.ctrlB'                                             => 'CTRL+B',
'fastSelling.removeProduct'                                     => 'CTRL+E - Remover Produto',
'fastSelling.print'                                             => 'CTRL+P - Imprimir',
'fastSelling.reprint'                                           => 'Re-Imprimir',
'fastSelling.detail'                                            => 'CTRL+D - Detalhar',
'fastSelling.anotherIntegral'                                   => 'Mais um integrante à mesa',
'fastSelling.new'                                               => 'novo',
'fastSelling.detail'                                            => 'Detalhes',
'fastSelling.next'                                              => 'Prosseguir',
'fastSelling.create'                                            => 'Criar',
'fastSelling.alreadyUsedIdentifier'                             => 'Usuário já está em uso',
'fastSelling.cannotCancel'                                      => 'A venda não pode ser cancelada porque possui pagamentos ou trocas. Estorne os pagamentos e cancele as trocas para proceguir',


// orderStatus
'orderStatus.budget'                                           	=> '2 - Orçamento',
'orderStatus.delivered'                                         => '4 - Entregue',
'orderStatus.done'                                          	=> '5 - Finalizado',
'orderStatus.ongoing'                                         	=> '3 - Em andamento',
'orderStatus.refused' 	                                        => '6 - Recusado',
'orderStatus.request'                                          	=> '1 - Solicitação',

// orderType
'orderType.commonSelling'                                    	=> 'Venda comum',
'orderType.fastSelling'	                                      	=> 'Venda rápida',
'orderType.taxRequest'                                 		=> 'Requerimento fiscal',

// penaltyType
'penaltyType.mulct'                                             => 'Multa',
'penaltyType.interest'                                          => 'Juros',

// operator
'operator.plus'                                                 => 'Soma',
'operator.minus'                                                => 'Subtração',
'operator.percentage'                                           => 'Porcentagem',

// payment
'payment.freeOrderNumber'                                       => 'L<b><u>i</u></b>berar comanda',
'payment.paymentAmountMoreThanSaleAmount'                       => 'O valor indicado é superior ao restante programado',
'payment.invalidValue'                                          => 'Valor submetido incoerente ou inferior ao pagamento já efetuado',
'payment.insufficientAmount'                                    => 'Quantia insuficiente',
'payment.person'                                                => 'Pessoa',
'payment.cannotAddPayer'                                        => 'Não é possível adicionar pagadores',
'payment.cannotRemovePayer'                                     => 'Não é possível remover pagador',
'payment.cannotClose'                                           => 'Não é possível fechar comanda',
'payment.backToCommand'                                         => 'Voltar para comanda',
'payment.back'                                                  => '<b><u>V</u></b>oltar',
'billing.consumption'                                           => 'Consumo',
'billing.discount'                                              => 'Desconto',
'billing.pending'                                               => 'Pendente',
'billing.orderId'                                               => 'ID do pedido',
'billing.commandNumber'                                         => 'Comanda nº',
'billing.aggregates'                                            => 'Agregados',
'billing.payers'                                                => 'Pagadores',
'billing.add'                                                   => 'Adicionar',
'billing.remove'                                                => 'Remover',
'billing.receive'                                               => 'Receber',
'billing.exchange'                                              => 'Troco',
'billing.received'                                              => 'Recebido',
'billing.printReceipt'                                          => 'Im<b><u>p</u></b>rimir Recibo',
'billing.printCoupon'                                           => 'I<b><u>m</u></b>primir cupom fiscal',
'billing.confirm'                                               => 'Confirmar',
'billing.invalidDiscount'                                       => 'Tentativa de alterar desconto inválida',

// printer
'printer.thermal'                                               => 'Impressora térmica',
'printer.standart'                                              => 'Impressora padrão',

// product
'product.codeBeingUsedInside'                                   => 'O código %s já está em uso dentro deste produto',
'product.barCodeBeingUsedInside'                          => 'O código de barras %s já está em uso dentro deste produto',
'product.codeBeingUsedOutside'                                  => 'O código %s já está em uso fora deste produto',
'product.barCodeBeingUsedOutside'                         => 'O código de barras %s já está em uso fora deste produto',

//acess
'acess.number'                                                  => 'Número',
'acess.goToCommand'                                             => 'Ir para comanda',
'acess.client'                                                  => 'Clientes',
'acess.identifier'                                              => 'Identificador',
'acess.addNewClient'                                            => 'Cadastrar Novo Cliente',
);

