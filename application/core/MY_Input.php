<?php if (! defined('BASEPATH')) exit('No direct script access allowed'); 

class MY_Input extends CI_Input {

  private $_POST_RAW;

  function __construct() {
    $this->_POST_RAW = $_POST;
    parent::__construct();
  }

  function post($index = null, $xss_clean = TRUE) {
    if(!$xss_clean)
      return $this->_POST_RAW[$index];
    return parent::post($index, $xss_clean);
  }
}
