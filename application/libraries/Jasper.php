<?php
require_once APPPATH.'libraries/jaspersoft/rest-client/autoload.dist.php';

use Jaspersoft\Client\Client;
class Jasper {
  public $client;
  public $rs;

  function __construct() {
    include APPPATH.'config/jasper.php';
    $this->client = new Client(
      $jasperConf['url']
    , $jasperConf['username']
    , $jasperConf['password']);
    $this->rs = $this->client->reportService();
  }  
}
