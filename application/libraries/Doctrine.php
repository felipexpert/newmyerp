<?php
use Doctrine\Common\ClassLoader,
    Doctrine\ORM\Configuration,
    Doctrine\ORM\EntityManager,
    Doctrine\Common\Cache\ArrayCache,
    Doctrine\DBAL\Logging\EchoSQLLogger;

require_once APPPATH.'libraries/doctrine/common/lib/Doctrine/Common/ClassLoader.php';
require_once APPPATH.'libraries/doctrine/orm/lib/Doctrine/ORM/Configuration.php';
require_once APPPATH.'libraries/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';
require_once APPPATH.'libraries/doctrine/cache/lib/Doctrine/Common/Cache/ArrayCache.php';
require_once APPPATH.'libraries/doctrine/dbal/lib/Doctrine/DBAL/Logging/EchoSQLLogger.php';


class Doctrine {

  public $em = null;

  public function __construct()
  {
    // load database configuration from CodeIgniter
    include APPPATH.'config/database.php';
    // Set up class loading. You could use different autoloaders, provided by your favorite framework,
    // if you want to.


    $doctrineClassLoader = new ClassLoader('Doctrine',  APPPATH.'libraries');
    $doctrineClassLoader->register();
    $entitiesClassLoader = new ClassLoader('models', rtrim(APPPATH, "/" ));
    $entitiesClassLoader->register();
    $proxiesClassLoader = new ClassLoader('Proxies', APPPATH.'models/proxies');
    $proxiesClassLoader->register();

    // Set up caches
    $config = new Configuration;
    $cache = new ArrayCache;
    $config->setMetadataCacheImpl($cache);
    $driverImpl = $config->newDefaultAnnotationDriver(array(APPPATH.'models/Entity'));
    $config->setMetadataDriverImpl($driverImpl);
    $config->setQueryCacheImpl($cache);

    $config->setQueryCacheImpl($cache);

    // Proxy configuration
    $config->setProxyDir(APPPATH.'/models/proxies');
    $config->setProxyNamespace('Proxies');

    // Set up logger
    //$logger = new EchoSQLLogger;
    //$config->setSQLLogger($logger);

    $config->setAutoGenerateProxyClasses( TRUE );

    // Database connection information
    $connectionOptions = array(
        'driver' => 'pdo_mysql',
        'user' =>     $db['default']['username'],
        'password' => $db['default']['password'],
        'host' =>     $db['default']['hostname'],
        'dbname' =>   $db['default']['database']
    );

    // Create EntityManager
    $this->em = EntityManager::create($connectionOptions, $config);

    #  Convert DB Charset and Collate to utf8 utf8_unicode_ci
    $this->em->getEventManager()
        ->addEventSubscriber(
            new \Doctrine\DBAL\Event\Listeners\MysqlSessionInit('utf8', 
                'utf8_unicode_ci'));
  }
}
