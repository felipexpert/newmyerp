<?php

abstract class Either extends Monad {
  
  private function __construct() {}

  static function success($right) {
    return new Right(M::checkNotNull($right));
  }
  static function fail($left) {
    return new Left(M::checkNotNull($left));
  }
  public static function fromNullable($right, $left) {
    return $right === null ? new Left(M::checkNotNull($left)) : new Present($right);
  }
  function isRight() {
    return !$this->isLeft();
  }
  function isLeft() {
    return !$this->isRight();
  }
  function ifLeft($func) {
    if($this->isLeft()) $func($this->left());
  }
  function ifRight($func) {
    if($this->isRight()) $func($this->right());
  }
  function ifElse($rightFunc, $leftFunc) {
    return $this->isRight() ? $rightFunc($this->right()) : $leftFunc($this->left());
  }
  function map($func) {
    if($this->isRight()) return static::success($func($this->right()));
    return $this;
  }
  abstract function left();
  abstract function right();
  abstract function equals($obj);

  // Monad Implementation
  static function ret($x) {
    return Either::success($x);
  }
  function bind($f) {
    if($this->isRight()) return $f($this->right());
    return $this;
  }
}
