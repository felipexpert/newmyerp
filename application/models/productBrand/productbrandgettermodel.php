<?php
class ProductBrandGetterModel extends EntityGetterModel {
  function prepareAll() {
    $bs = $this->em->createQuery('select b from Entity\ProductBrand b 
      where b.disabled = FALSE')->getResult();
    $this->prepare($bs);
    return $this;
  }
}
