<?php
class ProductBrandModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\ProductBrand";
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
  }
  function map() {
    $b = $this->entity();
    return [
      'id' => $b->getId()
    , 'name' => $b->getName()
    , 'disabled' => $b->isDisabled()
    ]; 
  }
}
