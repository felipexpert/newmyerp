<?php
class ConsignmentCreatorModel extends CI_Model {
  private $dealer;
  private $creation;
  private $obs;
  private $employee;

  private $orderCreatorModel;
  function __construct() {
    parent::__construct();
    $this->load->model('order/orderCreatorModel', '_orderCreatorModel');
    $this->orderCreatorModel = new OrderCreatorModel();
  }
  function create() {
    $order = $this->orderCreatorModel->setObs($this->obs)
      ->setCreation($this->creation)
      ->setEmployee($this->employee)->create();
    $consignment = new Entity\Consignment();
    $consignment->setOrder($order)
      ->setDealer($this->dealer);
    $this->em->persist($consignment);
    $this->em->flush();
    return $consignment;
  }
  public function getDealer() {
    return $this->dealer;
  }
  public function setDealer($dealer) {
    $this->dealer = $dealer;
    return $this;
  }
  public function getObs() {
    return $this->obs;
  }
  public function setObs($obs) {
    $this->obs = $obs;
    return $this;
  }
  public function getEmployee() {
    return $this->employee;
  }
  public function setEmployee($e) {
    $this->employee = $e;
    return $this;
  }
  public function getCreation() {
    return $this->creation;
  }
  public function setCreation($creation) {
    $this->creation = $creation;
    return $this;
  }
}
