<?php

class SaleHelperModel extends CI_Model {
  private $consignment;
  function __construct() {
    parent::__construct();
  }
  function makeSale() {
    if($this->hasSale()) return $this->getSale();
    $this->consignment->setClosing(new DateTime('now'));
    $creator = M::l('sale/saleCreator');  
    $order = $this->cloneOrderWithoutFlows($this->consignment->getOrder());
    $sale = $creator
      ->setOrder($order)
      ->setConsignment($this->consignment)
      ->create();
    $this->prepareSale($sale);
    $payerHelper = M::l('sale/payerHelper')->prepareBySale($sale);
    if($payerHelper->canAddPayer()) {
      if(!is_null($this->consignment->getDealer()))
        $payerHelper->addCustomerPayer($this->consignment->getDealer()->getCustomer());
      else
        $payerHelper->addPayer();
    }
    $this->em->flush();
    return $sale;
  }
  function cloneOrderWithoutFlows($order) {
    $orderCreator = M::l('order/orderCreator');
    $newOrder = $orderCreator->setObs($order->getObs())
      ->setEmployee($order->getEmployee())
      ->create();
    M::l('orderItem/orderItemCreator');
    $newOis = $order->getOrderItems()->map(function($oi) use($newOrder) {
      $oiCreator = new OrderItemCreatorModel();
      $noi = $oiCreator->setProductInstance($oi->getProductSnapshot()->getProductInstance())
        ->setOrder($newOrder)
        ->setDiscount($oi->getProductSnapshot()->getDiscount())
        ->setPrice($oi->getProductSnapshot()->getPrice())
        ->create();
      $noi->setDisplayOrder($oi->getDisplayOrder())
          ->setDisabled($oi->isDisabled());
      return $noi;
    });
    $this->em->flush();
    return $newOrder;
  }
  /*function makeSale() {
    if($this->hasSale()) return $this->getSale();
    $this->consignment->setClosing(new DateTime('now'));
    $creator = M::l('sale/saleCreator');  
    $sale = $creator->setOrder($this->consignment->getOrder())->create();
    $this->prepareSale($sale);
    $payerHelper = M::l('sale/payerHelper')->prepareBySale($sale);
    if($payerHelper->canAddPayer()) {
      if(!is_null($this->consignment->getDealer()))
        $payerHelper->addCustomerPayer($this->consignment->getDealer()->getCustomer());
      else
        $payerHelper->addPayer();
    }
    $this->em->flush();
    return $sale;
  }*/
  function hasSale() {
    return M::l('sale/saleGetter')->prepareByConsignmentId($this->consignment->getId())->has();
  }
  function getSale() {
    $saleGetter = M::l('sale/saleGetter');
    $saleGetter->prepareByConsignmentId($this->consignment->getId());
    return $saleGetter->has() ? $saleGetter->first() : null;
  }
  function setConsignment($consignment) {
    $this->consignment = $consignment;
    return $this;
  }
  function prepareByConsignmentId($consignmentId) {
    $consignmentGetterModel = M::l('consignment/consignmentGetter'); 
    if($consignmentGetterModel->prepareById($consignmentId)->has())
      $this->consignment = $consignmentGetterModel->first();
    return $this;
  }
  private function prepareSale($sale) {
    $consignmentModel = M::l('consign/consignment')->setEntity($this->consignment);
    $saleModel = M::l('sale/sale')->setEntity($sale);
    $consignmentOrderModel = M::l('order/order')->setEntity($this->consignment->getOrder());
    $sale->getOrder()->getOrderItems()
      ->filter(function($i) { return !$i->isDisabled(); })
      ->map(function($i) 
          use($consignmentModel, $saleModel, $consignmentOrderModel) {
        $i2 = $consignmentOrderModel->getItemByDisplayOrder($i->getDisplayOrder());
        $saleModel->updateOrderItemAmount($i, $consignmentModel->calcOrderItemAmount($i2)); 
      });
  }
}
