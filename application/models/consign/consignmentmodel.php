<?php
class ConsignmentModel extends OrderingEntityWrapperModel {
  const ENTITY_NAME = 'Entity\\Consignment';
  private $orderModel;
  private $orderItemModel;
  private $dealerModel;
  private $dealerGetterModel;
  private $customerModel;
  private $dealerCreatorModel;
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
  }
  function canCancel() {
    $saleHelper = M::l('consign/saleHelper')
      ->setConsignment($this->entity());
    return $saleHelper->hasSale()
      ? M::l('sale/sale')->setEntity($saleHelper->getSale())->canCancel()
      : TRUE;
  }
  function maybeCancel() {
    if(!$this->canCancel()) return Either::fail(lang('consign.cantCancel')); 
    $this->orderModel->disable();
    return Either::success($this->entity());
  }
  protected function hook() {
    $this->orderModel = M::l('order/order');
    $this->orderItemModel = M::l('orderItem/orderItem');
    $this->orderModel->setEntity($this->entity()->getOrder());  
    $this->dealerModel = M::l('dealer/dealer'); 
    $this->dealerGetterModel = M::l('dealer/dealerGetter');
    $this->customerModel = M::l('customer/customer'); 
    $this->dealerCreatorModel = M::l('dealer/dealerCreator');
  }
  function bindCustomer($customerId) {
    $dealer = null;
    if($this->dealerGetterModel->prepareByCustomerId($customerId)->has()) { 
      $dealer = $this->dealerGetterModel->first(); 
      $this->entity()->setDealer($dealer);
      $dealer->getConsignments()->add($this->entity());
      $this->em->flush();
    } else {
      $customer = $this->customerModel->prepareById($customerId)->entity();
      $dealer = $this->dealerCreatorModel->setCustomer($customer)->setConsignment($this->entity())->create();
    }
    return $dealer;
  }
  function getPositiveFlows() {
    return
    [ Entity\FlowType::OUT_CONSIGNED
    , Entity\FlowType::OUT_CONSIGNED_OFFSET];
  }
  function getNegativeFlows() {
    return
    [ Entity\FlowType::IN_CONSIGNED
    , Entity\FlowType::IN_CONSIGNED_OFFSET];
  }
  function hasSale() {
    return M::l('sale/saleGetter')->prepareByConsignmentId($this->entity()->getId())
      ->has();
  }
  function map($extra = '') {
    $c = $this->entity();
    $o = $c->getOrder();
    $mapped = [ 'id' => $c->getId()
      , 'order' => $this->orderModel->map()
      , 'dealer' => $c->getDealer() ? $this->dealerModel->setEntity($c->getDealer())->map() : null
      , 'previsionClosing' => D::millis($c->getPrevisionClosing())
      , 'hasSale' => $this->hasSale()
      , 'closing' => D::millis($c->getClosing())];
    if(T::contains('orderItems', $extra)) $mapped['orderItems'] = $o->getOrderItems()->map(function($oi) {
      return $this->mapOrderItem($oi);
    })->toArray();
    if(T::contains('totals', $extra)) {
      $mapped['positiveTotal'] = $this->calcPositiveOrderTotalPrice();
      $mapped['negativeTotal'] = $this->calcNegativeOrderTotalPrice();
      $mapped['total'] = $mapped['positiveTotal'] - $mapped['negativeTotal'];
    }
    return $mapped;
  }
  function mapOrderItem($oi) {
    if(is_numeric($oi)) $oi = DU::elemById($this->entity()->getOrder()->getOrderItems(), $oi);
    $mapped = $this->orderItemModel->setEntity($oi)->map();
    $mapped['summary'] = $this->summarize($oi);
    return $mapped;
  }
}
