<?php
class ConsignmentGetterModel extends EntityGetterModel {
  function prepareById($id) {
    $xs = $this->em->createQuery('select c from Entity\Consignment c
      inner join c.order o
      where c.id = :id')->setParameter('id', $id)->getResult();
    $this->prepare($xs);
    return $this;
  }
  /*function prepareAll() {
    $xs = $this->em->createQuery('select c, o, i, d from Entity\Consignment c
      inner join c.order o
      left join o.orderItems i
      left join c.dealer d
      where o.disabled = FALSE
      and c.closing is null')->getResult();
    $this->prepare($xs);
    return $this;
  }*/
  function prepareByFilter($items, $filter, $optional = null) {
    $qb = $this->em->createQueryBuilder();
    $qb->select(['c'])
      ->from('Entity\Consignment', 'c')
      ->leftJoin('c.order', 'o')
      ->leftJoin('o.orderItems', 'oi')
      ->leftJoin('oi.productSnapshot', 'snapshot')
      ->leftJoin('snapshot.productInstance', 'prod')
      ->leftJoin('c.dealer', 'dealer')
      ->leftJoin('dealer.customer', 'cus')
      ->leftJoin('cus.personAttr', 'person')
      ->where('o.disabled = FALSE');
    if(strLen($filter)) $qb->andWhere('person.name like :filter or person.cpf like :filter')
                           ->setParameter('filter', '%'.$filter.'%');
    if(isSet($optional)) {
      if($optional->onlyOpened)  
        $qb->andWhere('c.closing is null');
      if(isSet($optional->productInstanceId))
        $qb->andWhere('prod.id = :productInstanceId')
           ->setParameter('productInstanceId', $optional->productInstanceId);
      if(isSet($optional->dealerId))
        $qb->andWhere('dealer.id = :dealerId')
           ->setParameter('dealerId', $optional->dealerId);
    }
    $qb->orderBy('c.previsionClosing', 'ASC')
      ->addOrderBy('c.id', 'DESC')
      /*->setMaxResults($items)*/;
    $list = C::take($items, $qb->getQuery()->getResult());
    $this->prepare($list);
    return $this;
  }
}
