<?php
class PortionBucketCreatorModel extends CI_Model {
  private $portions;
  function create() {
    $pb = new Entity\PortionBucket();
    $pb->setPortions(C::forceCollection($this->portions)); 
    C::fe(function($portion) use($pb) {
      $portion->setPortionBucket($pb);
    }, $this->portions);
    $this->em->persist($pb);
    $this->em->flush();
    return $pb;
  }
  function setPortions($portions) {
    $this->portions = $portions;
    return $this;
  }
}
