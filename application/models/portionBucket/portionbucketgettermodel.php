<?php
class PortionBucketGetterModel extends EntityGetterModel {
  function prepareByPayerId($payerId) {
    $payers = $this->em->createQuery('select payer, pb, por 
      from Entity\Payer payer
      inner join payer.portionBucket pb
      left join pb.portions por
      where payer.id = :id')->setParameter('id',  $payerId)->getResult();
    $this->prepare(C::forceCollection($payers)->map(function($payer) { return $payer->getPortionBucket(); }));
    return $this;
  }
}
