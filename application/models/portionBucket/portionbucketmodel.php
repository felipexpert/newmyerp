<?php
class PortionBucketModel extends EntityWrapperModel {
  const ENTITY_NAME = 'Entity\\PortionBucket';
  private $portionModel;
  function __construct() {
    parent::__construct();
    $this->setEntityName(PortionBucketModel::ENTITY_NAME);
    $this->load->model('portion/portionModel', '_portionModel');
    $this->portionModel = new PortionModel();
  }
  function unchangeable() {
    return $this->paidAmount();
  }
  function isManipulable() {
    return N::eq($this->unchangeable(), 0);
  }
  function getAmount() {
    return $this->entity()->getAmount();
  }
  function setAmount($amount) {
    $this->entity()->setAmount($amount);
    $this->em->flush();
    return $this;
  }
  function outstanding() {
    return $this->entity()->getAmount() - $this->paidAmount(); 
  }
  function paidAmount() {
    return C::foldl(function($acc, $portion) {
      $this->portionModel->setEntity($portion);
      return $acc + $this->portionModel->paidAmount();
    }, 0, $this->entity()->getPortions());
  }
  function canAddPortion() {
    return N::lt($this->paidAmount(), $this->entity()->getAmount());
  }
  function addAndPayPortion($payments) {
    if(!$this->canAddPortion()) return;
    $paymentsTotal = C::foldl(function($acc, $p) { return $acc + $p->amount; }, 0, $payments); 
    $portionTotal = min($this->outstanding(), $paymentsTotal); 
    $this->load->model('portion/portionCreatorModel', 'portionCreator');
    $portion = $this->portionCreator->setPortionBucket($this->entity())->setTotal($portionTotal)->create();
    $this->load->model('paymentType/paymentTypeGetterModel', 'typeGetter');
    $types = $this->typeGetter->prepareAll()->all();
    $this->load->model('payment/paymentCreatorModel');
    C::fe(function($p) use($portion, $types) {
      $paymentType = $types->filter(function($paymentType) use ($p) {
        return $paymentType->getId() == $p->paymentTypeId;
      })->first();
      $creator = new PaymentCreatorModel();
      $creator->setPortion($portion)->setAmount($p->amount)->setPaymentType($paymentType)->create();
    }, $payments);
  }
  function predefinedParcelling($predefinedParcellingId) {
    if(!$this->canAddPortion() || !$this->isBoundToCustomer()) 
      throw new Exception('Cannot create predefined parcellings!');
    $this->load->model('predefinedParcelling/predefinedParcellingModel', 'predefined'); 
    $this->predefined->prepareById($predefinedParcellingId);
    $dates = $this->predefined->generateDates(new DateTime('now'));
    $this->createPortionsWithDates($dates);
  }
  function getCustomer() {
    $payerGetter = M::load('payer/payerGetter');
    $payerGetter->prepareByPortionBucketId($this->entity()->getId());
    return $payerGetter->has() ? Optional::fromNullable($payerGetter->first()->getCustomer()) : Optional::absent();
  }
  function getPayer() {
    $payerGetter = M::load('payer/payerGetter');
    $payerGetter->prepareByPortionBucketId($this->entity()->getId());
    return $payerGetter->has() ? Optional::of($payerGetter->first()) : Optional::absent();
  }
  function isBoundToCustomer() {
    $this->load->model('payer/payerGetterModel', 'payerGetter');
    $this->payerGetter->prepareByPortionBucketId($this->entity()->getId());
    return $this->payerGetter->has() ? !is_null($this->payerGetter->first()->getCustomer()) : FALSE;
  }
  function map($extra = null) {
    $pb = $this->entity(); 
    $maybeCustomer = $this->getCustomer();
    $maybePayer = $this->getPayer();
    $mapped =
      [ 'id' => $pb->getId()
      , 'amount' => $pb->getAmount()
      , 'isBoundToCustomer' => $maybeCustomer->isPresent()
      , 'isBoundToPayer' => $maybePayer->isPresent()
      , 'outstanding' => $this->outstanding()
      , 'portions' => $pb->getPortions()->map(function($portion) {
        return $this->portionModel->setEntity($portion)->map();
      })->toArray()];
    if(T::contains('customer', $extra))
      $mapped['customer'] = $maybeCustomer
        ->map(function($cus) { return M::load('customer/customer')->setEntity($cus)->map(); })
        ->getOrNull();
    if(T::contains('payer', $extra)) 
      $mapped['payer'] = $maybePayer
        ->map(function($payer) { return M::load('payer/payer')->setEntity($payer)->map(); })
        ->getOrNull();
    return $mapped;
  }
  function createPortions($portions) {
    if(!$this->canAddPortion() || !$this->isBoundToCustomer()) 
      throw new Exception('Cannot create portions!');
    $sortedPortions = C::sortCollection($this->entity()->getPortions(), function($pA, $pB) {
      return D::millis($pA->getValidUntil()) - D::millis($pB->getValidUntil());
    });
    $baseDate = $sortedPortions->count() 
      ? Optional::fromNullable($sortedPortions->last()->getValidUntil())->getOrElse(new DateTime('now')) 
      : new DateTime('now');
    $dates = C::produce($portions, function($i) use($baseDate) {
      return D::dateAdd($baseDate, 0, $i);
    }, TRUE);
    $this->createPortionsWithDates($dates);
  }
  function redistribute() {
    $this->load->model('calc/newtotalredistributormodel', 'newTotal');
    $models = $this->entity()->getPortions()->map(function($p) {
      return (new PortionModel())->setEntity($p);
    });
    $this->newTotal->setItems($models)->perform($this->entity()->getAmount());
  }
  function canRemove() {
    return N::eq($this->paidAmount(), 0); 
  }
  function unsafeRemove() {
    C::fe(function($portion) { $this->portionModel->setEntity($portion)->remove(); }, $this->entity()->getPortions());
    parent::unsafeRemove();
  }
  private function createPortionsWithDates($dates) {
    $this->load->model('portion/portionCreatorModel', 'portionCreator');
    $this->load->model('portion/portionModel', 'portion');
    C::fe(function($d) {
      return $this->portionCreator
        ->setTotal(0)
        ->setPortionBucket($this->entity())
        ->setValidUntil($d)->create();
    }, $dates);
    $this->redistribute();
  }
}
