<?php
final class Maybe {
  private $nothing;
  private $element;
  private function __construct() {
    $this->nothing = TRUE;
    $args = func_get_args();
    if(count($args)) {
      $this->element = $args[0];
      $this->nothing = FALSE;
    }
  }
  function apply($callback) { 
    if(!$this->nothing) return $callback($this->element); 
    return null;
  }
  function get() {
    if($this->nothing) throw new Exception('Nothing!');
    return $element;
  }
  function getNullable() {
    if($this->nothing) return null;
    return $this->element;
  }
  function isNothing() {
    return $this->nothing;
  }
  function has() {
    return !$this->nothing;
  }
  static function nothing() { return new Maybe(); }
  static function something($elem) { 
    if(is_null($elem)) throw new Exception('Element cannot be null!');
    return new Maybe($elem); 
  }
  static function nullable($elem) {
    if(is_null($elem)) return new Maybe();
    else                return new Maybe($elem);
  }
  static function applyTo($e, $callback) {
    if($e instanceof Maybe) return $e->apply($callback);
    return $callback($e);
  }
}
