<?php

namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Disableable.php';
require_once './application/models/Entity/Product.php';
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 * @Table(indexes={@Index(name="searchIdx", columns={"name"})})
 */
class ProductBrand implements Indexable, Disableable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @Column
   */
  private $name;
  /**
   * @OneToMany(targetEntity="Product", cascade="persist", mappedBy="productBrand")
   */
  private $products;
  /**
   * @Column(type="boolean")
   */
  private $disabled;

  function __construct() {
    $this->products = new ArrayCollection();
    $this->disabled = FALSE;
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function getProducts() {
    return $this->products;
  }
  public function setProducts($products) {
    $this->products = $products;
    return $this;
  }
  public function isDisabled() {
    return $this->disabled;
  }
  public function setDisabled($disabled) {
    $this->disabled = $disabled;
    return $this;
  }
}
