<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Sale.php';
require_once './application/models/Entity/PortionBucket.php';
require_once './application/models/Entity/Customer.php';
require_once './application/models/Entity/Disableable.php';
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 */
class Payer implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @ManyToOne(targetEntity="Sale", inversedBy="payers")
   * @JoinColumn(name="saleId", referencedColumnName="id")
   */
  private $sale;
  /**
   * @OneToOne(targetEntity="PortionBucket")
   * @JoinColumn(name="portionBucketId", referencedColumnName="id")
   */
  private $portionBucket;
  /**
   * @ManyToOne(targetEntity="Customer", inversedBy="payers")
   * @JoinColumn(name="customerId", referencedColumnName="id")
   */
  private $customer;
  /**
   * @Column 
   */
  private $description;
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getSale() {
    return $this->sale;
  }
  public function setSale($sale) {
    $this->sale = $sale;
    return $this;
  }
  public function getPortionBucket() {
    return $this->portionBucket;
  }
  public function setPortionBucket($portionBucket) {
    $this->portionBucket = $portionBucket;
    return $this;
  }
  public function getDescription() {
    return $this->description;
  }
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }
  public function getCustomer() {
    return $this->customer;
  }
  public function setCustomer($customer) {
    $this->customer = $customer;
    return $this;
  }
}
