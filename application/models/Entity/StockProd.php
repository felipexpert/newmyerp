<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Disableable.php';
require_once './application/models/Entity/Stock.php';
require_once './application/models/Entity/ProductInstance.php';

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 */
class StockProd implements Indexable, Disableable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @ManyToOne(targetEntity="Stock", inversedBy="stockProds")
   * @JoinColumn(name="stockId", referencedColumnName="id")
   */
  private $stock;
  /**
   * @ManyToOne(targetEntity="ProductInstance", inversedBy="stockProd")
   * @JoinColumn(name="stockId", referencedColumnName="id")
   */
  private $productInstance;
  /**
   * @Column(type="integer")
   */
  private $flowType;
  /**
   * @Column(type="float")
   */
  private $amount;
  /**
   * @Column(type="boolean")
   */
  private $disabled;
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getStock() {
    return $this->stock;
  }
  public function setStock($stock) {
    $this->stock = $stock;
    return $this;
  }
  public function getProductInstance() {
    return $this->productInstance;
  }
  public function setProductInstance($productInstance) {
    $this->productInstance = $productInstance;
    return $this;
  }
  public function getFlowType() {
    return $this->flowType;
  }
  public function setFlowType($flowType) {
    $this->flowType = $flowType;
    return $this;
  }
  public function getAmount() {
    return $this->amount;
  }
  public function setAmount($amount) {
    $this->amount = $amount;
    return $this;
  }
  public function isDisabled() {
    return $this->disabled;
  }
  public function setDisabled($disabled) {
    $this->disabled = $disabled;
    return $this;
  }
}
