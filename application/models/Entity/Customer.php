<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/PersonAttr.php';
require_once './application/models/Entity/Payer.php';

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 */
class Customer implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @ManyToOne(targetEntity="PersonAttr")
   * @JoinColumn(name="personAttrId", referencedColumnName="id")
   */
  private $personAttr;
  /**
   * @OneToMany(targetEntity="Payer", cascade="persist", mappedBy="customer")
   */
  private $payers;
  /**
   * @Column(type="datetime")
   */
  private $creation;
  /**
   * @Column(type="boolean")
   */
  private $disabled;
  function __construct() {
    $this->payers = new ArrayCollection();
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getPersonAttr() {
    return $this->personAttr;
  }
  public function setPersonAttr($personAttr) {
    $this->personAttr = $personAttr;
    return $this;
  }
  public function getPayers() {
    return $this->payers;
  }
  public function setPayers($payers) {
    $this->payers = $payers;
    return $this;
  }
  public function getCreation() {
    return $this->creation;
  }
  public function setCreation($creation) {
    $this->creation = $creation;
    return $this;
  }
  public function isDisabled() {
    return $this->disabled;
  }
  public function setDisabled($disabled) {
    $this->disabled = $disabled;
    return $this;
  }
}
