<?php

namespace Entity;

require_once './application/models/Entity/Employee.php';
/**
 * @Entity
 */
class User {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @Column(type="string")
   */
  private $login;
  /**
   * @Column(type="string")
   */
  private $passwd;
  
  /**
   * @OneToOne(targetEntity="Employee")
   * @JoinColumn(name="employeeId", referencedColumnName="id")
   */
  private $employee;
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getLogin() {
    return $this->login;
  }
  public function setLogin($login) {
    $this->login = $login;
    return $this;
  }
  public function getPasswd() {
    return $this->passwd;
  }
  public function setPasswd($passwd) {
    $this->passwd = $passwd;
    return $this;
  }
  public function getEmployee() {
    return $this->employee;
  }
  public function setEmployeeId($employee) {
    $this->employee = $employee;
    return $this;
  }
}
