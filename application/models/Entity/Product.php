<?php

namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Disableable.php';
require_once './application/models/Entity/ProductCategory.php';
require_once './application/models/Entity/ProductBrand.php';
require_once './application/models/Entity/ProductTaxRate.php';
require_once './application/models/Entity/ProductType.php';
require_once './application/models/Entity/ProductInstance.php';
/**
 * @Entity
 * @Table(indexes={@Index(name="searchIdx", columns={"name"})})
 */
class Product implements Indexable, Disableable {
  
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @ManyToOne(targetEntity="ProductCategory", inversedBy="products")
   * @JoinColumn(name="productCategoryId", referencedColumnName="id")
   */
  private $productCategory;
  /**
   * @ManyToOne(targetEntity="ProductBrand", inversedBy="products")
   * @JoinColumn(name="productBrandId", referencedColumnName="id")
   */
  private $productBrand;
  /**
   * @ManyToOne(targetEntity="ProductTaxRate", inversedBy="products")
   * @JoinColumn(name="productTaxRateId", referencedColumnName="id")
   */
  private $productTaxRate;
  /**
   * @OneToMany(targetEntity="ProductInstance", cascade="persist", mappedBy="product")
   */
  private $instances;
  /**
   * @Column
   */
  private $codePrefix;
  /**
   * @Column(type="float")
   */
  private $defaultPrice;
  /**
   * @Column(type="float")
   */
  private $defaultBuyingPrice;
  /**
   * @Column
   */
  private $name;
  /**
   * @ManyToOne(targetEntity="ProductType", inversedBy="products")
   * @JoinColumn(name="productTypeId", referencedColumnName="id")
   */
  private $productType;
  /**
   * @Column(type="text")
   */
  private $obs;
  /**
   * @Column(type="boolean")
   */
  private $disabled;
  function __construct() {
    $this->disabled = FALSE;
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getProductCategory() {
    return $this->productCategory;
  }
  public function setProductCategory($productCategory) {
    $this->productCategory = $productCategory;
    return $this;
  }
  public function getProductBrand() {
    return $this->productBrand;
  }
  public function setProductBrand($productBrand) {
    $this->productBrand = $productBrand;
    return $this;
  }
  public function getProductTaxRate() {
    return $this->productTaxRate;
  }
  public function setProductTaxRate($productTaxRate) {
    $this->productTaxRate = $productTaxRate;
    return $this;
  }
  public function getInstances() {
    return $this->instances;
  }
  public function setInstances($instances) {
    $this->instances = $instances;
    return $this;
  }
  public function getCodePrefix() {
    return $this->codePrefix;
  }
  public function setCodePrefix($codePrefix) {
    $this->codePrefix = $codePrefix;
    return $this;
  }
  public function getDefaultPrice() {
    return $this->defaultPrice;
  }
  public function setDefaultPrice($defaultPrice) {
    $this->defaultPrice = $defaultPrice;
    return $this;
  }
  public function getDefaultBuyingPrice() {
    return $this->defaultBuyingPrice;
  }
  public function setDefaultBuyingPrice($defaultBuyingPrice) {
    $this->defaultBuyingPrice = $defaultBuyingPrice;
    return $this;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function getProductType() {
    return $this->productType;
  }
  public function setProductType($productType) {
    $this->productType = $productType;
    return $this;
  }
  public function getObs() {
    return $this->obs;
  }
  public function setObs($obs) {
    $this->obs = $obs;
    return $this;
  }
  public function isDisabled() {
    return $this->disabled;
  }
  public function setDisabled($disabled) {
    $this->disabled = $disabled;
    return $this;
  }
}
