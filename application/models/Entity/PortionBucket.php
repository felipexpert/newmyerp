<?php namespace Entity;
require_once './application/models/Entity/Indexable.php';
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 */
class PortionBucket implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @Column(type="float")
   */
  private $amount;
  /**
   * @OneToMany(targetEntity="Portion", cascade="all", mappedBy="portionBucket")
   */
  private $portions;
  function __construct() {
    $this->portions = new ArrayCollection();
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getPortions() {
    return $this->portions;
  }
  public function setPortions($portions) {
    $this->portions = $portions;
    return $this;
  }
  public function getAmount() {
    return $this->amount;
  }
  public function setAmount($amount) {
    $this->amount = $amount;
    return $this;
  }
}
