<?php

namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Order.php';

/**
 * @Entity
 */
class Employee implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @Column(type="string")
   */
  private $name;
  
  /**
   * @OneToMany(targetEntity="Order", cascade="persist", mappedBy="employee")
   */
  private $orders;
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function getOrders() {
    return $this->orders;
  }
  public function setOrders($orders) {
    $this->orders = $orders;
    return $this;
  }
}
