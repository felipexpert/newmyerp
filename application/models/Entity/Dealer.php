<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Customer.php';
require_once './application/models/Entity/Consignment.php';

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 */
class Dealer implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @OneToOne(targetEntity="Customer")
   * @JoinColumn(name="customerId", referencedColumnName="id")
   **/
  private $customer;
  /**
   * @OneToMany(targetEntity="Consignment", cascade="persist", mappedBy="dealer")
   */
  private $consignments;
  function __construct() {
    $this->consignments = new ArrayCollection();
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getCustomer() {
    return $this->customer;
  }
  public function setCustomer($customer) {
    $this->customer = $customer;
    return $this;
  }
  public function getConsignments() {
    return $this->consignments;
  }
  public function setConsignments($consignments) {
    $this->consignments = $consignments;
    return $this;
  }
}
