<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/DisplayOrderable.php';
require_once './application/models/Entity/Disableable.php';
require_once './application/models/Entity/Order.php';
require_once './application/models/Entity/ProductSnapshot.php';
require_once './application/models/Entity/ProductFlow.php';
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 */
class OrderItem implements Indexable, DisplayOrderable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @ManyToOne(targetEntity="Order", inversedBy="orderItems")
   * @JoinColumn(name="orderId", referencedColumnName="id")
   */
  private $order;
  /**
   * @OneToOne(targetEntity="ProductSnapshot", inversedBy="orderItem")
   * @JoinColumn(name="productSnapshotId", referencedColumnName="id")
   **/
  private $productSnapshot;
  /**
   * @ManyToMany(targetEntity="ProductFlow")
   * @JoinTable(name="OrderItemProductFlow",
   *   joinColumns={@JoinColumn(name="orderItemId", referencedColumnName="id")},
   *   inverseJoinColumns={@JoinColumn(name="productFlowId", referencedColumnName="id")}
   *   )
   */ 
  private $productFlows;
  /**
   * @Column(type="datetime")
   */
  private $deliveredDate;
  /**
   * @Column(type="datetime", nullable=true)
   */
  private $printing;
  /**
   * @Column(type="integer")
   */
  private $displayOrder;
  /**
   * @Column(type="boolean")
   */
  private $disabled = FALSE;
  function __construct() {
    $this->productFlows = new ArrayCollection();
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getOrder() {
    return $this->order;
  }
  public function setOrder($order) {
    $this->order = $order;
    return $this;
  }
  public function getProductSnapshot() {
    return $this->productSnapshot;
  }
  public function setProductSnapshot($productSnapshot) {
    $this->productSnapshot = $productSnapshot;
    return $this;
  }
  public function getProductFlows() {
    return $this->productFlows;
  }
  public function setProductFlows($productFlows) {
    $this->productFlows = $productFlows;
    return $this;
  }
  public function getDeliveredDate() {
    return $this->deliveredDate;
  }
  public function setDeliveredDate($deliveredDate) {
    $this->deliveredDate = $deliveredDate;
    return $this;
  }
  public function getPrinting() {
    return $this->printing;
  }
  public function setPrinting($printing) {
    $this->printing = $printing;
    return $this;
  }
  public function getDisplayOrder() {
    return $this->displayOrder;
  }
  public function setDisplayOrder($displayOrder) {
    $this->displayOrder = $displayOrder;
    return $this;
  }
  public function isDisabled() {
    return $this->disabled;
  }
  public function setDisabled($disabled) {
    $this->disabled = $disabled;
    return $this;
  }
}
