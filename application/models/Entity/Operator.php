<?php namespace Entity;
class Operator {
  const PLUS = 1;
  const MINUS = 2;
  const PERCENTAGE = 3;
  private static $INSTANCES;
  static function init() {
    if(!self::$INSTANCES) self::$INSTANCES = array(
      self::PLUS => new Operator(self::PLUS, 'operator.plus', function($acc, $target, $scalar) {
        return $acc + $scalar;
      }),
      self::MINUS => new Operator(self::MINUS, 'operator.minus', function($acc, $target, $scalar) {
        return $acc - $scalar;
      }),
      self::PERCENTAGE => new Operator(self::PERCENTAGE, 'operator.percentage', function($acc, $target, $scalar) {
        $percentage = $scalar / 100;
        return $acc + $target * $percentage;
      })
    );
  }
  static function elt() { return self::$INSTANCES; }
  private $code;
  private $name;
  private $operateFunc;
  private function __construct($code, $name, $operateFunc) {
    $this->code = $code;
    $this->name = $name;
    $this->operateFunc = $operateFunc;
  }
  function getCode() {
    return $this->code;
  }
  function getName() {
    return lang($this->name);
  }
  function operate($acc, $target, $scalar) {
    $func = $this->operateFunc;
    return $func($acc, $target, $scalar); 
  }
}
Operator::init();
