<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/DisplayOrderable.php';
require_once './application/models/Entity/CharacteristicInstance.php';

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 */
class Characteristic implements Indexable, DisplayOrderable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @OneToMany(targetEntity="CharacteristicInstance", cascade="persist", mappedBy="characteristic")
   */
  private $instances;
  /**
   * @Column
   */
  private $name;
  /**
   * @Column(type="integer")
   */
  private $displayOrder;
  /**
   * @Column(type="boolean")
   */
  private $disabled;
  function __construct() {
    $this->instances = new ArrayCollection();
    $this->disabled = FALSE;
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getInstances() {
    return $this->instances;
  }
  public function setInstances($instances) {
    $this->instances = $instances;
    return $this;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function getDisplayOrder() {
    return $this->displayOrder;
  }
  public function setDisplayOrder($displayOrder) {
    $this->displayOrder = $displayOrder;
    return $this;
  }
  public function isDisabled() {
    return $this->disabled;
  }
  public function setDisabled($disabled) {
    $this->disabled = $disabled;
    return $this;
  }
}
