<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';

/**
 * @Entity
 */
class Printer implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @Column(type="string")
   */
  private $path;
  /**
   * @Column(type="string")
   */
  private $name;
  /**
   * @Column(type="integer")
   */
  private $printerType;
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getPath() {
    return $this->path;
  }
  public function setPath($path) {
    $this->path = $path;
    return $this;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function getPrinterType() {
    return PrinterType::elt()[$this->printerType];
  }
  public function setPrinterType($printerType) {
    $this->printerType = $printerType;
    return $this;
  }
}
