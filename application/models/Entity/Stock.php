<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Disableable.php';
require_once './application/models/Entity/StockProd.php';
require_once './application/models/Entity/ProductFlow.php';

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 */
class Stock implements Indexable, Disableable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @Column
   */
  private $name;
  /**
   * @OneToMany(targetEntity="StockProd", cascade="persist", mappedBy="stock")
   */
  private $stockProds;
  /**
   * @OneToMany(targetEntity="ProductFlow", cascade="persist", mappedBy="stock")
   */
  private $productFlows;
  /**
   * @Column(type="boolean")
   */
  private $disabled;
  function __construct() {
    $this->stockProds = new ArrayCollection();
    $this->productFlows = new ArrayCollection();
    $this->disabled = FALSE;
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function getStockProds() {
    return $this->stockProds;
  }
  public function setStockProds($stockProds) {
    $this->stockProds = $stockProds;
    return $this;
  }
  public function getProductFlows() {
    return $this->productFlows;
  }
  public function setProductFlows($productFlows) {
    $this->productFlows = $productFlows;
    return $this;
  }
  public function isDisabled() {
    return $this->disabled;
  }
  public function setDisabled($disabled) {
    $this->disabled = $disabled;
    return $this;
  }
}
