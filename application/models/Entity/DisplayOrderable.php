<?php namespace Entity;
require_once './application/models/Entity/Disableable.php';
interface DisplayOrderable extends Disableable {
  function getDisplayOrder();
  function setDisplayOrder($displayOrder);
}
