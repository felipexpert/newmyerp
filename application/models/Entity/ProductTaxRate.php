<?php

namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Product.php';
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 */
class ProductTaxRate implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @OneToMany(targetEntity="Product", cascade="persist", mappedBy="productTaxRate")
   */
  private $products;
  /**
   * @Column
   */
  private $description;
  /**
   * @Column
   */
  private $value;
  function __construct() {
    $this->products = new ArrayCollection();
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getProducts() {
    return $this->products;
  }
  public function setProducts($products) {
    $this->products = $products;
    return $this;
  }
  public function getDescription() {
    return $this->description;
  }
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }
  public function getValue() {
    return $this->value;
  }
  public function setValue($value) {
    $this->value = $value;
    return $this;
  }
}
