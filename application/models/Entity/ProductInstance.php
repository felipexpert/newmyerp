<?php

namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Product.php';
require_once './application/models/Entity/CharacteristicInstance.php';
require_once './application/models/Entity/ProductSnapshot.php';
require_once './application/models/Entity/ProductFlow.php';
require_once './application/models/Entity/StockProd.php';

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 * @Table(indexes={@Index(name="codeIdx", columns={"code"}), @Index(name="barCodeIdx", columns={"barCode"})})
 */
class ProductInstance implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @ManyToOne(targetEntity="Product", inversedBy="instances")
   * @JoinColumn(name="productId", referencedColumnName="id")
   */
  private $product;
  /**
   * @ManyToMany(targetEntity="CharacteristicInstance", inversedBy="instances")
   * @JoinTable(name="ProductCharacteristic",
   *   joinColumns={@JoinColumn(name="productInstanceId", referencedColumnName="id")},
   *   inverseJoinColumns={@JoinColumn(name="characteristicInstanceId", referencedColumnName="id")}
   *   )
   */ 
  private $characteristics;
  /**
   * @OneToMany(targetEntity="ProductSnapshot", cascade="persist", mappedBy="productInstance")
   */
  private $snapshots;
  /**
   * @OneToMany(targetEntity="ProductFlow", cascade="persist", mappedBy="productInstance")
   */
  private $productFlows;
  /**
   * @OneToMany(targetEntity="StockProd", cascade="persist", mappedBy="productInstance")
   */
  private $stockProds;
  /**
   * @Column(type="string")
   */
  private $code;
  /**
   * @Column(type="string")
   */
  private $barCode;
  /**
   * @Column(type="float")
   */
  private $price;
  /**
   * @Column(type="float")
   */
  private $buyingPrice;
  /**
   * @Column(type="boolean")
   */
  private $proportional;
  /**
   * @Column(type="text")
   */
  private $obs;
  /**
   * @Column(type="boolean")
   */
  private $disabled;
  /**
   * @Column(type="float")
   */
  private $initialStock;
  function __construct() {
    $this->characteristics = new ArrayCollection();
    $this->snapshots = new ArrayCollection();
    $this->productFlows = new ArrayCollection();
    $this->stockProds = new ArrayCollection();
    $this->disabled = FALSE;
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getProduct() {
    return $this->product;
  }
  public function setProduct($product) {
    $this->product = $product;
    return $this;
  }
  public function getCharacteristics() {
    return $this->characteristics;
  }
  public function setCharacteristics($characteristics) {
    $this->characteristics = $characteristics;
    return $this;
  }
  public function getSnapshots() {
    return $this->snapshots;
  }
  public function setSnapshots($snapshots) {
    $this->snapshots = $snapshots;
    return $this;
  }
  public function getProductFlows() {
    return $this->productFlows;
  }
  public function setProductFlows($productFlows) {
    $this->productFlows = $productFlows;
    return $this;
  }
  public function getStockProds() {
    return $this->stockProds;
  }
  public function setStockProds($stockProds) {
    $this->stockProds = $stockProds;
    return $this;
  }
  public function getCode() {
    return $this->code;
  }
  public function setCode($code) {
    $this->code = $code;
    return $this;
  }
  public function getBarCode() {
    return $this->barCode;
  }
  public function setBarCode($barCode) {
    $this->barCode = $barCode;
    return $this;
  }
  public function getPrice() {
    return $this->price;
  }
  public function setPrice($price) {
    $this->price = $price;
    return $this;
  }
  public function getBuyingPrice() {
    return $this->buyingPrice;
  }
  public function setBuyingPrice($buyingPrice) {
    $this->buyingPrice = $buyingPrice;
    return $this;
  }
  public function isProportional() {
    return $this->proportional;
  }
  public function setProportional($proportional) {
    $this->proportional = $proportional;
    return $this;
  }
  public function getObs() {
    return $this->obs;
  }
  public function setObs($obs) {
    $this->obs = $obs;
    return $this;
  }
  public function isDisabled() {
    return $this->disabled;
  }
  public function setDisabled($disabled) {
    $this->disabled = $disabled;
    return $this;
  }
  public function getInitialStock() {
    return $this->initialStock;
  }
  public function setInitialStock($initialStock) {
    $this->initialStock = $initialStock;
    return $this;
  }
}
