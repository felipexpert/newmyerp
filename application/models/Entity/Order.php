<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/OrderItem.php';
require_once './application/models/Entity/Employee.php';
require_once './application/models/Entity/User.php';
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 * @Table(name="`Order`")
 */
class Order implements Indexable, Disableable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @ManyToOne(targetEntity = "User")
   * @JoinColumn(name="userId", referencedColumnName="id")
   */
  private $user;
  /**
   * @ManyToOne(targetEntity="Employee", inversedBy="orders")
   * @JoinColumn(name="employeeId", referencedColumnName="id")
   */
  private $employee;
  /**
   * @OneToMany(targetEntity="OrderItem", cascade="persist", mappedBy="order")
   */
  private $orderItems;
  /**
   * @Column(type="string")
   */
  private $obs;
  /**
   * @Column(type="datetime")
   */
  private $creation;
  /**
   * @Column(type="float")
   */
  private $discount;
  /**
   * @Column(type="float")
   */
  private $secondDiscount;
  /**
   * @Column(type="boolean")
   */
  private $disabled = FALSE;
  function __construct() {
    $this->orderItems = new ArrayCollection();
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getCreation() {
    return $this->creation;
  }
  public function setCreation($creation) {
    $this->creation = $creation;
    return $this;
  }
  public function getUser() {
    return $this->user;
  }
  public function setUser($user) {
    $this->user = $user;
    return $this;
  }
  public function getOrderItems() {
    return $this->orderItems;
  }
  public function setOrderItems($orderItems) {
    $this->orderItems = $orderItems;
    return $this;
  }
  public function getObs() {
    return $this->obs;
  }
  public function setObs($obs) {
    $this->obs = $obs;
    return $this;
  }
  public function getEmployee() {
    return $this->employee;
  }
  public function setEmployee($employee) {
    $this->employee = $employee;
    return $this;
  }
  public function getDiscount() {
    return $this->discount;
  }
  public function setDiscount($discount) {
    $this->discount = $discount;
    return $this;
  }
  public function getSecondDiscount() {
    return $this->secondDiscount;
  }
  public function setSecondDiscount($secondDiscount) {
    $this->secondDiscount = $secondDiscount;
    return $this;
  }
  public function isDisabled() {
    return $this->disabled;
  }
  public function setDisabled($disabled) {
    $this->disabled = $disabled;
    return $this;
  }
}
