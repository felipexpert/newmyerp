<?php

namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/ProductTaxRate.php';
/**
 * @Entity
 */
class Conf implements Indexable {
  
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @Column(type="integer")
   */
  private $barCodeLength;
  /**
   * @ManyToOne(targetEntity="ProductTaxRate")
   * @JoinColumn(name="productTaxRateId", referencedColumnName="id")
   */
  private $productTaxRate;
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getBarCodeLength() {
    return $this->barCodeLength;
  }
  public function setBarCodeLength($barCodeLength) {
    $this->barCodeLength = $barCodeLength;
    return $this;
  }
  public function getProductTaxRate() {
    return $this->productTaxRate;
  }
  public function setProductTaxRate($ptr) {
    $this->productTaxRate = $ptr;
    return $this;
  }
}
