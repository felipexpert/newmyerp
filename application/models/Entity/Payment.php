<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Portion.php';
require_once './application/models/Entity/Penalty.php';
require_once './application/models/Entity/Reversal.php';

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 */
class Payment implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;

  /**
   * @ManyToOne(targetEntity="PaymentType", inversedBy="payments")
   * @JoinColumn(name="paymentTypeId", referencedColumnName="id")
   */
  private $paymentType;
  /**
   * @ManyToOne(targetEntity="Portion", inversedBy="payments")
   * @JoinColumn(name="portionId", referencedColumnName="id")
   */
  private $portion;
  /**
   * @OneToMany(targetEntity="Penalty", cascade="persist", mappedBy="payment")
   */
  private $penalities;
  /**
   * @OneToOne(targetEntity="Reversal", inversedBy="payment")
   * @JoinColumn(name="reversalId", referencedColumnName="id")
   **/
  private $reversal;
  /**
   * @Column(type="float")
   */
  private $amount;
  /**
   * @Column(type="datetime", nullable=true)
   */
  private $date;
  /**
   * @Column(type="datetime")
   */
  private $creation;
  function __construct() {
    $this->penalities = new ArrayCollection ();
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getPaymentType() {
    return $this->paymentType;
  }
  public function setPaymentType($paymentType) {
    $this->paymentType = $paymentType;
    return $this;
  }
  public function getPortion() {
    return $this->portion;
  }
  public function setPortion($portion) {
    $this->portion = $portion;
    return $this;
  }
  public function getPenalities() {
    return $this->penalities;
  }
  public function setPenalities($penalities) {
    $this->penalities = $penalities;
    return $this;
  }
  public function getReversal() {
    return $this->reversal;
  }
  public function setReversal($reversal) {
    $this->reversal = $reversal;
    return $this;
  }
  public function getAmount() {
    return $this->amount;
  }
  public function setAmount($amount) {
    $this->amount = $amount;
    return $this;
  }
  public function getDate() {
    return $this->date;
  }
  public function setDate($date) {
    $this->date = $date;
    return $this;
  }
  public function getCreation() {
    return $this->creation;
  }
  public function setCreation($creation) {
    $this->creation = $creation;
    return $this;
  }
}
