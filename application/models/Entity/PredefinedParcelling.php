<?php namespace Entity;
require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/PredefinedInterval.php';
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 */
class PredefinedParcelling implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @OneToMany(targetEntity="PredefinedInterval", cascade="persist", mappedBy="predefinedParcelling")
   */
  private $predefinedIntervals;
  /**
   * @Column
   */
  private $name;
  function __construct() {
    $this->predefinedIntervals = new ArrayCollection();
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getPredefinedIntervals() {
    return $this->predefinedIntervals;
  }
  public function setPredefinedIntervals($predefinedIntervals) {
    $this->predefinedIntervals = $predefinedIntervals;
    return $this;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
}
