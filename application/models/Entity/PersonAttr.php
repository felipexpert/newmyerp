<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
/**
 * @Entity
 */
class PersonAttr implements Indexable {
  
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @Column
   */
  private $name;
  /**
   * @Column(nullable=true)
   */
  private $rg;
  /**
   * @Column(nullable=true)
   */
  private $cpf;
  /**
   * @Column(nullable=true)
   */
  private $address;
  /**
   * @Column(nullable=true)
   */
  private $fone;
  /**
   * @Column(type="datetime")
   */
  private $creation;
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function getRg() {
    return $this->rg;
  }
  public function setRg($rg) {
    $this->rg = $rg;
    return $this;
  }
  public function getCpf() {
    return $this->cpf;
  }
  public function setCpf($cpf) {
    $this->cpf = $cpf;
    return $this;
  }
  public function getAddress() {
    return $this->address;
  }
  public function setAddress($address) {
    $this->address = $address;
    return $this;
  }
  public function getFone() {
    return $this->fone;
  }
  public function setFone($fone) {
    $this->fone = $fone;
    return $this;
  }
  public function getCreation() {
    return $this->creation;
  }
  public function setCreation($creation) {
    $this->creation = $creation;
    return $this;
  }
}
