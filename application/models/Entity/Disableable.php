<?php namespace Entity;

interface Disableable {
  function isDisabled();
  function setDisabled($disabled);
}
