<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Payment.php';

/**
 * @Entity
 */
class Reversal implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @OneToOne(targetEntity="Payment", mappedBy="reversal")
   **/
  private $payment;
  /**
   * @Column(type="datetime")
   */
  private $date;
  /**
   * @Column(type="datetime")
   */
  private $close;
  /**
   * @Column(type="string")
   */
  private $obs;
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getPayment() {
    return $this->payment;
  }
  public function setPayment($payment) {
    $this->payment = $payment;
    return $this;
  }
  public function getDate() {
    return $this->date;
  }
  public function setDate($date) {
    $this->date = $date;
    return $this;
  }
  public function getObs() {
    return $this->obs;
  }
  public function setObs($obs) {
    $this->obs = $obs;
    return $this;
  }
}
