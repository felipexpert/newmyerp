<?php

namespace Entity;

require_once './application/models/Entity/Indexable.php';
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 */
class ProductCategory implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @OneToMany(targetEntity="Product", cascade="persist", mappedBy="productCategory")
   */
  private $products;
  /**
   * @Column(type="string")
   */
  private $name;
  /**
   * @ManyToOne(targetEntity="Printer")
   * @JoinColumn(name="printerId", referencedColumnName="id")
   */
  private $printer;  
  function __construct() {
    $this->products = new ArrayCollection();
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getProducts() {
    return $this->products;
  }
  public function setProducts($products) {
    $this->products = $products;
    return $this;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function getPrinter() { return $this->printer; }
  public function setPrinter($printer) { 
    $this->printer = $printer;
    return $this;
  }
}
