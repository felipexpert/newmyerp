<?php

namespace Entity;

class FlowType {
  const OUT_SOLD = 1,
        OUT_CONSIGNED = 101,
        IN_CONSIGNED = 102,
        OUT_CONSIGNED_OFFSET = 103,
        IN_CONSIGNED_OFFSET = 104;

  static function getName($f) {
    switch($f) {
    case 1  : return 'OUT_SOLD';
    case 101: return 'OUT_CONSIGNED';
    case 102: return 'IN_CONSIGNED';
    case 103: return 'OUT_CONSIGNED_OFFSET';
    case 104: return 'IN_CONSIGNED_OFFSET';
    case 201: return 'IN_PURCHASE';
    case 202: return 'IN_MANUAL';
    default: throw new Exception('Invalid flowType');
    }
  }
}
