<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/OrderItem.php';
require_once './application/models/Entity/ProductInstance.php';
/**
 * @Entity
 */
class ProductSnapshot implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @OneToOne(targetEntity="OrderItem", mappedBy="productSnapshot")
   **/
  private $orderItem;
  /**
   * @ManyToOne(targetEntity="ProductInstance", inversedBy="snapshots")
   * @JoinColumn(name="productInstanceId", referencedColumnName="id")
   */
  private $productInstance;
  /**
   * @Column
   */
  private $name;
  /**
   * @Column(type="float")
   */
  private $price;
  /**
   * @Column(type="float")
   */
  private $buyingPrice;
  /**
   * @Column(type="float")
   */
  private $discount;
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getOrderItem() {
    return $this->orderItem;
  }
  public function setOrderItem($orderItem) {
    $this->orderItem = $orderItem;
    return $this;
  }
  public function getProductInstance() {
    return $this->productInstance;
  }
  public function setProductInstance($productInstance) {
    $this->productInstance = $productInstance;
    return $this;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function getPrice() {
    return $this->price;
  }
  public function setPrice($price) {
    $this->price = $price;
    return $this;
  }
  public function getBuyingPrice() {
    return $this->buyingPrice;
  }
  public function setBuyingPrice($buyingPrice) {
    $this->buyingPrice = $buyingPrice;
    return $this;
  }
  public function getDiscount() {
    return $this->discount;
  }
  public function setDiscount($discount) {
    $this->discount = $discount;
    return $this;
  }
}
