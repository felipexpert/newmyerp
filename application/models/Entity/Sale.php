<?php namespace Entity;
require_once './application/models/Entity/Order.php';
require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/SellingType.php';
require_once './application/models/Entity/Payer.php';
require_once './application/models/Entity/SaleAggregate.php';
require_once './application/models/Entity/SellingType.php';
require_once './application/models/Entity/Consignment.php';
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 */
class Sale implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @OneToOne(targetEntity="Order")
   * @JoinColumn(name="orderId", referencedColumnName="id")
   **/
  private $order;
  /**
   * @ManyToOne(targetEntity="Consignment")
   * @JoinColumn(name="consignmentId", referencedColumnName="id")
   */
  private $consignment;
  /**
   * @Column(type="string", nullable=true)
   */
  private $identifier;
  /**
   * @Column(type="integer", nullable=true)
   */
  private $orderNumber;
  /**
   * @OneToMany(targetEntity="SaleAggregate", cascade="persist", mappedBy="sale")
   */
  private $aggregates;
  /**
   * @Column(type="integer", nullable=true)
   */
  private $sellingType;
  /**
   * @Column(type="datetime", nullable=true)
   */
  private $closing;
  /**
   * @OneToMany(targetEntity="Payer", cascade="persist", mappedBy="sale")
   */
  private $payers;
  function __construct() {
    $this->aggregates = new ArrayCollection();
    $this->payers = new ArrayCollection();
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getOrder() {
    return $this->order;
  }
  public function setOrder($order) {
    $this->order = $order;
    return $this;
  }
  public function getConsignment() {
    return $this->consignment;
  }
  public function setConsignment($consignment) {
    $this->consignment = $consignment;
    return $this;
  }
  public function getIdentifier() {
    return $this->identifier;
  }
  public function setIdentifier($identifier) {
    $this->identifier = $identifier;
    return $this;
  }
  public function getOrderNumber() {
    return $this->orderNumber;
  }
  public function setOrderNumber($orderNumber) {
    $this->orderNumber = $orderNumber;
    return $this;
  }
  public function getAggregates() {
    return $this->aggregates;
  }
  public function setAggregates($aggregates) {
    $this->aggregates = $aggregates;
    return $this;
  }
  public function getSellingType() {
    return SellingType::elt()[$this->sellingType];
  }
  public function setSellingType($sellingType) {
    $this->sellingType = $sellingType;
    return $this;
  }
  public function getClosing() {
    return $this->closing;
  }
  public function setClosing($closing) {
    $this->closing = $closing;
    return $this;
  }
  public function getPayers() {
    return $this->payers;
  }
  public function setPayers($payers) {
    $this->payers = $payers;
    return $this;
  }
}
