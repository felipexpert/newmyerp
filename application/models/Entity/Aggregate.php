<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Disableable.php';
require_once './application/models/Entity/Operator.php';
require_once './application/models/Entity/SaleAggregate.php';

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 */
class Aggregate implements Indexable, Disableable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @OneToMany(targetEntity="SaleAggregate", cascade="persist", mappedBy="aggregate")
   */
  private $sales;
  /**
   * @Column(type="integer")
   */
  private $operator;
  /**
   * @Column
   */
  private $name;
  /**
   * @Column(type="float");
   */
  private $scalar;
  /**
   * @Column(type="datetime", nullable=true)
   */
  private $validUntil;
  /**
   * @Column(type="boolean")
   */
  private $disabled;
  function __construct() {
    $this->sales = new ArrayCollection();
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getSales() {
    return $this->sales;
  }
  public function setSales($sales) {
    $this->sales = $sales;
    return $this;
  }
  public function getOperator() {
    return Operator::elt()[$this->operator];
  }
  public function setOperator($operator) {
    $this->operator = $operator;
    return $this;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function getScalar() {
    return $this->scalar;
  }
  public function setScalar($scalar) {
    $this->scalar = $scalar;
    return $this;
  }
  public function getValidUntil() {
    return $this->validUntil;
  }
  public function setValidUntil($validUntil) {
    $this->validUntil = $validUntil;
    return $this;
  }
  public function isDisabled() {
    return $this->disabled;
  }
  public function setDisabled($disabled) {
    $this->disabled = $disabled;
    return $this;
  }
}
