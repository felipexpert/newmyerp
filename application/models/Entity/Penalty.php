<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Payment.php';
require_once './application/models/Entity/PenaltyType.php';
/**
 * @Entity
 */
class Penalty implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @ManyToOne(targetEntity="Payment", inversedBy="penalties")
   * @JoinColumn(name="paymentId", referencedColumnName="id")
   */
  private $payment;
  /**
   * @Column(type="float")
   */
  private $amount;
  /**
   * @Column(type="integer")
   */
  private $penaltyType;
  function getPenaltyType() {
    return PenaltyType::elt()[$this->penaltyType];
  }
  function setPenaltyType($penaltyTypeCode) {
    $this->penaltyType = $penaltyTypeCode;
    return $this;
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getPayment() {
    return $this->payment;
  }
  public function setPayment($payment) {
    $this->payment = $payment;
    return $this;
  }
  public function getAmount() {
    return $this->amount;
  }
  public function setAmount($amount) {
    $this->amount = $amount;
    return $this;
  }
 
  
}
