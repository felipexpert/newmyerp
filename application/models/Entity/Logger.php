<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';

/**
 * @Entity
 */
class Logger implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @Column(type="text")
   */
  private $message;
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getMessage() {
    return $this->message;
  }
  public function setMessage($message) {
    $this->message = $message;
    return $this;
  }
}
