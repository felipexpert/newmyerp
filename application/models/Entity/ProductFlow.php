<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Disableable.php';
require_once './application/models/Entity/Stock.php';
require_once './application/models/Entity/ProductInstance.php';
require_once './application/models/Entity/FlowType.php';
/**
 * @Entity
 */
class ProductFlow implements Indexable, Disableable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @ManyToOne(targetEntity="Stock", inversedBy="productFlows")
   * @JoinColumn(name="stockId", referencedColumnName="id")
   */
  private $stock;
  /**
   * @ManyToOne(targetEntity="ProductInstance", inversedBy="productFlows")
   * @JoinColumn(name="productInstanceId", referencedColumnName="id")
   */
  private $productInstance;
  /**
   * @Column(type="integer")
   */
  private $flowType;
  /**
   * @Column(type="float")
   */
  private $amount;
  /**
   * @Column(type="datetime")
   */
  private $creation;
  /**
   * @Column(type="boolean")
   */
  private $disabled;
  function __construct() {
    $this->disabled = FALSE;
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getStock() {
    return $this->stock;
  }
  public function setStock($stock) {
    $this->stock = $stock;
    return $this;
  }
  public function getProductInstance() {
    return $this->productInstance;
  }
  public function setProductInstance($productInstance) {
    $this->productInstance = $productInstance;
    return $this;
  }
  public function getFlowType() {
    return $this->flowType;
  }
  public function setFlowType($flowType) {
    $this->flowType = $flowType;
    return $this;
  }
  public function getAmount() {
    return $this->amount;
  }
  public function setAmount($amount) {
    $this->amount = $amount;
    return $this;
  }
  public function getCreation() {
    return $this->creation;
  }
  public function setCreation($creation) {
    $this->creation = $creation;
    return $this;
  }
  public function isDisabled() {
    return $this->disabled;
  }
  public function setDisabled($disabled) {
    $this->disabled = $disabled;
    return $this;
  }
}
