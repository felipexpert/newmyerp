<?php

namespace Entity;

class IntervalType {
  const DAY = 1;
  const WEEK = 2;
  const MONTH = 3;
  const YEAR = 4;
  private static $INSTANCES;
  static function init() {
    if(!self::$INSTANCES) self::$INSTANCES = 
      [ self::DAY => new IntervalType(self::DAY, 'day')
      , self::WEEK => new IntervalType(self::WEEK, 'week')
      , self::MONTH => new IntervalType(self::MONTH, 'month')
      , self::YEAR => new IntervalType(self::YEAR, 'year')];
      
  }
  static function elt() { return self::$INSTANCES; }
  private $code;
  private $name;
  function __construct($code, $name) {
    $this->code = $code;
    $this->name = $name;
  }
  public function getCode() {
    return $this->code;
  }
  public function getName() {
    return lang($this->name);
  }
}
IntervalType::init();
