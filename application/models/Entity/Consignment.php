<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Order.php';
require_once './application/models/Entity/Dealer.php';
require_once './application/models/Entity/Sale.php';

/**
 * @Entity
 */
class Consignment implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @OneToOne(targetEntity="Order")
   * @JoinColumn(name="orderId", referencedColumnName="id")
   **/
  private $order;
  /**
   * @ManyToOne(targetEntity="Dealer")
   * @JoinColumn(name="dealerId", referencedColumnName="id")
   **/
  private $dealer;
  /**
   * @OneToOne(targetEntity="Sale")
   * @JoinColumn(name="saleId", referencedColumnName="id")
   **/
  private $sale;
  /**
   * @Column(type="datetime", nullable=true)
   */
  private $previsionClosing;
  /**
   * @Column(type="datetime", nullable=true)
   */
  private $closing;
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getOrder() {
    return $this->order;
  }
  public function setOrder($order) {
    $this->order = $order;
    return $this;
  }
  public function getDealer() {
    return $this->dealer;
  }
  public function setDealer($dealer) {
    $this->dealer = $dealer;
    return $this;
  }
  public function getSale() {
    return $this->sale;
  }
  public function setSale($sale) {
    $this->sale = $sale;
    return $this;
  }
  public function getPrevisionClosing() {
    return $this->previsionClosing;
  }
  public function setPrevisionClosing($previsionClosing) {
    $this->previsionClosing = $previsionClosing;
    return $this;
  }
  public function getClosing() {
    return $this->closing;
  }
  public function setClosing($closing) {
    $this->closing = $closing;
    return $this;
  }
}
