<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/DisplayOrderable.php';
/**
 * @Entity
 */
use Doctrine\Common\Collections\ArrayCollection;
class PaymentType implements Indexable, DisplayOrderable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @OneToMany(targetEntity="Payment", cascade="persist", mappedBy="paymentType")
   */
  private $payments;
  /**
   * @Column
   */
  private $name;
  /**
   * @Column(length=2)
   */
  private $acronym;
  /**
   * @Column(type="integer")
   */
  private $displayOrder;
  /**
   * @Column(type="boolean")
   */
  private $disabled;
  function __construct() {
    $this->payments = new ArrayCollection();
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getPayments() {
    return $this->payments;
  }
  public function setPayments($payments) {
    $this->payments = $payments;
    return $this;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function getAcronym() {
    return $this->acronym;
  }
  public function setAcronym($acronym) {
    $this->acronym = $acronym;
    return $this;
  }
  public function getDisplayOrder() {
    return $this->displayOrder;
  }
  public function setDisplayOrder($displayOrder) {
    $this->displayOrder = $displayOrder;
    return $this;
  }
  public function isDisabled() {
    return $this->disabled;
  }
  public function setDisabled($disabled) {
    $this->disabled = $disabled;
    return $this;
  }
 
}
