<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Disableable.php';
require_once './application/models/Entity/Sale.php';
require_once './application/models/Entity/Aggregate.php';
/**
 * @Entity
 */
class SaleAggregate implements Indexable, Disableable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @ManyToOne(targetEntity="Sale", inversedBy="aggregates")
   * @JoinColumn(name="saleId", referencedColumnName="id")
   */
  private $sale;
  /**
   * @ManyToOne(targetEntity="Aggregate", inversedBy="sales")
   * @JoinColumn(name="aggregateId", referencedColumnName="id")
   */
  private $aggregate;
  /**
   * @Column(type="integer")
   */
  private $amount;
  /**
   * @Column(type="boolean")
   */
  private $disabled;
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getSale() {
    return $this->sale;
  }
  public function setSale($sale) {
    $this->sale = $sale;
    return $this;
  }
  public function getAggregate() {
    return $this->aggregate;
  }
  public function setAggregate($aggregate) {
    $this->aggregate = $aggregate;
    return $this;
  }
  public function getAmount() {
    return $this->amount;
  }
  public function setAmount($amount) {
    $this->amount = $amount;
    return $this;
  }
  public function isDisabled() {
    return $this->disabled;
  }
  public function setDisabled($disabled) {
    $this->disabled = $disabled;
    return $this;
  }
}
