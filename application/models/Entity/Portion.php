<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/PaymentType.php';
require_once './application/models/Entity/Payment.php';
require_once './application/models/Entity/Order.php';
require_once './application/models/Entity/PortionBucket.php';

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 */
class Portion implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @ManyToOne(targetEntity="PortionBucket", inversedBy="portions")
   * @JoinColumn(name="portionBucketId", referencedColumnName="id")
   */
  private $portionBucket;
  /**
   * @ManyToOne(targetEntity="PaymentType")
   * @JoinColumn(name="paymentTypeId", referencedColumnName="id")
   **/
  private $paymentType;
  /**
   * @OneToMany(targetEntity="Payment", cascade="all", mappedBy="portion")
   */
  private $payments;
  /**
   * @Column(type="float")
   */
  private $total;
  /**
   * @Column(type="datetime", nullable=true)
   */
  private $validUntil;
  /**
   * @Column(type="datetime")
   */
  private $creation;
  function __construct() {
    $this->payments = new ArrayCollection();
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getPortionBucket() {
    return $this->portionBucket;
  }
  public function setPortionBucket($portionBucket) {
    $this->portionBucket = $portionBucket;
    return $this;
  }
  public function getPaymentType() {
    return $this->paymentType;
  }
  public function setPaymentType($paymentType) {
    $this->paymentType = $paymentType;
    return $this;
  }
  public function getPayments() {
    return $this->payments;
  }
  public function setPayments($payments) {
    $this->payments = $payments;
    return $this;
  }
  public function getTotal() {
    return $this->total;
  }
  public function setTotal($total) {
    $this->total = $total;
    return $this;
  }
  public function getCreation() {
    return $this->creation;
  }
  public function setCreation($creation) {
    $this->creation = $creation;
    return $this;
  }
  public function getValidUntil() {
    return $this->validUntil;
  }
  public function setValidUntil($validUntil) {
    $this->validUntil = $validUntil;
    return $this;
  }
}
