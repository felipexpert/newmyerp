<?php namespace Entity;

require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/Disableable.php';
require_once './application/models/Entity/Characteristic.php';
require_once './application/models/Entity/ProductInstance.php';

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 */
class CharacteristicInstance implements Indexable, Disableable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @ManyToOne(targetEntity="Characteristic", inversedBy="instances")
   * @JoinColumn(name="characteristicId", referencedColumnName="id")
   */
  private $characteristic;
  /**
   * @ManyToMany(targetEntity="ProductInstance", mappedBy="characteristics")
   */
  private $instances;
  /**
   * @Column
   */
  private $name;
  /**
   * @Column(type="boolean")
   */
  private $disabled;
  function __construct() {
    $this->instances = new ArrayCollection();
    $this->disabled = FALSE;
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getCharacteristic() {
    return $this->characteristic;
  }
  public function setCharacteristic($characteristic) {
    $this->characteristic = $characteristic;
    return $this;
  }
  public function getInstances() {
    return $this->instances;
  }
  public function setInstances($instances) {
    $this->instances = $instances;
    return $this;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function isDisabled() {
    return $this->disabled;
  }
  public function setDisabled($disabled) {
    $this->disabled = $disabled;
    return $this;
  }
}
