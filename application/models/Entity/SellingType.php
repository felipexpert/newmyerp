<?php

namespace Entity;

class SellingType {
  const COMMON_SELLING = 1;
  const FAST_SELLING = 2;
  const TAX_REQUEST = 3;
  private static $INSTANCES;
  static function init() {
    if(!self::$INSTANCES) self::$INSTANCES = array (
        self::COMMON_SELLING => new SellingType ( self::COMMON_SELLING, 'orderType.commonSelling' ),
        self::FAST_SELLING => new SellingType ( self::FAST_SELLING, 'orderType.fastSelling' ),
        self::TAX_REQUEST => new SellingType ( self::TAX_REQUEST, 'orderType.taxRequest' ) 
    );
  }
  static function elt() { return self::$INSTANCES; }
  private $code;
  private $name;
  function __construct($code, $name) {
    $this->code = $code;
    $this->name = $name;
  }
  public function getCode() {
    return $this->code;
  }
  public function getName() {
    return lang ( $this->name );
  }
}
SellingType::init();
