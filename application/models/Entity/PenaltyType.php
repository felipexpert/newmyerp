<?php namespace Entity;

class PenaltyType {
  const MULCT = 1;
  const INTEREST = 2;
  private static $INSTANCES;
  static function init() {
    if(!self::$INSTANCES) self::$INSTANCES = array (
        self::MULCT => new PenaltyType(self::MULCT, 'penaltyType.mulct'),
        self::INTEREST => new PenaltyType(self::INTEREST, 'penaltyType.interest')
    );
  }
  static function elt() { return self::$INSTANCES; }
  private $code;
  private $name;
  private function __construct($code, $name) {
    $this->code = $code;
    $this->name = $name;
  }
  public function getCode() {
    return $this->code;
  }
  public function getName() {
    return lang($this->name);
  }
}

PenaltyType::init ();
