<?php namespace Entity;
class PrinterType {
  const STANDART = 1;
  const THERMAL = 2;
  private static $INSTANCES;
  static function init() {
    if(!self::$INSTANCES) self::$INSTANCES = array(
      self::STANDART => new PrinterType(self::STANDART, 'printer.standart', function($content) {
        //??????
      }),
      self::THERMAL  => new PrinterType(self::THERMAL, 'printer.thermal', function($content) {
        //??????
      })
    );
  }
  static function elt() { return self::$INSTANCES; }
  private $code;
  private $name;
  private $operateFunc;
  private function __construct($code, $name, $operateFunc) {
    $this->code = $code;
    $this->name = $name;
    $this->operateFunc = $operateFunc;
  }
  function getCode() {
    return $this->code;
  }
  function getName() {
    return lang($this->name);
  }
  function prt($content) {
    $func = $this->operateFunc;
    return $func($content); 
  }
}
PrinterType::init();
