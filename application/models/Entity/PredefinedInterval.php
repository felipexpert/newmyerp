<?php namespace Entity;
require_once './application/models/Entity/Indexable.php';
require_once './application/models/Entity/PredefinedParcelling.php';
require_once './application/models/Entity/IntervalType.php';

/**
 * @Entity
 */
class PredefinedInterval implements Indexable {
  /**
   * @Id @Column(type="integer")
   * @GeneratedValue
   */
  private $id;
  /**
   * @ManyToOne(targetEntity="PredefinedParcelling", inversedBy="predefinedIntervals")
   * @JoinColumn(name="predefinedParcellingId", referencedColumnName="id")
   */
  private $predefinedParcelling;
  /**
   * @Column(type="integer")
   */
  private $amount;
  /**
   * @Column(type="integer")
   */
  private $intervalType;

  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getPredefinedParcelling() {
    return $this->predefinedParcelling;
  }
  public function setPredefinedParcelling($predefinedParcelling) {
    $this->predefinedParcelling = $predefinedParcelling;
    return $this;
  }
  public function getAmount() {
    return $this->amount;
  }
  public function setAmount($amount) {
    $this->amount = $amount;
    return $this;
  }
  public function getIntervalType() {
    return IntervalType::elt()[$this->intervalType];
  }
  public function setIntervalType($intervalType) {
    $this->intervalType = $intervalType;
    return $this;
  }
}
