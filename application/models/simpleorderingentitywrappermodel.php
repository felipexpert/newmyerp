<?php

class SimpleOrderingEntityWrapperModel extends OrderingEntityWrapperModel {
  function __construct() {
    parent::__construct();
  }
  function updateOrderItemAmount($oi, $amount, $stockId = null) {
    parent::updateOrderItemAmount($oi, null, $amount, $stockId);
  }
  function addOrderItemFlow($oi, $amount, $stockId = null) {
    parent::addOrderItemFlow($oi, null, $amount, $stockId);
  }
  function getProductFlow() {
    return null;
  }
  function getPositiveFlows() {
    return [$this->getProductFlow()];
  }
}
