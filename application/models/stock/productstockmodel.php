<?php
use Doctrine\Common\Collections\ArrayCollection;
class ProductStockModel extends CI_Model {
  private $flows;
  function __construct() {
    parent::__construct();
  }
  function prepare($flows) {
    if(!$flows->count()) throw new Exception('Flows cannot be empty!');
    $fromSameProduct = function($flows) {
      $firstProdId = $flows->first()->getProductInstance()->getId();
      return $flows->forAll(function($k, $f) use($firstProdId) {
        return $f->getProductInstance()->getId() == $firstProdId;
      });    
    };
    if(!$fromSameProduct($flows)) throw new Exception('Flows cannot be from different products');
    $this->flows = $flows->filter(function($f) { return !$f->isDisabled(); });
    return $this;
  }
  function getProduct() {
    return $this->flows->first()->getProductInstance();
  }
  function getQuantity($flowType = null, $stockId = null) {
    $flows1 = isSet($stockId) 
      ? $this->flows->filter(function($f) use($stockId) { return $f->getStock()->getId() == $stockId; })
      : $this->flows;
    $flows2 = isSet($flowType)
      ? $flows1->filter(function($f) use($flowType) { return $f->getFlowType() == $flowType; })
      : $flows1;
    return C::foldl(function($acc, $f) {
      return $acc + $f->getAmount();  
    }, 0, $flows2);
  }
  function inStock($stockId = null) {
      
  }
  /** getValue($quantity) or getValue($flowType, $stockId) */
  function getValue($arg1, $arg2 = null) {
    if(isSet($arg2)) {
      $flowType = $arg1; $stockId = $arg2;
      return $this->getQuantity($flowType, $stockId) * $this->getProduct()->getPrice();
    }
    $quantity = $arg1;
    return $quantity * $this->getProduct()->getPrice();
  }
}
