
<?php
class StockGetterModel extends EntityGetterModel {
  function __construct() {
    parent::__construct();
  }
  function prepareAll() {
    $stocks = $this->em->createQuery('select s 
      from Entity\Stock s 
      left join s.productFlows pf
      left join pf.productInstance i
      where s.disabled = FALSE')->getResult();
    $this->prepare($stocks);
    return $this;
  }
}
