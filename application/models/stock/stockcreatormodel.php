<?php
class StockCreatorModel extends CI_Model {
  private $name;
  function create() {
    $stock = (new Entity\Stock())
      ->setName(ucFirst($this->name));
    $this->em->persist($stock);
    $this->em->flush();
    return $stock;
  }
  function setName($name) {
    $this->name = $name;
    return $this;
  }
}
