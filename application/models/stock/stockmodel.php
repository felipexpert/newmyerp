<?php
class StockModel extends EntityWrapperModel {
  const ENTITY_NAME = 'Entity\\Stock';
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
  }

  function map() {
    $s = $this->entity();
    return [
        'id' => $s->getId()
      , 'name' => $s->getName()
      , 'disabled' => $s->isDisabled()
    ];
  }
}
