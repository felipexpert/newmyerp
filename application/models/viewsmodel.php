<?php
class ViewsModel extends CI_Model {
  function simpleLoad($views) {
    self::check($views);
    $this->load->view('meta');
    C::fe(function($v, $k) {
      $this->load->view($k, $v);
    }, $views);
    $this->load->view('footer');
  }
  function fullLoad($views) {
    self::check($views);
    $this->simpleLoad(array_merge(['header' => []], $views));
  }
  private static function check(&$views) {
    if(!is_array($views)) $views = [$views => []];
  }
}
