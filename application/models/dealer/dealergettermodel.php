<?php
class DealerGetterModel extends EntityGetterModel {
  function prepareByCustomerId($cId) {
    $xs = $this->em->createQuery('select d, c, p from Entity\\Dealer d
      inner join d.customer c
      inner join c.personAttr p
      where c.id = :id')->setParameter('id', $cId)->getResult();
    $this->prepare($xs);
    return $this;
  }
  function prepareByFilter($items, $filter) {
    $xs = $this->em->createQuery('select d, c, p
      from Entity\\Dealer d
      inner join d.customer c
      inner join c.personAttr p
      where p.name like :name
      order by p.name')
      ->setParameter('name', "%${filter}%")
      ->setMaxResults($items)
      ->getResult();
    $this->prepare($xs);
    return $this;
  }
}
