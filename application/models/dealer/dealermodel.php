<?php
class DealerModel extends EntityWrapperModel {
  const ENTITY_NAME = 'Entity\\Dealer';

  private $customerModel;
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
    $this->load->model('customer/customerModel', '_customerModel');
    $this->customerModel = new CustomerModel();
  }
  protected function hook() {
    $this->customerModel->setEntity($this->entity()->getCustomer());
  } 
  function map() {
    $d = $this->entity();
    return [ 'id' => $d->getId()
      , 'customer' => $this->customerModel->map()];
  }
}
