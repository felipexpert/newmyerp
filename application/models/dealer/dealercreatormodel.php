<?php
class DealerCreatorModel extends CI_Model {
  private $customer;
  private $consignment;
  function __construct() {
    parent::__construct();
  }
  function create() {
    $dealer = (new Entity\Dealer())
      ->setCustomer($this->customer);
    if(isSet($this->consignment)) {
      $this->consignment->setDealer($dealer);
      $dealer->getConsignments()->add($this->consignment);
    }
    $this->em->persist($dealer);
    $this->em->flush();
    return $dealer;
  }
  function setCustomer($customer) {
    $this->customer = $customer;
    return $this;
  }
  function setConsignment($consignment) {
    $this->consignment = $consignment;
    return $this;
  }
}
