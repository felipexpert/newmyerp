<?php
class ProductTaxRateModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\ProductTaxRate";
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
  }
  function map() {
    $t = $this->entity();
    return [
      'id' => $t->getId()
    , 'description' => $t->getDescription()
    , 'value' => $t->getValue()
    ]; 
  }
}
