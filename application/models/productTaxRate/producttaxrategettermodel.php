<?php
class ProductTaxRateGetterModel extends EntityGetterModel {
  function prepareAll() {
    $ts = $this->em->createQuery('select t from Entity\ProductTaxRate t')->getResult();
    $this->prepare($ts);
    return $this;
  }
}
