<?php

/**
 * Javascript like
 */
final class Regex {
  private function __construct() {}
  static function test($regex, $text) {
    return preg_match($regex, $text) === 0 ? FALSE : TRUE;
  }
  static function exec($regex, $text) {
    $matches = Regex::exec2($regex, $text);
    $count = count($matches);
    if($count === 0) return null;
    $m = [];
    for($k = 0; $k < $count; $k++) $m[$k] = $matches[$k][0]; 
    $m['index'] = $matches[0][1];
    return $m;
  }
  static function exec2($regex, $text) {
    $matches = null;
    preg_match($regex, $text, $matches, PREG_OFFSET_CAPTURE);
    return $matches;
  }
}
class_alias('Regex', 'R');
