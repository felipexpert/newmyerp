<?php

final class Text {
  private function __construct() {}
  static function contains($substring, $string) {
    return strPos($string, $substring) !== FALSE;
  }
  static function indexOf($substring, $string) {
    return $pos === FALSE ? -1 : $pos;
  }
  static function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
  }
  static function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 
      0 && strpos($haystack, $needle, $temp) !== FALSE);
  }
  static function pad($text, $length, $paddingChar) {
    return str_pad($text, $length, $paddingChar, STR_PAD_LEFT); 
  }
}
class_alias('Text', 'T');
