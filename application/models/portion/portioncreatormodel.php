<?php
class PortionCreatorModel extends CI_Model {
  private $paymentType;
  private $portionBucket;
  private $total;
  private $validUntil;
  function __construct() {
    parent::__construct();
  }
  function create() {
    $portion = new Entity\Portion();
    $portion->setPaymentType($this->paymentType)
      ->setPortionBucket($this->portionBucket)
      ->setTotal($this->total)
      ->setValidUntil($this->validUntil)
      ->setCreation(new \DateTime('now'));
    $this->portionBucket->getPortions()->add($portion);
    $this->em->persist($portion);
    $this->em->flush();
    return $portion;   
  }
  function setPaymentType($paymentType) {
    $this->paymentType = $paymentType;
    return $this;
  }
  function setPortionBucket($portionBucket) {
    $this->portionBucket = $portionBucket;
    return $this;
  }
  function setTotal($total) {
    $this->total = $total;
    return $this;
  }
  function setValidUntil($validUntil) {
    $this->validUntil = $validUntil;
    return $this;
  }
}
