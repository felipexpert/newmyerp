<?php
use Doctrine\Common\Collections\ArrayCollection;
class PortionModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\Portion";
  private $paymentTypeModel;
  private $paymentModel;
  private $portionBucketModel;
  function __construct() {
    parent::__construct();
    $this->setEntityName(PortionModel::ENTITY_NAME);
    $this->load->model('paymentType/paymentTypeModel', '_paymentTypeModel');
    $this->paymentTypeModel = new PaymentTypeModel();
    $this->load->model('payment/paymentModel', '_paymentModel');
    $this->paymentModel = new PaymentModel();
  }
  function paidAmount() {
    $p = $this->entity()->getTotal() - $this->outstanding();
    //echo "{{important paidAmount! : ".$p." <<total: ".$this->entity()->getTotal().">> <<outstanding: "
    //  . $this->outstanding() .">>}}";
    return $p;
  }
  function outstanding() {
    $g = C::foldl(function($acc, $p) {
      return $acc + $p->getAmount();
    }, 0, $this->entity()->getPayments()->filter(function($payment) {
      return is_null($payment->getReversal());
    }));
    $f =  max(0, $this->entity()->getTotal() - $g);
    //echo 'outstanding: '.$f;
    //echo '  totalPaid: '. $g;
    return $f;
  }
  function getAmount() {
    return $this->entity()->getTotal(); 
  }
  function setAmount($amount) {
    $this->entity()->setTotal($amount);
    $this->em->flush();
    return $this;
  }
  function unchangeable() {
    return $this->paidAmount();
  }
  function isManipulable() {
    return N::eq($this->unchangeable(), 0);
  }
  function update() {} //Override parent update, which would be dangerous for newbie programmers
  function updateTotal($newTotal) {
    $this->load->model('calc/targetedRedistributorModel', 'targeted');
    $models = $this->entity()->getPortionBucket()->getPortions()->map(function($p) {
      return (new PortionModel())->setEntity($p);
    });
    $target = $models->filter(function($m) {
      return $m->entity() === $this->entity();
    })->first();
    $this->targeted->setItems($models)->setTarget($target)->clampAndPerform($newTotal);
  }
  function updateValidUntil($newTimeInMillis) {
    $this->entity()->setValidUntil(D::millisToDate($newTimeInMillis));
    $this->em->flush();
  }
  function canRemove() {
    return N::eq($this->paidAmount(), 0);
  }
  protected function unsafeRemove() {
    $this->load->model('portionBucket/portionBucketModel', '_portionBucketModel');
    $portionBucketModel = new PortionBucketModel();
    $portionBucket = $this->entity()->getPortionBucket();
    parent::unsafeRemove();
    $portionBucketModel->setEntity($portionBucket)->redistribute();
  }
  function map() {
    $portion = $this->entity();
    return 
      [ 'id' => $portion->getId()
      , 'total' => $portion->getTotal()
      , 'creation' => D::millis($portion->getCreation())
      , 'validUntil' => D::millis($portion->getValidUntil())
      , 'paymentType' => 
          $this->paymentTypeModel->setEntity($portion->getPaymentType())->hasEntity() ? $paymentTypeModel->map() : null
      , 'payments' => $portion->getPayments()->map(function($payment) {
        return $this->paymentModel->setEntity($payment)->map();
      })->toArray()];
  }
}
