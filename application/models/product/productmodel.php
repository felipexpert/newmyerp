<?php
class ProductModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\Product";
  function __construct() {
    parent::__construct();
    $this->setEntityName(static::ENTITY_NAME);
  }
  function map($extra = '') {
    $instanceModel = M::l('product/productInstance');
    $p = $this->entity();
    $mapped = [ 'id' => $p->getId()
      , 'name' => $p->getName()
      , 'category' => $this->maybeCategory()->map(function($c) {
        return M::l('productCategory/productCategory')->setEntity($c)->map();
      })->getOrNull()
      , 'brand' => $this->maybeBrand()->map(function($b) {
        return M::l('productBrand/productBrand')->setEntity($b)->map(); 
      })->getOrNull() 
      , 'taxRate' => $this->maybeTaxRate()->map(function($t) {
        return M::l('productTaxRate/productTaxRate')->setEntity($t)->map();
      })->getOrNull()
      , 'codePrefix' => $p->getCodePrefix()
      , 'defaultPrice' => $p->getDefaultPrice()
      , 'defaultBuyingPrice' => $p->getDefaultBuyingPrice()
      , 'obs' => $p->getObs()
      , 'disabled' => $p->isDisabled()];
    $instances = $p->getInstances();
    if(T::contains('onlyEnabled', $extra)) $instances = $instances->filter(function($i) { return !$i->isDisabled(); });
    $mapped['instances'] =  C::forceArray($instances->map(function($pi) use($instanceModel) {
      return $instanceModel->setEntity($pi)->map();
    }));
    return $mapped;
  }
  function put($obj) {
    $prodInstGetterModel = M::l('product/productInstanceGetter');
    $obj->instances = C::forceCollection($obj->instances);
    $instancesEnabled = $obj->instances->filter(function($i) { return !$i->disabled; });
    $eitherInside = Monad::chain(Either::ret(1), $instancesEnabled->map(function($i) use($instancesEnabled) {
      return function($x) use($instancesEnabled, $i) {
        if($instancesEnabled->filter(function($i2) use($i) { return $i->code == $i2->code; })->count() > 1) 
          return Either::fail(sprintf(lang('product.codeBeingUsedInside'), $i->code));
        elseif($instancesEnabled->filter(function($i2) use($i) { return $i->barCode == $i2->barCode; })->count() > 1)
          return Either::fail(sprintf(lang('product.barCodeBeingUsedInside'), $i->barCode));
        return Either::ret($x);
      };
    }));
    $eitherAll = Monad::chain($eitherInside, $instancesEnabled->map(function($i) use($prodInstGetterModel, $obj) {
      return function($x) use($prodInstGetterModel, $obj, $i) {
        if($prodInstGetterModel->prepareByCode($i->code)->has()
          && $prodInstGetterModel->first()->getProduct()->getId() != $obj->id
          && (is_null($i->id) || $prodInstGetterModel->first()->getId() != $i->id)
        ) return Either::fail(sprintf(lang('product.codeBeingUsedOutside'), $i->code));
        elseif($prodInstGetterModel->prepareByBarCode($i->barCode)->has()
          && $prodInstGetterModel->first()->getProduct()->getId() != $obj->id
          && (is_null($i->id) || $prodInstGetterModel->first()->getId() != $i->id)
        ) return Either::fail(sprintf(lang('product.barCodeBeingUsedOutside'), $i->barCode));
        return Either::ret($x);
      };
    }));
    return $eitherAll->map(function() use($obj) {
      $productInstanceModel = M::l('product/productInstance');
      M::l('product/productInstanceCreator');
      $p = $this->entity();
      $this->em->persist($p);
      $p->setName($obj->name)
        ->setProductCategory($obj->maybeCategoryId->map(function($id) {
          return M::l('productCategory/productCategory')->prepareById($id)->entity(); 
        })->getOrNull())
        ->setProductBrand($obj->maybeBrandId->map(function($id) {
          return M::l('productBrand/productBrand')->prepareById($id)->entity();
        })->getOrNull())
        ->setProductTaxRate($obj->maybeTaxRateId->map(function($id) {
          return M::l('productTaxRate/productTaxRate')->prepareById($id)->entity();
        })->getOrNull())
        ->setCodePrefix($obj->codePrefix)
        ->setDefaultPrice($obj->defaultPrice)
        ->setDefaultBuyingPrice($obj->defaultBuyingPrice)
        ->setObs($obj->obs);
      $p->getInstances()->map(function($i) {
        $this->em->persist($i);
        $i->setDisabled(TRUE);
      });
      $this->em->flush();
      $obj->instances->map(function($i) use($productInstanceModel, $p) {
        $maybeSame = C::find(function($pi) use($productInstanceModel, $i) {
          return $productInstanceModel->setEntity($pi)->sameCharacteristicIds($i->characteristicIds);
        }, $p->getInstances());
        if($maybeSame->isPresent()) return $productInstanceModel->setEntity($maybeSame->get())->put($i);
        return (new ProductInstanceCreatorModel())
          ->setProductId($p->getId())
          ->setCharacteristicIds($i->characteristicIds)
          ->setCode($i->code)
          ->setPrice($i->price)
          ->setBuyingPrice($i->buyingPrice)
          ->setProportional($i->proportional)
          ->setObs($i->obs)
          ->setDisabled($i->disabled)
          ->setInitialStock($i->initialStock)
          ->setBarCode($i->barCode)
          ->safeCreate()
          ->right();
      });
      $this->em->refresh($p);
      return $p;
    });
  }
  function maybeCategory() {
    return Optional::fromNullable($this->entity()->getProductCategory());
  }
  function maybeBrand() {
    return Optional::fromNullable($this->entity()->getProductBrand());
  }
  function maybeTaxRate() {
    return Optional::fromNullable($this->entity()->getProductTaxRate());
  }
}
