<?php
class ProductInstanceModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\ProductInstance";
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
  }
  function map() {
    $characModel = M::l('characteristic/characteristicInstance');
    $i = $this->entity();
    return [ 'id' => $i->getId()
      , 'code' => $i->getCode()
      , 'barCode' => $i->getBarCode()
      , 'name' => $this->name()
      , 'characteristicInstances' => $i->getCharacteristics()->map(function($c) use($characModel) {
        return $characModel->setEntity($c)->map();
      })->toArray()
      , 'price' => $i->getPrice()
      , 'buyingPrice' => $i->getBuyingPrice()
      , 'proportional' => $i->isProportional()
      , 'obs' => $i->getObs()
      , 'disabled' => $i->isDisabled()
      , 'initialStock' => $i->getInitialStock()];
  }
  function name() {
    $i = $this->entity();
    $p = $i->getProduct();
    if(!$i->getCharacteristics()->count()) return $p->getName();
    $zipped = $i->getCharacteristics()->map(function($c) {
      return [$c, $c->getCharacteristic()];
    });
    $characteristicsSorted = C::sortCollection($zipped, function($aa, $ab) {
      return $aa[1]->getDisplayOrder() - $ab[1]->getDisplayOrder();
    })->map(function($a) {
      return $a[0];
    });
    return $p->getName().' '.join(', ', $characteristicsSorted->map(function($c) {
      return $c->getName();
    })->toArray());
  }
  function sameCharacteristicIds($charIdsA) {
    $charIdsA = C::forceCollection($charIdsA);
    $charIdsB = DU::indexes($this->entity()->getCharacteristics());
    return $charIdsA->count() == $charIdsB->count()
        && $charIdsA->forAll(function($k, $c) use($charIdsB) {
          return $charIdsB->contains($c);
        }); 
  }
  function getProduct() {
    
  }
  function put($obj) {
    $this->update(['code' => $obj->code, 'barCode' => $obj->barCode, 'price' => $obj->price
      , 'buyingPrice' => $obj->buyingPrice, 'proportional' => $obj->proportional
      , 'obs' => $obj->obs, 'disabled' => $obj->disabled]); 
    return $this->entity();
  }
}
