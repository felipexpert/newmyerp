<?php
class ProductCreatorModel extends CI_Model {
  private $name;
  private $maybeCategoryId;
  private $maybeBrandId;
  private $maybeTaxRateId;
  private $instances;
  private $codePrefix;
  private $defaultPrice;
  private $defaultBuyingPrice;
  private $obs;
  function __construct() {
    parent::__construct();
    M::l('product/productInstanceCreator');
  }
  function safeCreate() {
    $instancesEnabled = $this->instances->filter(function($i) { return !$i->disabled; });
    $eitherInside = Monad::chain(Either::ret(1), $instancesEnabled->map(function($i) use($instancesEnabled) {
      return function($x) use($instancesEnabled, $i) {
        if($instancesEnabled->filter(function($i2) use($i) { return $i->code == $i2->code; })->count() > 1) 
          return Either::fail(sprintf(lang('product.codeBeingUsedInside'), $i->code));
        elseif($instancesEnabled->filter(function($i2) use($i) { return $i->barCode == $i2->barCode; })->count() > 1)
          return Either::fail(sprintf(lang('product.barCodeBeingUsedInside'), $i->barCode));
        return Either::ret($x);
      };
    }));
    $eitherAll = Monad::chain($eitherInside, $instancesEnabled->map(function($i) {
      return function($x) use($i) {
        if(!ProductInstanceCreatorModel::canUseCode($i->code)) 
          return Either::fail(sprintf(lang('product.codeBeingUsedOutside'), $i->code));
        if(!ProductInstanceCreatorModel::canUseBarCode($i->barCode)) 
          return Either::fail(sprintf(lang('product.barCodeBeingUsedOutside'), $i->barCode));
        return Either::ret($x);
      };
    }));
    return $eitherAll->map(function() {
      $product = new Entity\Product();
      $this->em->persist($product);
      $product->setName($this->name)
              ->setProductCategory($this->maybeCategoryId->map(function($id) {
                return M::l('productCategory/productCategory')->prepareById($id)->entity(); 
              })->getOrNull())
              ->setProductBrand($this->maybeBrandId->map(function($id) {
                return M::l('productBrand/productBrand')->prepareById($id)->entity();
              })->getOrNull())
              ->setProductTaxRate($this->maybeTaxRateId->map(function($id) {
                return M::l('productTaxRate/productTaxRate')->prepareById($id)->entity();
              })->getOrNull())
              ->setCodePrefix($this->codePrefix)
              ->setDefaultPrice($this->defaultPrice)
              ->setDefaultBuyingPrice($this->defaultBuyingPrice)
              ->setObs($this->obs ?: ''); 
      $this->em->flush();
      $pis = $this->instances->map(function($i) use($product) {
        return (new ProductInstanceCreatorModel())
          ->setProductId($product->getId())
          ->setCharacteristicIds($i->characteristicIds)
          ->setCode($i->code)
          ->setBarCode($i->barCode)
          ->setPrice($i->price)
          ->setBuyingPrice($i->buyingPrice)
          ->setProportional($i->proportional)
          ->setObs($i->obs)
          ->setDisabled($i->disabled)
          ->setInitialStock($i->initialStock) // 
          ->safeCreate()
          ->right();
      });
      $product->setInstances($pis);
      return $product;
    });
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function setMaybeBrandId($maybeBrandId) {
    $this->maybeBrandId = $maybeBrandId;
    return $this;
  }
  public function setMaybeTaxRateId($maybeTaxRateId) {
    $this->maybeTaxRateId = $maybeTaxRateId;
    return $this;
  }
  public function setMaybeCategoryId($maybeCategoryId) {
    $this->maybeCategoryId = $maybeCategoryId;
    return $this;
  }
  public function setInstances($instances) {
    $this->instances = C::forceCollection($instances);
    return $this;
  }
  public function setCodePrefix($codePrefix) {
    $this->codePrefix = $codePrefix;
    return $this;
  }
  public function setDefaultPrice($defaultPrice) {
    $this->defaultPrice = $defaultPrice;
    return $this;
  }
  public function setDefaultBuyingPrice($defaultBuyingPrice) {
    $this->defaultBuyingPrice = $defaultBuyingPrice;
    return $this;
  }
  public function setObs($obs) {
    $this->obs = $obs;
    return $this;
  }
}
