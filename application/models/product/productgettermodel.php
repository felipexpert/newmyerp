<?php
class ProductGetterModel extends EntityGetterModel {
  function __construct() {
    parent::__construct();
  }
  function prepareByFilter($items, $filter) {
    $prods = $this->em->createQuery('select p, i, c from Entity\Product p
       left join p.instances i
       left join p.productCategory c
       where p.name like :name
       order by p.name')->setParameter('name', '%'.$filter.'%')->setMaxResults($items)->getResult();
    $this->prepare($prods);
    return $this;
  }
  function prepareLast() {
    $prods = $this->em->createQuery('select p
      from Entity\Product p
      order by p.id desc')->setMaxResults(1)->getResult();
    $this->prepare($prods);
    return $this;
  }
}
