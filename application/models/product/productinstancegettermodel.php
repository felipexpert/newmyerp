<?php
class ProductInstanceGetterModel extends EntityGetterModel {
  private $productInstanceModel;
  function __construct() {
    parent::__construct();
    $this->productInstanceModel = M::l('product/productInstance');
  }
  function prepareByCode($code) {
    $prods = $this->em->createQuery('select i, p, c
      from Entity\ProductInstance i
      inner join i.product p
      left join p.productCategory c
      where i.disabled = FALSE
        and i.code = :code')->setParameter('code', $code)->getResult();
    $this->prepare($prods);
    return $this;
  }
  function prepareByBarCode($barCode) {
    $prods = $this->em->createQuery('select i, p, c
      from Entity\ProductInstance i
      inner join i.product p
      left join p.productCategory c
      where i.disabled = FALSE
        and i.barCode = :barCode')->setParameter('barCode', $barCode)->getResult();
    $this->prepare($prods);
    return $this;
  }
  function prepareByCodeOrBarCode($codeOrBarCode) {
    $prods = $this->em->createQuery('select i, p, c
      from Entity\ProductInstance i
      inner join i.product p
      left join p.productCategory c
      where i.disabled = FALSE
        and (i.code != \'\' and i.code = :codeOrBarCode
             or 
             i.barCode != \'\' and i.barCode = :codeOrBarCode)')
          ->setParameter('codeOrBarCode', $codeOrBarCode)
          ->getResult();
    $this->prepare($prods);
    return $this;
  }
  function prepareByFilter($items, $filter) {
    $prods = $this->em->createQuery('select i, p, c
      from Entity\ProductInstance i
      inner join i.product p
      left join p.productCategory c
      where p.name like :name
      order by p.name')->setParameter('name', "%${filter}%")->setMaxResults($items)->getResult();
    $this->prepare($prods);
    return $this;
  }
  function prepareAll() {
    $prods = $this->em->createQuery('select i, p, c
      from Entity\ProductInstance i
      inner join i.product p
      left join p.productCategory c
      where i.disabled = FALSE')->getResult();
    $this->prepare($prods);
    return $this;
  }
  function prepareByCharacteristicInstanceId($id) {
    $prods = $this->em->createQuery('select i, chars, p, c
      from Entity\ProductInstance i
      inner join i.product p
      left join i.characteristics chars
      left join p.productCategory c
      where i.disabled = FALSE
        and chars.id = :id')->setParameter('id', $id)->getResult();
    $this->prepare($prods);
    return $this;
  }
  function prepareLast() {
    $prods = $this->em->createQuery('select i
      from Entity\ProductInstance i
      order by i.id desc')->setMaxResults(1)->getResult();
    $this->prepare($prods);
    return $this;
  }
}
