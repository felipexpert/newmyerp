<?php
class ProductInstanceCreatorModel extends CI_Model {
  private $productId;
  private $characteristicIds;
  private $code;
  private $barCode;
  private $price;
  private $buyingPrice;
  private $proportional;
  private $obs;
  private $disabled;
  private $initialStock;
  function __construct() {
    parent::__construct();
  }
  function canCreate() {
    return static::canUseCode($this->code) && static::canUseBarCode($this->barCode);
  }
  function safeCreate() {
    if(!$this->canCreate()) return Either::fail(sprintF(lang('product.codeBeingUsed'), $this->code)); 
    $ciModel = M::l('characteristic/characteristicInstance');
    $i = new Entity\ProductInstance();
    $this->em->persist($i);
    $i->setProduct(M::l('product/product')->prepareById($this->productId)->entity())
      ->setCharacteristics(C::forceCollection($this->characteristicIds)->map(function($id) use($ciModel) {
        return $ciModel->prepareById($id)->entity();
      }))
      ->setCode($this->code)
      ->setBarCode($this->barCode)
      ->setPrice($this->price)
      ->setBuyingPrice($this->buyingPrice)
      ->setProportional($this->proportional)
      ->setObs($this->obs ?: '')
      ->setDisabled($this->disabled)
      ->setInitialStock($this->initialStock ?: 0);
    $this->em->flush();
    return Either::success($i);
  }
  public function setProductId($productId) {
    $this->productId = $productId;
    return $this;
  }
  public function setCharacteristicIds($characteristicIds) {
    $this->characteristicIds = $characteristicIds;
    return $this;
  }
  public function setCode($code) {
    $this->code = $code;
    return $this;
  }
  public function setBarCode($barCode) {
    $this->barCode = $barCode;
    return $this;
  }
  public function setPrice($price) {
    $this->price = $price;
    return $this;
  }
  public function setBuyingPrice($buyingPrice) {
    $this->buyingPrice = $buyingPrice;
    return $this;
  }
  public function setProportional($proportional) {
    $this->proportional = $proportional;
    return $this;
  }
  public function setDisabled($disabled) {
    $this->disabled = $disabled;
    return $this;
  }
  public function setObs($obs) {
    $this->obs = $obs;
    return $this;
  }
  public function setInitialStock($initialStock) {
    $this->initialStock = $initialStock;
    return $this;
  }
  static function canUseCode($code) {
    return !M::l('product/productInstanceGetter')->prepareByCode($code)->has();   
  }
  static function canUseBarCode($barCode) {
    return !M::l('product/productInstanceGetter')->prepareByBarCode($barCode)->has();   
  }
}
