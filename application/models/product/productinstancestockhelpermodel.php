<?php
use Entity\FlowType; 

class ProductInstanceStockHelperModel extends CI_Model {
  private $product
        , $outSold 
        , $outConsigned
        , $inConsigned;

  function __construct() {
    parent::__construct();
  }

  function prepare($product, $stock) {
    $this->product = product;
    $flows = isSet($stockId) ? $product->getProductFlows()->filter(function($f) { 
      return $f->getStock()->getId() == $stock->getId();
    });
    $enabledFlows = DU::getNonDisabled($this->product->getProductFlows());
    $totalize = C::foldl(function($acc, $f) {
      return $acc + $f->getAmount();
    }, 0);
    $filterFlowTypes = function($flowTypes) use($enabledFlows) {
      return $enabledFlows->filter(function($f) use($flowTypes) {
        return in_array($f->getFlowType(), $flowTypes);
      });
    };
    $this->outSold = $totalize($filterFlowTypes([FlowType::OUT_SOLD]));
    $this->outConsigned = $totalize($filterFlowTypes([FlowType::OUT_CONSIGNED, FlowType::OUT_CONSIGNED_OFFSET]));
    $this->inConsigned = $totalize($filterFlowTypes([FlowType::IN_CONSIGNED, FlowType::IN_CONSIGNED_OFFSET]));
  }
  public function getProduct() {
    return $this->product;
  }
  public function getOutSold() {
    return $this->outSold;
  }
  public function getOutConsigned() {
    return $this->outConsigned;
  }
  public function getInConsigned() {
    return $this->inConsigned;
  }
  
}
