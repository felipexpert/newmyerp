<?php

class SimpleItemModel {
  public $amount = 0;
  public $unchangeable = 0;
  public $manipulable = TRUE;

  function __construct() {
    $args = func_get_args();
    if(count($args) === 2) {
      $this->amount = $args[0];
      $this->unchangeable = $args[1];
    }
  }

  function unchangeable() {
    return $this->unchangeable;
  }
  function setAmount($amount) {
    $this->amount = $amount;
    return $this;
  }
  function getAmount() {
    return $this->amount;
  }
  function isManipulable() {
    return $this->manipulable;
  }
}
