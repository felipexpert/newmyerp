<?php

class TargetedRedistributorModel extends CI_Model {
  private $target;
  private $items;

  function __construct() {
    parent:: __construct();
    $this->load->model('calc/newTotalRedistributorModel', 'newTotal');
  }

  function setItems($items) {
    $this->items = $items;
    return $this;
  }  
  function setTarget($target) {
    $this->target = $target;
    return $this;
  }

  function min() {
    return $this->target->unchangeable();
  } 

  function max() {
    return $this->target->getAmount() + ($this->itemsTotal() - $this->itemsUnchangeable());
  }

  function clampAndPerform($newAmount) {
    $this->perform(max($this->min(), min($this->max(), $newAmount)));
  }

  function canPerform($newAmount) {
    return N::ge($newAmount, $this->min()) && N::le($newAmount, $this->max());
  }

  function perform($newAmount) {
    if(!$this->canPerform($newAmount)) throw new Exception('Cannot perform!');
    $newAmountFloored = N::floorDecimals($newAmount);
    $newTotalItems = $this->itemsTotal() - ($newAmountFloored - $this->target->getAmount());
    $this->newTotal->setItems($this->items())->perform($newTotalItems);
    $this->target->setAmount($newAmountFloored);
  } 

  private function items() {
    return C::forceCollection(array_values($this->items->slice($this->items->indexOf($this->target) + 1)));
  }

  private function itemsUnchangeable() {
    $items = $this->items();
    return C::foldl(function($acc, $i) {
      return $acc + $i->unchangeable();
    }, 0, $items);
  }

  private function itemsTotal() {
    $items = $this->items();
    return C::foldl(function($acc, $i) {
      return $acc + $i->getAmount();
    }, 0, $items);
  }
}
