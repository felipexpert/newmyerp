<?php

class NewTotalRedistributorModel {
  private $items;
  function setItems($items) {
    $this->items = $items;
    return $this;
  }  
  private function unchangeable() {
    return C::foldl(function($acc, $i) {
      return $acc + $i->unchangeable();
    }, 0, $this->items());
    return $total;
  }
  private function items() {
    return C::forceCollection(array_values($this->items->filter(function($i) { return $i->isManipulable(); })->toArray()));
  }
  private function newTotal($newTotal) {
    $nonManipulables = $this->items->filter(function($i) { return !$i->isManipulable(); });
    $subtract = C::foldl(function($acc, $i) {
      return $acc + $i->getAmount();
    }, 0, $nonManipulables);
    return $newTotal - $subtract;
  }
  function canPerform($newTotal) {
    $newTotal = $this->newTotal($newTotal);
    return N::ge($newTotal, $this->unchangeable());
  }
  function perform($newTotal) {
    $items = $this->items();
    if(!$items->count()) return;
    if(!$this->canPerform($newTotal)) throw new Exception('Cannot perform!');
    $newTotal = $this->newTotal($newTotal);
    $newAverage = N::floorDecimals($newTotal / $items->count());
    $moreThanAverage = $items->filter(function($i) use($newAverage) { 
      return N::gt($i->unchangeable(), $newAverage);
    });
    C::fe(function($i) use($newAverage) {
      $i->setAmount(max($i->unchangeable(), $newAverage));
    }, $moreThanAverage);
    if($moreThanAverage->count() > 0) {
      $moreThanAverageTotal = C::foldl(function($acc, $i) {
        return $acc + $i->getAmount();
      }, 0, $moreThanAverage);
      $applicable = C::subtractCollections($items, $moreThanAverage);
      $nt = new NewTotalRedistributorModel();
      $nt->setItems($applicable)->perform($newTotal - $moreThanAverageTotal);
    } else {
      $offset = $newAverage + ($newTotal - $items->count() * $newAverage);
      $adjusted = FALSE;
      for($i = $items->count() - 1; $i >= 0; $i--) {
        $item = $items->get($i);
        if(!$adjusted && N::ge($offset, $item->unchangeable())) {
          $item->setAmount($offset);
          $adjusted = TRUE;
        } else {
          $item->setAmount($newAverage);
        }
      }
    }
  } 
}
