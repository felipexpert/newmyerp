<?php

class PayerItemModel extends CI_Model {

  private $payer;

  function __construct() {
    parent::__construct();
    $this->load->model('payer/payerModel');
  }

  function unchangeable() {
    return $this->unchangeable;
  }
  function setAmount($amount) {
    $this->amount = $amount;
    return $this;
  }
  function getAmount() {
    return $this->amount;
  }
}
