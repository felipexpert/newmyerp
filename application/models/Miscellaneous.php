<?php

use Doctrine\Common\Collections\ArrayCollection;
final class Miscellaneous {
  private static $ci;
  private function __construct() {}
  static function grabDump($var) {
    ob_start();
    var_dump($var);
    return ob_get_clean(); 
  }
  /**
   * Giving @arg0 and @arg1, the first will be the message and the second the status
   * Giving @arg0, the message will be created according to the status (@arg0)
   * Giving no args, it will set _status to success and the default success message
   * 
   * The most basic status are success and fail 
   */
  static function result() {
    header('Content-Type: application/json;UTF-8');
    $args = func_get_args();
    $c = count($args);
    if($c >= 2) 
      echo json_encode(['_status' => ServerResult::elt()[$args[1]]->getName(), 'message' => $args[0]]);
    elseif($c == 1) 
      echo json_encode(['_status' => ServerResult::elt()[$args[0]]->getName()]);
    else 
      echo json_encode(['_status' => ServerResult::elt()[ServerResult::SUCCESS]->getName()]);
  }
  static function result_($isSuccess, $success = null, $fail = null, $successMessage = null, $failMessage = null) {
    if($isSuccess) {
      if(isSet($success)) $success();
      if(isSet($successMessage)) M::result($successMessage, ServerResult::SUCCESS);
      else M::result(ServerResult::SUCCESS);
    } else {
      if(isSet($fail)) $fail();
      if(isSet($failMessage)) M::result($failMessage, ServerResult::FAIL);
      else M::result(ServerResult::FAIL);
    }
  }
  static function clsc(&$thing, $default = '') { return isSet($thing) ? $thing : $default; }
  /**
   * Even if @thing is undefined at the caller, it will not complain
   */
  static function force(&$thing) { return isSet($thing) ? $thing : null; }
  static function printR($thing) {
    echo '<pre>';
    print_r($thing);
    echo '</pre>';
  }
  static function rest($get = null, $put = null, $delete = null, $post = null) {
    $method = 'GET';
    $type = 'application/json;UTF-8';
    if(array_key_exists('REQUEST_METHOD', $_SERVER)) $method = $_SERVER['REQUEST_METHOD'];
    if(array_key_exists('CONTENT_TYPE', $_SERVER)) $type = $_SERVER['CONTENT_TYPE'];
    $respond = function($method, $type) {
      if(!isSet($method)) return ['code' => 405];
      $input = file_get_contents('php://input');
      $req = ['type' => $type, 'body' => $input];
      if(T::contains('json', $type)) $req['obj'] = json_decode($input);
      $response = $method((object)$req);
      if(array_key_exists(0, $response)) $response['code'] = $response[0];
      if(array_key_exists(1, $response)) $response['obj'] = $response[1];
      if(array_key_exists(2, $response)) $response['body'] = $response[2];
      if(array_key_exists(3, $response)) $response['type'] = $response[3];
      return $response;
    };
    $response = ['code' => 405];
    switch($method) {
    case 'GET':
      $response = $respond($get, $type);
      break;
    case 'PUT':
      $response = $respond($put, $type);
      break;
    case 'DELETE':
      $response = $respond($delete, $type);
      break;
    case 'POST':
      $response = $respond($post, $type);
      break;
    }
    if(array_key_exists('code', $response))
      http_response_code($response['code']);  
    else
      http_response_code(200);  
    if(array_key_exists('type', $response))
      header('Content-Type: '.$response['type']);
    else
      header('Content-Type: application/json;UTF-8');
    if(array_key_exists('obj', $response))
      echo json_encode($response['obj']);
    elseif(array_key_exists('body', $response))
      echo $response['body'];
  }
  static function prepareCI($ci) {
    if(isSet(self::$ci)) throw new Exception('Cannot init CI twice');
    self::$ci = $ci;
  }
  static function load($modelName, $instantiate = TRUE) {
    return Miscellaneous::l($modelName, $instantiate);
  }
  static function l($modelName, $instantiate = TRUE) {
    if(!isSet(self::$ci)) self::$ci =& get_instance();
    if(!T::endsWith($modelName, 'Model')) $modelName .= 'Model';
    $varName = preg_replace("/^(.+?\/)*(.+?)$/", "$2", $modelName);
    self::$ci->load->model($modelName, '_'.$varName);
    $className = ucFirst($varName);
    if($instantiate) return new $className;
  }
  static function applyJsonFilter($items, $filter, $isntFilterFunc, $isFilterFunc) {
    if(Miscellaneous::isJsonFilter($filter)) $isFilterFunc($items, Miscellaneous::jsonFilter($filter));
    else                                     $isntFilterFunc($items, $filter);
  }
  private static function isJsonFilter($filter) {
    return R::test('/^=>\{.*\}$/', $filter);
  }
  private static function jsonFilter($filter) {
    $matches = R::exec('/^=>(\{.*\})$/', $filter);
    return json_decode($matches[1]);
  }
  /**
   * Make sure the passed reference is not null.
   */
  static function checkNotNull($reference, $message = null) {
    if($message === null) $message = "Unallowed null in reference found.";
    if($reference === null) throw new NullPointerException($message);
    return $reference;
  }
  static function maybeIndex($indexable) {
    return Optional::fromNullable($indexable)->map(function($i) {
      if(method_exists($i, 'getId')) return $i->getId();
      if(isSet($i->id)) return $i->id;
      throw Exception('Not an indexed object!');
    });
  }
  static function ifElse($bool, $thenF, $elseF) {
    return $bool ? $thenF() : $elseF();
  }
}
class_alias('Miscellaneous', 'M');
