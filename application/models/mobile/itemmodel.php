<?php
class ItemModel {
  private $orderItemId;
  private $productCode;
  private $price;
  private $amount;
  private $displayOrder;
  public function getOrderItemId() {
    return $this->orderItemId;
  }
  public function setOrderItemId($orderItemId) {
    $this->orderItemId = $orderItemId;
    return $this;
  }
  public function getProductCode() {
    return $this->productCode;
  }
  public function setProductCode($productCode) {
    $this->productCode = $productCode;
    return $this;
  }
  public function getPrice() {
    return $this->price;
  }
  public function setPrice($price) {
    $this->price = $price;
    return $this;
  }
  public function getAmount() {
    return $this->amount;
  }
  public function setAmount($amount) {
    $this->amount = $amount;
    return $this;
  }
  public function getDisplayOrder() {
    return $this->displayOrder;
  }
  public function setDisplayOrder($displayOrder) {
    $this->displayOrder = $displayOrder;
    return $this;
  }
  public function prepare($vi) {
    $this->setOrderItemId($vi->orderItemId)
        ->setProductCode($vi->productCode)->setPrice($vi->price)
        ->setAmount($vi->amount)->setDisplayOrder($vi->displayOrder);
    return $this;
  }
}
