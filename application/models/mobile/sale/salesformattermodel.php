<?php

class SalesFormatterModel extends MobileFormatterModel {
  function format() {
    $productsFormatterModel = M::l('mobile/product/productsFormatter');
    $saleModel = M::l('sale/sale');
    return $this->getEntities()->map(function($sale) use($productsFormatterModel, $saleModel) {
      $saleModel->setEntity($sale);
      $order = $sale->getOrder();
      $employee = $order->getEmployee();
      return ['_id' => $order->getId(), 'orderNumber' => $sale->getOrderNumber() ?: -1,
          'identifier' => $sale->getIdentifier(),
          'orderItemCollection' => $order->getOrderItems()->map(function($oi) use($productsFormatterModel, $saleModel) {
            return ['_id' => $oi->getId(), 
            'product' => $productsFormatterModel
                           ->setEntities([$oi->getProductSnapshot()->getProductInstance()])->format()[0],
                             'order' => ['_id' => $oi->getOrder()->getId()], 
                'price' => $oi->getProductSnapshot()->getPrice(),
                'amount' => $saleModel->calcOrderItemAmount($oi), 
                'displayOrder' => $oi->getDisplayOrder(), 
                'disabled' => $oi->isDisabled()];
          })->toArray(), 'obs' => $order->getObs(), 
          'waiter' => ($employee ? array('_id' => $employee->getId(), 'name' => $employee->getName()) : null)];
    })->toArray();
  }
}
