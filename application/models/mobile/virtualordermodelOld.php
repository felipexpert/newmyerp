<?php
class VirtualOrderModel extends CI_Model {
  private $orderNumber;
  private $orderId;
  private $waiterId;
  private $obs;
  private $items;

  function __construct() {
    parent::__construct();
    $this->load->model("sale/saleGetterModel");
    $this->load->model("sale/saleModel");
    $this->load->model("order/orderModel");
    $this->load->model("orderItem/orderItemModel");
    $this->load->model('mobile/itemModel');
  }

  function canMerge() {
    $this->saleGetterModel->prepareByOrderNumber($this->orderNumber);
    return !($this->orderId == -1 && $this->saleGetterModel->has());
  }
  function merge() {
    $this->logger->setMessage("CHEGOU 1 - ", $this->orderId)->create();
    if(!$this->canMerge()) throw new Exception("Cannot merge");
    if($this->orderId == -1) {
      $this->logger->setMessage("CHEGOU 2")->create();
      // create a new order
      $this->load->model('sale/saleCreatorModel');
      $this->logger->setMessage("CHEGOU 2.1")->create();
      $this->saleCreatorModel->setSellingType(Entity\SellingType::FAST_SELLING);
      $this->logger->setMessage("CHEGOU 2.2")->create();
      $this->saleCreatorModel->setOrderNumber($this->orderNumber);
      $this->logger->setMessage("CHEGOU 2.3 - wid - ", $this->waiterId)->create();
      $this->load->model('employee/employeeModel');
      $this->logger->setMessage("test ", $this->employeeModel->prepareById($this->waiterId)->entity()->getId())->create();
      $this->saleCreatorModel
          ->setEmployee(isSet($this->waiterId) ? $this->employeeModel->prepareById($this->waiterId)->entity() : null);
      $this->logger->setMessage("CHEGOU 2.4")->create();
      $this->saleCreatorModel->setObs($this->obs);
      $this->logger->setMessage("CHEGOU 2.5")->create();
      $sale = $this->saleCreatorModel->create();
      $this->logger->setMessage("CHEGOU 2.6")->create();
      $this->saleModel->setEntity($sale);
      $this->logger->setMessage("CHEGOU 2.7")->create();
      /*$this->saleModel->setEntity($this->saleCreatorModel->setSellingType(Entity\SellingType::FAST_SELLING)
          ->setOrderNumber($this->orderNumber)
          ->setEmployee(isSet($this->waiterId) ? $this->employeeModel->prepareById($this->waiterId)->entity() : null)
          ->setObs($this->obs)->create());*/
      $this->orderModel->setEntity($this->saleModel->entity()->getOrder());
      $this->logger->setMessage("CHEGOU 3")->create();
    } else {
      $this->logger->setMessage("CHEGOU 4.1")->create();
      // update existing one
      $this->saleGetterModel->prepareByOrderId($this->orderId);
      $this->logger->setMessage("CHEGOU 4.2")->create();
      $this->saleModel->prepareByEntityGetter($this->saleGetterModel);
      $this->logger->setMessage("CHEGOU 4.3", "has sale?", $this->saleModel->hasEntity() ? "y" : "n")->create();
      $this->orderModel->setEntity($this->saleModel->entity()->getOrder());
      $this->logger->setMessage("CHEGOU 4.4")->create();
      $this->load->model('employee/employeeModel');
      $this->logger->setMessage("CHEGOU 4.5")->create();
      $this->employeeModel->prepareById($this->waiterId);
      $this->logger->setMessage("CHEGOU 4.6")->create();
      $this->orderModel->update($this->employeeModel->hasEntity() ? $this->employeeModel->entity() : null, 
          $this->obs);
      $this->logger->setMessage("CHEGOU 5")->create();
    }
    $this->logger->setMessage("CHEGOU 6")->create();
    // merge items
    $items = C::forceCollection($this->items);
    $orderItems = $this->orderModel->entity()->getOrderItems();
    $this->logger->setMessage("CHEGOU 7")->create();
    C::fe(function($oi) {
      $oi->setDisabled(TRUE);
    }, $orderItems->filter(function($oi) use($items) {
      return !C::foldl(function($acc, $i) use($oi) {
        return $acc || $i->getOrderItemId() == $oi->getId();
      }, FALSE, $items);
    }));
    $this->logger->setMessage("CHEGOU 8")->create();
    C::fe(function($i) {
      $this->orderModel->addOrderItem($i->getProductCode(), $i->getAmount(), 0, $i->getPrice());
    }, $items->filter(function($i) {
      return $i->getOrderItemId() == -1;
    }));
    $this->logger->setMessage("CHEGOU 9")->create();
    C::fe(function($i) {
      $this->orderItemModel->setEntity($this->orderModel->getOrderItem($i->getOrderItemId()));
      $this->orderItemModel->update($i->getAmount(), $i->getPrice(), Maybe::nothing());
    }, $items->filter(function($i) {
      return $i->getOrderItemId() != -1;
    }));
    $this->logger->setMessage("CHEGOU 10")->create();
  }
  public function getOrderNumber() {
    return $this->orderNumber;
  }
  public function setOrderNumber($orderNumber) {
    $this->orderNumber = $orderNumber;
    return $this;
  }
  public function getOrderId() {
    return $this->orderId;
  }
  public function setOrderId($orderId) {
    $this->orderId = $orderId;
    return $this;
  }
  public function getWaiterId() {
    return $this->waiterId;
  }
  public function setWaiterId($waiterId) {
    $this->waiterId = $waiterId;
    return $this;
  }
  public function getObs() {
    return $this->obs;
  }
  public function setObs($obs) {
    $this->obs = $obs;
    return $this;
  }
  public function getItems() {
    return $this->items;
  }
  public function setItems($items) {
    $this->items = $items;
    return $this;
  }
}
