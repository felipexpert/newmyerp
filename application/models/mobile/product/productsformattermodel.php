<?php

class ProductsFormatterModel extends MobileFormatterModel {
  function format() {
    return $this->getEntities()->map(function($p) {
      $productCategory = $p->getProduct()->getProductCategory();
      if($productCategory) $productCategory = ['_id' => $productCategory->getId(), 'name' => $productCategory->getName()];
      return ['_id' => $p->getId(), 'code' => $p->getCode(), 
          'category' => $productCategory, 'name' => $p->getProduct()->getName(), 'price' => $p->getPrice()];
    })->toArray();
  }
}
