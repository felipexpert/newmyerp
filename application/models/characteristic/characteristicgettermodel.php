<?php
class CharacteristicGetterModel extends EntityGetterModel {
  function __construct() {
    parent::__construct();
  }
  function prepareById($id) {
    $xs = $this->em->createQuery('select c, i
      from Entity\Characteristic c
      left join c.instances i
      where c.id = :id
      and i.disabled = FALSE
      order by c.displayOrder, i.name')->setParameter('id', $id)->getResult();
    $this->prepare($xs);
    return $this;
  }
  function prepareAll() {
    $xs = $this->em->createQuery('select c, i
      from Entity\Characteristic c
      left join c.instances i
      where c.disabled = FALSE
      order by c.displayOrder, i.name')->getResult();
    $this->prepare($xs);
    return $this;
  }
}
