<?php
class CharacteristicInstanceCreatorModel extends CI_Model {
  private $characteristic;
  private $name;
  function create() {
    $instance = (new Entity\CharacteristicInstance())
      ->setCharacteristic($this->characteristic)
      ->setName($this->name);
    $this->em->persist($instance);
    $this->em->flush();
    return $instance;
  }
  public function getCharacteristic() {
    return $this->characteristic;
  }
  public function setCharacteristic($characteristic) {
    $this->characteristic = $characteristic;
    return $this;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
}
