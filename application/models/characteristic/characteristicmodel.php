<?php
class CharacteristicModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\Characteristic";
  private $characteristicInstanceModel;
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
  }
  function maybeUp() {
    $this->maybeChangeTo($this->entity()->getDisplayOrder() - 1);
  }
  function maybeDown() {
    $this->maybeChangeTo($this->entity()->getDisplayOrder() + 1);
  }
  function maybeChangeTo($newPos) {
    $chars = M::l('characteristic/characteristicGetter')->prepareAll()->all();
    $maybeC2 = DU::maybeElemByDisplayOrder($chars, $newPos);
    $maybeC2->ifPresent(function($c2) {
      $c = $this->entity();
      $pos = $c2->getDisplayOrder();
      $c2->setDisplayOrder($c->getDisplayOrder());
      $c->setDisplayOrder($pos);
      $this->em->persist($c);
      $this->em->persist($c2);
      $this->em->flush();
    });
  }
  function canRemove() {
    return $this->entity()->getInstances()->forAll(function($k, $instance) {
      return $this->characteristicInstanceModel->setEntity($instance)->canRemove();
    });
  }
  protected function unsafeRemove() {
    $characteristicGetter = M::l('characteristic/characteristicGetter');
    $characteristicGetter->prepareAll()->all()->filter(function($c) { 
      return $c->getDisplayOrder() > $this->entity()->getDisplayOrder();
    })->map(function($c) {
      $c->setDisplayOrder($c->getDisplayOrder() - 1);
      $this->em->persist($c);
    });
    // $this->em->flush(); Not necessary in this case
    $this->entity()->getInstances()->map(function($instance) {
      $this->characteristicInstanceModel->setEntity($instance)->remove();
    });
    parent::unsafeRemove();
  }
  protected function hook() {
    $this->characteristicInstanceModel = M::load('characteristic/characteristicInstance');
  }
  function map() {
    $c = $this->entity();
    return [ 'id' => $c->getId()
      , 'displayOrder' => $c->getDisplayOrder()
      , 'name' => $c->getName()
      , 'instances' => $c->getInstances()->map(function($i) {
        return $this->characteristicInstanceModel->setEntity($i)->map('disableable');
      })->toArray()
      , 'disableable' => $this->canRemove()
      , 'disabled' => $c->isDisabled()];
  }
  function put($obj) {
    $char = $this->entity();
    $char->setName($obj->name)
         ->setDisabled($obj->disabled);
    $char->getInstances()->map(function($instance) use($obj) {
      $i = C::find(function($i) use($instance) { return $instance->getId() == $i->id; }, $obj->instances)->get(); 
      $instance->setName($i->name)
               ->setDisabled($i->disabled);
      $this->em->persist($instance);
    }); 
    $this->em->persist($char);
    $this->em->flush();
  }
}
