<?php
class CharacteristicInstanceModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\CharacteristicInstance";
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
  }
  function canRemove() {
    $productInstanceGetter = M::l('product/productInstanceGetter');
    $productInstanceGetter->prepareByCharacteristicInstanceId($this->entity()->getId());
    return !$productInstanceGetter->has();
  }
  function map($extra = '') {
    $i = $this->entity();
    $mapped = [ 'id' => $i->getId()
      , 'name' => $i->getName()
      , 'displayOrder' => $i->getCharacteristic()->getDisplayOrder()
      , 'disabled' => $i->isDisabled()];
    if(T::contains('disableable', $extra)) $mapped['disableable'] = $this->canRemove();
    return $mapped;
  }
}
