<?php
class CharacteristicCreatorModel extends CI_Model {
  private $name;
  function create() {
    $this->load->model('characteristic/characteristicGetterModel', '_characteristicGetterModel');
    $characteristicGetter = new CharacteristicGetterModel();
    $displayOrder = $characteristicGetter->prepareAll()->size() + 1;
    $char = (new Entity\Characteristic())
      ->setName($this->name)
      ->setDisplayOrder($displayOrder); 
    $this->em->persist($char);
    $this->em->flush();
    return $char;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
}
