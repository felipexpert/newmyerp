<?php
class OrderGetterModel extends EntityGetterModel {
  function prepareBySaleId($saleId) {
    $sales = $this->em->createQuery('select s, o from Entity\Sale s
    inner join s.order o
    where s.id = :id')->setParameter('id', $saleId)->getResult();
    $orders = C::foldl(function($acc, $sale) {
      return C::push($sale->getOrder(), $acc);
    }, array(), $sales);
    $this->prepare($orders);
  }
}
