<?php
class OrderCreatorModel extends CI_Model {
  private $obs;
  private $employee;
  private $creation;
  function __construct() {
    parent::__construct();
  }
  function create() {
    if(!isSet($this->creation)) $this->creation = new DateTime("now");
    if(!isSet($this->obs)) $this->obs = '';
    $order = new Entity\Order();
    $order->setCreation($this->creation)
          ->setObs($this->obs)
          ->setEmployee($this->employee)
          ->setDiscount(0)
          ->setSecondDiscount(0);
    $this->em->persist($order);
    $this->em->flush();
    return $order;
  }
  public function getObs() {
    return $this->obs;
  }
  public function setObs($obs) {
    $this->obs = $obs;
    return $this;
  }
  public function getEmployee() {
    return $this->employee;
  }
  public function setEmployee($e) {
    $this->employee = $e;
    return $this;
  }
  public function getCreation() {
    return $this->creation;
  }
  public function setCreation($creation) {
    $this->creation = $creation;
    return $this;
  }
}
