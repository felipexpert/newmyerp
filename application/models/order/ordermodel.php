<?php

use Doctrine\Common\Collections\ArrayCollection;
class OrderModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\Order";
  private $orderItemCreator;
  private $productInstanceGetter;
  private $orderItemGetter;
  private $orderItem;
  private $productCategory;
  function __construct() {
    parent::__construct();
    $this->setEntityName(OrderModel::ENTITY_NAME);
    $this->load->model('orderItem/orderItemCreatorModel', '_orderItemCreatorModel');
    $this->orderItemCreator = new OrderItemCreatorModel();
    $this->load->model('product/productInstanceGetterModel', '_productInstanceGetterModel');
    $this->productInstanceGetter = new ProductInstanceGetterModel();
    $this->load->model("orderItem/orderItemGetterModel", '_orderItemGetterModel');
    $this->orderItemGetter = new OrderItemGetterModel();
    $this->load->model("orderItem/orderItemModel", '_orderItemModel');
    $this->orderItem = new OrderItemModel();
    $this->load->model('productCategory/productCategoryModel', '_productCategoryModel');
    $this->productCategory = new ProductCategoryModel();
  }
  function getOrderItems() {
    return $this->getSortedItems();
  }
  function getSortedItems() {
    return DU::getSortedDisplayOrderables($this->entity()->getOrderItems());
  }
  function getItemByDisplayOrder($displayOrder) {
    return DU::getByDisplayOrder($this->entity()->getOrderItems(), $displayOrder);
  }
  function addOrGetExistingOrderItem($productCode) {
    $xs = DU::getNonDisabled($this->entity()->getOrderItems()->filter(function($oi) use($productCode) {
      return $oi->getProductSnapshot()->getProductInstance()->getCode() == $productCode;
    }));
    $orderItem = $xs->count() ? $xs->first() : $this->addOrderItem($productCode);
    return $orderItem;
  }
  function addOrderItem($productCode, $discount = 0, $price = null) {
    $this->productInstanceGetter->prepareByCode($productCode);
    $orderItem = $this->orderItemCreator->setProductInstance($this->productInstanceGetter->first())
        ->setOrder($this->entity())
        ->setDiscount($discount)
        ->setPrice($price)
        ->create();
    return $orderItem;
  }
  function removeOrderItem($displayOrder) {
    $this->orderItemGetter->prepareByDisplayOrder($displayOrder, $this->entity()->getId());
    if($this->orderItemGetter->has()) {
      $this->orderItem->prepareByEntityGetter($this->orderItemGetter);
      $this->orderItem->disable();
    }
  }
  function exists($orderItemId) {
    return DU::exists($this->entity()->getOrderItems(), $orderItemId);
  }
  function getOrderItem($orderItemId) {
    return DU::elemById($this->entity()->getOrderItems(), $orderItemId);
  }
  function update($employee, $obs) {
    Maybe::applyTo($employee, function($employee) { $this->entity()->setEmployee($employee); });
    Maybe::applyTo($obs, function($obs) { $this->entity()->setObs($obs); });
    $this->em->flush();
    return $this;
  }
  function getItemsWithoutCat() {
    return $this->getOrderItems()->filter(function($oi) {
      return is_null($oi->getProductSnapshot()->getProductInstance()->getProduct()->getProductCategory());
    });
  }
  function getItemsByCats() {
    $cats = $this->getOrderItems()->map(function($oi) {
      return $oi->getProductSnapshot()->getProductInstance()->getProduct()->getProductCategory();
    });
    $cats = C::unique($cats)->filter(function($cat) { return isSet($cat); });
    return $cats->map(function($cat) {
      return (new CatItemsModel())->setProductCategory($cat)
          ->setItems($this->getItemsByCat($cat));
    });
  }
  function getItemsByCat($productCategory) {
    return $this->getOrderItems()->filter(function($oi) use($productCategory) {
      return $oi->getProductSnapshot()->getProductInstance()->getProduct()->getProductCategory() == $productCategory;
    });
  }
  function getItemsByCatId($productCategoryId) {
    return $this->getProdByCat($this->productCategory->prepareById($productCategoryId)->entity());
  }
  function discount() {
    return $this->entity()->getDiscount() + $this->entity()->getSecondDiscount();
  }
  function disable() {
    $o = $this->entity();
    $o->setDisabled(TRUE);
    C::fe(function($oi) {
      M::l('orderItem/orderItem')
        ->setEntity($oi)
        ->disable();
    }, $o->getOrderItems());
    $this->em->flush();
  }
  function map($extra = '') {
    $o = $this->entity();
    $mapped = [ 'id' => $o->getId()
      , 'discount' => $o->getDiscount()
      , 'secondDiscount' => $o->getSecondDiscount()
      , 'creation' => D::millis($o->getCreation())
      , 'disabled' => $o->isDisabled()];
    if(T::contains('orderItems', $extra)) $mapped['orderItems'] = 
    DU::getNonDisabled($o->getOrderItems())->map(function($oi) {
      return $this->orderItem->setEntity($oi)->map();
    })->toArray();
    return $mapped;
  }
}
