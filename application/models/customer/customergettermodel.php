<?php
class CustomerGetterModel extends EntityGetterModel {
  function prepareByAttrId($attrId) {
    $xs = $this->em->createQuery("select c, p 
      from Entity\Customer c
      inner join c.personAttr p
      where p.id = :id;")->setParameter('id', $attrId)->getResult();
    $this->prepare($xs);
    return $this;
  }
  function prepareByFilter($items, $filter) {
    $xs = $this->em->createQuery("select c, p 
      from Entity\Customer c
      inner join c.personAttr p
      where p.name like :name 
      order by p.name")->setParameter('name', "%${filter}%")->setMaxResults($items)->getResult();
    $this->prepare($xs);
    return $this;
  }
}
