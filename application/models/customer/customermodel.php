<?php

use Doctrine\Common\Collections\ArrayCollection;
class CustomerModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\Customer";
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
    $this->load->model('person/personAttrModel', 'personAttr');
  }
  function map() {
    $c = $this->entity();
    return
      [ 'id' => $c->getId()
      , 'personAttr' => $this->personAttr->setEntity($c->getPersonAttr())->map()];
  }
}
