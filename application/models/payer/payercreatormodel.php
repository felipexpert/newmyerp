<?php
use Doctrine\Common\Collections\ArrayCollection;
class PayerCreatorModel extends CI_Model {
  private $sale;
  private $amount;
  private $description;
  function __construct() {
    parent::__construct();
  }
  function create() {
    $this->load->model('portionBucket/portionBucketCreatorModel');
    $pb = new Entity\PortionBucket();
    $pb->setAmount($this->amount);
    $payer = new Entity\Payer();
    $payer->setSale($this->sale)
        ->setPortionBucket($pb)
        ->setDescription($this->description);
    $this->sale->getPayers()->add($payer);
    $this->em->persist($pb);
    $this->em->persist($payer);
    $this->em->flush();
    return $payer;
  }
  public function setSale($sale) {
    $this->sale = $sale;
    return $this;
  }
  public function setAmount($amount) {
    $this->amount = $amount;
    return $this;
  }
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }
}
