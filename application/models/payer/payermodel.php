<?php
use Doctrine\Common\Collections\ArrayCollection;
class PayerModel extends EntityWrapperModel {
  const ENTITY_NAME = 'Entity\\Payer';
  private $portionBucketModel;
  private $customerModel;
  function __construct() {
    parent::__construct();
    $this->setEntityName(PayerModel::ENTITY_NAME);
    $this->load->model('customer/customerModel', '_customerModel');
    $this->customerModel = new CustomerModel();
  }
  function nonParceledAmount() {
    return $this->portionBucketModel->nonParceledAmount();
  }
  function paidAmount() {
    return $this->portionBucketModel->paidAmount();
  }
  function unchangeable() {
    return $this->portionBucketModel->unchangeable();
  }
  function isManipulable() {
    return $this->portionBucketModel->isManipulable();
  }
  function getAmount() {
    return $this->portionBucketModel->getAmount();
  }
  function setAmount($amount) {
    $this->portionBucketModel->setAmount($amount);
    return $this;
  }
  protected function hook() {
    $this->portionBucketModel = M::l('portionBucket/portionBucket');
    $this->portionBucketModel->setEntity($this->entity()->getPortionBucket());
  }

  function canRemove() {
    return N::eq($this->paidAmount(), 0);
  }

  protected function unsafeRemove() {
    $payerHelperModel = M::l('sale/payerHelper');
    $payerHelperModel->prepareBySale($this->entity()->getSale());
    parent::unsafeRemove();
    $this->portionBucketModel->remove();
    $payerHelperModel->redistribute();
  }

  function update($description, $amount, $customerId) {
    Maybe::applyTo($description, function($description) {
      $this->entity()->setDescription($description);
    });
    Maybe::applyTo($amount, function($amount) {
      $this->load->model('calc/targetedRedistributorModel', 'targeted');
      $models = $this->entity()->getSale()->getPayers()->map(function($p) {
        return (new PayerModel())->setEntity($p);
      });
      $target = $models->filter(function($m) {
        return $m->entity() === $this->entity();
      })->first();
      $this->targeted->setItems($models)->setTarget($target)->clampAndPerform($amount);
    });
    Maybe::applyTo($customerId, function($customerId) {
      $this->entity()->setCustomer($this->customerModel->prepareById($customerId)->entity()); 
    });
    $this->em->flush();
  }

  function map() {
    $payer = $this->entity();
    return 
      [ 'id' => $payer->getId()
      , 'description' => $payer->getDescription()
      , 'portionBucket' => $this->portionBucketModel->map()
      , 'customer' => $payer->getCustomer() ? $this->customerModel->setEntity($payer->getCustomer())->map() : null];
  }
}
