<?php
class PayerGetterModel extends EntityGetterModel {
  function prepareById($id) {
    $payers = $this->em->createQuery('select payer, pb, por from Entity\Payer payer
      inner join payer.portionBucket pb
      left join pb.portions por
      where payer.id = :id')->setParameter('id', $id)->getResult();
    $this->prepare($payers);
    return $this;
  }
  function prepareBySaleId($saleId) {
    $sale = $this->em->createQuery('select s, p, b, por from Entity\Sale s
        left join s.payers p
        left join p.portionBucket b
        left join b.portions por
        where s.id = :saleId')->setParameter('saleId', $saleId)->getResult()[0];
    $this->prepare($sale->getPayers());
    return $this;
  }
  function prepareByPortionBucketId($pbId) {
    $payers = $this->em->createQuery('select payer, customer from Entity\Payer payer
      left join payer.customer customer
      inner join payer.portionBucket pb
      where pb.id = :id')->setParameter('id', $pbId)->getResult();
    $this->prepare($payers);
    return $this;
  }
}
