<?php

use Doctrine\Common\Collections\ArrayCollection;
class CatItemsModel {
  private $productCategory;
  private $items;
  function __construct() {
    $this->items = new ArrayCollection();
  }
  function getProductCategory() { return $this->productCategory; }
  function setProductCategory($productCategory) {
    $this->productCategory = $productCategory;
    return $this;
  }
  function getItems() { return $this->items; }
  function setItems($items) {
    $this->items = $items;
    return $this;
  }
}
