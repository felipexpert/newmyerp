<?php
class PrinterModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\Printer";
  function __construct() {
    parent::__construct();
    $this->setEntityName(static::ENTITY_NAME);
  }
  function map() {
    $p = $this->entity();
    return [
      'id' => $p->getId(),
      'path' => $p->getPath(),
      'name' => $p->getName(),
      'printerType' => $p->getPrinterType()->getCode()
    ];
  }
}
