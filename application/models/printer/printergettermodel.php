<?php
class PrinterGetterModel extends EntityGetterModel {
  function prepareAll() {
    $printers = $this->em->createQuery('select p from Entity\Printer p')->getResult();
    $this->prepare($printers);
    return $this;
  }
  function prepareById($id) {
    $printers = $this->em->createQuery('select p from Entity\Printer p
        where p.id = :id')->setParameter('id', $id)->getResult();
    $this->prepare($printers);
    return $this;
  }
}
