<?php
class PrinterHelperModel extends CI_Model {
  private $sale;
  private $redo = FALSE;
  function execute() {
    $this->load->model('sale/saleModel');
    $this->saleModel->setEntity($this->sale);
    $this->load->model('order/orderModel');
    $this->load->model('orderItem/orderItemModel');
    $this->orderModel->setEntity($this->saleModel->entity()->getOrder());
    C::fe(function($catItems) {
      $this->load->model('productCategory/productCategoryModel');
      $this->productCategoryModel->setEntity($catItems->getProductCategory());
      if($this->productCategoryModel->hasPrinterPath() && $catItems->getItems()->count()) {
        $texto  = '';
        //cabeçalho
        $texto .= $this->productCategoryModel->getPrinterPath()."\r\n";
        $texto .= "----------------------------------------\r\n";
        $texto .= $catItems->getProductCategory()->getName()."\r\n";
        $texto .= "----------------------------------------\r\n";
        //cabeçalho - comanda, data, etc
        $texto .= "Comanda: ".$this->saleModel->entity()->getOrderNumber()."\r\n";
        $texto .= "ID do pedido: ".$this->saleModel->entity()->getId()."\r\n";
        $texto .= "Impresso em: ".date("d/m/Y h:i:s")."\r\n\r\n";
        //itens
        C::fe(function($item) use(&$texto) {
          if(!$this->redo && !is_null($item->getPrinting())) return;
          $texto .= str_pad($this->saleModel
                              ->calcOrderItemAmount($item)."x ", 4, " ", STR_PAD_RIGHT)
                   .$item->getProductSnapshot()->getName()."\r\n";
          $this->orderItemModel->setEntity($item)
            ->update(Maybe::nothing(), new DateTime('now'));
        }, $catItems->getItems());
        //criar arquivo .txt
        $texto .= "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n";
	$filePath = PRINTING_PATH.uniqid()."_pedido_{$this->saleModel->entity()->getId()}_".date('Ymd_his').".txt";
        $file     = fopen($filePath, "w+");
        fwrite($file, $texto);
        fclose($file);
        unset($texto);
      }
    }, $this->orderModel->getItemsByCats());
  }
  function setSaleId($id) {
    $this->load->model('sale/saleGetterModel');
    $this->sale = $this->saleGetterModel->prepareById($id)->first();
    return $this;
  }
  function setSale($sale) {
    $this->sale = $sale;
    return $this;
  }
  function setRedo($redo) {
    $this->redo = $redo;
    return $this;
  }
}
