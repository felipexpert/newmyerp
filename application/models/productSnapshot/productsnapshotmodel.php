<?php
class ProductSnapshotModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\ProductSnapshot";
  private $productInstanceModel;
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
    $this->load->model('product/productInstanceModel', '_productInstanceModel');
    $this->productInstanceModel = new ProductInstanceModel();
  }
  function map() {
    $s = $this->entity();
    return 
      [ 'id' => $s->getId()
      , 'product' => $this->productInstanceModel->setEntity($s->getProductInstance())->map()
      , 'name' => $s->getName()
      , 'price' => $s->getPrice()
      , 'buyingPrice' => $s->getBuyingPrice()
      , 'discount' => $s->getDiscount()];
  }
}
