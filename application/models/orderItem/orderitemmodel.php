<?php
class OrderItemModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\OrderItem";
  private $productFlowCreator;
  private $productFlow;
  private $snapshot;
  function __construct() {
    parent::__construct();
    $this->setEntityName(OrderItemModel::ENTITY_NAME);
  }
  protected function hook() {
    $this->productFlowCreator = M::l('productFlow/productFlowCreator');
    $this->productFlow = M::l('productFlow/productFlow');
    $this->snapshot = M::l('productSnapshot/productSnapshot');
  }
  function update($price, $printing) {
    Maybe::applyTo($price, function($price) { $this->entity()->getProductSnapshot()->setPrice($price); });
    Maybe::applyTo($printing, function($printing) { $this->entity()->setPrinting($printing); });
    $this->em->flush();
  }
  /**
   * updateAmount($flowType, $amount, $stockId) or updateAmount($positiveFlows, $negativeFlows, $flowType, $amount, $stockId)
   **/
  function updateAmount($arg1, $arg2, $arg3 = null, $arg4 = null, $arg5 = null) {
    $currAmount = 0; $amount = 0; $stockId = null; $coeficient = 1;
    if(isSet($arg4)) {
      $positiveFlows = $arg1; $negativeFlows = $arg2; $flowType = $arg3; $amount = $arg4; $stockId = $arg5;
      if(in_array($flowType, $negativeFlows)) $coeficient = -1;
      elseif(!in_array($flowType, $positiveFlows)) throw new Exception('Your flow has to be positive or negative!');
      $currAmount = $this->amount($positiveFlows, $negativeFlows);
    } else {
      $flowType = $arg1; $amount = $arg2; $stockId = $arg3;
      $currAmount = $this->amount($flowType);
    }
    $flowAmount = $amount - $currAmount;
    $this->addFlow($flowType, $coeficient * $flowAmount, $stockId);
  }
  function addFlow($flowType, $amount, $stockId = null) {
    $this->productFlowCreator->setProductInstance($this->entity()->getProductSnapshot()->getProductInstance())
      ->setFlowType($flowType)
      ->setAmount($amount);
    if(isSet($stockId)) $this->productFlowCraetor->setStockId($stockId);
    $pf = $this->productFlowCreator->create();
    $this->entity()->getProductFlows()->add($pf);
    $this->em->flush();
  }
  /**
   * amount($flowType, $stockId) or amount($positiveFlows, $negativeFlows, $stockId)
   **/
  function amount($arg1, $arg2 = null, $arg3 = null) {
    if(is_numeric($arg1)) {
      $flowType = $arg1; $stockId = $arg2;
      $pfs = $this->entity()->getProductFlows()->filter(function($pf) use($flowType, $stockId) {
        return !$pf->isDisabled()
          && $pf->getFlowType() == $flowType 
          && ($stockId == null || $pf->getStock() != null && $pf->getStock()->getId() == $stockId);
      });
      return C::foldl(function($acc, $pf) {
        return $acc + $pf->getAmount();
      }, 0, $pfs);  
    }
    $positiveFlows = $arg1; $negativeFlows = $arg2; $stockId = $arg3;
    $fold = C::foldl(function($acc, $f) use($stockId) {
      return $acc + $this->amount($f, $stockId);
    }, 0);
    return $fold($positiveFlows) - $fold($negativeFlows) ;
  }
  /**
   * price($flowType, $stockId) or price($positiveFlows, $negativeFlows, $stockId)
   **/
  function price($arg1, $arg2 = null, $arg3 = null) {
    return $this->amount($arg1, $arg2, $arg3) * $this->entity()->getProductSnapshot()->getPrice() 
           - $this->entity()->getProductSnapshot()->getDiscount(); 
  }
  function disable() {
    $oi = $this->entity();
    $oi->setDisabled(TRUE);
    C::fe(function($f) {
      $f->setDisabled(TRUE);
    }, $oi->getProductFlows());
    C::fe(function($oi) {
      if($oi->getDisplayOrder() > $this->entity()->getDisplayOrder())
        $oi->setDisplayOrder($oi->getDisplayOrder() - 1);
    }, $this->entity()->getOrder()->getOrderItems());
    $this->em->flush();
  }
  function map() {
    $i = $this->entity();
    return 
      [ 'id' => $i->getId()
      , 'productSnapshot' => $this->snapshot->setEntity($i->getProductSnapshot())->map()
      , 'displayOrder' => $i->getDisplayOrder()
      , 'productFlows' => $i->getProductFlows()->map(function($pf) {
        return $this->productFlow->setEntity($pf)->map();
      })->toArray()
      , 'deliveredDate' => D::millis($i->getDeliveredDate())
      , 'disabled' => $i->isDisabled()]; 
  }
}
