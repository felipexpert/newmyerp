<?php
class OrderItemGetterModel extends EntityGetterModel {
  function prepareByDisplayOrder($displayOrder, $orderId) {
    $orderItems = $this->em->createQuery("select oi from Entity\OrderItem oi
        where oi.order = :order and oi.displayOrder = :displayOrder and oi.disabled = FALSE")
        ->setParameter('order', $orderId)
        ->setParameter('displayOrder', $displayOrder)
        ->getResult();
    $this->prepare($orderItems);
    return $this;
  }
}
