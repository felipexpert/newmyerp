<?php
class OrderItemCreatorModel extends CI_Model {
  private $productInstance;
  private $order;
  private $discount;
  private $price;

  private $orderModel;
  private $productInstanceModel;

  function __construct() {
  }
  
  function create() {
    $this->load->model('order/orderModel', '_orderModel');
    $this->orderModel = new OrderModel();
    $this->load->model('product/productInstanceModel', '_productInstanceModel');
    $this->productInstanceModel = new ProductInstanceModel();
    $this->orderModel->setEntity($this->order);
    $displayOrder = 1;
    $sortedItems = $this->orderModel->getSortedItems();
    if($sortedItems->count())
      $displayOrder = $sortedItems->last()->getDisplayOrder() + 1;
    $productSnapshot = new Entity\ProductSnapshot();
    $productSnapshot->setProductInstance($this->productInstance)
      ->setName($this->productInstanceModel->setEntity($this->productInstance)->name())
      ->setPrice(isSet($this->price) ? $this->price : $this->productInstance->getPrice())
      ->setBuyingPrice($this->productInstance->getBuyingPrice())
      ->setDiscount($this->discount);
    $orderItem = new Entity\OrderItem();
    $orderItem->setProductSnapshot($productSnapshot)
        ->setOrder($this->order)
        ->setDeliveredDate(new DateTime("now"))
        ->setDisplayOrder($displayOrder);
    $productSnapshot->setOrderItem($orderItem);
    $this->order->getOrderItems()->add($orderItem);
    $this->em->persist($orderItem);
    $this->em->persist($productSnapshot);
    $this->em->flush();
    return $orderItem;
  }
  public function getProductInstance() {
    return $this->productInstance;
  }
  public function setProductInstance($productInstance) {
    $this->productInstance = $productInstance;
    return $this;
  }
  public function getOrder() {
    return $this->order;
  }
  public function setOrder($order) {
    $this->order = $order;
    return $this;
  }
  public function getDiscount() {
    return $this->discount;
  }
  public function setDiscount($discount) {
    $this->discount = $discount;
    return $this;
  }
  public function getPrice() {
    return $this->price;
  }
  public function setPrice($p) {
    $this->price = $p;
    return $this;
  }
}
