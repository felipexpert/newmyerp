<?php
class EntityWrapperModel extends CI_Model {
  private $entity;
  private $entityName;
  function __construct() { parent::__construct(); }
  function entity() {
    $this->checkEntity();
    return $this->entity;
  }
  function setEntity($entity) {
    $this->checkEntityName();
    $this->entity = $entity;
    $this->hook();
    return $this;
  }
  function update($fields) {
    C::fe(function($v, $k) {
      $method = 'set'.ucFirst($k);
      if(method_exists($this->entity(), $method)) $this->entity()->$method($v);
      else throw new Exception('invalid setter: '.$method);
    }, $fields);
    $this->em->persist($this->entity());
    $this->em->flush();
  }
  function prepareById($id) {
    $this->checkEntityName();
    $this->entity = $this->em->find($this->entityName, $id);
    $this->hook();
    return $this;
  }
  function prepareByEntityGetter($entityGetter) { 
    $entity = $entityGetter->first(); 
    $this->checkEntityName();
    $this->entity = $entity;
    $this->hook();
    return $this;
  }
  function prepareAndPutIn($obj, $id = null) {
    if(is_null($id)) $id = $obj->id;
    $this->prepareById($id);
    return $this->put($obj);
  }
  protected function hook() {}
  function map() {
    throw new Exception("Map wasn't declared in subclass yet!");
  }
  function put($obj) {
    throw new Exception("Put wasn't declared in subclass yet!");
  }
  function getEntityName() { return $this->entityName; } 
  function hasEntity() { return isSet($this->entity); }
  protected function setEntityName($entityName) {
    $this->entityName = $entityName;
    return $this;
  }
  /**
   * You may override it to make it safier
   */
  function canRemove() {
    $this->checkEntity();
    return TRUE;
  }
  /**
   * Use this method to remove and efficiently get a status back of whether it was successful or not
   */
  function maybeRemove() {
    $this->checkEntity();
    if($this->canRemove()) {
      $this->unsafeRemove();
      return Either::success($this->entity());
    }
    return Either::fail(lang('notAllowedRemotion'));
  }
  /**
   * Use it only when you are sure canRemove returns TRUE, otherwise an Exception will the thrown
   */
  function remove() {
    $this->checkEntity();
    if(!$this->canRemove()) throw new Exception('This entity cannot be removed!');
    $this->unsafeRemove();
    return $this;
  }
  /*
   * Override it to change the remotion behavior (take care this method don't check canRemove())
   * Don't use it to actually make a remotion, use either maybeRemove() or remove(), which apply 
   * this function in a safe way
   */
  protected function unsafeRemove() {
    $this->checkEntity();
    $this->em->remove($this->entity);
    $this->em->flush();
    return $this;
  }

  /**
   * Consistence checkups
   */
  private function checkEntity() {
    if(!isSet($this->entity)) throw new Exception($this->entityName.': Didn\'t set entity yet!');
  }
  private function checkEntityName() {
    if(!isSet($this->entityName)) throw new Exception($this->entityName.': Didn\'t set entityName');
  }
}
