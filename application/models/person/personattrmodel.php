<?php

use Doctrine\Common\Collections\ArrayCollection;
class PersonAttrModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\PersonAttr";
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
  }
  function map() {
    $a = $this->entity();
    return
      [ 'id' => $a->getId()
      , 'name' => $a->getName()
      , 'rg' => $a->getRg()
      , 'cpf' => $a->getCpf()
      , 'address' => $a->getAddress()
      , 'fone' => $a->getFone()
      , 'creation' => D::millis($a->getCreation())];
  }
}
