<?php
use Doctrine\Common\Collections\ArrayCollection;
class PersonAttrCreatorModel extends CI_Model {
  private $name;
  private $address;
  private $fone;
  private $cpf;
  private $rg;
  private $customer;
  private $creation;
  function __construct() {
    parent::__construct();
  }
  function create() {
    if(!$this->creation)
      $this->creation = new DateTime("now");
    $attr = new Entity\PersonAttr();
    $attr->setName($this->name)->setAddress($this->address)->setFone($this->fone)->setCpf($this->cpf)
      ->setRg($this->rg)->setCreation($this->creation);
    $this->em->persist($attr);
    if($this->customer) {
      $customer = new Entity\Customer();
      $customer->setPersonAttr($attr)->setCreation($this->creation)
        ->setDisabled(FALSE);
      $this->em->persist($customer);
    }
    $this->em->flush();
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function setAddress($address) {
    if($address) $this->address = $address;
    return $this;
  }
  public function setFone($fone) {
    if($fone) $this->fone = $fone;
    return $this;
  }
  public function setCpf($cpf) {
    if($cpf) $this->cpf = $cpf;
    return $this;
  }
  public function setRg($rg) {
    if($rg) $this->rg = $rg;
    return $this;
  }
  public function isCustomer() {
    return $this->customer;
  }
  public function setCustomer($customer) {
    $this->customer = $customer;
    return $this;
  }
  public function getCreation() {
    return $this->creation;
  }
  public function setCreation($creation) {
    $this->creation = $creation;
    return $this;
  }
}
