<?php
class PersonAttrGetterModel extends EntityGetterModel {
  function prepareByCustomerId($customerId) {
    $xs = C::forceCollection($this->em->createQuery("select c, p 
      from Entity\Customer c
      inner join c.personAttr p
      where c.id = :id")->setParameter('id', $customerId)->getResult())->map(function($c) {
        return $c->getPersonAttr();
      });
    $this->prepare($xs);
    return $this;
  }
}
