<?php

class PersonFacadeModel extends CI_Model {
  private $id;
  private $name;
  private $fone;
  private $address;
  private $cpf;
  private $rg;
  private $customer = FALSE;
  
  function initFromPost() {
    $this->id = set_value('id');
    $this->name = set_value('name');
    $this->address = set_value('address') ?: null;
    $this->fone = set_value('fone') ?: null;
    $this->cfp = set_value('cpf') ?: null;
    $this->rg = set_value('rg') ?: null;
    $this->customer = set_value('idCustomer') == 'On' ? TRUE : FALSE;
  }

  function initFromAttrId($attrId) {
    $this->load->model('person/personAttrModel', 'attr');
    $this->load->model('customer/customerGetterModel');
    $attr = $this->attr->prepareById($id)->entity();
    $this->id = $attrId;
    $this->name = $attr->getName();
    $this->address = M::clsc($attr->getAddress()); 
    $this->fone = M::clsc($attr->getFone());
    $this->cpf = M::clsc($attr->getCpf());
    $this->rg = M::clsc($attr->getRg());
    if($this->customerGetterModel->prepareByAttrId($attrId)->has()) {
      $customer = $this->customerGetterModel->first();
      if(!$customer->isDisabled()) {
        $this->customer = TRUE;
      }
    }
  }
  public function getId() {
    return $this->id;
  }
  public function setId($id) {
    $this->id = $id;
    return $this;
  }
  public function getName() {
    return $this->name;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function getFone() {
    return $this->fone;
  }
  public function setFone($fone) {
    $this->fone = $fone;
    return $this;
  }
  public function getAddress() {
    return $this->address;
  }
  public function setAddress($address) {
    $this->address = $address;
    return $this;
  }
  public function getCpf() {
    return $this->cpf;
  }
  public function setCpf($cpf) {
    $this->cpf = $cpf;
    return $this;
  }
  public function getRg() {
    return $this->rg;
  }
  public function setRg($rg) {
    $this->rg = $rg;
    return $this;
  }
  public function getCustomer() {
    return $this->customer;
  }
  public function setCustomer($customer) {
    $this->customer = $customer;
    return $this;
  }
}
