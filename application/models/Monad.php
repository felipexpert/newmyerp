<?php

abstract class Monad {
  static function ret($x) { throw new Exception('Undefined return!'); }
  abstract function bind($f);
  function then($mb) {
    return $this->bind(function() use($mb) { return $mb; });
  }
  static function chain($m, $functions) {
    return C::foldl(function($acc, $func) {
      return $acc->bind($func);
    }, $m, $functions);
  }
}
