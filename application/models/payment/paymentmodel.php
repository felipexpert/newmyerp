<?php
use Doctrine\Common\Collections\ArrayCollection;
class PaymentModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\Payment";
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
  }
  function map() {
    $this->load->model('paymentType/paymentTypeModel');
    $paymentTypeModel = new PaymentTypeModel();
    $payment = $this->entity();
    return
      [ 'id' => $payment->getId()
      , 'amount' => $payment->getAmount()
      , 'paymentType' => $paymentTypeModel->setEntity($payment->getPaymentType())->hasEntity() ? $paymentTypeModel->map() : null
      , 'hasReversal' => !is_null($payment->getReversal())];
  }
}
