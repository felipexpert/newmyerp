<?php
class PaymentCreatorModel extends CI_Model {
  private $portion;
  private $amount;
  private $paymentType;
  private $creation;
  function create() {
    if(!$this->creation) $this->creation = new DateTime('now'); 
    $payment = new Entity\Payment();
    $payment->setPortion($this->portion)
        ->setPaymentType($this->paymentType)
        ->setAmount($this->amount)
        ->setCreation($this->creation);
    $this->portion->getPayments()->add($payment);
    $this->em->persist($payment);
    $this->em->flush();
  }
  function setPortion($portion) {
    $this->portion = $portion;
    return $this;
  }
  function setAmount($amount) {
    $this->amount = $amount;
    return $this;
  }
  function setPaymentType($paymentType) {
    $this->paymentType = $paymentType;
    return $this;
  }
  function setCreation($creation) {
    $this->creation = $creation;
    return $this;
  }
}
