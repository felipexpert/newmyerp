<?php
class EmployeeGetterModel extends EntityGetterModel {
  function prepareAll() {
    $employees = $this->em->createQuery('select e from Entity\Employee e')->getResult();
    $this->prepare($employees);
  }
}
