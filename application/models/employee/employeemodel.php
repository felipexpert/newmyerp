<?php

use Doctrine\Common\Collections\ArrayCollection;
class EmployeeModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\Employee";
  function __construct() {
    parent::__construct();
    $this->setEntityName(EmployeeModel::ENTITY_NAME);
  }
}
