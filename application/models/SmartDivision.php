<?php
class SmartDivision {
  private $number;
  private $divider;
  private $decimals;
  private $primary;
  private $last;
  private $all;
  function __construct($number, $divider, $decimals = null) {
    if(is_null($decimals)) $decimals = lang('decimals');
    $this->number = $number;
    if($divider === 0) throw new Exception('Divider must be greater than 0!');
    $this->divider = $divider;
    $this->decimals = $decimals;
    $this->calc();
  }
  function primary() { return $this->primary; }
  function last() { return $this->last; }
  function either($predicate) { return $predicate ? $this->primary : $this->last; }
  function all() { return $this->all; } 
  private function calc() {
    $d = pow(10, $this->decimals);
    $number = floor($this->number * $d) / $d; 
    $n = $number / $this->divider;
    if(is_nan($n)) throw new Exception('Divided $n cannot be NaN');
    $n = floor($n * $d) / $d;
    if(is_nan($n)) throw new Exception('Truncated $n cannot be NaN');
    $l = $n + ($number - $n * $this->divider);
    if(is_nan($l)) throw new Exception('After adjustments, $l cannot be NaN');
    $this->primary = $n;
    $this->last = $l;
    $array = array();
    for($i = 1; $i < $this->divider; $i++) $array[] = $n;
    if($this->divider > 1) $array[] = $l;
    $this->all = $array;
  }
}
class_alias('SmartDivision', 'SD');
