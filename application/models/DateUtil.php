<?php

final class DateUtil {
  private function __construct() {}
  static function millis($date) {
    if(is_null($date)) return null;
    return strToTime($date->format('Y-m-d H:i:s')) * 1000;
  }
  static function dateToMillis($date) {
    return D::millis($date);
  }
  static function millisToDate($millis) {
    return new DateTime(date('Y/m/d H:i:s', $millis / 1000));
  }
  static function copyDate($date) {
    return D::millisToDate(D::dateToMillis($date));
  }
  static function dateAdd($date, $years = 0, $months = 0, 
    $days = 0, $hours = 0, $minutes = 0, $seconds = 0) {
    return D::dateInterval($date, TRUE, $years, $months, $days, $hours, $minutes, $seconds);
  }
  static function dateSub($date, $years = 0, $months = 0, 
    $days = 0, $hours = 0, $minutes = 0, $seconds = 0) {
    return D::dateInterval($date, FALSE, $years, $months, $days, $hours, $minutes, $seconds);
  }
  static function dateInterval($date, $add = TRUE, $years = 0, $months = 0, 
    $days = 0, $hours = 0, $minutes = 0, $seconds = 0) {
    $method = $add ? 'add' : 'sub';
    $copy = D::copyDate($date);
    $copy->$method(new DateInterval("P${years}Y${months}M${days}DT${hours}H${minutes}M${seconds}S"));
    return $copy;
  }
  static function adf($date) { //android date format
    if($date == null) return null;
    return $date->format('Y-m-d H:i:s');
  }
}
class_alias('DateUtil', 'D');
