<?php

final class Number {
  private function __construct() {}
  static function eq($a, $b) {
    // 1/128 -> EPSILON
    return abs($a - $b) < 0.0078125; 
  }
  static function le($a, $b) { return N::eq($a, $b) || $a < $b; }
  static function ge($a, $b) { return N::eq($a, $b) || $a > $b; }
  static function lt($a, $b) { return !N::eq($a, $b) && $a < $b; }
  static function gt($a, $b) { return !N::eq($a, $b) && $a > $b; }
  static function equalsIgnoreCase($a, $b) { return strCaseCmp($a, $b) === 0; }
  static function df($date, $full = TRUE) {
    if(is_null($date)) return null;
    return $date->format($full ? lang('fullDate') : lang('date'));
  }
  static function floorDecimals($n, $decimals = null) { 
    if(is_null($decimals)) $decimals = lang('decimals');
    $d = pow(10, $decimals);
    return floor($n * $d) / $d; 
  }
  static function ceilDecimals($n, $decimals = null) {
    if(is_null($decimals)) $decimals = lang('decimals');
    $d = pow(10, $decimals);
    return ceil($n * $d) / $d; 
  }
  static function truncateNums($numbers, $total = null, $decimals = null) {
    $tnumbers = C::foldl(function($acc, $n) use($decimals) {
      return C::push(N::floorDecimals($n, $decimals), $acc);
    }, array(), $numbers);
    if($total && count($tnumbers)) {
      $d = pow(10, $decimals);
      $total = round($total * $d) / $d;
      $nt = C::foldl1(function($acc, $n) { return $acc + $n; }, $tnumbers);
      $tnumbers[0] += $total - $nt;
    }
    return $tnumbers;
  }
}
class_alias('Number', 'N');
