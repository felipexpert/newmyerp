<?php
class PaymentTypeGetterModel extends EntityGetterModel {
  function prepareAll() {
    $pt = $this->em->createQuery('select pt from Entity\PaymentType pt')->getResult();
    $pt = DU::getSortedDisplayOrderables(C::forceCollection($pt));
    $this->prepare($pt);
    return $this;
  }
}
