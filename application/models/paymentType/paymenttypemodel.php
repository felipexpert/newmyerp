<?php
use Doctrine\Common\Collections\ArrayCollection;
class PaymentTypeModel extends EntityWrapperModel {
  const ENTITY_NAME = 'Entity\\PaymentType';
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
  }
  function map() {
    $type = $this->entity();
    return
      [ 'id' => $type->getId()
      , 'name' => $type->getName()
      , 'acronym' => $type->getAcronym()];
  }
}
