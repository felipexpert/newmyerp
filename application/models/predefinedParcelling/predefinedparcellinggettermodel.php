<?php
class PredefinedParcellingGetterModel extends EntityGetterModel {
  function prepareAll() {
    $pps = $this->em->createQuery('select pp, pi 
      from Entity\PredefinedParcelling pp
      left join pp.predefinedIntervals pi')->getResult();
    $this->prepare($pps);
    return $this;
  }
}
