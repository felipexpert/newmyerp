<?php
use Doctrine\Common\Collections\ArrayCollection;
class PredefinedParcellingModel extends EntityWrapperModel {
  const ENTITY_NAME = 'Entity\\PredefinedParcelling';
  private $predefinedIntervalModel;
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
    $this->load->model('predefinedInterval/predefinedIntervalModel', '_predefinedIntervalModel');
    $this->predefinedIntervalModel = new PredefinedIntervalModel();
  }
  function generateDates($baseDate) {
    $dates = $this->entity()->getPredefinedIntervals()->map(function($interval) use ($baseDate) {
      $date = null;
      switch($interval->getIntervalType()->getCode()) {
        case 1: //Day
          $date = D::dateAdd($baseDate, 0, 0, $interval->getAmount());
          break;
        case 2: //Week
          $date = D::dateAdd($baseDate, 0, 0, $interval->getAmount() * 7);
          break;
        case 3: //Month
          $date = D::dateAdd($baseDate, 0, $interval->getAmount());
          break;
        case 4: //Year
          $date = D::dateAdd($baseDate, $interval->getAmount());
          break;
        default: assert(FALSE, 'Bogus value!');
      }
      return $date;
    });  
    $dates2 = C::sortCollection($dates, function($dA, $dB) {
      return D::millis($dA) - D::millis($dB);
    });
    return $dates2;
  }
  function map() {
    $pp = $this->entity();
    return 
      [ 'id' => $pp->getId()
      , 'name' => $pp->getName()
      , 'predefinedIntervals' => $pp->getPredefinedIntervals()->map(function($i) {
        return $this->predefinedIntervalModel->setEntity($i)->map();
      })->toArray()]; 
  }
}
