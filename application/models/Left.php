<?php

class Left extends Either {
  private $left;
  function __construct($left) {
    $this->left = $left;
  }
  function isLeft() {
    return TRUE; 
  }
  function left() {
    return $this->left;
  }
  function right() {
    throw new Exception("Left cannot be right");
  }
  function equals($obj) {
    return $obj instanceof Left && $obj->left() === $this->left();
  }
}
