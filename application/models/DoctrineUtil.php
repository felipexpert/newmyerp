<?php

final class DoctrineUtil {
  private function __construct() {}
  static function getNonDisabled($disableables) { //doctrine
    $filtered = $disableables->filter(function($d) {
      return !$d->isDisabled();
    });
    return Collections::adjustIndex($filtered);
  }
  /** 
   * @Deprecated
   */
  static function exists($indexables, $id) { //doctrine
    return static::maybeElemById($indexables, $id)->isPresent();
  }
  /** 
   * @Deprecated
   */
  static function elemById($indexables, $id) { //doctrine
    return static::maybeElemById($indexables, $id)->get();
  }
  static function maybeElemById($indexables, $id) {
    $filtered =  $indexables->filter(function($i) use($id) {
      return $i->getId() == $id;
    });
    return $filtered->count() > 0 ? Optional::of($filtered->first()) : Optional::absent();
  }
  static function getSortedDisplayOrderables($displayOrderables) { //doctrine
    return C::sortCollection(DU::getNonDisabled($displayOrderables), function($a, $b) {
      $diff = $a->getDisplayOrder() - $b->getDisplayOrder();
      assert(is_int($diff), '$diff should be of type int!');
      return $diff !== 0 ? $diff : $a->getId() - $b->getId();
    });
  }
  /** 
   * @Deprecated
   */
  static function getByDisplayOrder($displayOrderables, $displayOrder) { //doctrine
    return static::maybeElemByDisplayOrder($displayOrderables, $displayOrder)->get();
  }
  static function maybeElemByDisplayOrder($displayOrderables, $displayOrder) {
    $displayOrderables = DU::getNonDisabled($displayOrderables);
    $filtered = $displayOrderables->filter(function ($do) use($displayOrder) {
      return $do->getDisplayOrder() == $displayOrder;
    });
    return $filtered->count() > 0 ? Optional::of($filtered->first()) : Optional::absent();
  }
  static function indexes($indexables) {
    return C::forceCollection($indexables)->map(function($i) {
      if(method_exists($i, 'getId')) return $i->getId();
      if(isSet($i->id)) return $i->id;
      throw Exception('Not an indexed object!');
    }); 
  }
}
class_alias('DoctrineUtil', 'DU');
