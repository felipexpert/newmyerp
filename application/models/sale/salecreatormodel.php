<?php
use Doctrine\Common\Collections\ArrayCollection;
class SaleCreatorModel extends CI_Model {
  private $sellingType;
  private $order;
  private $consignment;
  private $orderNumber;
  private $identifier;
  private $aggregatesOff;
  private $creation;
  private $obs;
  private $employee;

  private $aggregateGetterModel;
  private $orderCreatorModel;
  function __construct() {
    parent::__construct();
    $this->aggregateGetterModel = M::l('aggregate/aggregateGetter');
    $this->orderCreatorModel = M::l('order/orderCreator');
  }
  function safeCreate() {
    $saleGetterModel = M::l('sale/saleGetter');
    $hasOthers = isSet($this->orderNumber) 
      ? $saleGetterModel->prepareByOrderNumber($this->orderNumber)->has()
      : FALSE;
    if($hasOthers && C::any(function($s) { return $s->getIdentifier() === null; }, $saleGetterModel->all()))
      return Either::fail(lang('fastSelling.emptyIdentifierErrorOnLastSale'));
    if(!isSet($this->identifier)) {
      if($hasOthers) {
        return Either::fail(lang('fastSelling.emptyIdentifierError'));
      }
    } else {
      if(!M::l('sale/saleIdentifierHelper')->setIdentifier($this->identifier)->canUseIdentifier()) {
        return Either::fail(lang('fastSelling.alreadyUsedIdentifier'));
      }
    }
    $sale = $this->create();
    if(isSet($this->identifier))
      M::l('sale/saleIdentifierHelper')->setSaleId($sale->getId())->setIdentifier($this->identifier)->useIdentifier();
    return Either::success($sale);
  }
  function create() {
    $order = $this->order;
    if(!isSet($order)) $order = $this->orderCreatorModel->setObs($this->obs)
      ->setCreation($this->creation)
      ->setEmployee($this->employee)->create();
    $sale = new Entity\Sale();
    $sale->setOrder($order)->setOrderNumber($this->getOrderNumber())
      ->setSellingType($this->getSellingType())->setConsignment($this->consignment);
    if(!$this->aggregatesOff) C::fe(function($aggregate) use($sale) {
      $sa = new Entity\SaleAggregate();
      $sa->setAggregate($aggregate)
         ->setAmount(1)
         ->setSale($sale)
         ->setDisabled(FALSE);
      $sale->getAggregates()->add($sa);
      $this->em->persist($sa);
    }, $this->aggregateGetterModel->prepareAll()->all());
    $this->em->persist($sale);
    $this->em->flush();
    return $sale;
  }
  public function getSellingType() {
    return $this->sellingType;
  }
  public function setSellingType($sellingType) {
    $this->sellingType = $sellingType;
    return $this;
  }
  public function getOrder() {
    return $this->order;
  }
  public function setOrder($order) {
    $this->order = $order;
    return $this;
  }
  public function getConsignment() {
    return $this->consignment;
  }
  public function setConsignment($consignment) {
    $this->consignment = $consignment;
    return $this;
  }
  public function getOrderNumber() {
    return $this->orderNumber;
  }
  public function setOrderNumber($orderNumber) {
    $this->orderNumber = $orderNumber;
    return $this;
  }
  public function setIdentifier($identifier) {
    $this->identifier = $identifier;
    return $this;
  }
  public function getObs() {
    return $this->obs;
  }
  public function setObs($obs) {
    $this->obs = $obs;
    return $this;
  }
  public function getEmployee() {
    return $this->employee;
  }
  public function setEmployee($e) {
    $this->employee = $e;
    return $this;
  }
  public function isAggregatesOff() {
    return $this->aggregatesOff;
  }
  public function setAggregatesOff($aggregatesOff) {
    $this->aggregatesOff = $aggregatesOff;
    return $this;
  }
  public function getCreation() {
    return $this->creation;
  }
  public function setCreation($creation) {
    $this->creation = $creation;
    return $this;
  }
}
