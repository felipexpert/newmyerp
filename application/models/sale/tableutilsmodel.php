<?php
use Doctrine\Common\Collections\ArrayCollection;
class TableUtilsModel extends CI_Model {
  function __construct() {
    parent::__construct();
    $this->load->model('sale/saleGetterModel');
    $this->load->model('sale/tableModel');
  }
  function getTables() {
    $sales = $this->saleGetterModel->prepareAllActive()->all();
    $sales = $sales->filter(function($sale) { 
      return !is_null($sale->getOrderNumber()); 
    });
    return C::foldl(function($acc, $sale) {
      $tables = $acc->filter(function($table) use($sale) {
        return $table->getOrderNumber() == $sale->getOrderNumber();
      });
      $count = $tables->count();
      if($count == 0) $acc->add((new TableModel())->setOrderNumber($sale->getOrderNumber())->addSale($sale));
      elseif($count == 1) $tables->first()->addSale($sale);
      else  assert(FALSE, 'Count is going wrong!');
      return $acc;
    }, new ArrayCollection(), $sales);
  }
  function getLooseSales() {
    return $this->saleGetterModel->prepareAllActive()->all()->filter(function($sale) {
      return is_null($sale->getOrderNumber());
    });
  }
  function simplifyTables() {
    return $this->getTables()->map(function($t) {
      return array('orderNumber' => $t->getOrderNumber(), 'sales' => $this->simplifySales($t->getSales()));
    })->toArray();
  }
  function simplifyLooseSales() {
    return $this->simplifySales($this->getLooseSales());
  }
  private function simplifySales($sales) {
    return array_values($sales->map(function($s) {
      return array('id' => $s->getId(), 'orderNumber' => $s->getOrderNumber(), 
        'identifier' => $s->getIdentifier() ?: $s->getId());
    })->toArray());
  }
}
