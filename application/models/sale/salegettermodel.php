<?php
class SaleGetterModel extends EntityGetterModel {
  private $consignmentGetter;
  function __construct() {
    parent::__construct();
    $this->load->model('consign/consignmentGetterModel', '_consignmentGetterModel');
    $this->consignmentGetter = new consignmentGetterModel();
  }
  function prepareById($id) {
    $sales = $this->em->createQuery('select s, o, i from Entity\Sale s
        inner join s.order o
        left join o.orderItems i
        where s.id = :id')->setParameter('id', $id)->getResult();
    $this->prepare($sales);
    return $this;
  }
  function prepareByOrderNumber($orderNumber) {
    $sales = $this->em->createQuery('select s, o, i from Entity\Sale s
        inner join s.order o
        left join o.orderItems i
        where o.disabled = FALSE
        and s.orderNumber = :orderNumber
        and s.closing is null')->setParameter('orderNumber', $orderNumber)->getResult();
    $this->prepare($sales);
    return $this;
  }
  function prepareAll() {
    $sales = $this->em->createQuery('select s, o, i from Entity\Sale s
        inner join s.order o
        left join o.orderItems i
        where o.disabled = FALSE
        and s.closing is null')->getResult();
    $this->prepare($sales);
    return $this;
  }
  function prepareAllActive() {
    $sales = $this->em->createQuery('select s, o, i from Entity\Sale s
        inner join s.order o
        left join o.orderItems i
        where o.disabled = FALSE
        and s.closing is null')->getResult();
    $this->prepare($sales);
    return $this;
  }
  function prepareOngoing() {
    $sales = $this->em->createQuery('select s, o, i from Entity\Sale s 
      inner join s.order o
      left join o.orderItems i
      where s.closing is null
      and o.disabled = FALSE
      and s.orderNumber is not null')->getResult(); 
    $this->prepare($sales);
    return $this;
  }
  function prepareByOrderId($orderId) {
    $sales = $this->em->createQuery('select s, o, i from Entity\Sale s
        inner join s.order o
        left join o.orderItems i
        where o.id = :id')->setParameter('id', $orderId)->getResult();
    $this->prepare($sales);
    return $this;
  }
  function prepareByConsignmentId($cId) {
    $sales = $this->em->createQuery('select s from Entity\Sale s
      inner join s.order o
      inner join s.consignment c
      where c.id = :cId
        and o.disabled = FALSE')->setParameter('cId', $cId)->getResult();
    $this->prepare($sales);
    return $this;
  }
}
