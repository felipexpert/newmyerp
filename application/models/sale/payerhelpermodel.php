<?php

class PayerHelperModel extends CI_Model {
  private $saleModel;
  private $canAddPayer;
  private $payerModel;
  private $payerCreatorModel;
  private $newTotal;
  function __construct() {
    parent::__construct();
    $this->load->model('payer/payerModel', '_payerModel');
    $this->payerModel = new PayerModel();
    $this->load->model('payer/payerCreatorModel', '_payerCreatorModel');
    $this->payerCreatorModel = new PayerCreatorModel();
    $this->load->model('calc/newTotalRedistributorModel', '_newTotal');
    $this->newTotal = new NewTotalRedistributorModel();
    $this->load->model('sale/saleModel', '_saleModel');
  }
  function canAddPayer() {
    $this->canAddPayer = N::gt($this->saleModel->total(), C::foldl(function($acc, $p) {
      $this->payerModel->setEntity($p);
      return $acc + $this->payerModel->parceledAmount();
    },0 ,$this->saleModel->entity()->getPayers()));
    return $this->canAddPayer;
  }
  function redistribute() {
    $models = $this->saleModel->entity()->getPayers()->map(function($payer) {
      $pm = new PayerModel();
      $pm->setEntity($payer);
      return $pm;
    });
    $this->newTotal->setItems($models)->perform($this->saleModel->total());
  }
  function addPayer() {
    if(is_null($this->canAddPayer)) $this->canAddPayer();
    if(!$this->canAddPayer) throw new Exception('Cannot add payer');
    $nameNumber = $this->saleModel->entity()->getPayers()->count() + 1;
    $payer = $this->payerCreatorModel->setDescription(lang('payment.person') . ' ' . $nameNumber)
      ->setSale($this->saleModel->entity())->setAmount(0)->create();
    $this->saleModel->prepareById($this->saleModel->entity()->getId());
    $this->em->refresh($this->saleModel->entity());
    $this->redistribute();
    return $payer;
  }
  function addCustomerPayer($customer) {
    $payer = $this->addPayer()->setCustomer($customer);
    $this->em->flush();
    return $payer;
  }

  function canRemovePayerById($payerId) {
    $this->payerModel->prepareById($payerId);
    return N::eq($this->payerModel->paidAmount(), 0);
  }
  function removePayerById($payerId) {
    if(!$this->canRemovePayerById($payerId)) throw new Exception('Cannot remove payer');
    $this->payerModel->prepareById($payerId)->remove();
    $this->payerModel->redistribute();
  }
  function initPayers() {
    if(N::eq($this->saleModel->paidAmount(), 0)) {
      if($this->saleModel->entity()->getPayers()->count() == 0)
        $this->addPayer();
      if(!N::eq($this->saleModel->total(), $this->saleModel->paidAmount()))
        $this->redistribute();
    }
  }
  function setSaleModel($saleModel) {
    $this->saleModel = $saleModel;
    return $this;
  }
  function prepareBySale($sale) {
    $this->saleModel = new SaleModel();
    if(is_numeric($sale)) $this->saleModel->prepareById($sale);
    else $this->saleModel->setEntity($sale);
    return $this;
  }
}
