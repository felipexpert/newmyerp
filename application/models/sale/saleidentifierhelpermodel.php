<?php
class SaleIdentifierHelperModel extends CI_Model {
  private $identifier;
  private $saleId = null; /* Optimal */
  private $canUse = null;
  function __construct() {
    parent::__construct();
    $this->load->model('sale/saleGetterModel');
  }
  function canUseIdentifier() {
    if(isSet($this->saleId)) {
      $sale = $this->saleGetterModel->prepareById($this->saleId)->first();
      if(N::equalsIgnoreCase(trim($sale->getIdentifier()), trim($this->identifier))) return $this->canUse = TRUE;
    }
    $sales = $this->saleGetterModel->prepareAllActive()->all();
    $equalSales = $sales->filter(function($sale) {
      return N::equalsIgnoreCase(trim($sale->getIdentifier()), trim($this->identifier));
    });
    return $this->canUse = $equalSales->count() === 0;
  }
  function useIdentifier() {
    if(is_null($this->canUse)) $this->canUseIdentifier();
    assert(isSet($this->canUse), 'canUse attrib cannot be null!');
    if(!$this->canUse) throw new Exception('Cannot change the identifier in the given conditions!');
    if(!isSet($this->saleId)) throw new Exception('No sale ID!');
    $sale = $this->saleGetterModel->prepareById($this->saleId)->first();
    $sale->setIdentifier(trim($this->identifier));
    $this->em->flush();
    return $this;
  }
  function forceIdentifier() {
    if(!$this->canUseIdentifier()) {
      $this->identifier .= " .";
      return $this->forceIdentifier();
    }
    return $this;
  }
  function setIdentifier($identifier) {
    $this->identifier = $identifier;
    return $this;
  }
  function setSaleId($saleId) {
    $this->saleId = $saleId;
    return $this;
  }
}
