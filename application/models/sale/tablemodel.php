<?php

use Doctrine\Common\Collections\ArrayCollection;
class TableModel {
  private $orderNumber;
  private $sales;
  function __construct() {
    $this->sales = new ArrayCollection();
  }
  public function getOrderNumber() {
    return $this->orderNumber;
  }
  public function setOrderNumber($orderNumber) {
    $this->orderNumber = $orderNumber;
    return $this;
  }
  public function getSales() {
    return $this->sales;
  }
  public function setSales($sales) {
    $this->sales = $sales;
    return $this;
  }
  function addSale($sale) {
    $this->sales->add($sale);
    return $this;
  }
}
