<?php

class SaleHashcoderModel extends CI_Model {
  private $orderNumber;
  const DELIMITER = ":";
  const SUB_DELIMITER = ",";
  function generateForMobile() {
    $hashcode = $this->orderNumber;
    $this->load->model('sale/saleGetterModel', 'sgm');
    $this->sgm->prepareByOrderNumber($this->orderNumber);
    if (!$this->sgm->has()) {
      $hashcode .= self::DELIMITER . "-1";
    } else {
      $order = $this->sgm->first()->getOrder();
      $this->load->model('order/orderModel');
      $this->orderModel->setEntity($order);
      $hashcode .= self::DELIMITER . $order->getId();
      $sortedItems = $this->orderModel->getSortedItems();
      if (count($sortedItems)) {
        $hashcode .= self::DELIMITER;
        forEach($sortedItems as $orderItem)
          $hashcode .= "(" . $orderItem->getId() . self::DELIMITER . floatVal ( $orderItem->getAmount () ) 
              . ")" . self::SUB_DELIMITER;
      }
      $hashcode .= self::DELIMITER . $order->getObs();
    }
    return $hashcode;
  }
  function generateForFastSelling() {
    $hashcode = $this->orderNumber;
    $this->load->model('sale/saleGetterModel', 'sgm');
    $this->sgm->prepareByOrderNumber($this->orderNumber);
    if (!$this->sgm->has()) {
      $hashcode .= self::DELIMITER . "-1";
    } else {
      $order = $this->sgm->first()->getOrder();
      $this->load->model('order/orderModel');
      $this->orderModel->setEntity($order);
      $hashcode .= self::DELIMITER . $this->sgm->first()->getId();
      $sortedItems = $this->orderModel->getSortedItems ();
      if(count($sortedItems)) {
        $hashcode .= self::DELIMITER;
        forEach($sortedItems as $orderItem)
          $hashcode .= "(" . $orderItem->getDisplayOrder() . self::DELIMITER . $orderItem->getProduct()->getCode() 
              . self::DELIMITER . floatVal ( $orderItem->getAmount () ) . ")" . self::SUB_DELIMITER;
      }
    }
    return $hashcode;
  }
  public function getOrderNumber() {
    return $this->orderNumber;
  }
  public function setOrderNumber($orderNumber) {
    $this->orderNumber = $orderNumber;
    return $this;
  }
}
