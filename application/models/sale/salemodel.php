<?php
use Doctrine\Common\Collections\ArrayCollection;
M::l('order/order');
M::l('payer/payer');
M::l('saleAggregate/saleAggregate');
class SaleModel extends SimpleOrderingEntityWrapperModel {
  const ENTITY_NAME = 'Entity\\Sale';
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
  }
  function prepareById($id) {
    $this->setEntity(M::l('sale/saleGetter')->prepareById($id)->first());
    $this->hook();
    return $this;
  }
  function canClose() {
    $payerAmountTotal = C::foldl(function($acc, $payer) {
      return $acc + $payer->getPortionBucket()->getAmount();
    }, 0, $this->entity()->getPayers());
    $payerParceledTotal = C::foldl(function($acc, $payer) {
      return $acc + (new PayerModel)->setEntity($payer)->paidAmount();
    }, 0, $this->entity()->getPayers());
    return N::eq($payerAmountTotal, $this->total()) && N::eq($payerParceledTotal, $payerAmountTotal) 
      && $this->entity()->getClosing() == null;
  }
  function close() {
    if(!$this->canClose()) throw new Exception('This sale is cannot be closed!');
    $this->entity()->setClosing(new DateTime('now'));
    $this->em->flush();
  }
  function paidAmount() {
    return C::foldl(function($acc, $p) {
      return $acc + (new PayerModel)->setEntity($p)->paidAmount();
    }, 0, $this->entity()->getPayers());
  }
  function outstanding() {
    return $this->total() - $this->paidAmount();
  }
  function canCancel() {
    return N::eq($this->paidAmount(), 0) && !$this->hasEnabledExchanges();
  }
  function cancel() {
    if(!$this->canCancel()) throw new Exception('This sale cannot be canceled!');
    M::l('order/order')->setEntity($this->entity()->getOrder())->disable();
  }
  function maybeCancel() {
    if(!$this->canCancel()) return Either::fail(lang('fastSelling.cannotCancel'));
    $this->cancel();
    return Either::success($this->entity());
  }
  function calcAggregateAmount($aggr) {
    $total = $this->calcOrderTotalPrice();
    return N::ceilDecimals($aggr->getOperator()->operate(0, $total, $aggr->getScalar()));
  }
  function aggregateTotal() {
    $totalOrder = $this->calcOrderTotalPrice();
    return C::foldl(function($acc, $sa) use($totalOrder) {
      return $sa->getAggregate()->getOperator()->operate($acc, $totalOrder, $sa->getAggregate()->getScalar()
        * $sa->getAmount());
    }, 0, DU::getNonDisabled($this->entity()->getAggregates()));
  }
  function total($stockId = null) {
    $order = $this->entity()->getOrder();
    return N::ceilDecimals($this->calcOrderTotalPrice($stockId) + $this->aggregateTotal()
        - (new OrderModel)->setEntity($this->entity()->getOrder())->discount());
  }
  function changeDiscount($discount) {
    $order = $this->entity()->getOrder();
    if(N::lt($discount + $order->getSecondDiscount(), $this->calcOrderTotalPrice())) {
      $order->setDiscount($discount);
      $this->em->flush();
    }
  }
  function changeSecondDiscount($sd) {
    $order = $this->entity()->getOrder();
    if(N::lt($sd + $order->getDiscount(), $this->calcOrderTotalPrice())) {
      $order->setSecondDiscount($sd);
      $this->em->flush();
    }
  }
  function getProductFlow() {
    return Entity\FlowType::OUT_SOLD;
  }
  function hasEnabledExchanges() {
    $this->load->database();
    $s = $this->entity();
    $count = $this->db->query('
    select count(e.id) c 
      from Exchange e
      inner join `Order` o on o.id = e.order 
      inner join Sale s on s.id = e.sale
    where s.id = ? and o.disabled = 0;'
    , [$s->getId()])->row()->c;
    return $count > 0;
  }
  function map($extra = '') {
    $s = $this->entity();
    $mapped = [ 'id' => $s->getId()
    , 'orderNumber' => $s->getOrderNumber()
    , 'identifier' => $s->getIdentifier()
    , 'order' => (new OrderModel)->setEntity($this->entity()->getOrder())
        ->map(T::contains('orderItems', $extra) ? 'orderItems' : '') 
    , 'saleAggregates' => $s->getAggregates()->map(function($sa) {
      $saMapped = (new SaleAggregateModel)->setEntity($sa)->map(); 
      $saMapped['subtotal'] = $this->calcAggregateAmount($sa->getAggregate()); 
      return $saMapped;
    })->toArray()
    , 'saleAggregatesTotal' => $this->aggregateTotal()
    , 'outstanding' => $this->outstanding()
    , 'totalOrder' => $this->calcOrderTotalPrice()
    , 'total' => $this->total()
    , 'consignment' => Optional::fromNullable($s->getConsignment())
      ->map(function($c) { return M::l('consign/consignment')->setEntity($c)->map(); })
      ->getOrNull()
    , 'hasEnabledExchanges' => $this->hasEnabledExchanges()
    , 'closing' => D::millis($s->getClosing())];
    if(T::contains('payers', $extra)) $mapped['payers'] = $s->getPayers()->map(function($p) {
      return (new PayerModel)->setEntity($p)->map();
    })->toArray();
    return $mapped;
  }
}
