<?php

use Doctrine\Common\Collections\ArrayCollection;
final class Collections {
  private function __construct() {}
  static function unique($collection) {
    $collection = C::forcecollection($collection);
    return C::foldl(function($acc, $e) {
      if(!$acc->contains($e)) $acc->add($e);
      return $acc;
    }, new arraycollection(), $collection);
  }
  static function produce($amount, $f, $oneBased = false, $array = null) {
    if(!is_array($array)) $array = array();
    if($oneBased) for($i = 1; $i <= $amount; $i++) $array[] = $f($i);
    else for($i = 0; $i < $amount; $i++) $array[] = $f($i);
    return $array;
  }
  static function forceArray($array) {
    if(!is_array($array))
      return array_values($array->toArray());
    return array_values($array);
  }
  static function forceCollection($list) {
    if (is_array($list))
      return new ArrayCollection($list);
    return new ArrayCollection($list->toArray());
  }
  static function adjustIndex($xs) {
    if(is_array($xs)) return Collections::forceArray($xs);
    else return Collections::forceCollection(Collections::forceArray($xs));
  }
  static function sortCollection($collection, $criteria) {
    $iterator =  $collection->getIterator();
    $iterator->uasort($criteria);
    return new ArrayCollection(iterator_to_array($iterator)); 
  }
  static function dropWhile($f, $xs) {
    $drop = TRUE;
    return C::foldl(function($acc, $v, $k) use($f, &$drop) {
      if(!$f($v, $k)) $drop = FALSE;
      if(!$drop) $acc[] = $v;
      return $acc;
    }, [], $xs);
  }
  static function foldl($f, $acc = null, $collection = null) {
    if(isSet($collection)) {
      forEach($collection as $k => $v)
        $acc = $f($acc, $v, $k);
      return $acc;
    } elseif(isSet($acc)) {
      return function($collection) use($f, $acc) {
        return C::foldl($f, $acc, $collection);
      };
    } else {
      return function($acc, $collection = null) use($f) {
        return C::foldl($f, $acc, $collection);
      };
    }
  }
  static function foldl1($f, $collection) {
    $acc = null;
    $first = true;
    forEach($collection as $k => $v)
      if($first) { $acc = $v; $first = false; } else { $acc = $f($acc, $v, $k); }
    return $acc;
  }
  static function fe($f, $collection) {
    forEach($collection as $k => $v)
      $f($v, $k, $collection);
  }
  static function any($f, $collection) {
    forEach($collection as $k => $v)
      if($f($v, $k)) return TRUE;
    return FALSE;
  }
  static function all($f, $collection) {
    forEach($collection as $k => $v)
      if(!$f($v, $k)) return FALSE;
    return TRUE;
  }
  /**
   * Given an @arg0 and an @arg1, it creates a new array which has all elements from @arg1, in
   * the correct key positions (0 until count(@arg1) - 1) and then pushes the @arg0
   * Given an @arg0, @arg1 and an @arg2, it creates a new array which has all elements from @arg2, in
   * the correct key positions (0 until count(@arg2) - 1) and then pushes the @arg1 at the
   * @arg0 position
   */
  static function push() {
    $args = func_get_args();
    if(count($args) == 3) {
      $array = array_values($args[2]);
      $array[$args[0]] = $args[1];
      return $array;
    }
    $array = array_values($args[1]);
    $array[] = $args[0]; 
    return $array;
  }
  static function zipWith($f, $xs, $ys) {
    $xs = C::forceArray($xs);
    $ys = C::forceArray($ys);
    $xsc = count($xs);
    $ysc = count($ys);
    if($ysc < $xsc) $xsc = $ysc;
    $result = array();
    for($i = 0; $i < $xsc; $i++) $result[$i] = $f($xs[$i], $ys[$i]);
    return $result;
  }
  static function reverseCollection($collection) {
    $result = new ArrayCollection();
    for($i = $collection->count() - 1; $i >= 0; $i--)
      $result->add($collection->get($i));
    assert($collection->count() === $result->count(), 'the given collection and the result aren\'t of same size!');
    return $result;
  }
  static function subtractCollections($xs, $ys) {
    $zs = $xs->filter(function($e) use($ys) {
      return !$ys->contains($e);
    })->toArray();
    $zs2 = array_values($zs);
    return new ArrayCollection($zs2);
  }
  static function find($predicate, $array) {
    forEach($array as $value) if($predicate($value)) return Optional::of($value);
    return Optional::absent(); 
  }
  static function take($quantity, $array) {
    return array_slice(Collections::forceArray($array), 0, $quantity);
  }
}
class_alias('Collections', 'C');
