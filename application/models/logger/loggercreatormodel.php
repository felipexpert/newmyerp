<?php
class LoggerCreatorModel extends CI_Model {
  private $message;
  function create() {
    $logger = new Entity\Logger();
    $logger->setMessage($this->message);
    $this->em->persist($logger);
    $this->em->flush();
    return $logger;
  }
  function d() {
    $this->setMessageByArray(func_get_args());
    $this->create();
    return $this;
  }
  function setMessage() {
    return $this->setMessageByArray(func_get_args());
  }
  function setMessageByArray($array) {
    $this->message = '';
    forEach($array as $m)
      $this->message .= $m.' ';
    return $this;
  }
}
