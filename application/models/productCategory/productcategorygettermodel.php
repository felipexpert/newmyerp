<?php
class ProductCategoryGetterModel extends EntityGetterModel {
  function prepareAll() {
    $cats = $this->em->createQuery('select c, p from Entity\ProductCategory c
      left join c.printer p')->getResult();
    $this->prepare($cats);
    return $this;
  }
  function prepareByFilter($items, $filter, $optional = null) {
    $cats = $this->em->createQuery('select c, p from Entity\ProductCategory c
      left join c.printer p
      where c.name like :name')->setParameter('name', '%'.$filter.'%')->setMaxResults($items)->getResult();
    $this->prepare($cats);
    return $this;
  }
}
