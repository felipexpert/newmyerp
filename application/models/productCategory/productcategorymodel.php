<?php

use Doctrine\Common\Collections\ArrayCollection;
class ProductCategoryModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\ProductCategory";
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
  }
  function update($name, $printerId) {
    Maybe::applyTo($name, function($name) { $this->entity()->setName($name); });
    Maybe::applyTo($printerId, function($printerId) { 
      $this->load->model('printer/printerGetterModel');
      $this->entity()->setPrinter($this->printerGetterModel->prepareById($printerId)->first());
    });
    $this->em->flush();
    return $this;
  }
  function hasPrinterPath(){
    return !is_null($this->entity()->getPrinter());
  }
  function getPrinterPath(){
    if(!$this->hasPrinterPath()) throw new Exception("There is no printer path!");
    assert(!is_null($this->entity()->getPrinter()->getPath()), "Printer has no path!");
    return $this->entity()->getPrinter()->getPath();
  }
  function map() {
    $c = $this->entity();
    return
      [
        'id' => $c->getId()
      , 'name' => $c->getName()
      , 'printer' => Optional::fromNullable($c->getPrinter())->map(function($printer) {
        return M::l('printer/printer')->setEntity($printer)->map();
      })->getOrNull()
      ];
  }
}
