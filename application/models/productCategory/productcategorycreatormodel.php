<?php
use Doctrine\Common\Collections\ArrayCollection;
class ProductCategoryCreatorModel extends CI_Model {
  private $name;
  private $printer;
  function create() {
    $cat = new Entity\ProductCategory(); 
    $cat->setName($this->name)
        ->setPrinter($this->printer);
    $this->em->persist($cat);
    $this->em->flush();
    return $cat;
  }
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
  public function setPrinter($printer) {
    $this->printer = $printer;
    return $this;
  }
  public function preparePrinterById($printerId) {
    $this->load->model('printer/printerGetterModel');
    $this->printer = $this->printerGetterModel->prepareById($printerId)->first();
    return $this;
  }
}
