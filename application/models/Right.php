<?php

class Right extends Either {
  private $right;
  function __construct($right) {
    $this->right = $right;
  }
  function isRight() {
    return TRUE; 
  }
  function left() {
    throw new Exception("Right cannot be left");
  }
  function right() {
    return $this->right;
  }
  function equals($obj) {
    return $obj instanceof Right && $obj->right() === $this->right();
  }
}
