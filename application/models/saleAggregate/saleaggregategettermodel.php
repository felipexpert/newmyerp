<?php
class SaleAggregateGetterModel extends EntityGetterModel {
  function prepareBySaleId($saleId) {
    $xs = $this->em->createQuery('select sa, a from Entity\SaleAggregate sa
      inner join sa.aggregate a
      where sa.sale = :sale')->setParameter('sale', $saleId)->getResult();
    $this->prepare($xs);
    return $this;
  }
}
