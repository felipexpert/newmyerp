<?php

use Doctrine\Common\Collections\ArrayCollection;
class SaleAggregateModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\SaleAggregate";
  function __construct() {
    parent::__construct();
    $this->setEntityName(SaleAggregateModel::ENTITY_NAME);
  }
  function updateAmount($amount) {
    $this->entity()->setAmount($amount);
    $this->em->flush();
  }
  function map() {
    $sa = $this->entity();
    $aggr = $sa->getAggregate();
    return
       [ 'id' => $sa->getId()
       , 'name' => $aggr->getName()
       , 'amount' => $sa->getAmount()];
  }
}
