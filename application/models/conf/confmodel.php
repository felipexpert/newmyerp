<?php
class ConfModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\Conf";
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
  }
  function map() {
    $c = $this->entity();
    return [ 'barCodeLength' => $c->getBarCodeLength()
      , 'productTaxRate' => $this->maybeProductTaxRate()->map(function($ptr) {
        return M::l('productTaxRate/productTaxRate')->setEntity($ptr)->map(); 
      })->getOrNull()];
  }

  function maybeProductTaxRate() {
    return Optional::fromNullable($this->entity()->getProductTaxRate());
  }
}
