<?php
class ProductFlowModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\ProductFlow";
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
  }
  function map() {
    $f = $this->entity();
    return 
      [ 'id' => $f->getId()
      , 'flowType' => $f->getFlowType()
      , 'flowTypeName' => Entity\FlowType::getName($f->getFlowType())
      , 'amount' => $f->getAmount()
      , 'creation' => D::millis($f->getCreation())];
  }
}
