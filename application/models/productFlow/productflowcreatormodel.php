<?php
class ProductFlowCreatorModel extends CI_Model {
  private $productInstance;
  private $stock;
  private $flowType;
  private $amount;
  function create() {
    if(!isSet($this->stock)) $this->setStockId(1);
    $productFlow = new Entity\ProductFlow();
    $productFlow->setProductInstance($this->productInstance)
       ->setFlowType($this->flowType)
       ->setAmount($this->amount)
       ->setStock($this->stock)
       ->setCreation(new DateTime('now'));
    $this->productInstance->getProductFlows()->add($productFlow);
    $this->em->persist($productFlow);
    $this->em->flush();
    return $productFlow;
  }
  function setStockId($stockId) {
    $stockModel = M::l('stock/stock');
    $this->stock = $stockModel->prepareById($stockId)->entity(); 
  }
  public function getProductInstance() {
    return $this->productInstance;
  }
  public function setProductInstance($productInstance) {
    $this->productInstance = $productInstance;
    return $this;
  }
  public function getStock() {
    return $this->stock;
  }
  public function setStock($stock) {
    $this->stock = $stock;
    return $this;
  }
  public function getFlowType() {
    return $this->flowType;
  }
  public function setFlowType($flowType) {
    $this->flowType = $flowType;
    return $this;
  }
  public function getAmount() {
    return $this->amount;
  }
  public function setAmount($amount) {
    $this->amount = $amount;
    return $this;
  }
 
}
