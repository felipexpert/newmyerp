<?php
use Doctrine\Common\Collections\ArrayCollection;
class EntityGetterModel extends CI_Model {
  private $has;
  private $size;
  private $list;
  private $first;
  function isPrepared() {
    return isSet($this->has);
  }
  function has() {
    if(!isSet($this->has))
      throw new Exception ( 'First prepare the object.' );
    return $this->has;
  }
  function size() {
    if(!isSet($this->has))
      throw new Exception ( 'First prepare the object.' );
    return $this->size;
  }
  function first() {
    if(!isSet($this->has))
      throw new Exception ( 'First prepare the object.' );
    return $this->first;
  }
  function all() {
    if(!isSet($this->has))
      throw new Exception ( 'First prepare the object.' );
    return $this->list;
  }
  protected function prepare($list) {
    $list = C::forceCollection($list);
    $this->list = $list;
    $this->size = count($list);
    $this->has = $this->size ? TRUE : FALSE;
    $this->first = $this->has ? $list->first() : null;
  }
}
