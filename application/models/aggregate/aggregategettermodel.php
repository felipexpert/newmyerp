<?php
class AggregateGetterModel extends EntityGetterModel {
  function prepareAll() {
    $aggrs = $this->em->createQuery('select a from Entity\Aggregate a
    where a.disabled = FALSE and 
    (a.validUntil is null or a.validUntil > CURRENT_TIMESTAMP())')->getResult();
    $this->prepare($aggrs);
    return $this;
  }
}
