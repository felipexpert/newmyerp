<?php

class AggregateModel extends EntityWrapperModel {
  const ENTITY_NAME = "Entity\\Aggregate";
  function __construct() {
    parent::__construct();
    $this->setEntityName(OrderModel::ENTITY_NAME);
  }
  function name() {
    return $this->entity()->getName()
      .($this->entity()->getOperator() == Entity\Operator::elt()[Entity\Operator::PERCENTAGE]
      ? ' ('.intVal($this->entity()->getScalar()).'%)' : '');
  }
}
