<?php
use Doctrine\Common\Collections\ArrayCollection;
class PredefinedIntervalModel extends EntityWrapperModel {
  const ENTITY_NAME = 'Entity\\PredefinedInterval';
  function __construct() {
    parent::__construct();
    $this->setEntityName(self::ENTITY_NAME);
  }
  function map() {
    $pi = $this->entity();
    return 
      [ 'id' => $pi->getId()
      , 'amount' => $pi->getAmount()
      , 'intervalType' => 
        [ 'code' => $pi->getIntervalType()->getCode() 
        , 'name' => $pi->getIntervalType()->getName()]
      ];
  }
}
