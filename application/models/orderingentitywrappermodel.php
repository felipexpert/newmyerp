<?php

class OrderingEntityWrapperModel extends EntityWrapperModel {
  function __construct() {
    parent::__construct();
  }
  function summarize($oi, $stockId = null) {
    if(is_numeric($oi)) $oi = DU::elemById($this->entity()->getOrder()->getOrderItems(), $oi);
    $summarize = function($flows) use($oi, $stockId) {
      $mapped = [];
      $mapped['flows'] = C::forceCollection($flows)->map(function($f) use($oi, $stockId) {
        return [ 'flowType' => Entity\FlowType::getName($f)
          , 'amount' => $this->calcOrderItemAmountByFlowType($oi, $f, $stockId)
          , 'price' => $this->calcOrderItemPriceByFlowType($oi, $f, $stockId)];
      })->toArray();
      $mapped['amount'] = C::foldl(function($acc, $i) {
        return $acc + $i['amount'];
      } , 0, $mapped['flows']);
      $mapped['price'] = C::foldl(function($acc, $i) {
        return $acc + $i['price'];
      } , 0, $mapped['flows']);
      return $mapped;
    };
    return 
      [ 'positiveFlows' => $summarize($this->getPositiveFlows())
      , 'negativeFlows' => $summarize($this->getNegativeFlows())
      , 'amount' => $this->calcOrderItemAmount($oi)
      , 'price' => $this->calcOrderItemPrice($oi)];
  }
  function calcOrderItemAmountByFlowType($oi, $flowType, $stockId = null) {
    if(is_numeric($oi)) $oi = DU::elemById($this->entity()->getOrder()->getOrderItems(), $oi);
    if(!$this->containsOrderItem($oi)) throw new Exception("The given orderItem doesn't exist in Order"); 
    if(!in_array($flowType, $this->getNegativeFlows()) && !in_array($flowType, $this->getPositiveFlows())) 
      throw new Exception('Your flow has to be positive or negative!');
    return M::l('orderItem/orderItem')
             ->setEntity($oi)
             ->amount($flowType, $stockId);
  }
  function calcOrderItemPriceByFlowType($oi, $flowType, $stockId = null) {
    if(is_numeric($oi)) $oi = DU::elemById($this->entity()->getOrder()->getOrderItems(), $oi);
    if(!$this->containsOrderItem($oi)) throw new Exception("The given orderItem doesn't exist in Order"); 
    if(!in_array($flowType, $this->getNegativeFlows()) && !in_array($flowType, $this->getPositiveFlows())) 
      throw new Exception('Your flow has to be positive or negative!');
    return M::l('orderItem/orderItem')
             ->setEntity($oi)
             ->price($flowType, $stockId);
  }
  function calcPositiveOrderItemAmount($oi, $stockId = null) {
    if(is_numeric($oi)) $oi = DU::elemById($this->entity()->getOrder()->getOrderItems(), $oi);
    if(!$this->containsOrderItem($oi)) throw new Exception("The given orderItem doesn't exist in Order"); 
    return M::l('orderItem/orderItem')
             ->setEntity($oi)
             ->amount($this->getPositiveFlows(), [], $stockId);
  }
  function calcNegativeOrderItemAmount($oi, $stockId = null) {
    if(is_numeric($oi)) $oi = DU::elemById($this->entity()->getOrder()->getOrderItems(), $oi);
    if(!$this->containsOrderItem($oi)) throw new Exception("The given orderItem doesn't exist in Order"); 
    return -1 * M::l('orderItem/orderItem')
             ->setEntity($oi)
             ->amount([], $this->getNegativeFlows(), $stockId);
  }
  function calcOrderItemAmount($oi, $stockId = null) {
    if(is_numeric($oi)) $oi = DU::elemById($this->entity()->getOrder()->getOrderItems(), $oi);
    if(!$this->containsOrderItem($oi)) 
      throw new Exception('The given orderItem doesn\'t exist in Order - orderItemOrderId: ('
        . $oi->getOrder()->getId() . '), orderId: (' . $this->entity()->getOrder()->getId() . ')');
    return M::l('orderItem/orderItem')
             ->setEntity($oi)
             ->amount($this->getPositiveFlows(), $this->getNegativeFlows(), $stockId);
  }
  function calcPositiveOrderItemPrice($oi, $stockId = null) {
    if(is_numeric($oi)) $oi = DU::elemById($this->entity()->getOrder()->getOrderItems(), $oi);
    if(!$this->containsOrderItem($oi)) throw new Exception("The given orderItem doesn't exist in Order"); 
    return M::l('orderItem/orderItem')
             ->setEntity($oi)
             ->price($this->getPositiveFlows(), [], $stockId);
  }
  function calcNegativeOrderItemPrice($oi, $stockId = null) {
    if(is_numeric($oi)) $oi = DU::elemById($this->entity()->getOrder()->getOrderItems(), $oi);
    if(!$this->containsOrderItem($oi)) throw new Exception("The given orderItem doesn't exist in Order"); 
    return -1 * M::l('orderItem/orderItem')
             ->setEntity($oi)
             ->price([], $this->getNegativeFlows(), $stockId);
  }
  function calcOrderItemPrice($oi, $stockId = null) {
    if(is_numeric($oi)) $oi = DU::elemById($this->entity()->getOrder()->getOrderItems(), $oi);
    if(!$this->containsOrderItem($oi)) throw new Exception("The given orderItem doesn't exist in Order"); 
    return M::l('orderItem/orderItem')
             ->setEntity($oi)
             ->price($this->getPositiveFlows(), $this->getNegativeFlows(), $stockId);
  }
  function updateOrderItemAmount($oi, $flowType, $amount, $stockId = null) {
    if(is_numeric($oi)) $oi = DU::elemById($this->entity()->getOrder()->getOrderItems(), $oi);
    if(!$this->containsOrderItem($oi)) throw new Exception("The given orderItem doesn't exist in Order"); 
    if(!isSet($flowType)) $flowType = $this->defaultFlow();
    if(!in_array($flowType, $this->getNegativeFlows()) && !in_array($flowType, $this->getPositiveFlows())) 
      throw new Exception('Your flow has to be positive or negative!');
    M::l('orderItem/orderItem')
      ->setEntity($oi)
      ->updateAmount($this->getPositiveFlows(), $this->getNegativeFlows(), $flowType, $amount, $stockId);
  }
  function addOrderItemFlow($oi, $flowType, $amount, $stockId = null) {
    if(is_numeric($oi)) $oi = DU::elemById($this->entity()->getOrder()->getOrderItems(), $oi);
    if(!$this->containsOrderItem($oi)) throw new Exception("The given orderItem doesn't exist in Order"); 
    if(!isSet($flowType)) $flowType = $this->defaultFlow();
    if(!in_array($flowType, $this->getNegativeFlows()) && !in_array($flowType, $this->getPositiveFlows())) 
      throw new Exception('Your flow has to be positive or negative!');
    M::l('orderItem/orderItem')
      ->setEntity($oi)
      ->addFlow($flowType, $amount, $stockId);
  }
  function calcPositiveOrderTotalPrice($stockId = null) {
    return C::foldl(function($acc, $oi) use($stockId) {
      return $acc + $this->calcPositiveOrderItemPrice($oi, $stockId);
    }, 0, DU::getNonDisabled($this->entity()->getOrder()->getOrderItems()));
  }
  function calcNegativeOrderTotalPrice($stockId = null) {
    return C::foldl(function($acc, $oi) use($stockId) {
      return $acc + $this->calcNegativeOrderItemPrice($oi, $stockId);
    }, 0, DU::getNonDisabled($this->entity()->getOrder()->getOrderItems()));
  }
  function calcOrderTotalPrice($stockId = null) {
    return C::foldl(function($acc, $oi) use($stockId) {
      return $acc + $this->calcOrderItemPrice($oi, $stockId);
    }, 0, DU::getNonDisabled($this->entity()->getOrder()->getOrderItems()));
  }
  function getPositiveFlows() {
    return [];
  }
  function getNegativeFlows() {
    return [];
  }
  private function containsOrderItem($oi) {
    return DU::exists($this->entity()->getOrder()->getOrderItems(), $oi->getId()); 
  }
  private function defaultFlow() {
    if(count($this->getPositiveFlows())) return $this->getPositiveFlows()[0];
    if(count($this->getNegativeFlows())) return $this->getNegativeFlows()[0];
    throw new Exception("There is no default flow!");
  }
}
