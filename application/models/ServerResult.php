<?php
class ServerResult {
  const SUCCESS = 1;
  const FAIL = 2;
  const UNEXPECTED = 3;
  private static $INSTANCES;
  static function init() {
    if(!self::$INSTANCES) self::$INSTANCES = array(
      self::SUCCESS => new ServerResult(self::SUCCESS, 'success'),
      self::FAIL => new ServerResult(self::FAIL, 'fail'),
      self::UNEXPECTED => new ServerResult(self::UNEXPECTED, 'unexpected')
    );
  }
  static function elt() { return self::$INSTANCES; }
  private $code;
  private $name;
  private function __construct($code, $name) {
    $this->code = $code;
    $this->name = $name;
  }
  function getCode() {
    return $this->code;
  }
  function getName() {
    return $this->name;
  }
}
ServerResult::init();
