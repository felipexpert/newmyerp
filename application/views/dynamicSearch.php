<div id="<?=$id?>" class="dynamicSearch" data-target-input="<?=$inputTarget?>">
  <div class="searchForm">
    <h3><?=$title?></h3>
    <hr class="hrStyle">
    <form action="<?=$action?>" class="form-inline clearfix enterAsTab">
      <div class="form-group">
        <label for="filter" class="small"><?=lang('ds.filterBy')?>: </label>
        <?=$filterSelect?>
      </div>
      <div class="form-group">
        <label for="data" class="small"><?=lang('ds.filter')?>: </label>
        <input type="text" name="data"  class="form-control" autocomplete="off" data-focus="true">
      </div>
      <div class="form-group">
        <label for="" class="small">&nbsp</label>
        <input type="submit" value="OK"  class="form-control btn btn-success">
      </div>
    </form>
  </div>
  <div class="tableTarget table-responsive"></div>
</div>
