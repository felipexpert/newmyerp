<!DOCTYPE html>
<html ng-app="webapp">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?=lang('mindware')?></title>
    <link rel="icon" href="<?php echo base_url(); ?>resource/image/icon.png">
    <!-- Bootstrap -->
    <link href="<?=base_url()?>resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url()?>resource/css/main.css?v=<?=CSS_JS_VERSION?>">
    <!--Estilização dos Glyphicons-->
    <link rel="stylesheet" href="<?=base_url()?>resource/css/font-awesome.min.css">
    <!--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">-->
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body style="padding: 0;">
