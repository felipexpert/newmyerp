<style>
    #panelTitle .titleDiv{background:#e9e9e9; font-size: 10px; border-right: 1px solid #555; border-bottom: 1px solid #333;}
    .panelButtons a, .panelButtons button{width: 40px;}
    .btnPrint{left: -24px;}
    .bottom40{margin-bottom: 40px;}
    #bodyBilling{padding-top: 0; clear: both;}
    #bodyBilling .row{padding-top: 10px; border-bottom: 1px solid #333;}
    .right0{margin-right: 0;}
</style>
<main>
    <div ng-controller="BillingController as billingCtrl" ng-init="billingCtrl.init(<?=$saleId?>)">

        <div class="panel panelHeader">
            <div class="panel-heading">
                <div class="panelButtons col-lg-3 col-md-12 col-sm-12 col-xs-12">
                  <a id="ancBack" href="<?=base_url()?>vwSale/<?=$saleId?>" 
                        class="btn navbar-btn btn-warning" title="<?=lang('back')?>">
                    <i class="fa fa-rotate-left"></i>
                    <span class="btnShortcut">V</span>
                  </a>
                  <a id="ancHome" href="<?=OUTER_URL?>" class="btn btn-primary navbar-btn" title="Inicio">
                    <i class="fa fa-home"></i>
                    <span class="btnShortcut">J</span>
                  </a>
                  <a id="ancTables" href="<?=base_url()?>selling/tables" 
                        id="backOrderNumbers" title="<?=lang('fastSelling.orders')?>" 
                        class="btn navbar-btn btn-primary">
                    <i class="fa fa-edit"></i>
                    <span class="btnShortcut">T</span>
                  </a>
                  <button ng-click="billingCtrl.printECF()"
                                     title="Imprimir Cupom Fiscal" 
                                     type="button" 
                                     class="btn navbar-btn btn-primary">
                    <i class="fa fa-print"></i><span class="newShortcut">ECF</span><span class="btnShortcut btnPrint">E</span>
                  </button>
                  <button ng-click="billingCtrl.printInvoice()"
                                     title="Imprimir Recibo" 
                                     type="button" 
                                     class="btn navbar-btn btn-info">
                    <i class="fa fa-print"></i><span class="newShortcut">REC</span><span class="btnShortcut btnPrint">I</span>
                  </button>
                  <button ng-show="billingCtrl.canClose()" ng-click="billingCtrl.close()" 
                          title="Liberar Comanda" 
                          type="button" 
                          class="btn navbar-btn btn-success">
                    <i class="fa fa-unlock"></i><span class="btnShortcut">F</span>
                  </button>
                </div>
                <h3 class="panel-title panelStatus text-center col-lg-3 col-md-4 col-sm-6 col-xs-12" style="color: #428bca;">
                    <?=lang('billing.consumption')?>:
                    <span id="totalItems">{{ billingCtrl.sale.totalOrder|lMoney }}</span>
                </h3>
                <h3 class="panel-title panelStatus text-center col-lg-3 col-md-4 col-sm-6 col-xs-12" style="color: #5cb85c;">
                    <?=lang('total')?>:
                    <span id="total">{{ billingCtrl.sale.total|lMoney }}</span>
                </h3>
                <h3 class="panel-title panelStatus text-center col-lg-3 col-md-4 col-sm-12 col-xs-12" style="color: #d9534f;">
                    <?=lang('billing.pending')?>:
                    <span id="difference">{{billingCtrl.pending|lMoney}}</span></strong>
                </h3>
            </div>
            <div class="panel-body panelFooter">
                <h6 class="col-lg-8 col-md-8 col-sm-8 col-xs-6 text-left panelFooterLeft">
                <?=lang('billing.orderId')?>: {{ billingCtrl.saleId }}</h6>
                <h5 class="col-lg-4 col-md-4 col-sm-4 col-xs-6 text-center panelFooterRight">
                <?=lang('billing.commandNumber')?>: {{ billingCtrl.sale.orderNumber }}</h5>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 bottom40">
        <div class="panel panel-default">
            <div class="panel-heading panelPayers">
            </div>
            <div class="panel-body">
                <xp-discount sale-id="<?=$saleId?>" update="billingCtrl.updateSale()"></xp-discount>
            </div>
        </div>
            <xp-aggregates ng-if="billingCtrl.sale.saleAggregates.length" 
                           sale-id="<?=$saleId?>" 
                           update="billingCtrl.updateSale()"></xp-aggregates>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 bottom40">
            <div class="panel panel-default">
                <div class="panel-heading panelPayers" style="border: #333;">
                    <h3 class="panel-title text-left col-lg-4 col-md-3 col-sm-4 col-xs-4"><?=lang('billing.payers')?></h3>
                    <div class="col-lg-8 col-md-9 col-sm-8 col-xs-8 text-right" style="margin-top: 8px;">
                        <!--<input style="width: 45px; height: 22px; display: inline; " class="form-control" type="text" id="formGroupInputSmall" placeholder="">-->
                        <button id="addPayer" class="btn btn-xs btn-primary" ng-click="billingCtrl.addPayer()"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span><?=lang('billing.add')?> </button>
                    </div>
                </div>
                <div id="panelTitle" class="text-center" style="clear: both;">
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" style="padding: 0;">
                        <div class="titleDiv col-lg-1 col-md-2 col-sm-1">
                            Id
                        </div>
                        <div class="titleDiv col-lg-5 col-md-4 col-sm-5">
                            Nome
                        </div>
                        <div class="titleDiv col-lg-3 col-md-3 col-sm-3">
                            Valor
                        </div>
                        <div class="titleDiv col-lg-3 col-md-3 col-sm-3">
                            A pagar
                        </div>
                    </div>
                    <div class="titleDiv col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        Pagamento
                    </div>
                    <div class="titleDiv col-lg-1 col-md-1 col-sm-2 col-xs-2">
                        Pagar
                    </div>
                    <div class="titleDiv col-lg-1 col-md-1 col-sm-2 col-xs-2">
                        Vincular
                    </div>
                    <div class="titleDiv col-lg-1 col-md-1 col-sm-2 col-xs-2">
                        Excluir
                    </div>
                    <div class="titleDiv col-lg-1 col-md-1 col-sm-2 col-xs-2">
                        Atualizar
                    </div>
                </div>
                <div id="bodyBilling" class="panel-body">
                    <div ng-class="{danger:!billingCtrl.ok}"> <!--Fagner, this div contains the payers-->
                        <p ng-show="billingCtrl.sale.closing">{{billingCtrl.sale.closing|date2}}</p>
                        <div ng-repeat="payer in billingCtrl.sale.payers">
                            <div class="form-inline">
                                <div class="row" id="payer1">
                                    <xp-payer payer="payer" update="billingCtrl.calcPending"></xp-payer>
                                    <div class="form-group col-lg-1 col-md-1 col-sm-2 col-xs-2 text-center">
                                        <button ng-click="billingCtrl.removePayer(payer)" class="btn btn-danger" title="Remover Pagador"><span class="glyphicon glyphicon-remove-sign right0"></span></button>
                                    </div>
                                    <div class="form-group col-lg-1 col-md-1 col-sm-2 col-xs-2 text-center">
                                        <button ng-click="billingCtrl.updatePayer(payer)" class="btn btn-warning" title="Atualizar Dados"><span class="glyphicon glyphicon-refresh right0"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
