<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>resource/css/billing.css" />
<main>
<div ng-controller="PayerController as payerCtrl" ng-init="init(<?=$saleId?>)">
  <div class="panel panelHeader">
    <div class="panel-heading">
      <div>
        <div class="panelButtons col-lg-3 col-md-12 col-sm-12 col-xs-12">
        <a href="<?=base_url()?>sale/fsById/<?=$saleId?>" class="btn btn-default navbar-btn btn-warning" title="#"><i class="fa fa-rotate-left"></i></a>
        <a href="<?=base_url()?>sale/tables" id="backOrderNumbers" title="Comandas" 
            class="btn btn-default navbar-btn btn-primary"><i class="fa fa-edit"></i></a>
          <button id="print" title="Imprimir Cupom Fiscal" type="button" class="btn navbar-btn btn-primary" style="width: 40px;">
            <i class="fa fa-print"></i><span class="newShortcut">ECF</span><span class="btnShortcut" style="left: -24px;">?</span>
          </button>
          <button id="print" title="Imprimir Recibo" type="button" class="btn navbar-btn btn-info" style="width: 40px;">
            <i class="fa fa-print"></i><span class="newShortcut">REC</span><span class="btnShortcut" style="left: -24px;">?</span>
          </button>
          <button id="printRedo" title="Liberar Comanda" type="button" class="btn navbar-btn btn-success" style="width: 40px;">
            <i class="fa fa-unlock"></i><span class="btnShortcut">?</span>
          </button>
        </div>
        <h3 class="panel-title panelStatus text-center col-lg-3 col-md-4 col-sm-6 col-xs-12" style="color: #428bca;">
          Consumo
          <span id="totalItems">{{ payerCtrl.totalItems | sMoney }}</span>
        </h3>
        <h3 class="panel-title panelStatus text-center col-lg-3 col-md-4 col-sm-6 col-xs-12" style="color: #5cb85c;">
          Total
          <span id="total">{{ (payerCtrl.totalItems + payerCtrl.aggregates - payerCtrl.discount) | sMoney }}</span>
        </h3>
        <h3 class="panel-title panelStatus text-center col-lg-3 col-md-4 col-sm-12 col-xs-12" style="color: #d9534f;">
          Pendente
          <span id="difference">{{ payerCtrl.nonParceled | sMoney }}</span></strong>
      </h3>
    </div>
  </div>
  <div class="panel-body panelFooter">
    <h6 class="col-lg-8 col-md-8 col-sm-8 col-xs-6 text-left panelFooterLeft">ID do pedido: <?=$saleId?></h6>
    <h5 class="col-lg-4 col-md-4 col-sm-4 col-xs-6 text-center panelFooterRight"><span ng-show="payerCtrl.orderNumber">Comanda nº {{ payerCtrl.orderNumber }}</span></h5>
  </div>
</div>

<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-bottom: 40px;">
  <div ng-controller="AggregateController as aggregateCtrl" ng-init="init(<?=$saleId?>)">
    <div class="panel panel-default">
      <div class="panel-heading panelPayers">
        <h3 class="panel-title text-left col-lg-4 col-md-3 col-sm-8 col-xs-4"><?=lang('billing.aggregates')?></h3>
      </div>
      <div class="panel-body">
        <ul>
          <li ng-repeat="aggregate in aggregateCtrl.aggregates">
          <div>
            {{ aggregate.name }} - {{ aggregateCtrl.total(aggregate) | lMoney }}
          </div>
          <div> {{ aggregate.subtotal | sMoney }} X 
            <xp-number ng-model="aggregate.amount"></xp-number>
          </div>
          </li>
        </ul>
        <button ng-click="aggregateCtrl.save()">Salvar</button>                    
      </div>
    </div>
  </div>
  <div ng-controller="DiscountController as discountCtrl" ng-init="init(<?=$saleId?>)">
    <div class="panel panel-default">
      <div class="panel-heading panelPayers">
        <h3 class="panel-title text-left col-lg-4 col-md-3 col-sm-8 col-xs-4"><?=lang('billing.discount')?></h3>
      </div>
      <div id="#" class="panel-body">
        <div class="form-inline">
          <div ng-show="discountCtrl.canChange">
            <div class="form-group col-lg-9 col-md-8 col-sm-8 col-xs-4" style="padding-left: 0;">
              <xp-number ng-model="discountCtrl.discount"></xp-number>
            </div>
            <div class="form-group col-lg-3 col-md-4 col-sm-4 col-xs-4" style="padding-left: 0;">
              <button ng-click="discountCtrl.save()">Salvar</button>
            </div>
          </div>
          <div ng-hide="discountCtrl.canChange">
            {{ discountCtrl.discount }} 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12" style="margin-bottom: 40px;">
  <div class="panel panel-default">
    <div class="panel-heading panelPayers">
      <h3 class="panel-title text-left col-lg-4 col-md-3 col-sm-4 col-xs-4"><?=lang('billing.payers')?></h3>
      <div class="col-lg-8 col-md-9 col-sm-8 col-xs-8 text-right" style="margin-top: 8px;">
        <button ng-click="payerCtrl.add()" style="color: #000000;">Adicionar</button>
      </div>
    </div>
    <div class="panel-body" style="padding-top: 0;">
      <div class="form-inline">
        <div class="row" id="payer1" style="/*background: #e4e4e4*/;padding-top: 10px; border-bottom: 1px solid #333;">
          <ul ng-class="{danger:!payerCtrl.ok}">
            <li ng-repeat="payer in payerCtrl.payers">
            <div>
              <input type="text" ng-model="payer.description">
              <xp-number ng-model="payer.amount"></xp-number>
              <button ng-click="payerCtrl.updatePayer(payer)">Alterar Pagador</button>
              <button ng-click="payerCtrl.removePayer(payer)">Remover Pagador</button>
              <button ng-click="payerCtrl.accessPayer(payer)">Acessar Pagador</button>
            </div>
            <div>Faltando acertar: {{ payer.nonParceled | sMoney }}</div>
            <div ng-repeat="portion in payer.portions">
              <div>
                {{ portion.validUntil ? (portion.validUntil | date2) : 'A vista' }}
                {{ portion.total | lMoney }}
              </div>
              <ul ng-if="portion.payments.length">
                <li ng-repeat="payment in portion.payments">
                {{ payerCtrl.paymentType(payment.paymentTypeId).name }} {{ payment.amount | lMoney }}  
                </li>
                <li>
                {{ payerCtrl.paymentStatus(portion) }}
                </li>
              </ul>
            </div>
            </li>
          </ul>

          <button ng-show="payerCtrl.canClose" ng-click="payerCtrl.close()">Liberar</button>
          <div ng-show="payerCtrl.closing">Fechado em {{ payerCtrl.closing | date2 }}</div>
          <div id="receiptModal">
            <div ng-controller="CashReceiptController as cash">
              <div ng-if="cash.payer">
                <h4>{{ cash.payer.description }}</h4>
                <h5>Lancar pagamentos</h5>
                <ul>
                  <li ng-repeat="slot in cash.slots">
                  {{slot.paymentType.name}} <xp-number ng-model="slot.amount"></xp-number>
                  </li>
                </ul>
                <div ng-show="cash.change() > 0">Troco: {{ cash.change() | sMoney }}</div>
                <button ng-click="cash.confirm();payerCtrl.closeReceiptModal()">Confirmar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</main>
