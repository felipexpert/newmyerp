<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>resource/css/billing.css" />
<main>
    <div class="panel panelHeader">
      <div class="panel-heading">
        <div class="panelButtons col-lg-3 col-md-12 col-sm-12 col-xs-12">
          <a href="<?=base_url()?>sale/fsById/<?=$id?>" 
              class="btn btn-default navbar-btn btn-warning" title="<?=lang('back')?>"><i class="fa fa-rotate-left"></i></a>
          <a href="<?=base_url()?>sale/tables" id="backOrderNumbers" title="<?=lang('fastSelling.orders')?>" 
              class="btn btn-default navbar-btn btn-primary"><i class="fa fa-edit"></i></a>
        <button id="printRedo" title="Imprimir Cupom Fiscal" type="button" class="btn navbar-btn btn-primary" style="width: 40px;">
          <i class="fa fa-print"></i><span class="btnShortcut">?</span>
        </button>
        <button id="printRedo" title="Imprimir Recibo" type="button" class="btn navbar-btn btn-info" style="width: 40px;">
          <i class="fa fa-file-text-o"></i><span class="btnShortcut">?</span>
        </button>
        <button id="printRedo" title="Liberar Comanda" type="button" class="btn navbar-btn btn-success" style="width: 40px;">
          <i class="fa fa-unlock"></i><span class="btnShortcut">?</span>
        </button>
        </div>
        <h3 class="panel-title panelStatus text-center col-lg-3 col-md-4 col-sm-6 col-xs-12" style="color: #428bca;">
          <?=lang('billing.consumption')?>:
          <span id="totalItems" class="numberToMoney" data-totalItems="<?=$totalItems?>"><?=$totalItems?></span>
        </h3>
        <h3 class="panel-title panelStatus text-center col-lg-3 col-md-4 col-sm-6 col-xs-12" style="color: #5cb85c;">
          <?=lang('total')?>:
          <span id="total"></span>
        </h3>
        <h3 class="panel-title panelStatus text-center col-lg-3 col-md-4 col-sm-12 col-xs-12" style="color: #d9534f;">
          <?=lang('billing.pending')?>:
          <span id="difference"></span></strong>
        </h3>
      </div>
      <div class="panel-body panelFooter">
        <h6 class="col-lg-8 col-md-8 col-sm-8 col-xs-6 text-left panelFooterLeft"><?=lang('billing.orderId')?>: <?=$id?></h6>
        <h5 class="col-lg-4 col-md-4 col-sm-4 col-xs-6 text-center panelFooterRight"><?=lang('billing.commandNumber')?> <?=$orderNumber?></h5>
      </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-bottom: 40px;">
        <div class="panel panel-default">
            <div class="panel-heading panelPayers">
                <h3 class="panel-title text-left col-lg-4 col-md-3 col-sm-8 col-xs-4">Agregados</h3>
            </div>
            <div id="#" class="panel-body">
                Agregados
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading panelPayers">
                <h3 class="panel-title text-left col-lg-4 col-md-3 col-sm-8 col-xs-4">Desconto</h3>
            </div>
            <div id="#" class="panel-body">
                <form class="form-inline">
                    <div class="form-group col-lg-9 col-md-8 col-sm-8 col-xs-4" style="padding-left: 0;">
                      <label class="sr-only" for="inputDiscount">Desconto</label>
                      <input type="number" class="form-control" id="inputDiscount" placeholder="R$ 00,00">
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-4 col-xs-4" style="padding-left: 0;">
                    <button type="submit" class="btn btn-primary">Alterar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12" style="margin-bottom: 40px;">
        <div class="panel panel-default">
            <div class="panel-heading panelPayers">
                <h3 class="panel-title text-left col-lg-4 col-md-3 col-sm-8 col-xs-4"><?=lang('billing.payers')?></h3>
                <div class="col-lg-8 col-md-9 col-sm-4 col-xs-8 text-right" style="margin-top: 8px;">
                  <input style="width: 45px; height: 22px; display: inline; " class="form-control" type="text" id="formGroupInputSmall" placeholder="">
                  <button id="addPayer" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span><?=lang('billing.add')?> </button>
                </div>
            </div>
            <div id="#" class="panel-body" style="padding-top: 0;">
                <form class="form-inline">
                    <div class="row" id="payer1" style="/*background: #e4e4e4*/;padding-top: 10px; border-bottom: 1px solid #333;">
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                            <div class="form-group col-lg-1 col-md-1 col-sm-2 col-xs-2" style="padding-left: 0;">
                                <button type="button" class="btn btn-primary" value="Selecionar" data-toggle="modal" data-target="#myModal"><span style="margin-right: 0;" class="glyphicon glyphicon-unchecked"></span></button>
                            </div>
                            <div class="form-group col-lg-5 col-md-5 col-sm-4 col-xs-4" style="padding-left: 0;">
                                <label class="sr-only" for="inputName">Nome</label>
                                <input type="text" class="form-control" id="inputName" placeholder="Nome">
                            </div>
                            <div class="form-group col-lg-5 col-md-5 col-sm-4 col-xs-4" style="padding-left: 0;">
                                <label class="sr-only" for="inputDiscount">Valor</label>
                                <input type="number" class="form-control text-right" id="inputDiscount" placeholder="R$ 00,00">
                            </div>
                            <div class="form-groupcol-lg-1 col-md-1 col-sm-2 col-xs-2" style="padding-left: 0;">
                                <button type="submit" class="btn btn-danger">
                                    <span style="margin-right: 0;" class="glyphicon glyphicon-remove-sign"></span>
                                </button>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <ul class="list-inline">
                                <li style="height: 34px; line-height: 34px;">
                                    <ul class="list-inline" style="margin-top: 0;">
                                        <li>
                                            Dinheiro (DN)
                                        </li>
                                        -
                                        <li>R$5,00</li>
                                    </ul>
                                    <ul class="list-inline" style="margin-top: 0; border-top: 1px solid #ddd;">
                                        <li>
                                            Cheque (DN)
                                        </li>
                                        -
                                        <li>R$5,00</li>
                                    </ul>
                                    <ul class="list-inline" style="margin-top: 0; border-top: 1px solid #ddd; font-weight: 600;">
                                        <li>
                                            Troco
                                        </li>
                                        :
                                        <li>R$0,50</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row" id="payer1" style="/*background: #e4e4e4*/;padding-top: 10px; border-bottom: 1px solid #333;">
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                            <div class="form-group col-lg-1 col-md-1 col-sm-2 col-xs-2" style="padding-left: 0;">
                                <button type="button" class="btn btn-primary" value="Selecionar" data-toggle="modal" data-target="#myModal"><span style="margin-right: 0;" class="glyphicon glyphicon-unchecked"></span></button>
                            </div>
                            <div class="form-group col-lg-5 col-md-5 col-sm-4 col-xs-4" style="padding-left: 0;">
                                <label class="sr-only" for="inputName">Nome</label>
                                <input type="text" class="form-control" id="inputName" placeholder="Nome">
                            </div>
                            <div class="form-group col-lg-5 col-md-5 col-sm-4 col-xs-4" style="padding-left: 0;">
                                <label class="sr-only" for="inputDiscount">Valor</label>
                                <input type="number" class="form-control text-right" id="inputDiscount" placeholder="R$ 00,00">
                            </div>
                            <div class="form-groupcol-lg-1 col-md-1 col-sm-2 col-xs-2" style="padding-left: 0;">
                                <button type="submit" class="btn btn-danger">
                                    <span style="margin-right: 0;" class="glyphicon glyphicon-remove-sign"></span>
                                </button>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <ul class="list-inline">
                                <li style="height: 34px; line-height: 34px;">
                                    <ul class="list-inline" style="margin-top: 0;">
                                        <li>
                                            Dinheiro (DN)
                                        </li>
                                        -
                                        <li>R$5,00</li>
                                    </ul>
                                    <ul class="list-inline" style="margin-top: 0; border-top: 1px solid #ddd;">
                                        <li>
                                            Cheque (DN)
                                        </li>
                                        -
                                        <li>R$5,00</li>
                                    </ul>
                                    <ul class="list-inline" style="margin-top: 0; border-top: 1px solid #ddd; font-weight: 600;">
                                        <li>
                                            Troco
                                        </li>
                                        :
                                        <li>R$0,50</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row" id="payer2" style="padding-top: 10px;">
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                            <div class="form-group col-lg-1 col-md-1 col-sm-2 col-xs-2" style="padding-left: 0;">
                                <button type="button" class="btn btn-primary" value="Selecionar" data-toggle="modal" data-target="#myModal"><span style="margin-right: 0;" class="glyphicon glyphicon-unchecked"></span></button>
                            </div>
                            <div class="form-group col-lg-5 col-md-5 col-sm-4 col-xs-4" style="padding-left: 0;">
                                <label class="sr-only" for="inputName">Nome</label>
                                <input type="text" class="form-control" id="inputName" placeholder="Nome">
                            </div>
                            <div class="form-group col-lg-5 col-md-5 col-sm-4 col-xs-4" style="padding-left: 0;">
                                <label class="sr-only" for="inputDiscount">Valor</label>
                                <input type="number" class="form-control text-right" id="inputDiscount" placeholder="R$ 00,00">
                            </div>
                            <div class="form-group col-lg-1 col-md-1 col-sm-2 col-xs-2" style="padding-left: 0;">
                                <button type="submit" class="btn btn-danger">
                                    <span style="margin-right: 0;" class="glyphicon glyphicon-remove-sign"></span>
                                </button>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <span style="height: 34px; line-height: 34px; font-weight: 800; color: #d9534f;">NÃO PAGOU</span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Nome Cliente</h4>
          <h4 style="position: absolute; top: 10px; right: 45px;">R$5,00</h4>
        </div>
        <div class="modal-body" style="padding: 0;">
          <form id="payment" class="enterAsTab">
                <table class="table table-striped" style="margin-bottom: 0;">
                    <tbody>
                        <tr style="display: none;"><td></td><td></td></tr>
                        <tr>
                            <td class="col-lg-6 text-center" style="padding: 8px 15px;">
                              Dinheiro (DN)
                            </td>
                            <td class="col-lg-6" style="padding: 8px 15px;">
                              <input type="text" data-name="pt1" class=" form-control text-right">
                            </td>
                        </tr>
                        <tr>
                            <td class="col-lg-6 text-center" style="padding: 8px 15px;">
                              Cartão Crédito (CC)  </td>
                            <td class="col-lg-6" style="padding: 8px 15px;">
                              <input type="text" data-name="pt2" class=" form-control text-right">
                            </td>
                        </tr>
                        <tr>
                            <td class="col-lg-6 text-center" style="padding: 8px 15px;">
                              Cartão Débito (CD)  </td>
                            <td class="col-lg-6" style="padding: 8px 15px;">
                              <input type="text" data-name="pt3" class=" form-control text-right">
                            </td>
                        </tr>
                        <tr>
                            <td class="col-lg-6 text-center" style="padding: 8px 15px;">
                              Cheque (CH)  </td>
                            <td class="col-lg-6" style="padding: 8px 15px;">
                              <input type="text" data-name="pt4" class=" form-control text-right">
                            </td>
                        </tr>
                        <tr>
                            <td class="col-lg-6 text-center" style="padding: 8px 15px;">
                              Boleto Bancário (BB)  </td>
                            <td class="col-lg-6" style="padding: 8px 15px;">
                              <input type="text" data-name="pt5" class=" form-control text-right">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="receiveConfirm">
                  <input title="Confirmar" type="submit" class="btn navbar-btn btn-success col-lg-12 col-md-12 col-sm-12 col-xs-12" value="Confirmar">
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <h4 style="position: absolute; bottom: 10px; left: 25px; font-weight: 600; color: #d9534f;">Troco: R$0,50</h4>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>      
    </div>
  </div>
</div>
</main>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

