  <style>
  .glyphicon.glyphicon-remove-sign:hover { cursor: pointer; color: #445566; }
  .payerInfo { margin: 5px; }
</style>
<main class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 0; padding: 0;">
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-bottom: 40px;">
      <section style="border: 1px solid #ddd; margin-bottom: 10px;">
        <h4 style="background: #eee; margin: 0; padding: 10px 15px 5px 15px; border-bottom: 1px solid #ddd; font-weight: 600;">ID da comanda <?=$orderNumber?></h4>
      </section>
      <section style="border: 1px solid #ddd; margin-bottom: 10px;">
        <h4 style="background: #eee; margin: 0; padding: 10px 15px 5px 15px; border-bottom: 1px solid #ddd; font-weight: 600;">ID do pedido <?=$id?></h4>
      </section>
      <section style="border: 1px solid #ddd; margin-bottom: 10px;">
        <h4 style="background: #eee; margin: 0; padding: 10px 15px 5px 15px; border-bottom: 1px solid #ddd; font-weight: 600;">Consumo</h4>
        <div class="row" style="padding: 0 15px;">
          <div class="col-lg-12" style="margin: 10px 0">
            <span id="totalItems" class="numberToMoney" data-totalItems="<?=$totalItems?>"><?=$totalItems?></span>
          </div>
        </div>
      </section>
      
      <section style="border: 1px solid #ddd; margin-bottom: 10px;">
        <h4 style="background: #eee; margin: 0; padding: 14px 15px 9px 15px; border-bottom: 1px solid #ddd; font-weight: 600;">Agregados (CTRL+n)</h4>
        <table id="aggregates" class="table table-striped enterAsTab" data-focus='[data-name="payersAmount"]' style="margin-bottom: 0;">
          <tr style="display: none;"><td></td><td></td></tr>
          <?=$aggrHists?>
        </table>
      </section>
      
      <section style="border: 1px solid #ddd; margin-bottom: 10px;">
        <h4 style="background: #eee; margin: 0; padding: 10px 15px 5px 15px; border-bottom: 1px solid #ddd; font-weight: 600;">Total</h4>
        <div class="row" style="padding: 0 15px;">
          <div class="col-lg-12" style="margin: 10px 0">
            <span id="total"></span>
          </div>
        </div>
      </section>

      <section style="border: 1px solid #ddd; margin-bottom: 10px;">
        <h4 style="background: #eee; margin: 0; padding: 10px 15px 5px 15px; border-bottom: 1px solid #ddd; font-weight: 600;">Status rápido</h4>
        <div class="row" style="padding: 0 15px;">
          <div class="col-lg-12" style="margin: 10px 0">
            <strong>Faltando acertar: <span id="difference"></span></strong>
          </div>
        </div>
      </section>
      <section style="border: 1px solid #ddd; margin-bottom: 10px;">
        <div class="row" style="padding: 0 15px;">
          <div style="background: #eee; margin: 0; border-bottom: 1px solid #ddd; height: 42px;">
            <div class="col-xs-5" style="margin: 0;">
              <h4 style="font-weight: 600;">Pagadores</h4>
            </div>
            <button id="addPayer">Adicinar</button>
            <button id="removePayer">Remover</button>
          </div>
        </div>
        <div id="payers"></div>
      </section>
  </div>
  
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-bottom: 40px;">
      <section style="border: 1px solid #ddd; margin-bottom: 10px;">
        <h4 style="background: #eee; margin: 0; padding: 14px 15px 9px 15px; border-bottom: 1px solid #ddd; font-weight: 600;">Receber</h4>
        <div class="row" style="padding: 0 15px;">
          <form id="payment" class="enterAsTab">
            <table class="table table-striped" style="margin-bottom: 0;">
              <tr style="display: none;"><td></td><td></td></tr>
                <?=$paymentTypes?>
            </table>
            <div style="padding: 0 15px; border-top: 1px solid #ddd;">
              <input title="Confirmar" type="submit" class="btn navbar-btn btn-success col-lg-12 col-md-12 col-sm-12 col-xs-12" value="Confirmar">
            </div>
          </form>
        </div>
      </section>
      <section style="border: 1px solid #ddd; margin-bottom: 10px;">
        <h4 style="background: #eee; margin: 0; padding: 10px 15px 5px 15px; border-bottom: 1px solid #ddd; font-weight: 600;">Troco</h4>
        <div class="row" style="padding: 0 15px;">
          <div class="col-lg-12" style="margin: 10px 0">
            <strong id="change"></strong>
          </div>
        </div>
      </section>
  </div>
  
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-bottom: 40px;">
      <section style="border: 1px solid #ddd; margin-bottom: 10px;">
        <h4 style="background: #eee; margin: 0; padding: 10px 15px 5px 15px; border-bottom: 1px solid #ddd; font-weight: 600;">Recebido</h4>
        <div class="row" style="padding: 0 15px;">
          <ol id="receipts"></ol>
          <div style="padding: 0 15px; border-top: 1px solid #ddd;">
            <div id="closing"></div>
            <button id="confirm" title="Confirmar" type="button" class="btn navbar-btn btn-warning col-lg-12 col-md-12 col-sm-12 col-xs-12"><?=lang('payment.freeOrderNumber')?></button>
          </div>
        </div>
      </section>
  </div>
</main>

