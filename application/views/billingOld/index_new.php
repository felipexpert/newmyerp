<style type="text/css">
  #payment{
    margin: 0; padding: 0;
  }
  #paymentCol{
    margin-bottom: 40px;
  }
  
  #paymentCol section{
  border: 1px solid #ddd; margin-bottom: 10px;
  }
  
  .sectionTitle{
  background: #eee; margin: 0; padding: 14px 15px 9px 15px; border-bottom: 1px solid #ddd; font-weight: 600;
  }
  .row{
  padding: 0 15px;
  }
  
  #consumo, #troco{
  font-size: 24px; height: auto; margin: 10px 0;
  }
  .table{
  margin-bottom: 0;
  }
  
  #rateio{
  background: #eee; border-bottom: 1px solid #ddd; height: 42px;
  }
  #rateio h4{
  margin: 0; padding: 14px 15px 9px 0; font-weight: 600;
  }
  
  #rateio input{
  margin-top: 5px;
  }
  
  #rateioCont .row{
  margin-bottom: 10px;
  margin-top: 10px;
  }
  
</style>

<main class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="paymentCol">
      <section>
        <h4 class="sectionTitle">CONSUMO</h4>
        <div class="row" >
          <div class="col-lg-12">
            <input type="text" class="form-control text-right" id="consumo" placeholder="R$ " >
          </div>
        </div>
      </section>
      
      <section>
        <h4 class="sectionTitle">AGREGADOS</h4>
        <table class="table table-striped">
            <tr style="display: none;"><td></td><td></td></tr>
            <tr>
              <td>
                <label>
                  <input type="checkbox" value="" >
                  Agregado 1
                </label>
              </td>
            </tr>
            <tr>
              <td>
                <label >
                  <input type="checkbox" value="" >
                  Agregado 2
                </label>
              </td>
            </tr>
            <tr>
              <td class="" >
                <label >
                  <input type="checkbox" value="" >
                  Agregado 3
                </label>
              </td>
            </tr>
          </table>
      </section>
      
      <section>
        <div class="row">
          <div id="rateio">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <h4>RATEIOS</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
              <input type="text" class="form-control text-right" id="rateioVlr" placeholder="Quantidade de rateios">
            </div>
          </div>
        </div>
        <div id="rateioCont">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
              <input type="text" class="form-control" id="rateioQtnd" placeholder="Pessoa 1">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
              <input type="text" class="form-control text-right" id="rateioVlr" placeholder="R$ ">
            </div>
          </div>
          <div class="row" >
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
              <input type="text" class="form-control" id="rateioQtnd" placeholder="Pessoa 2">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" >
              <input type="text" class="form-control text-right" id="rateioVlr" placeholder="R$ ">
            </div>
          </div>
        </div>
      </section>
  </div>
  
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="paymentCol">
      <section >
        <h4 class="sectionTitle">RECEBER</h4>
        <div class="row" >
          <table class="table table-striped" >
            <tr style="display: none;"><td></td><td></td></tr>
            <tr>
              <td class="col-lg-6 text-center" >
                DN
              </td>
              <td class="col-lg-6" >
                <input type="text" class="form-control text-right" id="rateioVlr" placeholder="R$ ">
              </td>
            </tr>
            <tr>
              <td class="col-lg-6 text-center">
                CC
              </td>
              <td class="col-lg-6">
                <input type="text" class="form-control text-right" id="rateioVlr" placeholder="R$ ">
              </td>
            </tr>
            <tr>
              <td class="col-lg-6 text-center" >
                CD
              </td>
              <td class="col-lg-6" >
                <input type="text" class="form-control text-right" id="rateioVlr" placeholder="R$ ">
              </td>
            </tr>
            <tr>
              <td class="col-lg-6 text-center" >
                CH
              </td>
              <td class="col-lg-6" >
                <input type="text" class="form-control text-right" id="rateioVlr" placeholder="R$ ">
              </td>
            </tr>
          </table>
          <div >
            <button id="confirm" title="Confirmar" type="button" class="btn navbar-btn btn-success col-lg-12 col-md-12 col-sm-12 col-xs-12">CONFIRMAR</button>
          </div>
        </div>
      </section>
      
      <section >
        <h4 class="sectionTitle">TROCO</h4>
        <div class="row"">
          <div class="col-lg-12">
            <input type="text" class="form-control text-right" id="troco" placeholder="R$ ">
          </div>
        </div>
      </section>
  </div>
  
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="paymentCol">
      <section >
        <h4 class="sectionTitle">RECEBIDO</h4>
        <div class="row" >
          <table class="table table-striped">
            <tbody>
              <tr style="display: none;"><td></td><td></td></tr>
              <tr>
                <th scope="row">1</th>
                <td>
                  <table>
                  <ul>
                    <li>Valor 1</li>
                    <li>Valor 2</li>
                    <li>Troco</li>
                  </ul>
                  </table>
                </td>
                <td class="text-right">
                  <ul >
                    <li>R$ 00,00</li>
                    <li>R$ 00,00</li>
                    <li>R$ 00,00</li>
                  </ul>
                </td>
              </tr>
              <tr>
                <th scope="row">2</th>
                <td>Descricao</td>
                <td class="text-right">R$ 00,00</td>
              </tr>
              <tr>
                <th scope="row">3</th>
                <td>Descricao</td>
                <td class="text-right">R$ 00,00</td>
              </tr>
            </tbody>
          </table>
          <div >
            <button id="confirm" title="Confirmar" type="button" class="btn navbar-btn btn-danger col-lg-12 col-md-12 col-sm-12 col-xs-12">FINALIZAR</button>
          </div>
        </div>
      </section>
  </div>
  
  
</main>

