<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>resource/css/billing.css" />
<main>
    <div class="panel panelHeader">
      <div class="panel-heading">
        <div class="panelButtons col-lg-3 col-md-12 col-sm-12 col-xs-12">
          <a href="<?=base_url()?>sale/fsById/<?=$id?>" 
              class="btn btn-default navbar-btn btn-warning" title="<?=lang('back')?>"><i class="fa fa-rotate-left"></i></a>
          <a href="<?=base_url()?>sale/tables" id="backOrderNumbers" title="<?=lang('fastSelling.orders')?>" 
              class="btn btn-default navbar-btn btn-primary"><i class="fa fa-edit"></i></a>
        </div>
        <h3 class="panel-title panelStatus text-center col-lg-3 col-md-4 col-sm-6 col-xs-12" style="color: #428bca;">
          <?=lang('billing.consumption')?>:
          <span id="totalItems" class="numberToMoney" data-totalItems="<?=$totalItems?>"><?=$totalItems?></span>
        </h3>
        <h3 class="panel-title panelStatus text-center col-lg-3 col-md-4 col-sm-6 col-xs-12" style="color: #5cb85c;">
          <?=lang('total')?>:
          <span id="total"></span>
        </h3>
        <h3 class="panel-title panelStatus text-center col-lg-3 col-md-4 col-sm-12 col-xs-12" style="color: #d9534f;">
          <?=lang('billing.pending')?>:
          <span id="difference"></span></strong>
        </h3>
      </div>
      <div class="panel-body panelFooter">
        <h6 class="col-lg-8 col-md-8 col-sm-8 col-xs-6 text-left panelFooterLeft"><?=lang('billing.orderId')?>: <?=$id?></h6>
        <h5 class="col-lg-4 col-md-4 col-sm-4 col-xs-6 text-center panelFooterRight"><?=lang('billing.commandNumber')?> 
            <span id="orderNumber"><?=$orderNumber?></span></h5>
      </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-bottom: 40px;">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><?=lang('billing.aggregates')?></h3>
          </div>
          <div> <!--class="panel-body"-->
            <table id="aggregates" class="table table-striped enterAsTab" data-focus='[data-name="payersAmount"]' style="margin-bottom: 0;">
              <tr style="display: none;"><td></td><td></td></tr>
              <?=$aggrHists?>
            </table>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Desconto</h3>
          </div>
          <div> <!--class="panel-body"-->
            <form id="discountForm" class="noReset" action="<?=base_url()?>billing/discountAjax/<?=$id?>" method="post">
              <input type="text" name="discount" class="nf" data-decimal="2" value="<?=$discount?>">
              <input type="submit" value="Alterar">  
            </form>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading panelPayers">
            <h3 class="panel-title text-left col-lg-4 col-md-3 col-sm-8 col-xs-4"><?=lang('billing.payers')?></h3>
            <div class="col-lg-8 col-md-9 col-sm-4 col-xs-8 text-right" style="margin-top: 8px;">
              <button id="addPayer" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span><?=lang('billing.add')?> </button>
              <button id="removePayer" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span><?=lang('billing.remove')?> </button>
            </div>
          </div>
          <div id="payers" class="panel-body">
          </div>
        </div>    
    </div>
  
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-bottom: 40px;">
      <div class="panel panel-default panelReceive">
        <div class="panel-heading">
          <h3 class="panel-title"><?=lang('billing.receive')?></h3>
        </div>
        <div> <!--class="panel-body"-->
          <form id="payment" class="enterAsTab">
            <table class="table table-striped" style="margin-bottom: 0;">
              <tr style="display: none;"><td></td><td></td></tr>
                <?=$paymentTypes?>
            </table>
            <div class="receiveConfirm">
              <input title="<?=lang('billing.confirm')?>" type="submit" class="btn navbar-btn btn-success col-lg-12 col-md-12 col-sm-12 col-xs-12" value="<?=lang('billing.confirm')?>">
            </div>
          </form>
        </div>
      </div>
      
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><?=lang('billing.exchange')?></h3>
        </div>
        <div class="changeBody"> <!--class="panel-body"-->
          <strong id="change"></strong>
        </div>
      </div>
  </div>
  
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-bottom: 40px;">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><?=lang('billing.received')?></h3>
        </div>
        <div id="divReceipts" class="row"> <!--class="panel-body"-->
        <div class="col-lg-12">
          <ol id="receipts"></ol>
          <div class="receiptsRelease">
            <div id="closing"></div>
            <button id="confirm" title="<?=lang('payment.freeOrderNumber')?>" type="button" class="btn navbar-btn btn-warning col-lg-12 col-md-12 col-sm-12 col-xs-12"><?=lang('payment.freeOrderNumber')?></button>
            <button id="print" title="<?=lang('billing.printReceipt')?>" type="button" class="btn btn-default navbar-btn btn-primary"><?=lang('billing.printReceipt')?></button>
            <button id="printECF" title="<?=lang('billing.printCoupon')?>" type="button" class="btn btn-default navbar-btn btn-primary"><?=lang('billing.printCoupon')?></button>
          </div>
        </div>
        </div>
      </div>
  </div>
</main>

