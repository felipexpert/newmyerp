<div id="detailModal">
  <h3 class="text-center"><?=lang('fastSelling.detail')?></h3>
  <hr class="hrStyle">
  <form class="form-horizontal" id="detail" action="">
        <div class="form-group">
          <label for="displayOrder" class="col-sm-4 control-label"><?=lang("position")?>: </label>
          <div class="col-sm-8">
            <input class="form-control" type="text" name="displayOrder" readonly>
          </div>
        </div>
        <div class="form-group">
          <label for="name" class="col-sm-4 control-label"><?=lang('description')?>: </label>
          <div class="col-sm-8">
            <input class="form-control" type="text" name="name" readonly>
          </div>
        </div>
        <div class="form-group">
          <label for="amount" class="col-sm-4 control-label"><?=lang('amount')?>: </label>
          <div class="col-sm-8">
            <input  type="text" class="nf form-control" data-decimal="3" name="amount">
          </div>
        </div>
        <div class="text-center" >
          <input style="margin-top: 20px;" class="btn btn-success" type="submit" value="<?=lang('billing.confirm')?>">
        </div>
      </form>
</div>
