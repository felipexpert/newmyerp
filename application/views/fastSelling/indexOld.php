<main>

  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div id="navbar" class="navbar-collapse collapse">
	<ul class="nav navbar-nav navbar-right">
	  <li style="color: white; font-size:24px; margin:7px 15px">
	    <span id="orderTotal"></span>
	  </li>
	</ul>
      </div>
    </div>
  </nav>

  <section>
  <div class="col-xs-3 pull-right" style="text-align: right">
    <button id="back" class="form-control"><u>V</u>oltar para comandas</button>
    <button id="searchProd" class="form-control"><u>B</u>uscar produto</button>
    <button id="removeProd" class="form-control">R<u>e</u>mover produto</button>
    <button id="print" class="form-control">Im<u>p</u>rimir</button>
  </div>
    <p>Número da comanda: <span id="orderNumber"><?=$orderNumber?></span></p>
    <p><?=lang('fastSelling.orderId')?>: <span id="orderId"><?=$orderId?></span></p>
  </section>
  <section>
    <h3>Adicionar produto</h3>
    <form id="getProd" class="form-inline clearfix enterAsTab" action="<?=base_url()?>order/getProductAjax">
      <input type="hidden" class="form-control" name="orderId" value="<?=$orderId?>">
      <div class="form-group" >
        <label for="code" class="small"><u>C</u>ódigo</label>
        <input type="text" class="form-control" name="code" autofocus>
      </div>

      <div class="form-group">
        <label for="amount" class="small"><u>Q</u>uantidade</label>
        <input type="text" class="nf selAll form-control" data-decimal="0" name="amount" value="1">
      </div>

      <div class="form-group">
        <label for="price" class="small">Valor</label>
        <input type="text" class="nf form-control" name="price" readonly>
      </div>

      <div class="form-group">
        <label for="name" class="small">Descrição</label>
        <input type="text" class="form-control" name="name" readonly>
      </div>

      <div class="form-group">
        <label for="" class="small">&nbsp</label>
        <input type="submit" class="form-control" value="OK">
      </div>
    </form>
  </section>
  <section style="margin-top:50px;">
    <h3>Lista de produtos</h3>
    <div class="table-responsive">
      <?=$table?>
    </div>
  </section>
  <?=$searchProds?>
</main>
