<main>
  <hr class="topHr" >
  <nav class="navHeader navbar navbar-inverse">
    <div class="navHeaderLeft navbar-header col-lg-9 col-md-9 col-sm-7 col-xs-12">
      <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 text-left">
        <a id="back" href="<?=base_url()?>selling/tables" title="<?=lang('fastSelling.ctrlV').' - '.lang('payment.backToCommand')?>" class="btn navbar-btn btn-primary">
          <i class="fa fa-rotate-left"></i><span class="btnShortcut">V</span>
        </a>
        <a id="ancBack" href="<?=base_url()?>" class="btn btn-primary navbar-btn" title="Inicio">
          <i class="fa fa-home"></i>
          <span class="btnShortcut">?</span>
        </a>
        <button id="searchProd" title="<?=lang('fastSelling.ctrlB').' - '.lang('searchProds')?>" type="button" class="btn navbar-btn btn-primary">
          <i class="fa fa-search"></i><span class="btnShortcut">B</span>
        </button>
        <button id="removeProd" title="<?=lang('fastSelling.removeProduct')?>" type="button" class="btn navbar-btn btn-danger">
          <i class="fa fa-times-circle"></i><span class="btnShortcut">E</span>
        </button>
        <button id="print" title="<?=lang('fastSelling.print')?>" type="button" class="btn navbar-btn btn-info">
          <i class="fa fa-print"></i><span class="newShortcut">new</span><span class="btnShortcut" style="left: -24px;">P
        </button>
        <button id="printRedo" title="<?=lang('fastSelling.reprint')?>" type="button" class="btn navbar-btn btn-primary">
          <i class="fa fa-print"></i>
        </button>
        <button id="detailButton" title="<?=lang('fastSelling.detail')?>" type="button" class="btn navbar-btn btn-primary">
          <i class="fa fa-th-list"></i><span class="btnShortcut">D</span>
        </button>
        <? if($orderNumber) { ?>
        <button class="btn btn-primary" id="newButton" title="Mais um integrante a mesa">
          <i class="fa fa-user-plus"></i>
        </button>
        <? } ?>
        <button id="billing" class="btn btn-default navbar-btn btn-primary" title="Recebimento">
          <i class="fa fa-money"></i><span class="btnShortcut" style="left: -12px;">$</span>
        </button>
        <a <?=$canCancel ? "" : "disabled"?> id="ancCancel" href="<?=base_url()?>selling/cancelSale/<?=$saleId?>" title="Cancelar Venda" 
            class="btn btn-default navbar-btn btn-primary">
          <i class="fa fa-close"></i>
        </a>
      </div>
    </div>
    <div class="navHeaderRight col-lg-3 col-md-3 col-sm-5 col-xs-12 text-center" style="padding: 15px 0;">
      <span class="navbar-text text-center" id="orderTotal" style="margin: 0; color: #eee;"></span>
    </div>
  </nav>

  <section>
    <nav class="navSubHeader">
      <div class="navSubHeaderLeft navbar-header col-lg-8 col-md-6 col-sm-6 col-xs-12">
        <div class="form-inline">
          <div class="form-group form-group-sm col-sm-3 col-xs-3" style="width: 95px; margin-left: -15px;">
            <label for="identifier"><?=lang('acess.identifier')?></label>
          </div>
          <div class="form-group form-group-sm col-lg-4 col-md-6 col-sm-8 col-xs-6" style="padding-left: 0; margin-top: 4px;">
            <input type="text" class="form-control" value="<?=M::clsc($identifier)?>" id="identifier">
          </div>
          <div class="form-group form-group-sm col-sm-1 col-xs-1" style="padding-left: 0; margin-left: -10px;">
            <button id="btnIdentifier" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button></button>
          </div>
        </div>
      </div>
      <div class="navSubHeaderLeft navbar-header col-lg-2 col-md-3 col-sm-3 col-xs-12 text-center" id="navSaleId">
        <p><?=lang('fastSelling.saleId')?>: <span id="saleId"><?=$saleId?></span></p>
      </div>
      <?php if($orderNumber) { ?>
        <div class="navSubHeaderRight col-lg-2 col-md-3 col-sm-3 col-xs-12 text-center">
          <p><?=lang('fastSelling.orderNr')?>: <span id="orderNumber"><?=$orderNumber?></span></p>
        </div>
      <?php }else{?>
        <div class="navSubHeaderRight col-lg-2 col-md-3 col-sm-3 col-xs-12 text-center">
          <p><span id="orderNumber">-</span></p>
        </div>
      <? } ?>
    </nav>
  </section>
  
  <section class="addSection">
    <h3><?=lang('fastSelling.addProd')?></h3>
      <form id="getProd" class="form-inline clearfix enterAsTab">
        <input type="hidden" class="form-control" name="saleId" value="<?=$saleId?>">
        <div class="form-group col-lg-1 col-md-1 col-sm-3 col-xs-6">
          <label for="code" class="small"><?=lang('fastSelling.codeProd')?></label>
          <input type="text" class="form-control" name="code" autofocus>
        </div>

        <div class="formValue form-group col-lg-1 col-md-1 col-sm-3 col-xs-6">
          <label for="amount" class="small"><?=lang('fastSelling.amount')?></label>
          <input type="text" class="nf form-control" data-decimal="0" name="amount" value="1" data-focus="1">
        </div>

        <div class="formValue form-group col-lg-1 col-md-2 col-sm-3 col-xs-6">
          <label for="price" class="small"><?=lang('fastSelling.value')?></label>
          <input type="text" class="nf form-control" name="price" readonly>
        </div>

        <div class="formValue form-group col-lg-1 col-md-2 col-sm-3 col-xs-6">
          <label for="subtotal" class="small"><?=lang('subtotal')?></label>
          <input type="text" class="form-control" name="subtotal" readonly>
        </div>

        <div class="form-group col-lg-7 col-md-5 col-sm-10 col-xs-9">
          <label for="name" class="small"><?=lang('fastSelling.description')?></label>
          <input type="text" class="form-control" name="name" readonly>
        </div>

        <div class="form-group col-lg-1 col-md-1 col-sm-2 col-xs-3">
          <label for="" class="small">&nbsp</label>
          <input type="submit" class="form-control btn-success" value="OK">
        </div>
      </form>
  </section>
  
  <section class="addList">
    <div class="table-responsive">
      <?=$table?>
    </div>
  </section>
  <?=$searchProds?>
  <?=$detail?>
  <div id="new">
    <form class="enterAsTab">
      <div class="form-group">
        <label for="currIdentifier">Identificador atual</label>
        <input class="form-control" name="currIdentifier" type="text">
      </div>
      <div class="form-group">
        <label for="newIdentifier">Identificador novo</label>
        <input class="form-control" name="newIdentifier" type="text">
      </div>
      <div class="form-group">
        <input class="btn btn-success col-lg-12 col-md-12 col-sm-12 col-xs-12" type="submit" value="<?=lang('fastSelling.next')?>">
      </div>
    </form>
  </div>
</main>
