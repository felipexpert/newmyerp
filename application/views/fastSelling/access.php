<main>
  <div class="panel panelHeader" id="navAcess">
    <div class="panel-heading" >
      <div class="panelButtons col-lg-9 col-md-8 col-sm-7 col-xs-12">
        <a href="<?=OUTER_URL?>index.php?p=pedido" title="<?=lang('back')?>" class="btn btn-default btn-warning"><i class="fa fa-rotate-left"></i></a>
      </div>
      <div class="panelButtons col-lg-3 col-md-4 col-sm-5 col-xs-12 text-right">
        <form class="form-inline text-right" id="go">
          <div class="form-group col-lg-10 col-md-10 col-sm-10 col-xs-10" style="margin-left: -15px;">
            <label class="sr-only" for="number"><?=lang('acess.number')?></label>
            <input type="text" class="form-control" name="number" placeholder="<?=lang('acess.goToCommand')?>" autofocus autocomplete="off">
          </div><!--form-group-->
          <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2 text-left">
            <button type="submit" class="btn btn-success" value="<?=lang('go')?>"><i class="fa fa-send" title="<?=lang('go')?>"></i></button>
          </div>
        </form>
      </div><!--panelButtons col-lg-4 col-md-12 col-sm-12 col-xs-12-->
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h3><i class="fa fa-edit"></i> <?=lang('fastSelling.orders')?></h3>
        <div id="tables">
          <ul class="list-inline"><?=$tables?></ul>
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div>
        <div class="form-inline">
          <div style="width: 145px;" class="form-group col-lg-3 col-md-2 col-sm-2 col-xs-2" >
            <h3><span class="fa fa-user"></span> <?=lang('acess.client')?></h3>
          </div>
          <div style="margin-top: 15px;" class="form-group col-lg-3 col-md-2 col-sm-2 col-xs-2">
            <button id="newButton" class="btn btn-success" title="<?=lang('acess.addNewClient')?>"><i class="fa fa-user-plus"></i></button>
          </div>
        </div>
        <div class="form-inline col-lg-12 col-md-12 col-sm-12 col-xs-12" id=singletons><?=$singletons?></div>
        <div class="form-inline" id="shared"></div>
        <div id="new">
          <form>
            <div class="form-group">
              <label for="identifier">Identificador </label>
              <input class="form-control" name="identifier" type="text" autocomplete="off">
            </div>
            <div class="form-group">
              <input class="btn btn-success col-lg-12 col-md-12 col-sm-12 col-xs-12" type="submit" value="<?=lang('fastSelling.create')?>" >
            </div>
          </form>    
        </div>
      </div>
    </div>
  </div>
</main>
