<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>resource/css/billing.css" />
<main>
      <div class="panel panelHeader">
      <div class="panel-heading">
        <div class="panelButtons col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <button id="back" title="CTRL+V - Voltar para as comandas" type="button" class="btn btn-default navbar-btn btn-warning"><?=lang('fastSelling.backToOrder')?></button>
          <button id="searchProd" title="CTRL+B - Buscar produto" type="button" class="btn btn-default navbar-btn btn-primary"><?=lang('fastSelling.searchProd')?></button>
          <button id="removeProd" title="CTRL+E - Remover produto" type="button" class="btn btn-default navbar-btn btn-danger"><?=lang('fastSelling.removeProd')?></button>
          <button id="print" title="CTRL+P - Imprimir" type="button" class="btn btn-default navbar-btn btn-primary"><?=lang('fastSelling.print')?></button>
          <button id="detailButton" title="CTRL+D - Detalhar" type="button" class="btn btn-default navbar-btn btn-primary"><b><u>D</u></b>etalhar</button>
          <button id="billing" class="btn btn-default navbar-btn btn-primary"><?=lang('fastSelling.billing')?></button>
        </div>
      </div>
      <div class="panel-body panelFooter">
        <h6 class="col-lg-8 col-md-8 col-sm-8 col-xs-6 text-left panelFooterLeft"><?=lang('fastSelling.saleId')?>: <span id="saleId"><?=$saleId?></h6>
        <?php if($orderNumber) { ?>
          <h5 class="col-lg-4 col-md-4 col-sm-4 col-xs-6 text-center panelFooterRight"><?=lang('fastSelling.orderNr')?>: <?=$orderNumber?></h5>
        <?php } ?>
      </div>
    </div>
      
  <nav class="navHeader navbar navbar-inverse" style="display:  none;">
    <div>
      <div class="navHeaderRight col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center">
        <span class="navbar-text text-center" id="orderTotal"></span>
      </div>
    </div>
  </nav>
  <section class="addSection">
    <h3 style=""><?=lang('fastSelling.addProd')?></h3>
      <form id="getProd" class="form-inline clearfix enterAsTab">
        <input type="hidden" class="form-control" name="saleId" value="<?=$saleId?>">
        <div class="form-group col-lg-1 col-md-1 col-sm-4 col-xs-4">
          <label for="code" class="small"><?=lang('fastSelling.codeProd')?></label>
          <input type="text" class="form-control" name="code" autofocus>
        </div>

        <div class="formValue form-group col-lg-1 col-md-1 col-sm-4 col-xs-4">
          <label for="amount" class="small"><?=lang('fastSelling.amount')?></label>
          <input type="text" class="nf form-control" data-decimal="0" name="amount" value="1" data-focus="1">
        </div>

        <div class="formValue form-group col-lg-1 col-md-2 col-sm-4 col-xs-4">
          <label for="price" class="small"><?=lang('fastSelling.value')?></label>
          <input type="text" class="nf form-control" name="price" readonly>
        </div>

        <div class="formValue form-group col-lg-1 col-md-2 col-sm-4 col-xs-4">
          <label for="subtotal" class="small"><?=lang('subtotal')?></label>
          <input type="text" class="form-control" name="subtotal" readonly>
        </div>

        <div class="form-group col-lg-8 col-md-7 col-sm-10 col-xs-9">
          <label for="name" class="small"><?=lang('fastSelling.description')?></label>
          <input type="text" class="form-control" name="name" readonly>
        </div>

        <div class="form-group col-lg-1 col-md-1 col-sm-2 col-xs-3">
          <label for="" class="small">&nbsp</label>
          <input type="submit" class="form-control btn-success" value="OK">
        </div>
      </form>
  </section>
  
  <section class="addList">
    <div class="table-responsive">
      <?=$table?>
    </div>
  </section>
  <?=$searchProds?>
  <?=$detail?>
</main>
