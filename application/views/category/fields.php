<div class="form-horizontal">
  <div class="form-group">
    <label class="col-sm-1 control-label " for="name">Nome</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="name" value="<?=M::clsc($name)?>" autofocus>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-1 control-label" for="printer">Impressora</label>
    <div class="col-sm-6">
      <?=$printers?>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-1 col-sm-6">
      <input type="submit" value="Confirmar" class="btn btn-success">
      <a href="<?=OUTER_URL?>index.php?p=produto&modo=categoria" class="btn btn-primary">Voltar</a>
    </div>
  </div>
</div>
