<main>
  <form class="ajax enterAsTab<?=isSet($id) ? ' noReset' : ''?>" action="<?=$action?>" method="post">
    <div style="position: absolute; top: 10px; right: 10px;">
      <label for="id" style="color: white;">Id: </label>
      <input type="text" name="id" value="<?=M::clsc($id)?>" readonly>
    </div>
    <div>
      <a id="ancBack" href="<?=OUTER_URL?>index.php?p=cliente" title="Voltar" class="btn btn-primary">
        <i class="fa fa-rotate-left"></i>
        <span class="btnShortcut"></span>
      </a>
      <a href="<?=OUTER_URL?>" class="btn btn-primary navbar-btn" title="Inicio">
        <i class="fa fa-home"></i>
        <span class="btnShortcut"></span>
      </a>
    </div>
    <?=$fields?>
  </form>
</main>
