<style>
/*Deixar barra de rolagem visísvel*/
html {
overflow: -moz-scrollbars-vertical; 
overflow-y: scroll;
}
.error {
  background-color: #fcc;
}
table#order-items tbody tr:first-child {
  border: 3px solid red;
  font-weight: bold;
}
.input-output {
  width: 100px;
}
</style>
<main>
  <div ng-controller="ConsignmentController" ng-init="init(<?=$id?>)">
    <hr class="topHr">
    <nav class="navHeader navbar navbar-inverse">
      <div class="row">
        <div class="col-md-3">
          <div class="navHeaderLeft navbar-header text-left">
            <a id="ancBack" 
               class="btn navbar-btn btn-primary" 
               title="Voltar para lista" 
               href="{{baseUrl}}vwConsignment/access">
              <i class="fa fa-rotate-left"></i><span class="btnShortcut">V</span>
            </a>
            <button class="btn btn-primary navbar-btn" title="Vincular com Revendedor(a)" ng-click="openCustomersModal()">
              <i class="fa fa-link"></i>
              <span class="btnShortcut">T</span>
            </button>
            <button class="btn btn-primary navbar-btn" title="Venda" 
                    ng-if="consign"
                    ng-click="makeSale()">
              <i class="fa fa-cart-arrow-down"></i>
              <span class="btnShortcut">P</span>
            </button>
            <button id="btnReport" target="_blank" 
                                   class="btn navbar-btn btn-primary" 
                                   title="Contrato" 
                                   ng-if="consign" ng-click="report()">
              <i class="fa fa-file-text-o"></i><span class="btnShortcut">F</span>
            </button>
            <button class="btn navbar-btn btn-danger" 
                    title="Cancelar" 
                    ng-show="consign" 
                    ng-click="cancel()">
              <i class="fa fa-times-circle"></i><span class="btnShortcut">E</span>
            </button>
          </div>
        </div>
        <div style="padding: 15px 0;" class="navHeaderRight col-md-2 text-center">
          <span style="margin: 0; color: #5cb85c;" id="orderTotal" class="navbar-text text-center">
            Custo total: {{consign ? (costTotal()|lMoney) : "-"}}
          </span>
        </div>
        <div style="padding: 15px 0;" class="navHeaderRight col-md-2 text-center">
          <span style="margin: 0; color: #5cb85c;" id="orderTotal" class="navbar-text text-center">
            Total Saída: {{consign ? (positiveTotal()|lMoney) : "-"}}
          </span>
        </div>
        <div style="padding: 15px 0;" class="navHeaderRight col-md-2 text-center">
          <span style="margin: 0; color: #d9534f;" id="orderTotal" class="navbar-text text-center">
            Total Entrada: {{consign ? (negativeTotal()|lMoney) : "-"}}
          </span>
        </div>
        <div style="padding: 15px 0;" class="navHeaderRight col-md-2 text-center">
          <span style="margin: 0; color: #428bca;" id="orderTotal" class="navbar-text text-center">
            Total: {{consign ? (total()|lMoney) : "-"}}
          </span>
        </div>
      </div>
    </nav>
    <section>
      <nav class="navSubHeader">
        <div class="navSubHeaderLeft navbar-header col-lg-8 col-md-6 col-sm-6 col-xs-12">
        <div ng-if="consign && consign.dealer">
          <a ng-href="{{baseUrl}}vwCustomer/elt/{{consign.dealer.customer.id}}" target="_blank" style="color: #eee;">
            Vinculado com <b>{{consign.dealer.customer.personAttr.name}}</b>
          </a>
        </div>
        </div>
        <div id="navSaleId" class="navSubHeaderLeft navbar-header col-lg-2 col-md-3 col-sm-3 col-xs-12 text-center">
            <div class="form-group" style="margin-top: 4px;">
              <select class="form-control input-sm" ng-model="coeficient" ng-init="coeficient='1'">
                <option style="font-size: 14px; padding: 5px 0;" value="-1">{{"consign.in"|t}}</option>
                <option style="font-size: 14px; padding: 5px 0;" value="1">{{"consign.out"|t}}</option>
              </select>
            </div>
        </div>
        <div class="navSubHeaderRight col-lg-2 col-md-3 col-sm-3 col-xs-12 text-center">
            <p>Id: {{consign ? consign.id : ""}}</p>
        </div>
      </nav>
    </section>
    <section class="addSection">
          <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label for="">Previsão</label>
                <xp-date ng-model="previsionClosing" data-blur="updatePrevisionClosing()" ></xp-date>
              </div>
            </div>
          </div>
      <xp-product-helper notify-prod="addProd"></xp-product-helper>
    </section>
    
    <div >
    </div>
    <hr>
    <table id="order-items" class="table table-bordered table-condensed">
      <thead>
        <tr>
          <th>Linha</th>
          <th>Código</th>
          <th>Descrição</th>
          <th>{{"consign.out"|t}} 
            <span class="bg-danger">{{consign ? (positiveAmount()|number) : ""}}</span> 
          </th>
          <th>{{"consign.in"|t}} 
            <span class="bg-danger">{{consign ? (negativeAmount()|number) : ""}}</span>
          </th>
          <th>{{"consign.totalOut"|t}} 
            <span class="bg-danger">{{consign ? (amount()|number) : ""}}</span>
          </th>
          <th>Preço</th>
          <th>{{"subtotal"|t}}</th>
          <th>Ação</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="(index,oi) in activeOrderItems" 
            ng-class="{success: isSuccess(oi), warning: isWarning(oi),  error: isError(oi)}">
          <td>{{index + 1}}</td>
          <td>{{oi.productSnapshot.product.code}}</td>
          <td><input type="text" ng-blur="updateSnapshot(oi.productSnapshot)" ng-model="oi.productSnapshot.name"/></td>
          <td class="input-output">
            <xp-number ng-model="oi.outOffset" 
                       ng-init="oi.outOffset=oi.summary.positiveFlows.amount"
                       data-blur="updatePositiveFlow(oi)">
            </xp-number>
          </td>
          <td class="input-output">
            <xp-number ng-model="oi.inOffset" 
                       ng-init="oi.inOffset=oi.summary.negativeFlows.amount"
                       data-blur="updateNegativeFlow(oi)"></xp-number>
          </td>
          <td>{{oi.summary.amount|number:3}}</td>
          <td>{{oi.productSnapshot.price|sMoney}}</td>
          <td>{{oi.summary.price|sMoney}}</td>
          <td>
            <button class="btn btn-primary" ng-click="openItem(oi)">Acessar</button>
            <button class="btn btn-primary" ng-click="deleteItem(oi)">Excluir</button>
          </td>
        </tr>
      </tbody>
    </table>
    <div id="selectedOrderItemModal" class="modalOff" style="padding: 0;">
      <xp-consigned-order-item data-order-item="selectedOrderItem"></xp-consigned-order-item>
    </div>
    <div id="customersModal" class="modalOff">
      <xp-table-selector data-link="{{baseUrl}}vwCustomer/elt" 
      get-data="feedCustomers(items, filter)" 
      select="bindCustomer(selectedRow)">
      </xp-table-selector>
    </div>
  </div>
</main>
