<main>
  <div ng-controller="ConsignmentAccessController">
    <div>
      <a href="<?=OUTER_URL?>index.php?p=pedido" title="<?=lang('back')?>" class="btn btn-default btn-warning">
        <i class="fa fa-rotate-left"></i>
      </a>
    </div>
    <label>Apenas em aberto: <input type="checkbox" ng-model="optional.onlyOpened"></label>
    <div>
      <div>
        <label>
          Filtrar por produto: 
          <span ng-if="productName">{{productName}}</span>
          <button ng-click="showProductsModal()">Escolher produto</button>
        </label>
        <button ng-click="cleanProductFilter()">Limpar produto</button>
      </div>
    </div>
    <div>
      <div>
        <label>
          Filtrar por revendedor(a): 
          <span ng-if="dealerName">{{dealerName}}</span>
          <button ng-click="showDealersModal()">Escolher revendedor(a)</button>
        </label>
        <button ng-click="cleanDealerFilter()">Limpar revendedor(a)</button>
      </div>
    </div>
    <xp-table-selector data-link="<?=base_url()?>vwConsignment" 
      get-data="feedConsignments(items, filter, optional)" 
      data-optional="optional"
      select="accessConsignment(selectedRow)">
    </xp-table-selector>
    <div id="productsModal" class="modalOff">
      <xp-table-selector data-link="{{baseUrl}}vwProduct" 
                         get-data="feedProducts(items, filter)" 
                         select="bindProduct(selectedRow)">
      </xp-table-selector>
    </div>
    <div id="dealersModal" class="modalOff">
      <xp-table-selector data-link="" 
                         get-data="feedDealers(items, filter)" 
                         select="bindDealer(selectedRow)">
      </xp-table-selector>
    </div>
  </div>
</main>
