<style>
  main .navHeader{min-height: 65px;}
  main .addSection{padding-top: 10px;}
  .inputTotalPay{font-size: 22px; color: blue;}
  .payment .form-control[readonly]{color: #d43f3f; font-weight: 700;}
  .payment .panelPayers button{margin: 3px 3px 0 0; max-width: 37px;}
</style>
<main>
  <div ng-controller="SaleController" ng-init="init(<?=$id ?: 'null'?>, <?=$orderNumber ?: 'null'?>)">
    <hr class="topHr" >
    <nav class="navHeader navbar navbar-inverse">
      <div class="navHeaderLeft navbar-header col-lg-9 col-md-9 col-sm-7 col-xs-12">
        <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 text-left">
          <a id="ancBack" ng-href={{backUrl}} title="Voltar" class="btn btn-primary">
            <i class="fa fa-rotate-left"></i><span class="btnShortcut">V</span>
          </a>
          <!--Não funciona-->
            <a id="ancHome" href="<?=OUTER_URL?>" class="btn btn-primary navbar-btn" title="Inicio">
              <i class="fa fa-home"></i>
              <span class="btnShortcut">J</span>
            </a>
            <div ng-show="sale && sale.order.orderItems.length" style="display: inline-block;">
              <a id="ancReceipt" ng-href="<?=base_url()?>billing/receipt/{{sale.id}}" 
                 title="Recebimento" class="btn navbar-btn btn-primary">
                <i class="fa fa-money"></i><span class="btnShortcut" style="left: -12px;">$</span>
              </a>
            </div>
            <div ng-if="sale" style="display: inline-block;">
              <a title="Nova devolução/troca" 
                      class="btn navbar-btn btn-primary"
                      ng-href="{{ newExchangeUrl(sale.id) }}">
                <i class="fa fa-exchange"></i><span class="btnShortcut">T</span>
              </a>
            </div>
            <button ng-show="sale" id="print" title="Imprimir item novo" type="button" class="btn navbar-btn btn-primary"
                    ng-click="printSale(false)">
              <i class="fa fa-print"></i><span class="newShortcut">new</span>
              <span class="btnShortcut" style="left: -24px;">P</span>
            </button>
            <button ng-show="sale" id="printRedo" title="Imprimir fechamento" type="button" 
                    class="btn navbar-btn btn-warning"
                    ng-click="printSale(true)">
              <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
              <span class="btnShortcut" style="left: -24px;">N</span>
            </button>
            <div ng-show="sale && sale.consignment" style="display: inline-block;">
              <a ng-href="<?=base_url()?>vwConsignment/{{sale.consignment.id}}"
                 title="Consignado que originou esta venda" 
                 class="btn navbar-btn btn-primary">
                <i class="fa fa-briefcase"></i>
              </a>
            </div>
            <div ng-show="sale" style="display: inline-block;">
              <button title="Cancelar" 
                      class="btn navbar-btn btn-danger"
                      ng-click="cancel()">
                <i class="fa fa-times-circle"></i><span class="btnShortcut">E</span>
              </button>
            </div>
        </div>
      </div>
      <div class="navHeaderRight col-lg-3 col-md-3 col-sm-5 col-xs-12 text-center" style="padding: 15px 0;">
        <span class="navbar-text text-center" id="orderTotal" style="margin: 0; color: #eee;">Total: {{total()|lMoney}}</span>
      </div>
    </nav>
    <section>
      <nav class="navSubHeader">
        <div class="navSubHeaderLeft navbar-header col-lg-8 col-md-6 col-sm-6 col-xs-12">
          <div class="form-inline">
            <div class="form-group form-group-sm col-sm-2 col-xs-2">
              <label for="identifier">Busca rápida</label>
            </div>
            <div class="form-group form-group-sm col-lg-4 col-md-6 col-sm-8 col-xs-6" style="padding-left: 0; margin-top: 4px;">
              <input type="text" ng-model="identifier" ng-blur="updateIdentifier()" class="form-control" id="identifier">
            </div>
            <div class="form-group form-group-sm col-sm-1 col-xs-1" style="padding-left: 0; margin-left: -10px;">
              <button ng-show="orderNumber" ng-click="addSale()" title="Adicionar à mesa" class="btn btn-success btn-sm"><i class="fa fa-user-plus"></i></button>
            </div>
          </div>
        </div>
        <div class="navSubHeaderLeft navbar-header col-lg-2 col-md-3 col-sm-3 col-xs-12 text-center" id="navSaleId">
          <p>ID: <span id="saleId">{{sale ? sale.id : "---"}}</span></p>
        </div>
        <div class="navSubHeaderRight col-lg-2 col-md-3 col-sm-3 col-xs-12 text-center">
          <p><span ng-show="orderNumber">Mesa: {{orderNumber}}</span></p>
        </div>
      </nav>
    </section>
    <section class="addSection container-fluid">
      <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12" style="padding-right: 7.5px;">
          <xp-product-helper notify-prod="addProd"></xp-product-helper>
          <table class="table table-bordered table-condensed table-striped">
          <thead>
            <tr>
              <th>Pos</th>
              <th>Código</th>
              <th>Descrição</th>
              <th>Quantidade</th>
              <th>Preço</th>
              <th>Desconto</th>
              <th>{{"subtotal"|t}}</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="oi in sale.order.orderItems.slice().reverse()">
              <td>{{oi.displayOrder}}</td>
              <td>{{oi.productSnapshot.product.code}}</td>
              <td><input type="text" ng-model="oi.productSnapshot.name" ng-blur="updateSnapshot(oi.productSnapshot)" class="form-control"/></td>
              <td ng-init="oi.amount=orderItemAmount(oi)">
                <xp-number ng-model="oi.amount" data-blur="updateOrderItemAmount(oi)"></xp-number>
              </td>
              <td>
                <xp-number ng-model="oi.productSnapshot.price" data-blur="updateSnapshot(oi.productSnapshot)"></xp-number>
              </td>
              <td>
                <xp-percentage data-is-percentage="true"
                               data-reference-number="oi.amount * oi.productSnapshot.price"
                               data-number="oi.productSnapshot.discount" 
                               data-blur="updateSnapshot(oi.productSnapshot)">
              </td>
              <td>{{orderItemAmount(oi) * oi.productSnapshot.price - oi.productSnapshot.discount|sMoney}}</td>
              <td><button class="btn btn-danger" ng-click="deleteItem(oi)">Excluir</button></td>
            </tr>
          </tbody>
        </table>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 payment" style="padding-left: 7.5px;">
          <div ng-if="exchanges" style="border: 1px solid black;padding: 5px;border-radius: 5px;">
            <h3>Trocas</h3>
            <div ng-repeat="e in exchanges.data">
              <a ng-href="{{ exchangeUrl(e.id) }}">
                  {{ e.attributes.created|date:'medium' }} 
                - {{ isExchangeEnabled(e) ? "Ativo" : "Desativado" }}
              </a>
            </div>
          </div>
          <div ng-if="sale && sale.payers.length" ng-show="sale && sale.order.orderItems.length">
            <div class="panel panel-default">
              <div class="panel-heading panelPayers">
                <!--<h3 class="panel-title text-left col-lg-4 col-md-3 col-sm-8 col-xs-4">{{ 'billing.discount'|t }}</h3>-->
                <h3 class="panel-title" style="float: left;">Pagamento</h3>
                <div style="float: right;">
                  <button title="Imprimir Cupom Fiscal" type="button" 
                          class="btn navbar-btn btn-primary btn-sm" 
                          ng-click="printECF()">
                    <i class="fa fa-print"></i><span class="newShortcut">ECF</span>                   
                  </button>
                  <button title="Imprimir Recibo" type="button" 
                          class="btn navbar-btn btn-info btn-sm" 
                          ng-click="printInvoice()">
                    <i class="fa fa-print"></i><span class="newShortcut" style="">REC</span>
                  </button>
                  <button ng-show="canClose()" ng-click="close()" 
                          title="Finalizar" 
                          type="button" 
                          class="btn navbar-btn btn-success btn-sm" style="margin: 3px 3px 0 0;">
                    <i class="fa fa-unlock"></i>
                  </button>
                </div>
              </div>
              <div class="panel-body">
                <div class="form-horizontal">
                  <div class="form-group">
                    <xp-discount sale-id="{{sale.id}}" update="updateSale()"></xp-discount>
                  </div>
                  <xp-aggregates ng-if="sale.saleAggregates.length" 
                                 sale-id="{{sale.id}}" 
                                 update="updateSale()"></xp-aggregates>
                  <xp-portion-bucket data-id="{{sale.payers[0].portionBucket.id}}" 
                                     data-refresh="portionBucketRefresh"
                                     set-portion-bucket="setPortionBucket(portionBucket)">
                  </xp-portion-bucket>
                </div>
                <div>
                  <h4>Pagador status</h4>
                  <xp-payer-status data-payer="sale.payers[0]"></xp-payer-status>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>    
  </div>
</main>
