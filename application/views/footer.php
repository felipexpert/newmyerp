  <footer>
  
  </footer>
</body>
 <!--when require.js loads it will inject another script tag
      (with async attribute) for scripts/main.js-->

  <!-- RequireJS! -->
  <script id="require-js" data-main="<?=base_url()?>resource/js/main" 
          src="<?=base_url()?>resource/js/require.js?v=<?=CSS_JS_VERSION?>"
          data-version="<?=CSS_JS_VERSION?>">
  </script>
  <!-- RequireJS! -->
  <!-- Initial setup START -->
  <script src="<?=base_url()?>resource/js/language/<?='portuguese'?>.js"></script>
  <script>
    const baseUrl = "<?=base_url()?>", // don't use this
          outerUrl = "<?=OUTER_URL?>", // don't use this
          finalUrl = "<?=FINAL_URL?>", // don't use this
          sys = Object.create(null);

    setupConstant(sys, "BASE_URL", baseUrl);
    setupConstant(sys, "OUTER_URL", outerUrl);
    setupConstant(sys, "MONEY_PRECISION", 2);
    setupConstant(sys, "GENERIC_PRECISION", 3);

    function setupConstant(obj, fieldName, value) {
      Object.defineProperty(obj, fieldName, {
        get: function() { return value; },
        set: function(value) { 
          throw new Error("Cannot set " + value + ". The field" + fieldName + " is a constant."); 
        }
      }); 
    }
  </script>
  <!-- Initial setup END -->
</html>
