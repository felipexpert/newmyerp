<main>
  <h2><?=lang('product')?></h2>
  <form method="post" class="ajax noReset" action="<?=base_url()?>product/updateProductAjax">
    <div>
      <label for="id"><?=lang('id')?>: </label>
      <input type="text" name="id" id="id" value="<?=$product->getId()?>" 
          readonly>
    </div>
    <div>
      <label for="name"><?=lang('name')?>: </label>
      <input type="text" name="name" id="name" value="<?=$product->getName()?>">
    </div>
    <div>
      <label for="code"><?=lang('code')?>: </label>
      <input type="text" name="code" id="code" value="<?=$product->getCode()?>">
    </div>
    <div>
      <?=$category?>
    </div>
    <div>
      <label for="price"><?=lang('price')?>: </label>
      <input type="text" name="price" id="price" value="<?=$product->getPrice()?>">
    </div>
    <div><input type="submit" value="<?=lang('save')?>"></div>
  </form>
</main>
