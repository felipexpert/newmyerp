<style>
  main .navHeader{min-height: 65px;}
  main .addSection{padding-top: 10px;}
  .inputTotalPay{font-size: 22px; color: blue;}
  .payment .form-control[readonly]{color: #d43f3f; font-weight: 700;}
  .payment .panelPayers button{margin: 3px 3px 0 0; max-width: 37px;}
  #variacao .form-group{margin-bottom: 5px; min-height: 32px;}
  #variacao{border: 1px solid #c4c4c4; padding-top: 15px;}
  .varLabel{padding: 0; margin-top: 5px;}
  .varInput{padding: 0 0 0 15px;}
</style>

<main>
  <div>
    <hr class="topHr" >
    <nav class="navHeader navbar navbar-inverse">
      <div class="navHeaderLeft navbar-header col-lg-9 col-md-9 col-sm-7 col-xs-12">
        <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 text-left">
          <a id="ancBack" ng-href={{backUrl}} title="Voltar" class="btn btn-primary">
            <i class="fa fa-rotate-left"></i>
            <span class="btnShortcut">V</span>
          </a>
          <!--Não funciona-->
          <a id="ancHome" href="#" class="btn btn-primary navbar-btn" title="Inicio">
            <i class="fa fa-home"></i>
            <span class="btnShortcut">J</span>
          </a>
        </div>
      </div>
      <div class="navHeaderRight col-lg-3 col-md-3 col-sm-5 col-xs-12 text-center" style="padding: 15px 0;">
        <span class="navbar-text text-center" id="orderTotal" style="margin: 0; color: #eee;">ID: </span>
      </div>
    </nav>
    
    <section class="addSection container-fluid">
      <div class="row">
        <div style="padding-right: 7.5px;" class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
          <div class="helperProd ng-isolate-scope" notify-prod="addProd">
            <div class="panel panel-default">
              <div class="panel-heading panelPayers">
                <h3 class="panel-title">Produto</h3>
              </div>
              <div class="panel-body">
                <div class="form-inline">
                  <div class="ng-isolate-scope">
                    <div class="col-lg-12" style="padding: 5px 0;">
                      <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <label for="" class="small">Nome</label>
                        <input type="text" class="form-control" value="">
                      </div>
                      <div class="form-group col-lg-2">
                        <label for="" class="small">Código Prefixo</label>
                        <input type="text" class="form-control" value="">
                      </div>
                      <div class="form-group col-lg-3" style="padding-right: 0;">
                        <label for="" class="small">Categoria</label>
                        <input type="text" class="form-control" readonly="" placeholder="Selecione uma categoria">
                      </div>
                      <div class="form-group col-lg-1" style="padding-right: 0;">
                         <label for="" class="small">&nbsp;</label>
                        <button class="btn btn-primary"><i class="fa fa-search"></i></button>
                      </div>
                      <div class="form-group col-lg-3">
                        <label for="" class="small">Marca</label>
                        <select class="form-control" style="width: 100%;">
                          <option>Selecione</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-12" style="padding: 5px 0;">
                      <div class="form-group col-lg-2">
                        <label for="" class="small">Valor Compra</label>
                        <span>
                          <input type="text" class="form-control" value="" placeholder="R$ 00,00">
                        </span>
                      </div>
                      <div class="form-group col-lg-2">
                        <label for="" class="small">Lucro</label>
                        <span>
                          <input type="text" class="form-control" value="" placeholder="%">
                        </span>
                      </div>
                      <div class="form-group col-lg-2">
                        <label for="" class="small" >Valor Venda</label>
                        <span>
                          <input type="text" class="form-control" value="" placeholder="R$ 00,00">
                        </span>
                      </div>
                      <div class="form-group col-lg-3">
                        <label for="" class="small">Alíquota</label>
                        <select class="form-control" style="width: 100%; margin: 5px 0;">
                          <option>Selecione</option>
                        </select>
                      </div>
                      <div class="form-group col-lg-2">
                        <label for="" class="small">Lucro Auto.</label>
                        <div style="margin: 5px;">
                          <input type="checkbox" style="min-height: 30px;">                        
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="helperProd ng-isolate-scope" notify-prod="addProd">
            <div class="panel panel-default">
              <div class="panel-heading panelPayers">
                <h3 class="panel-title" style="float: left;">Variações</h3>
                <button style="float: right; margin: 3px;" class="btn btn-sm btn-success"><i class="fa fa-save"></i> <b>SALVAR</b></button>
              </div>
              <div class="panel-body">
                <div class="col-lg-4 panel" id="variacao">
                  <div class="form-group">
                    <input type="text" class="form-control" value="Dados">
                  </div>
                  <div class="form-group">
                    <div class="col-lg-4 varLabel">
                      <label for="" class="small">Código </label>
                    </div>
                    <div class="col-lg-8 varInput">
                      <input type="text" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-lg-4 varLabel">
                      <label for="" class="small">Valor Compra</label>
                    </div>
                    <div class="col-lg-8 varInput">
                      <input type="text" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-lg-4 varLabel">
                      <label for="" class="small">Lucro</label>
                    </div>
                    <div class="col-lg-8 varInput">
                      <input type="text" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-lg-4 varLabel">
                      <label for="" class="small">Valor Venda</label>
                    </div>
                    <div class="col-lg-8 varInput">
                      <input type="text" class="form-control" value="">
                    </div>
                  </div>
                  <div class="form-group col-lg-6" style="padding-left: 0;">
                      <label for="" class="small" style="vertical-align: top; margin-top: 10px;">Lucro Auto. </label>
                      <input type="checkbox" style="min-height: 30px;">
                  </div>
                  <div class="form-group col-lg-6" style="padding-left: 0;">
                    <label for="" class="small" style="vertical-align: top; margin-top: 10px;">Desativado </label>
                    <input type="checkbox" style="min-height: 30px;">  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div style="padding-right: 7.5px;" class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="helperProd ng-isolate-scope" notify-prod="addProd">
            <div class="panel panel-default">
              <div class="panel-heading panelPayers">
                <h3 style="float: left;" class="panel-title">Características</h3>
                <button style="float: right; margin: 3px;" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></button>
              </div>
              <div class="panel-body">
                <div class="panel panel-primary">
                  <div class="panel-heading panelPayers">
                    <div style="float: left; margin: 1px 0 0 1px;">
                      <input type="text" class="form-control" value="Nome">
                    </div>
                    <div style="float: right; margin: 3px;">
                      <button class="btn btn-sm btn-success"><i class="fa fa-plus"></i></button>
                      <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                      <button class="btn btn-sm btn-warning"><i class="fa fa-level-down"></i></i></button>
                    </div>
                  </div>
                  <div class="panel-body">
                    <ul class="form-inline">
                      <li style="margin-bottom: 3px;">
                        <div class="form-group">
                          <input type="text" class="form-control" value="Sub">
                        </div>
                        <button class="btn btn-sm btn-default"><i class="fa fa-circle"></i></i></button>
                        <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                      </li>
                      <li>
                        <div class="form-group">
                          <input type="text" class="form-control" value="Sub">
                        </div>
                        <button class="btn btn-sm btn-success"><i class="fa fa-circle"></i></i></button>
                        <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    
  </div>
  <div id="product-controller" ng-controller="ProductController" ng-init="init(<?=$id?>)">
    <div>Id: {{product.id || "---"}}</div>
    <div>
      <button class="btn btn-primary" ng-click="showCategoryModal()">Escolher Categoria</button>
      <div ng-if="product.category">Categoria escolhida: {{product.category.name}}</div>
    </div>
    <div>
      <select ng-model="product.brand"
              ng-options="value as value.name for value in brands">
              <option value="">Unknown</option>
      </select>
    </div>
    <div>
      <select ng-model="product.taxRate"
              ng-options="value as value.description for value in taxRates">
              <option value="">Unknown</option>
      </select>
    </div>
    <div>
      <label>Nome: <input type="text" ng-model="product.name"></label>
    </div>
    <div>
      <label>Código prefixo: <input type="text" ng-model="product.codePrefix"></label>
    </div>
    <div>
      <label>Lucro automático: 
        <input type="checkbox" ng-model="proportional" ng-change="proportionalChanged()">
      </label>
    </div>
    <div>
      <label>Preço de compra base: 
        <xp-number ng-model="product.defaultBuyingPrice" data-keyup="defaultBuyingPriceUpdated()"></xp-number>
      </label>
    </div>
    <div>
      <label>Percentual base: 
        <xp-number ng-model="product.defaultProfitPercentage" data-keyup="defaultProfitPercentageUpdated()"></xp-number>
      </label>
    </div>
    <div>
      <label>Preço base: 
        <xp-number ng-model="product.defaultPrice" data-keyup="defaultPriceUpdated()"></xp-number>
      </label>
    </div>
    <div>
        <div ng-repeat="i in product.instances">
          <div>ID: {{i.id || "Sem ID"}}, NAME: {{prodInstName(i)}}</div>
          <div>
            <label>Sufixo do codigo: <input type="text" ng-model="i.codeSuffix"></label>
          </div>
          <div>
            <label>Lucro automático: <input type="checkbox" ng-model="i.proportional"></label>
          </div>
          <div>
            <label>Preco  de compra: 
              <xp-number ng-model="i.buyingPrice" data-keyup="updatePrice(i)"></xp-number>
            </label>
          </div>
          <div>
            <label>Percentual: 
              <xp-number ng-model="i.profitPercentage" data-keyup="updatePrice(i)"></xp-number>
            </label>
          </div>
          <div>
            <label>Preco: 
              <xp-number ng-model="i.price" data-keyup="updateProfitPercentage(i)"></xp-number>
            </label>
          </div>
          <div>
            <label>Desativado: <input type="checkbox" ng-model="i.disabled"></label>
          </div>
        </div>
    </div>
    <div ng-show="product.id"><button class="btn btn-primary" ng-click="update()">Atualizar</button></div>
    <div ng-show="!product.id"><button class="btn btn-primary" ng-click="save()">Salvar</button></div>
    <xp-product-characteristic data-selected="selected" 
                               set-characteristics="setCharacteristics(characteristics)">
    </xp-product-characteristic>
    <div id="category-modal">
      <xp-table-selector data-link="" 
                         get-data="feedProductCategories(items, filter)" 
                         select="bindProductCategory(selectedRow)">
      </xp-table-selector>
    </div>
  </div>
</main>
