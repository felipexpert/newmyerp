<style>
  main .navHeader{min-height: 65px;}
  main .addSection{padding-top: 10px;}
  #variacao .form-group{margin-bottom: 5px; min-height: 32px;}
  #variacao{border: 1px solid #c4c4c4; padding-top: 15px;}
  .on { color: green; }
  .disable {cursor: pointer; font-size: 22px; font-weight: 100; color: #b4b4b4;}
  #prodDesc .form-group{margin-bottom: 10px;}
  .input-normal { color: black; font-size: 14px; font-weight: normal; }
  input.input-normal { padding: 5px; width: 40px; }
  select.input-normal { padding: 2px; width: 60px; }
</style>

<main>
  <div id="product-controller" ng-controller="ProductController" ng-init="init(<?=$id?>)">
    <div style="position: fixed;width: 100%; height: auto; z-index: 99;">
      <hr class="topHr" >
      <nav class="navHeader navbar navbar-inverse">
        <div class="navHeaderLeft navbar-header col-lg-11 col-md-11 col-sm-10 col-xs-10">
          <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 text-left">
            <a id="ancBack" href="<?=OUTER_URL?>index.php?p=produto" title="Voltar" class="btn btn-primary">
              <i class="fa fa-rotate-left"></i>
              <span class="btnShortcut"></span>
            </a>
            <a href="<?=OUTER_URL?>" class="btn btn-primary navbar-btn" title="Inicio">
              <i class="fa fa-home"></i>
              <span class="btnShortcut"></span>
            </a>
            <div style="display: inline-block;">
              <div ng-show="product.id"><button class="btn btn-success navbar-btn" ng-click="update()"><i class="fa fa-save"></i></button></div>
              <div ng-show="!product.id"><button class="btn btn-success navbar-btn" ng-click="save()"><i class="fa fa-save"></i></button></div>
            </div>
          </div>
        </div>
        <div class="navHeaderRight col-lg-1 col-md-1 col-sm-2 col-xs-2" style="padding: 15px 0;">
          <span class="navbar-text" id="orderTotal" style="margin: 0; color: #eee; font-size: 16px;">ID: {{product.id || "---"}}</span>
        </div>
      </nav>
    </div>
    <section class="addSection container-fluid" style="padding-top: 80px;">
      <div class="row">
        <div style="padding-right: 7.5px;" class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
          <div class="helperProd ng-isolate-scope" notify-prod="addProd">
            <div class="panel panel-default">
              <div class="panel-heading panelPayers">
                <h3 class="panel-title">
                  {{product.name ? product.name : "Produto"}}
                  <div ng-show="product.id" class="pull-right" style="margin-top: -7px; margin-right: 5px">
                    <div class="barcode">
                      <select class="input-normal" 
                              ng-model="labelType"
                              ng-options="value as value.name for value in labelTypes">
                        <option value="">Tipo</option>
                      </select>
                      <input class="input-normal" type="text" placeholder="Qtd" ng-model="labelAmount">
                      <a ng-href="<?=OUTER_URL?>produto_imprimir_barcode_zebra.php?tipo={{labelType.id}}&qtd={{labelAmount}}&ids={{product.id}}&produtoBase=true" 
                         class="btn btn-info btn-sm" target="_blank">
                        <i class="fa fa-barcode"></i>
                        Etiqueta
                      </a>
                    </div>
                  </div>
                </h3>
              </div>
              <div class="panel-body">
                <div class="form-inline">
                  <div class="ng-isolate-scope" id="prodDesc">
                    <div class="row">
                      <div class="form-group col-lg-3 col-md-4 col-sm-3 col-xs-3">
                        <label class="small">Nome</label>
                        <input type="text" class="form-control" ng-model="product.name">
                      </div>
                      <div class="form-group col-lg-2 col-md-4">
                        <label for="" class="small">Código Prefixo</label>
                        <input type="text" class="form-control" ng-model="product.codePrefix">
                      </div>
                      <div class="form-group col-lg-4 col-md-4">
                        <label for="" class="small">Categoria</label>
                        <input style="width: calc(100% - 39px)" type="text" class="form-control pull-left" 
                               readonly="" placeholder="Selecione uma categoria" value="{{product.category.name}}">
                        <button class="btn btn-primary pull-left" ng-click="showCategoryModal()"><i class="fa fa-search"></i></button>
                      </div>
                      <div class="form-group col-lg-3 col-md-3">
                        <label for="" class="small">Marca</label>
                        <select ng-model="product.brand"
                              ng-options="value as value.name for value in brands" class="form-control" style="width: 100%;">
                              <option value=""></option>
                        </select>
                      </div>
                      <div class="form-group col-lg-2 col-md-3">
                        <label for="" class="small">Valor Compra</label>
                        <xp-number ng-model="product.defaultBuyingPrice" 
                                   data-keyup="defaultBuyingPriceUpdated()"
                                   data-decimals="2">
                        </xp-number>
                      </div>
                      <div class="form-group col-lg-2 col-md-3">
                        <label for="" class="small">Lucro (%)</label>
                        <xp-number ng-model="product.defaultProfitPercentage" 
                                   data-keyup="defaultProfitPercentageUpdated()"
                                   data-decimals="2">
                        </xp-number>
                      </div>
                      <div class="form-group col-lg-2 col-md-3">
                        <label for="" class="small">Valor Venda</label>
                        <xp-number ng-model="product.defaultPrice" 
                                   data-keyup="defaultPriceUpdated()"
                                   data-decimals="2">
                        </xp-number>
                      </div>
                      <div class="form-group col-lg-3 col-md-3">
                        <label for="" class="small">Alíquota</label>
                        <select ng-model="product.taxRate"
                                ng-options="value as value.description for value in taxRates" class="form-control" style="width: 100%;">
                                <option value=""></option>
                        </select>
                      </div>
                      <div class="form-group col-lg-2 col-md-3">
                        <label for="" class="small">Lucro automatico</label>
                        <input type="checkbox" class="form-control" ng-model="proportional" ng-change="proportionalChanged()">
                      </div>
                      <div class="form-group col-lg-3 col-md-4 col-sm-3 col-xs-3">
                        <label class="small">Obs</label>
                        <input type="text" class="form-control" ng-model="product.obs">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="helperProd" ng-repeat="i in product.instances">
            <div class="panel panel-info">
              <div class="panel-heading panelPayers">
                <h3 class="panel-title pull-left" style="width: 100%; display: block;">
                  {{prodInstName(i)}}
                  <div ng-show="i.id" class="pull-right" style="margin-top: -7px; margin-right: 5px">
                    <select class="input-normal" 
                            ng-model="labelType"
                            ng-options="value as value.name for value in labelTypes">
                      <option value="">Tipo</option>
                    </select>
                    <input class="input-normal" type="text" placeholder="Qtd" ng-model="labelAmount">
                    <a ng-href="<?=OUTER_URL?>produto_imprimir_barcode_zebra.php?tipo={{labelType.id}}&qtd={{labelAmount}}&ids={{i.id}}" 
                       class="btn btn-info btn-sm" target="_blank">
                      <i class="fa fa-barcode"></i>
                      Etiqueta
                    </a>

                    <a ng-href="<?=OUTER_URL?>index.php?p=produto_detalhe&id={{i.id}}&modo=estoque" 
                          class="btn btn-info btn-sm">
                      <i class="fa fa-cubes"></i>
                      Estoque
                    </a>
                  </div>
                </h3>
              </div>
              <div class="panel-body">
                  <div class="row">
                    <div class="form-group col-lg-3 col-md-3">
                      <label for="" class="small">Código</label>
                      <input type="text" class="form-control" ng-model="i.codeSuffix">
                    </div>
                    <div class="form-group col-lg-3 col-md-3">
                      <label for="" class="small">Código de barras</label>
                      <input type="text" class="form-control" ng-model="i.barCode">
                    </div>
                    <div class="form-group col-lg-3 col-md-3">
                      <label for="" class="small">Preço Compra</label>
                      <xp-number ng-model="i.buyingPrice" 
                                 data-keyup="updatePrice(i)"
                                 data-decimals="2">
                      </xp-number>
                    </div>
                    <div class="form-group col-lg-3 col-md-3">
                      <label for="" class="small">Lucro (%)</label>
                      <xp-number ng-model="i.profitPercentage" 
                                 data-keyup="updatePrice(i)"
                                 data-decimals="2">
                      </xp-number>
                    </div>
                    <div class="form-group col-lg-3 col-md-3">
                      <label for="" class="small">Preço</label>
                      <xp-number ng-model="i.price" 
                                 data-keyup="updateProfitPercentage(i)"
                                 data-decimals="2">
                      </xp-number>
                    </div>
                    <div class="form-group col-lg-3 col-md-4 col-sm-3 col-xs-3">
                      <label class="small">Obs</label>
                      <input type="text" class="form-control" ng-model="i.obs">
                    </div>
                    <div ng-if="!i.id" class="form-group col-lg-3 col-md-4 col-sm-3 col-xs-3">
                      <label class="small">Estoque inicial</label>
                      <input type="text" class="form-control" ng-model="i.initialStock">
                    </div>
                    <div class="form-group col-lg-2 col-md-2"> 
                        <label for="" class="small" style="vertical-align: top; margin-top: 10px;">Lucro Auto. </label>
                        <input type="checkbox" ng-model="i.proportional" class="form-control">
                    </div>
                    <div class="form-group col-lg-2 col-md-2">
                      <label for="" class="small" style="vertical-align: top; margin-top: 10px;">Desativado </label>
                      <input type="checkbox" ng-model="i.disabled" style="min-height: 30px;" class="form-control">
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
        
        <div style="padding-right: 7.5px;" class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="helperProd">
          <xp-product-characteristic data-selected="selected" 
                                     data-characteristics="characteristics"
                                     notify-change="updateInstances()">
          </xp-product-characteristic>
          </div>
        </div>
        
          <div id="category-modal">
            <xp-table-selector data-link="" 
                               get-data="feedProductCategories(items, filter)" 
                               select="bindProductCategory(selectedRow)">
            </xp-table-selector>
          </div>
      </div>
    </section>
  </div>
</main>
