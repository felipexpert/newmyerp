<?php
class Initializer {
  function init() {
    $ci =& get_instance();
    $ci->lang->load('message','portuguese');
    $ci->lang->load('form_validation','portuguese');
    $ci->em = $ci->doctrine->em; 
    $ci->load->model('logger/loggerCreatorModel', 'logger');
  }
}
