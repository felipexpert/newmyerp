use newmyerp;
drop trigger if exists NEW_syncBOrderItemInsert;
delimiter //
create trigger NEW_syncBOrderItemInsert before insert on newmyerp.OrderItem
for each row
begin
	SET @antigoId 	= (SELECT myerp.tblpedido_item.fldId FROM myerp.tblpedido_item 
					ORDER BY myerp.tblpedido_item.fldId DESC LIMIT 1);
	SET @novoId		= ifnull((SELECT newmyerp.OrderItem.id FROM newmyerp.OrderItem
					ORDER BY newmyerp.OrderItem.id DESC LIMIT 1), 0);
	IF @antigoId > @novoId THEN
		SET new.id = @antigoId + 1;
	END IF;
end;
//
delimiter ;
