use newmyerp;
drop procedure if exists NEW_syncProduct;
delimiter //
create procedure NEW_syncProduct(productId INT(20))
begin
	set @exist = (SELECT count(myerp.tblproduto.fldId) FROM myerp.tblproduto WHERE myerp.tblproduto.fldId = productId);
	set @nome = (select CONCAT(newmyerp.Product.name, ' ',
								ifNull((select GROUP_CONCAT(newmyerp.CharacteristicInstance.name 
								ORDER BY newmyerp.Characteristic.displayOrder SEPARATOR ', ') 
								from newmyerp.ProductCharacteristic
								left join newmyerp.CharacteristicInstance on 
									newmyerp.CharacteristicInstance.id = newmyerp.ProductCharacteristic.characteristicInstanceId
								left join newmyerp.Characteristic on 
									newmyerp.Characteristic.id = newmyerp.CharacteristicInstance.characteristicId
								where newmyerp.ProductCharacteristic.productInstanceId = newmyerp.ProductInstance.id), '')) as name
							from newmyerp.ProductInstance
							left join newmyerp.Product on newmyerp.Product.id = newmyerp.ProductInstance.productId
							where newmyerp.ProductInstance.id = productId);
	IF @exist > 0 THEN
		update myerp.tblproduto
			left join newmyerp.ProductInstance on newmyerp.ProductInstance.id = productId
			left join newmyerp.Product on newmyerp.Product.id = newmyerp.ProductInstance.productId set
			myerp.tblproduto.fldId = newmyerp.ProductInstance.id, 
			myerp.tblproduto.fldCodigo = newmyerp.ProductInstance.code,
			myerp.tblproduto.fldRefCodigo = newmyerp.Product.id,
			myerp.tblproduto.fldCategoria_Id = newmyerp.Product.productCategoryId, 
			myerp.tblproduto.fldMarca_Id = newmyerp.Product.productBrandId, 
			myerp.tblproduto.fldNome = @nome,
			myerp.tblproduto.fldValorCompra = newmyerp.ProductInstance.buyingPrice, 
			myerp.tblproduto.fldValorVenda = newmyerp.ProductInstance.price,
			myerp.tblproduto.fldEcf_Aliquota_Id = newmyerp.Product.productTaxRateId, 
			myerp.tblproduto.fldTipo_Id = newmyerp.Product.productTypeId,
                        myerp.tblproduto.fldObservacao = newmyerp.ProductInstance.obs,
                        myerp.tblproduto.fldCodigoBarras = newmyerp.ProductInstance.barcode,
                        myerp.tblproduto.fldExcluido = newmyerp.ProductInstance.disabled
			where myerp.tblproduto.fldId = productId;
	ELSE
		insert into myerp.tblproduto(myerp.tblproduto.fldId, myerp.tblproduto.fldCodigo, myerp.tblproduto.fldRefCodigo,
			myerp.tblproduto.fldCategoria_Id, myerp.tblproduto.fldMarca_Id, myerp.tblproduto.fldNome, 
			myerp.tblproduto.fldValorCompra, myerp.tblproduto.fldValorVenda, myerp.tblproduto.fldEcf_Aliquota_Id, 
			myerp.tblproduto.fldTipo_Id, myerp.tblproduto.fldObservacao, myerp.tblproduto.fldCodigoBarras)
		(select newmyerp.ProductInstance.id, newmyerp.ProductInstance.code, newmyerp.Product.id,
			newmyerp.Product.productCategoryId, newmyerp.Product.productBrandId, @nome, newmyerp.ProductInstance.buyingPrice, 
			newmyerp.ProductInstance.price, newmyerp.Product.productTaxRateId, newmyerp.Product.productTypeId, 
                        newmyerp.ProductInstance.obs, newmyerp.ProductInstance.barcode
			from newmyerp.ProductInstance left join newmyerp.Product on newmyerp.Product.id = newmyerp.ProductInstance.productId
		where newmyerp.ProductInstance.id = productId);

                set @estoqueInicial = (SELECT newmyerp.ProductInstance.initialStock 
                                        FROM newmyerp.ProductInstance WHERE newmyerp.ProductInstance.id = productId);
                
                IF @estoqueInicial > 0 THEN
                  INSERT INTO myerp.tblproduto_estoque_movimento(fldData, fldHora, fldProduto_Id, 
                  fldEntrada, fldSaida, fldTipo_Id, fldReferencia_Id, fldItem_Id, fldEstoque_Id, 
                  fldEstoque_Controle)
		  VALUES (NOW(), NOW(), productId, @estoqueInicial, '0', '7', '', '', '1', 3);
                END IF;
	END IF;
end;
//
delimiter ;
