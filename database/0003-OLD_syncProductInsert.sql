use myerp;
drop trigger if exists OLD_syncProductInsert;
/*delimiter //
create trigger OLD_syncProductInsert after insert on myerp.tblproduto
for each row
begin
	set @categoriaId = if(new.fldCategoria_Id > 0, new.fldCategoria_Id, null);
	set @produtoTipo = if(new.fldTipo_Id > 0, new.fldTipo_Id, null);
	set @produtoId 	 = new.fldId;
  	insert into newmyerp.Product(id, codePrefix, defaultPrice, defaultBuyingPrice, name, disabled, 
  		productCategoryId, productTypeId) 
	values(@produtoId, '', 0, 0, new.fldNome, '0', @categoriaId, @produtoTipo);

	insert into newmyerp.ProductInstance(code, price, buyingPrice, disabled, productId)
  	values(new.fldCodigo, new.fldValorVenda, new.fldValorCompra, '0', @produtoId);
end;
//
delimiter ;
*/