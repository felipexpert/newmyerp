use newmyerp;
drop trigger if exists NEW_syncBSaleInsert;
delimiter //
create trigger NEW_syncBSaleInsert before insert on newmyerp.Sale
for each row
begin
	SET @antigoId = (SELECT myerp.tblpedido.fldId FROM myerp.tblpedido ORDER BY myerp.tblpedido.fldId DESC LIMIT 1);
	SET @novoId = ifnull((SELECT newmyerp.Sale.id FROM newmyerp.Sale ORDER BY newmyerp.Sale.id DESC LIMIT 1), 0);
	insert into newmyerp.Logger (message) values (@antigoId);
	insert into newmyerp.Logger (message) values (@novoId);
	IF @antigoId > @novoId THEN
		SET new.id = @antigoId + 1;
	END IF;
end;
//
delimiter ;
