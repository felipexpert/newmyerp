use newmyerp;
drop procedure if exists NEW_syncPayer;
delimiter //
create procedure NEW_syncPayer(payerId INT(20))
begin
	update myerp.tblpedido_parcela left join newmyerp.Payer on newmyerp.Payer.id = payerId set
	fldObservacao = newmyerp.Payer.description,
	fldPedido_Id = newmyerp.Payer.saleId
	where fldId IN ((SELECT id FROM newmyerp.Portion WHERE portionBucketId = newmyerp.Payer.portionBucketId));

	update myerp.tblpedido left join newmyerp.Payer on newmyerp.Payer.id = payerId set
	fldCliente_Id = newmyerp.Payer.customerId
	where fldId = newmyerp.Payer.saleId;
end;
//
delimiter ;
