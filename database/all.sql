use myerp;
drop trigger if exists OLD_syncEmployeeInsert;
drop trigger if exists OLD_syncEmployeeUpdate;
drop trigger if exists OLD_syncProductInsert;
drop trigger if exists OLD_syncProductUpdate;
drop trigger if exists OLD_syncProductTypeInsert;
drop trigger if exists OLD_syncProductTypeUpdate;
drop trigger if exists OLD_syncUserInsert;
drop trigger if exists OLD_syncUserUpdate;
drop trigger if exists OLD_syncPaymentInsert;
drop trigger if exists OLD_syncProductBrandInsert;
drop trigger if exists OLD_syncProductBrandUpdate;

use newmyerp;
drop trigger if exists NEW_syncSaleInsert;
drop trigger if exists NEW_BsyncSaleInsert;
drop trigger if exists NEW_syncSaleUpdate;
drop trigger if exists NEW_syncOrderUpdate;
drop trigger if exists NEW_syncOrderItemInsert;
drop trigger if exists NEW_syncOrderItemUpdate;
drop trigger if exists NEW_syncPortionInsert;
drop trigger if exists NEW_syncPortionUpdate;
drop procedure if exists NEW_syncPayer;
drop trigger if exists NEW_syncPayerUpdate;
drop trigger if exists NEW_syncPaymentInsert;
drop trigger if exists NEW_syncPenaltyInsert;
drop trigger if exists NEW_syncPenaltyUpdate;
drop trigger if exists NEW_syncPayerDelete;
drop trigger if exists NEW_syncCategoryInsert;
drop trigger if exists NEW_syncCategoryUpdate;
drop trigger if exists NEW_syncCustomerInsert;
drop trigger if exists NEW_syncCustomerUpdate;
drop trigger if exists NEW_syncPaymentUpdate;
drop trigger if exists NEW_syncProductFlowInsert;
drop procedure if exists NEW_syncItem;

drop trigger if exists NEW_syncCharacteristicUpdate;
drop trigger if exists NEW_syncCharacteristicInstanceUpdate;
drop trigger if exists NEW_syncBOrderItemInsert;
drop trigger if exists NEW_syncOrderItemProductFlowInsert;
drop trigger if exists NEW_syncBPaymntInsert;
drop trigger if exists NEW_syncBPortionInsert;
drop trigger if exists NEW_syncProductUpdate;
drop trigger if exists NEW_syncProductCategoryInsert;
drop trigger if exists NEW_syncProductCategoryUpdate;
drop trigger if exists NEW_syncProductCharacteristicInsert;
drop trigger if exists NEW_syncProductCharacteristicUpdate;
drop trigger if exists NEW_syncProductInstanceUpdate;
drop trigger if exists NEW_syncProductSnapshotUpdate;
drop trigger if exists NEW_syncBSaleInsert;
drop trigger if exists NEW_syncPortionDelete;
drop trigger if exists newSyncProductFlow;
drop trigger if exists newSyncProductFlow2;
drop trigger if exists OLD_syncPaymentUpdate;

use myerp;
drop trigger if exists OLD_syncEmployeeInsert;
delimiter //
create trigger OLD_syncEmployeeInsert after insert on myerp.tblfuncionario
for each row
begin
  insert into newmyerp.Employee(id, name) values(new.fldId, new.fldNome);
end;
//
delimiter ;
use myerp;
drop trigger if exists OLD_syncEmployeeUpdate;
delimiter //
create trigger OLD_syncEmployeeUpdate after update on myerp.tblfuncionario
for each row
begin
  update newmyerp.Employee set newmyerp.Employee.name = new.fldNome where newmyerp.Employee.id = old.fldId;
end;
//
delimiter ;
use myerp;
drop trigger if exists OLD_syncProductInsert;
/*delimiter //
create trigger OLD_syncProductInsert after insert on myerp.tblproduto
for each row
begin
	set @categoriaId = if(new.fldCategoria_Id > 0, new.fldCategoria_Id, null);
	set @produtoTipo = if(new.fldTipo_Id > 0, new.fldTipo_Id, null);
	set @produtoId 	 = new.fldId;
  	insert into newmyerp.Product(id, codePrefix, defaultPrice, defaultBuyingPrice, name, disabled, 
  		productCategoryId, productTypeId) 
	values(@produtoId, '', 0, 0, new.fldNome, '0', @categoriaId, @produtoTipo);

	insert into newmyerp.ProductInstance(code, price, buyingPrice, disabled, productId)
  	values(new.fldCodigo, new.fldValorVenda, new.fldValorCompra, '0', @produtoId);
end;
//
delimiter ;
*/
use myerp;
drop trigger if exists OLD_syncProductUpdate;
/*delimiter //
create trigger OLD_syncProductUpdate after update on myerp.tblproduto
for each row
begin
	set @categoriaId = if(new.fldCategoria_Id > 0, new.fldCategoria_Id, null);
	set @produtoTipo = if(new.fldTipo_Id > 0, new.fldTipo_Id, null);
	update newmyerp.Product set
	newmyerp.Product.name = new.fldNome, 
	newmyerp.Product.productCategoryId = @categoriaId, 
	newmyerp.Product.productTypeId = @produtoTipo
	where newmyerp.Product.id = old.fldId;

	update newmyerp.ProductInstance set
	newmyerp.ProductInstance.code = new.fldCodigo,
	newmyerp.ProductInstance.price = new.fldValorVenda,
	newmyerp.ProductInstance.buyingPrice = new.fldValorVenda,
	newmyerp.ProductInstance.disabled = '0'
	where newmyerp.ProductInstance.code = old.fldCodigo
	and newmyerp.ProductInstance.productId = old.fldId;
end;
//
delimiter ;*/
use myerp;
drop trigger if exists OLD_syncProductTypeInsert;
delimiter //
create trigger OLD_syncProductTypeInsert after insert on myerp.tblproduto_tipo
for each row
begin
  insert into newmyerp.ProductType(id, name, disabled)
  values(new.fldId, new.fldTipo, new.fldInativo);
end;
//
delimiter ;
use myerp;
drop trigger if exists OLD_syncProductTypeUpdate;
delimiter //
create trigger OLD_syncProductTypeUpdate after update on myerp.tblproduto_tipo
for each row
begin
  update newmyerp.ProductType set
  newmyerp.ProductType.name = new.fldTipo, 
  newmyerp.ProductType.disabled = new.fldInativo
  where id = old.fldId;
end;
//
delimiter ;
use myerp;
drop trigger if exists OLD_syncUserInsert;
delimiter //
create trigger OLD_syncUserInsert after insert on myerp.tblusuario
for each row
begin
  insert into newmyerp.User(id, login, passwd, employeeId)
  values(new.fldId, new.fldUsuario, new.fldSenha, new.fldFuncionario_Id);
end;
//
delimiter ;
use myerp;
drop trigger if exists OLD_syncUserUpdate;
delimiter //
create trigger OLD_syncUserUpdate after update on myerp.tblusuario
for each row
begin
  update newmyerp.User set
  newmyerp.User.login = new.fldUsuario, 
  newmyerp.User.passwd = new.fldSenha, 
  newmyerp.User.employeeId = new.fldFuncionario_Id
  where newmyerp.User.id = old.fldId;
end;
//
delimiter ;
use newmyerp;
drop trigger if exists NEW_syncSaleInsert;
delimiter //
create trigger NEW_syncSaleInsert after insert on newmyerp.Sale
for each row
begin
	DECLARE done INT DEFAULT FALSE;
  DECLARE ids INT;
  DECLARE cur CURSOR FOR (
    select oi.id 
      from newmyerp.OrderItem oi
      inner join newmyerp.Consignment c on c.orderId = oi.orderId
    where c.id = new.consignmentId
  );
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	-- set @consignado = (SELECT COUNT(id) FROM newmyerp.Consignment WHERE newmyerp.Consignment.orderId = new.orderId);
        insert into newmyerp.Logger(message) values('veio0 START');
        insert into newmyerp.Logger(message)
          values(concat('saleInsert veio1 consignmentId <<', if(isNULL(new.consignmentId),'null', new.consignmentId), '>>'));
 	IF (!isNull(new.consignmentId)) THEN
              insert into newmyerp.Logger(message)
                values('saleInsert veio2');
		OPEN cur;
	      ins_loop: LOOP
	          FETCH cur INTO ids;
	          IF done THEN
	              LEAVE ins_loop;
	          END IF;
                  insert into newmyerp.Logger(message)
                    values(concat('saleInsert veio3 <<', ids, '>>'));
	          CALL balancearEstoque(ids);
	      END LOOP;
	  CLOSE cur;
 	END IF;
        insert into newmyerp.Logger(message)
        values('saleInsert veio4 end');

  insert into myerp.tblpedido(fldId, fromNew, fldCliente_Id, fldComanda_Numero, fldComanda_Fechamento, fldTipo_Id,
 	fldObservacao, fldCadastroData, fldPedidoData, fldCadastroHora, fldExcluido, fldUsuario_Id, fldStatus)
 	select new.id, '1', '0', new.orderNumber, new.closing, new.sellingType, newmyerp.Order.obs, 
 	newmyerp.Order.creation, newmyerp.Order.creation, newmyerp.Order.creation, newmyerp.Order.disabled, newmyerp.Order.userId,
 	'4' as status from newmyerp.Order where newmyerp.Order.id = new.orderId;
end;
//
delimiter ;

use newmyerp;
drop trigger if exists NEW_syncSaleUpdate;
delimiter //
create trigger NEW_syncSaleUpdate after update on newmyerp.Sale
for each row
begin
  update myerp.tblpedido set
  	fldComanda_Numero = new.orderNumber,
  	fldComanda_Fechamento = new.closing,
  	fldTipo_id = new.sellingType
  where fldId = old.id;
end;
//
delimiter ;

use newmyerp;
drop trigger if exists NEW_syncOrderUpdate;
delimiter //
create trigger NEW_syncOrderUpdate after update on newmyerp.Order
for each row
begin
  set @saleId = (SELECT id FROM newmyerp.Sale WHERE newmyerp.Sale.orderId = old.id);
  IF @saleId != '' THEN
  	update myerp.tblpedido set 
  		fldObservacao = new.obs,
  		fldCadastroData = new.creation,
  		fldCadastroHora = new.creation,
  		fldExcluido = new.disabled,
  		fldUsuario_Id = new.userId,
      fldDescontoReais = new.discount
  	where fldId = @saleId;
  END IF;
end;
//
delimiter ;

use newmyerp;
drop trigger if exists NEW_syncOrderItemInsert;
/*delimiter //
create trigger NEW_syncOrderItemInsert after insert on newmyerp.OrderItem
for each row
begin
	set @saleId 	= (SELECT id FROM newmyerp.Sale WHERE newmyerp.Sale.orderId = new.orderId);
	set @quantidade = 0;

	insert into myerp.tblpedido_item (fldId, fldProduto_Id, fldPedido_Id, fldValor,
	fldValor_Compra, fldDesconto, fldQuantidade, fldDescricao, fldExcluido)
	(select newmyerp.OrderItem.id, newmyerp.ProductSnapshot.productInstanceId, newmyerp.Sale.id, 
	newmyerp.ProductSnapshot.price, newmyerp.ProductSnapshot.buyingPrice, newmyerp.OrderItem.discount, 
	newmyerp.ProductFlow.amount, newmyerp.ProductSnapshot.name, '0' from OrderItem
	left join newmyerp.Sale on newmyerp.Sale.orderId = newmyerp.OrderItem.orderId
	left join newmyerp.OrderItemProductFlow on newmyerp.OrderItemProductFlow.orderItemId = newmyerp.OrderItem.id 
	left join newmyerp.ProductFlow on newmyerp.OrderItemProductFlow.productFlowId = newmyerp.ProductFlow.id
	left join newmyerp.ProductSnapshot on newmyerp.ProductSnapshot.id = newmyerp.OrderItem.productSnapshotId
	where newmyerp.OrderItem.id = @orderItemId);
	
	insert into myerp.tblpedido_item(fldId, fldProduto_Id, fldPedido_Id, fldValor, fldValor_Compra,
		fldDesconto, fldQuantidade, fldDescricao, fldExcluido, fldEntregueData, fldExibicaoOrdem)
	values(new.id, new.productId, @saleId, new.price, new.buyingPrice, new.discount, new.amount,
		new.description, new.disabled, new.deliveredDate, new.displayOrder);

	set @permissao = (SELECT fldEstoque_Controle FROM myerp.tblproduto WHERE fldId = new.productId);
	IF @permissao = 1 OR @permissao = 3 THEN
		insert into myerp.tblproduto_estoque_movimento(fldData, fldHora, fldProduto_Id, 
		fldSaida, fldTipo_Id, fldReferencia_Id, fldItem_Id, fldUsuario_Id, fldEstoque_Id)
		values(NOW(), NOW(), new.productId, new.amount, '1', @saleId, new.id, '1', '1');
	END IF;
end;
//
delimiter ;
*/
use newmyerp;
drop trigger if exists NEW_syncOrderItemUpdate;
delimiter //
create trigger NEW_syncOrderItemUpdate after update on newmyerp.OrderItem
for each row
  begin
    update myerp.tblpedido_item set fldExcluido = new.disabled
    where fldId = old.id;
  end;
  //
  delimiter ;

use newmyerp;
drop trigger if exists NEW_syncPortionInsert;
delimiter //
create trigger NEW_syncPortionInsert after insert on newmyerp.Portion
for each row
begin
	set @portionNumber = (SELECT COUNT(id) FROM newmyerp.Portion 
				WHERE newmyerp.Portion.portionBucketId = new.portionBucketId) + 1;
	set @validUntil		= if(new.validUntil is null, NOW(), new.validUntil);
	set @payerId 		= (SELECT id FROM newmyerp.Payer WHERE newmyerp.Payer.portionBucketId = new.portionBucketId);
	insert into myerp.tblpedido_parcela(fldId, fldParcela, fldVencimento, fldValor, fldPagamento_Id, fldStatus)
	values(new.id, @portionNumber, @validUntil, new.total, '1', '1');
	CALL NEW_syncPayer(@payerId);
end;
//
delimiter ;

use newmyerp;
drop trigger if exists NEW_syncPortionUpdate;
delimiter //
create trigger NEW_syncPortionUpdate after update on newmyerp.Portion
for each row
begin
	set @validUntil		= if(new.validUntil is null, NOW(), new.validUntil);
	set @payerId 		= (SELECT id FROM newmyerp.Payer WHERE newmyerp.Payer.portionBucketId = new.portionBucketId);
	update myerp.tblpedido_parcela set
		fldVencimento 	= @validUntil,
		fldValor 		= new.total
	where fldId = old.id;
	CALL NEW_syncPayer(@payerId);
end;
//
delimiter ;

use newmyerp;
drop procedure if exists NEW_syncPayer;
delimiter //
create procedure NEW_syncPayer(payerId INT(20))
begin
	update myerp.tblpedido_parcela left join newmyerp.Payer on newmyerp.Payer.id = payerId set
	fldObservacao = newmyerp.Payer.description,
	fldPedido_Id = newmyerp.Payer.saleId
	where fldId IN ((SELECT id FROM newmyerp.Portion WHERE portionBucketId = newmyerp.Payer.portionBucketId));

	update myerp.tblpedido left join newmyerp.Payer on newmyerp.Payer.id = payerId set
	fldCliente_Id = newmyerp.Payer.customerId
	where fldId = newmyerp.Payer.saleId;
end;
//
delimiter ;

use newmyerp;
drop trigger if exists NEW_syncPayerUpdate;
delimiter //
create trigger NEW_syncPayerUpdate after update on newmyerp.Payer
for each row
begin
	CALL NEW_syncPayer(new.id);
end;
//
delimiter ;
use newmyerp;
drop trigger if exists NEW_syncPaymentInsert;
delimiter //
create trigger NEW_syncPaymentInsert after insert on newmyerp.Payment
for each row
begin
        insert into newmyerp.Logger (message)
          values ("comeco");
	set @verificarParcela = (SELECT count(myerp.tblpedido_parcela_baixa.fldId) FROM myerp.tblpedido_parcela_baixa WHERE myerp.tblpedido_parcela_baixa.fldId = new.id);
	IF @verificarParcela = 0 THEN
                set @isDebito = (select count(co.id) 

                                  from newmyerp.Payment pay

                                  inner join newmyerp.Portion por on por.id = pay.portionId
                                  inner join newmyerp.PortionBucket pb on pb.id = por.portionBucketId
                                  inner join newmyerp.CashOut co on co.portionBucket = pb.id

                                  where pay.id = new.id);
		set @parcelaValor = (SELECT total FROM newmyerp.Portion WHERE id = new.portionId);
		set @valor = IF(new.amount > @parcelaValor, @parcelaValor, new.amount);
		set @excluido = IF(new.reversalId > 0, 1, 0);

		insert into myerp.tblpedido_parcela_baixa(fldId, fldParcela_Id, fldDataCadastro, fldValor, fldDataRecebido, fldExcluido, fldPayment)
	 	values (new.id, new.portionId, new.creation, @valor, new.creation, @excluido, new.id);

		set @contaId 		= (SELECT fldValor FROM myerp.tblsistema 
						RIGHT JOIN myerp.tblpagamento_tipo ON myerp.tblpagamento_tipo.fldId = new.paymentTypeId
						WHERE fldParametro = CONCAT('pedido_parcela_acao_', myerp.tblpagamento_tipo.fldSigla));
		set @caixaData	= (SELECT fldData_Caixa FROM myerp.tblfinanceiro_conta WHERE fldId = @contaId);
                set @movimentoTipo = (
               select 
  coalesce
  ( if(isNull(s.id), null, 3)
  , if(isNull(coe.id), null, 10)
  , if(isNull(cie.id), null, 10) ) 

from Payment pay

-- Portion bucket
inner join Portion por on por.id = pay.portionId
inner join PortionBucket pb on pb.id = por.portionBucketId

-- Sale
left join ( 
  Payer p 
    inner join Sale s on s.id = p.saleId 
) on p.portionBucketId = pb.id

-- Exchange Out
left join (
  CashOut cashOut
    inner join Exchange coe on coe.cashOut = cashOut.id
) on cashOut.portionBucket = pb.id

-- Exchange In
left join (
  CashIn cashIn
    inner join Exchange cie on cie.cashIn = cashIn.id
) on cashIn.portionBucket = pb.id
where pay.id = new.id
                );

		insert into myerp.tblfinanceiro_conta_fluxo(fldData, fldDataInserida, fldHora, fldPagamento_Tipo_id,
		fldReferencia_Id, fldCredito, fldDebito, fldConta_Id, fldMovimento_Tipo)
		values(coalesce(@caixaData, NOW()), new.creation, new.creation, new.paymentTypeId, new.id, if(@isDebito = 0, @valor, 0), if(@isDebito > 0, @valor, 0), @contaId, @movimentoTipo);
    
    update myerp.tblpedido_parcela set myerp.tblpedido_parcela.fldPagamento_Id = new.paymentTypeId WHERE myerp.tblpedido_parcela.fldId = new.portionId;
	END IF;
        insert into newmyerp.Logger (message)
          values ("fim");
end;
//
delimiter ;

use newmyerp;
drop trigger if exists NEW_syncPaymentUpdate;
delimiter //
create trigger NEW_syncPaymentUpdate after update on newmyerp.Payment
for each row
begin
  /*
	set @parcelaValor = (SELECT total FROM newmyerp.Portion WHERE id = new.portionId);
	set @valor 				= IF(new.amount > @parcelaValor, @parcelaValor, new.amount);
	-- set @excluido = IF(new.reversalId > 0, 1, 0);
	update myerp.tblpedido_parcela_baixa set
		fldParcela_Id = new.portionId,
		fldDataCadastro = new.creation,
		fldValor = @valor,
		fldDataRecebido = new.creation
	--	fldExcluido = @excluido
	where fldId = old.id;
  */
end;
//
delimiter ;

use newmyerp;
drop trigger if exists NEW_syncPenaltyInsert;
delimiter //
create trigger NEW_syncPenaltyInsert after insert on newmyerp.Penalty
for each row
begin
	update myerp.tblpedido_parcela_baixa set fldMulta = new.amount where fldId = new.paymentId;
end;
//
delimiter ;
use newmyerp;
drop trigger if exists NEW_syncPenaltyUpdate;
delimiter //
create trigger NEW_syncPenaltyUpdate after update on newmyerp.Penalty
for each row
begin
	update myerp.tblpedido_parcela_baixa set fldMulta = new.amount where fldId = old.paymentId;
end;
//
delimiter ;
use newmyerp;
drop trigger if exists NEW_syncPayerDelete;
delimiter //
create trigger NEW_syncPayerDelete after delete on newmyerp.Payer
for each row
begin
	set @portionId = (SELECT id FROM newmyerp.Portion WHERE portionBucketId = old.portionBucketId);
	update myerp.tblpedido_parcela set fldExcluido = '1' where fldId = @portionId;
end;
//
delimiter ;
use newmyerp;
drop trigger if exists NEW_syncProductCategoryInsert;
delimiter //
create trigger NEW_syncProductCategoryInsert after insert on newmyerp.ProductCategory
for each row
begin
  insert into myerp.tblcategoria(fldId, fldNome) values(new.id, new.name);
end;
//
delimiter ;
use newmyerp;
drop trigger if exists NEW_syncProductCategoryUpdate;
delimiter //
create trigger NEW_syncProductCategoryUpdate after update on newmyerp.ProductCategory
for each row
begin
  update myerp.tblcategoria set myerp.tblcategoria.fldNome = new.name where myerp.tblcategoria.fldId = old.id;
end;
//
delimiter ;
use newmyerp;
drop trigger if exists NEW_syncCustomerInsert;
delimiter //
create trigger NEW_syncCustomerInsert after insert on newmyerp.Customer
for each row
begin
  insert into myerp.tblcliente(fldId, fldNome, fldCPF_CNPJ, fldRG_IE, fldEndereco, fldTelefone1, fldStatus_Id)
	select new.id, newmyerp.PersonAttr.name, newmyerp.PersonAttr.cpf, newmyerp.PersonAttr.rg, newmyerp.PersonAttr.address, 
	newmyerp.PersonAttr.fone, '4' from newmyerp.PersonAttr where newmyerp.PersonAttr.id = new.personAttrId;
end;
//
delimiter ;

use newmyerp;
drop trigger if exists NEW_syncCustomerUpdate;
delimiter //
create trigger NEW_syncCustomerUpdate after update on newmyerp.PersonAttr
for each row
begin
  update myerp.tblcliente set 
	myerp.tblcliente.fldNome = new.name, 
	myerp.tblcliente.fldCPF_CNPJ = new.cpf, 
	myerp.tblcliente.fldRG_IE = new.rg, 
	myerp.tblcliente.fldEndereco = new.address, 
	myerp.tblcliente.fldTelefone1 = new.fone
  where myerp.tblcliente.fldId = (SELECT id FROM newmyerp.Customer WHERE newmyerp.Customer.personAttrId = new.id);
end;
//
delimiter ;

use myerp;
drop trigger if exists OLD_syncPaymentInsert;
delimiter //
create trigger OLD_syncPaymentInsert after insert on myerp.tblpedido_parcela_baixa
for each row
begin
	set @verificarPagamento = (SELECT count(newmyerp.Payment.id) FROM newmyerp.Payment WHERE newmyerp.Payment.id = new.fldId);
	IF @verificarPagamento = 0 THEN
		set @verificarParcela = (SELECT count(newmyerp.Portion.id) FROM newmyerp.Portion WHERE newmyerp.Portion.id = new.fldParcela_Id);
		IF @verificarParcela = 1 THEN
			insert into newmyerp.Payment (id, amount, date, creation, paymentTypeId, portionId, reversalId)
			values (new.fldId, new.fldValor, null, new.fldDataRecebido, null, new.fldParcela_Id, null);
		END IF;
	END IF;
end;
//
delimiter ;
use newmyerp;
drop trigger if exists NEW_syncOrderItemProductFlowInsert;
delimiter //
create trigger NEW_syncOrderItemProductFlowInsert after insert on newmyerp.OrderItemProductFlow
for each row
begin
  declare consignId int(20);
  declare pfId int(20);
  declare pfAmount double;
  declare pfProductInstanceId int(20);
  -- update ProductFlow set OrderItem = new.orderItemId where id = new.productFlowId;
  CALL NEW_syncItem(new.orderItemId);

  insert into newmyerp.Logger (message)
    values ("syncProductFlowInsert - beginning");
  select s.consignmentId, pf.id, pf.amount, pf.productInstanceId
    into consignId, pfId, pfAmount, pfProductInstanceId
    from ProductFlow pf
    inner join OrderItemProductFlow oipf on oipf.productFlowId = pf.id
    inner join OrderItem oi on oi.id = oipf.orderItemId
    inner join Sale s on s.orderId = oi.orderId
  where pf.id = new.productFlowId;
  insert into newmyerp.Logger (message)
    values (concat("syncProductFlowInsert - before if, pfAmount ", coalesce(pfAmount, "null")));
  if(!isNull(consignId)) then
    insert into newmyerp.Logger (message)
      values ("syncProductFlowInsert - inside if start");
    set @positive = if(pfAmount >= 0, pfAmount, 0);
    set @negative = if(pfAmount < 0, pfAmount, 0);

    INSERT INTO myerp.tblproduto_estoque_movimento(fldData, fldHora, fldProduto_Id, 
    fldEntrada, fldSaida, fldTipo_Id, fldReferencia_Id, fldItem_Id, fldEstoque_Id, 
    fldConsignmentId)
    VALUES (NOW(), NOW(), pfProductInstanceId, @positive, @negative, '6', '', 
            '', '1', consignId);
    insert into newmyerp.Logger (message)
      values ("syncProductFlowInsert - inside if end");
  end if; 
  insert into newmyerp.Logger (message)
    values ("syncProductFlowInsert - after if");
  insert into newmyerp.Logger (message)
    values ("syncProductFlowInsert - end");

end;
//
delimiter ;

use newmyerp;
drop procedure if exists NEW_syncItem;
delimiter //
create procedure NEW_syncItem(orderItemId INT(20))
begin
        insert into newmyerp.Logger (message)
          values ("comeco");
	SET @orderItemId = orderItemId;
	SET @productFlow = (SELECT max(newmyerp.OrderItemProductFlow.productFlowId) FROM newmyerp.OrderItemProductFlow 
		WHERE newmyerp.OrderItemProductFlow.orderItemId = @orderItemId);
	SET @flowType = (SELECT newmyerp.ProductFlow.flowType FROM newmyerp.ProductFlow where newmyerp.ProductFlow.id = @productFlow);
	SET @insertEstoque = 1;
	SET @amount_orig = (SELECT newmyerp.ProductFlow.amount FROM newmyerp.ProductFlow where newmyerp.ProductFlow.id = @productFlow);
	SET @amount = ABS(@amount_orig);

	IF @flowType = 1 THEN
		-- checkar se pedido_item existe, se existir, update, else, insert
		SET @quantidade = (SELECT SUM(newmyerp.ProductFlow.amount) 
			FROM newmyerp.OrderItemProductFlow 
			LEFT JOIN newmyerp.ProductFlow ON newmyerp.ProductFlow.id = newmyerp.OrderItemProductFlow.productFlowId				
			WHERE newmyerp.OrderItemProductFlow.orderItemId = @orderItemId
			AND newmyerp.ProductFlow.flowType = @flowType);
		SET @quantidadeOld = IFNULL((SELECT myerp.tblpedido_item.fldQuantidade 
			FROM myerp.tblpedido_item WHERE myerp.tblpedido_item.fldId = orderItemId), 0);
		SET @valor 	= (SELECT newmyerp.ProductSnapshot.price FROM newmyerp.OrderItem
				LEFT JOIN newmyerp.ProductSnapshot ON newmyerp.ProductSnapshot.id = newmyerp.OrderItem.productSnapshotId
				WHERE newmyerp.OrderItem.id = @orderItemId);
		SET @desconto = (SELECT ((newmyerp.ProductSnapshot.discount * 100) / newmyerp.ProductSnapshot.price)  FROM newmyerp.OrderItem
			LEFT JOIN newmyerp.ProductSnapshot ON newmyerp.ProductSnapshot.id = newmyerp.OrderItem.productSnapshotId
			WHERE newmyerp.OrderItem.id = @orderItemId) / @quantidade;
		SET @exist = (select count(fldId) from myerp.tblpedido_item where myerp.tblpedido_item.fldId = @orderItemId);
		IF @exist > 0 THEN
			update myerp.tblpedido_item set myerp.tblpedido_item.fldQuantidade = @quantidade,
			myerp.tblpedido_item.fldValor = @valor,
			myerp.tblpedido_item.fldQuantidade = @quantidade,
			myerp.tblpedido_item.fldDesconto = @desconto
			where myerp.tblpedido_item.fldId = @orderItemId limit 1;
		ELSE
			insert into myerp.tblpedido_item (fldId, fldProduto_Id, fldPedido_Id, fldValor,
			fldValor_Compra, fldDesconto, fldQuantidade, fldDescricao, fldExcluido)
			(select newmyerp.OrderItem.id, newmyerp.ProductInstance.productId, newmyerp.Sale.id, 
			@valor, newmyerp.ProductSnapshot.buyingPrice, @desconto, @quantidade, newmyerp.ProductSnapshot.name, 
			'0' from newmyerp.OrderItem
			left join newmyerp.Sale on newmyerp.Sale.orderId = newmyerp.OrderItem.orderId
			left join newmyerp.OrderItemProductFlow on newmyerp.OrderItemProductFlow.orderItemId = newmyerp.OrderItem.id 
			left join newmyerp.ProductFlow on newmyerp.OrderItemProductFlow.productFlowId = newmyerp.ProductFlow.id
			left join newmyerp.ProductSnapshot on newmyerp.ProductSnapshot.id = newmyerp.OrderItem.productSnapshotId
			left join newmyerp.ProductInstance on newmyerp.ProductInstance.id = newmyerp.ProductSnapshot.productInstanceId
			where newmyerp.OrderItem.id = @orderItemId limit 1);
		END IF;

		IF @quantidade != @quantidadeOld THEN
			IF @amount_orig < 0 THEN
				SET @tipo = '2';
				SET @entrada = @amount;
				SET @saida = 0;
			ELSE
				SET @tipo = '1';
				SET @entrada = 0;
				SET @saida = @amount;
			END IF;
		ELSE
			SET @entrada = 0;
			SET @saida = 0;
		END IF;
	ELSEIF @flowType = 101 OR @flowType = 103 THEN
		SET @tipo = '15';
		SET @entrada = 0;
		SET @saida = @amount;
	ELSEIF @flowType = 102 OR @flowType = 104 THEN
		SET @tipo = '14';
		SET @entrada = @amount;
		SET @saida = 0;
	ELSEIF @flowType = 201 THEN
		SET @tipo = '16';
		SET @entrada = 0;
		SET @saida = @amount;
	ELSEIF @flowType = 202 THEN
		SET @tipo = '17';
		SET @entrada = @amount;
		SET @saida = 0;
	END IF;
                SET @produtoId = (SELECT newmyerp.ProductInstance.id FROM newmyerp.OrderItem
                                                left join newmyerp.ProductSnapshot on newmyerp.ProductSnapshot.id = newmyerp.OrderItem.productSnapshotId
                                                left join newmyerp.ProductInstance on newmyerp.ProductInstance.id = newmyerp.ProductSnapshot.productInstanceId
                                                where newmyerp.OrderItem.id = @orderItemId limit 1);
                INSERT INTO myerp.tblproduto_estoque_movimento(fldData, fldHora, fldProduto_Id, fldEntrada, fldSaida, fldTipo_Id, fldReferencia_Id, fldItem_Id, fldEstoque_Id, fldProductFlow)
                VALUES (NOW(), NOW(), @produtoId, @entrada, @saida, @tipo, @orderItemId, @orderItemId, '1', @productFlow);
        insert into newmyerp.Logger (message)
          values ("fim");
end;
//
delimiter ;

use newmyerp;
drop trigger if exists NEW_syncBSaleInsert;
delimiter //
create trigger NEW_syncBSaleInsert before insert on newmyerp.Sale
for each row
begin
	SET @antigoId = (SELECT myerp.tblpedido.fldId FROM myerp.tblpedido ORDER BY myerp.tblpedido.fldId DESC LIMIT 1);
	SET @novoId = ifnull((SELECT newmyerp.Sale.id FROM newmyerp.Sale ORDER BY newmyerp.Sale.id DESC LIMIT 1), 0);
	insert into newmyerp.Logger (message) values (@antigoId);
	insert into newmyerp.Logger (message) values (@novoId);
	IF @antigoId > @novoId THEN
		SET new.id = @antigoId + 1;
	END IF;
end;
//
delimiter ;

use newmyerp;
drop trigger if exists NEW_syncBOrderItemInsert;
delimiter //
create trigger NEW_syncBOrderItemInsert before insert on newmyerp.OrderItem
for each row
begin
	SET @antigoId 	= (SELECT myerp.tblpedido_item.fldId FROM myerp.tblpedido_item 
					ORDER BY myerp.tblpedido_item.fldId DESC LIMIT 1);
	SET @novoId		= ifnull((SELECT newmyerp.OrderItem.id FROM newmyerp.OrderItem
					ORDER BY newmyerp.OrderItem.id DESC LIMIT 1), 0);
	IF @antigoId > @novoId THEN
		SET new.id = @antigoId + 1;
	END IF;
end;
//
delimiter ;

use newmyerp;
drop trigger if exists NEW_syncBPortionInsert;
delimiter //
create trigger NEW_syncBPortionInsert before insert on newmyerp.Portion
for each row
begin
	SET @antigoId = (SELECT myerp.tblpedido_parcela.fldId FROM myerp.tblpedido_parcela 
					ORDER BY myerp.tblpedido_parcela.fldId DESC LIMIT 1);
	SET @novoId		= ifnull((SELECT newmyerp.Portion.id FROM newmyerp.Portion
					ORDER BY newmyerp.Portion.id DESC LIMIT 1), 0);
	IF @antigoId > @novoId THEN
		SET new.id = @antigoId + 1;
	END IF;
end;
//
delimiter ;

use newmyerp;
drop trigger if exists NEW_syncBPaymentInsert;
delimiter //
create trigger NEW_syncBPaymentInsert before insert on newmyerp.Payment
for each row
begin
	SET @antigoId = (SELECT myerp.tblpedido_parcela_baixa.fldId FROM myerp.tblpedido_parcela_baixa 
					ORDER BY myerp.tblpedido_parcela_baixa.fldId DESC LIMIT 1);
	SET @novoId		= ifnull((SELECT newmyerp.Payment.id FROM newmyerp.Payment
					ORDER BY newmyerp.Payment.id DESC LIMIT 1), 0);
	IF @antigoId > @novoId THEN
		SET new.id = @antigoId + 1;
	END IF;
end;
//
delimiter ;

use newmyerp;
drop trigger if exists NEW_syncProductSnapshotUpdate;
delimiter //
create trigger NEW_syncProductSnapshotUpdate after update on newmyerp.ProductSnapshot
for each row
begin
  set @orderItemId = (SELECT newmyerp.OrderItem.id FROM newmyerp.OrderItem WHERE newmyerp.OrderItem.productSnapshotId = old.id LIMIT 1);
  CALL NEW_syncItem(@orderItemId);
end;
//
delimiter ;

use newmyerp;
drop procedure if exists balancearEstoque;
delimiter //
create procedure balancearEstoque(itemId INT(20))
begin
        insert into newmyerp.Logger(message)
          values(concat('Going to balance orderItem (', itemId, ')'));
	SET @saidas = (SELECT SUM(newmyerp.ProductFlow.amount) as saidas FROM newmyerp.OrderItemProductFlow
	inner JOIN newmyerp.ProductFlow on newmyerp.ProductFlow.id = newmyerp.OrderItemProductFlow.productFlowId
	WHERE newmyerp.OrderItemProductFlow.orderItemId = itemId 
	and newmyerp.ProductFlow.flowType IN (101, 103)
        and newmyerp.ProductFlow.disabled = 0);
	SET @entradas = (SELECT SUM(newmyerp.ProductFlow.amount) as entradas FROM newmyerp.OrderItemProductFlow
	inner JOIN newmyerp.ProductFlow on newmyerp.ProductFlow.id = newmyerp.OrderItemProductFlow.productFlowId
	WHERE newmyerp.OrderItemProductFlow.orderItemId = itemId 
	and newmyerp.ProductFlow.flowType IN (102, 104)
        and newmyerp.ProductFlow.disabled = 0);
	SET @quantidade = coalesce(@saidas,0) - coalesce(@entradas,0);
        if @quantidade != 0 then
          SET @produtoId = (SELECT newmyerp.ProductInstance.productId FROM newmyerp.OrderItem
                                  left join newmyerp.ProductSnapshot on newmyerp.ProductSnapshot.id = newmyerp.OrderItem.productSnapshotId
                                  left join newmyerp.ProductInstance on newmyerp.ProductInstance.id = newmyerp.ProductSnapshot.productInstanceId
                                  where newmyerp.OrderItem.id = itemId limit 1);
          
          insert into newmyerp.Logger(message) 
            values(concat('quantity to going to apply <<', @quantidade, '>>'));
          set @consignmentId = (
            select c.id as consignmentId
              from newmyerp.OrderItem oi
              inner join newmyerp.`Order` o on o.id = oi.orderId
              inner join newmyerp.Consignment c on c.orderId = o.id
            where oi.id = itemId
          );
          INSERT INTO myerp.tblproduto_estoque_movimento(fldData, fldHora, fldProduto_Id, fldEntrada, fldSaida, fldTipo_Id, fldReferencia_Id, fldItem_Id, fldEstoque_Id, fldConsignmentId)
          VALUES (NOW(), NOW(), @produtoId, @quantidade, '0', '6', itemId, itemId, '1', @consignmentId);
        end if;
end;
//
delimiter ;

use newmyerp;
drop procedure if exists NEW_syncProduct;
delimiter //
create procedure NEW_syncProduct(productId INT(20))
begin
	set @exist = (SELECT count(myerp.tblproduto.fldId) FROM myerp.tblproduto WHERE myerp.tblproduto.fldId = productId);
	set @nome = (select CONCAT(newmyerp.Product.name, ' ',
								ifNull((select GROUP_CONCAT(newmyerp.CharacteristicInstance.name 
								ORDER BY newmyerp.Characteristic.displayOrder SEPARATOR ', ') 
								from newmyerp.ProductCharacteristic
								left join newmyerp.CharacteristicInstance on 
									newmyerp.CharacteristicInstance.id = newmyerp.ProductCharacteristic.characteristicInstanceId
								left join newmyerp.Characteristic on 
									newmyerp.Characteristic.id = newmyerp.CharacteristicInstance.characteristicId
								where newmyerp.ProductCharacteristic.productInstanceId = newmyerp.ProductInstance.id), '')) as name
							from newmyerp.ProductInstance
							left join newmyerp.Product on newmyerp.Product.id = newmyerp.ProductInstance.productId
							where newmyerp.ProductInstance.id = productId);
	IF @exist > 0 THEN
		update myerp.tblproduto
			left join newmyerp.ProductInstance on newmyerp.ProductInstance.id = productId
			left join newmyerp.Product on newmyerp.Product.id = newmyerp.ProductInstance.productId set
			myerp.tblproduto.fldId = newmyerp.ProductInstance.id, 
			myerp.tblproduto.fldCodigo = newmyerp.ProductInstance.code,
			myerp.tblproduto.fldRefCodigo = newmyerp.Product.id,
			myerp.tblproduto.fldCategoria_Id = newmyerp.Product.productCategoryId, 
			myerp.tblproduto.fldMarca_Id = newmyerp.Product.productBrandId, 
			myerp.tblproduto.fldNome = @nome,
			myerp.tblproduto.fldValorCompra = newmyerp.ProductInstance.buyingPrice, 
			myerp.tblproduto.fldValorVenda = newmyerp.ProductInstance.price,
			myerp.tblproduto.fldEcf_Aliquota_Id = newmyerp.Product.productTaxRateId, 
			myerp.tblproduto.fldTipo_Id = newmyerp.Product.productTypeId,
                        myerp.tblproduto.fldObservacao = newmyerp.ProductInstance.obs,
                        myerp.tblproduto.fldCodigoBarras = newmyerp.ProductInstance.barcode,
                        myerp.tblproduto.fldExcluido = newmyerp.ProductInstance.disabled
			where myerp.tblproduto.fldId = productId;
	ELSE
		insert into myerp.tblproduto(myerp.tblproduto.fldId, myerp.tblproduto.fldCodigo, myerp.tblproduto.fldRefCodigo,
			myerp.tblproduto.fldCategoria_Id, myerp.tblproduto.fldMarca_Id, myerp.tblproduto.fldNome, 
			myerp.tblproduto.fldValorCompra, myerp.tblproduto.fldValorVenda, myerp.tblproduto.fldEcf_Aliquota_Id, 
			myerp.tblproduto.fldTipo_Id, myerp.tblproduto.fldObservacao, myerp.tblproduto.fldCodigoBarras)
		(select newmyerp.ProductInstance.id, newmyerp.ProductInstance.code, newmyerp.Product.id,
			newmyerp.Product.productCategoryId, newmyerp.Product.productBrandId, @nome, newmyerp.ProductInstance.buyingPrice, 
			newmyerp.ProductInstance.price, newmyerp.Product.productTaxRateId, newmyerp.Product.productTypeId, 
                        newmyerp.ProductInstance.obs, newmyerp.ProductInstance.barcode
			from newmyerp.ProductInstance left join newmyerp.Product on newmyerp.Product.id = newmyerp.ProductInstance.productId
		where newmyerp.ProductInstance.id = productId);

                set @estoqueInicial = (SELECT newmyerp.ProductInstance.initialStock 
                                        FROM newmyerp.ProductInstance WHERE newmyerp.ProductInstance.id = productId);
                
                IF @estoqueInicial > 0 THEN
                  INSERT INTO myerp.tblproduto_estoque_movimento(fldData, fldHora, fldProduto_Id, 
                  fldEntrada, fldSaida, fldTipo_Id, fldReferencia_Id, fldItem_Id, fldEstoque_Id, 
                  fldEstoque_Controle)
		  VALUES (NOW(), NOW(), productId, @estoqueInicial, '0', '7', '', '', '1', 3);
                END IF;
	END IF;
end;
//
delimiter ;

use newmyerp;
drop trigger if exists NEW_syncProductInstanceUpdate;
delimiter //
create trigger NEW_syncProductInstanceUpdate after update on newmyerp.ProductInstance
for each row
begin
  CALL NEW_syncProduct(new.id);
end;
//
delimiter ;
use newmyerp;
drop trigger if exists NEW_syncProductCharacteristicInsert;
delimiter //
create trigger NEW_syncProductCharacteristicInsert after insert on newmyerp.ProductCharacteristic
for each row
begin
  CALL NEW_syncProduct(new.productInstanceId);
end;
//
delimiter ;
use newmyerp;
drop trigger if exists NEW_syncProductCharacteristicUpdate;
delimiter //
create trigger NEW_syncProductCharacteristicUpdate after update on newmyerp.ProductCharacteristic
for each row
begin
  CALL NEW_syncProduct(new.productInstanceId);
end;
//
delimiter ;
use newmyerp;
drop trigger if exists NEW_syncCharacteristicInstanceUpdate;
delimiter //
create trigger NEW_syncCharacteristicInstanceUpdate after update on newmyerp.CharacteristicInstance
for each row
begin
	set @productId = (SELECT productInstanceId FROM newmyerp.ProductCharacteristic 
									WHERE newmyerp.ProductCharacteristic.CharacteristicInstanceId = new.id);
  CALL NEW_syncProduct(@productId);
end;
//
delimiter ;
use newmyerp;
drop trigger if exists NEW_syncCharacteristicUpdate;
delimiter //
create trigger NEW_syncCharacteristicUpdate after update on newmyerp.Characteristic
for each row
begin
	DECLARE done INT DEFAULT FALSE;
  DECLARE ids INT;
  DECLARE cur CURSOR FOR SELECT DISTINCT productInstanceId 
  	FROM ProductCharacteristic 
  	LEFT JOIN CharacteristicInstance ON CharacteristicInstance.characteristicId = new.id
  	WHERE CharacteristicInstance.characteristicId = new.id;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	
	OPEN cur;
      ins_loop: LOOP
          FETCH cur INTO ids;
          IF done THEN
              LEAVE ins_loop;
          END IF;
          CALL NEW_syncProduct(ids);
      END LOOP;
  CLOSE cur;
end;
//
delimiter ;
use newmyerp;
drop trigger if exists NEW_syncProductUpdate;
delimiter //
create trigger NEW_syncProductUpdate after update on newmyerp.Product
for each row
begin
	DECLARE done INT DEFAULT FALSE;
  DECLARE ids INT;
  DECLARE cur CURSOR FOR SELECT DISTINCT id FROM ProductInstance WHERE ProductInstance.productId = new.id;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	
	OPEN cur;
      ins_loop: LOOP
          FETCH cur INTO ids;
          IF done THEN
              LEAVE ins_loop;
          END IF;
          CALL NEW_syncProduct(ids);
      END LOOP;
  CLOSE cur;
end;
//
delimiter ;
use myerp;
drop trigger if exists OLD_syncProductBrandInsert;
delimiter //
create trigger OLD_syncProductBrandInsert after insert on myerp.tblmarca
for each row
begin
  insert into newmyerp.ProductBrand(id, name, disabled)
  values(new.fldId, new.fldNome, '0');
end;
//
delimiter ;
use myerp;
drop trigger if exists OLD_syncProductBrandUpdate;
delimiter //
create trigger OLD_syncProductBrandUpdate after update on myerp.tblmarca
for each row
begin
	update newmyerp.ProductBrand set
	newmyerp.ProductBrand.name = new.fldNome
	where newmyerp.ProductBrand.id = new.fldId;
end;
//
delimiter ;
use newmyerp;
drop trigger if exists NEW_syncPortionDelete;
delimiter //
create trigger NEW_syncPortionDelete after delete on newmyerp.Portion
for each row
begin
	update myerp.tblpedido_parcela set
		fldExcluido 	= '1'
	where fldId = old.id;
end;
//
delimiter ;
drop trigger if exists NEW_ProductFlowUpdate;
delimiter //
create trigger NEW_ProductFlowUpdate after update on newmyerp.ProductFlow
for each row
begin
  insert into Logger (message) 
    values ("ProductFlow update started");
  set @hasProductEstoqueMovimento = (
    select count(pem.fldId) 
    from myerp.tblproduto_estoque_movimento pem
    where pem.fldProductFlow = new.id
  );
  insert into Logger (message)
  values ("Has product movement? " + @hasProductEstoqueMovimento);
  if @hasProductEstoqueMovimento > 0 then
      set @productEstoqueMovimento = (
      select pem.fldId
      from myerp.tblproduto_estoque_movimento pem
      where pem.fldProductFlow = new.id
      limit 1
    );
    set @hasEntrada = (
      select pem.fldEntrada
      from myerp.tblproduto_estoque_movimento pem
      where pem.fldId = @productEstoqueMovimento
      limit 1
    );
    set @hasSaida = (
      select pem.fldSaida
      from myerp.tblproduto_estoque_movimento pem
      where pem.fldId = @productEstoqueMovimento
      limit 1
    );
    set @entrada = if(@hasEntrada != 0, new.amount, 0);
    set @saida = if(@hasSaida != 0, new.amount, 0);
    update myerp.tblproduto_estoque_movimento pem set
      pem.fldEntrada = @entrada, pem.fldSaida = @saida
      where pem.fldId = @productEstoqueMovimento;
    if new.disabled = 1 then
      insert into Logger (message) 
        values ("will delete movement related to productFlow id " + new.id);
      delete from myerp.tblproduto_estoque_movimento
        where fldProductFlow = new.id;
    end if;
  end if;
  insert into Logger (message) 
    values ("ProductFlow update ended");
end;
//
delimiter ;

use myerp;
drop trigger if exists OLD_syncPaymentUpdate;
delimiter //
create trigger OLD_syncPaymentUpdate after update on myerp.tblpedido_parcela_baixa
for each row
begin
  insert into newmyerp.Logger(message) values('parcela_baixa update start');
  if (!isNull(new.fldPayment) and old.fldExcluido = 0 and new.fldExcluido = 1) then
    insert into newmyerp.Reversal(`date`, `close`, `obs`) values (now(), now(), '');
    SET @lastId = (select id from newmyerp.Reversal order by id desc limit 1);
    update newmyerp.Payment set newmyerp.Payment.reversalId = @lastId 
      where newmyerp.Payment.id = new.fldPayment;
  end if;
  insert into newmyerp.Logger(message) values('parcela_baixa update end');
end;
//
delimiter ;

use newmyerp;
drop trigger if exists NEW_syncOrderUpdate;
delimiter //
create trigger NEW_syncOrderUpdate after update on newmyerp.`Order`
for each row
begin
  set @consignmentId = (select s.consignmentId from newmyerp.Sale s
    inner join newmyerp.`Order` o on o.id = s.orderId
    where o.id = new.id);
  insert into newmyerp.Logger(message)
    values("Going to decide whether it will delete offsets");
  if (old.disabled = 0 and new.disabled != 0 and !isNull(@consignmentId)) then
    insert into newmyerp.Logger(message)
      values("Going to delete offsets");
    delete from myerp.tblproduto_estoque_movimento where fldConsignmentId = @consignmentId;
    insert into newmyerp.Logger(message)
      values(concat("Offsets with fldConsignmentId = ", @consignmentId, " has been deleted"));
  end if;
  insert into newmyerp.Logger(message)
    values("OrderUpdate ended");
end;
//
delimiter ;


