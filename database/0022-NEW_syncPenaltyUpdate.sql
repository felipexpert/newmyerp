use newmyerp;
drop trigger if exists NEW_syncPenaltyUpdate;
delimiter //
create trigger NEW_syncPenaltyUpdate after update on newmyerp.Penalty
for each row
begin
	update myerp.tblpedido_parcela_baixa set fldMulta = new.amount where fldId = old.paymentId;
end;
//
delimiter ;