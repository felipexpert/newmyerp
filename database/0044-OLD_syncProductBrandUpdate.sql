use myerp;
drop trigger if exists OLD_syncProductBrandUpdate;
delimiter //
create trigger OLD_syncProductBrandUpdate after update on myerp.tblmarca
for each row
begin
	update newmyerp.ProductBrand set
	newmyerp.ProductBrand.name = new.fldNome
	where newmyerp.ProductBrand.id = new.fldId;
end;
//
delimiter ;