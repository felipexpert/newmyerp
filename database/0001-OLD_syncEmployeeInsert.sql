use myerp;
drop trigger if exists OLD_syncEmployeeInsert;
delimiter //
create trigger OLD_syncEmployeeInsert after insert on myerp.tblfuncionario
for each row
begin
  insert into newmyerp.Employee(id, name) values(new.fldId, new.fldNome);
end;
//
delimiter ;