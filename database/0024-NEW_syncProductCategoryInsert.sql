use newmyerp;
drop trigger if exists NEW_syncProductCategoryInsert;
delimiter //
create trigger NEW_syncProductCategoryInsert after insert on newmyerp.ProductCategory
for each row
begin
  insert into myerp.tblcategoria(fldId, fldNome) values(new.id, new.name);
end;
//
delimiter ;