use newmyerp;
drop trigger if exists NEW_syncOrderItemInsert;
/*delimiter //
create trigger NEW_syncOrderItemInsert after insert on newmyerp.OrderItem
for each row
begin
	set @saleId 	= (SELECT id FROM newmyerp.Sale WHERE newmyerp.Sale.orderId = new.orderId);
	set @quantidade = 0;

	insert into myerp.tblpedido_item (fldId, fldProduto_Id, fldPedido_Id, fldValor,
	fldValor_Compra, fldDesconto, fldQuantidade, fldDescricao, fldExcluido)
	(select newmyerp.OrderItem.id, newmyerp.ProductSnapshot.productInstanceId, newmyerp.Sale.id, 
	newmyerp.ProductSnapshot.price, newmyerp.ProductSnapshot.buyingPrice, newmyerp.OrderItem.discount, 
	newmyerp.ProductFlow.amount, newmyerp.ProductSnapshot.name, '0' from OrderItem
	left join newmyerp.Sale on newmyerp.Sale.orderId = newmyerp.OrderItem.orderId
	left join newmyerp.OrderItemProductFlow on newmyerp.OrderItemProductFlow.orderItemId = newmyerp.OrderItem.id 
	left join newmyerp.ProductFlow on newmyerp.OrderItemProductFlow.productFlowId = newmyerp.ProductFlow.id
	left join newmyerp.ProductSnapshot on newmyerp.ProductSnapshot.id = newmyerp.OrderItem.productSnapshotId
	where newmyerp.OrderItem.id = @orderItemId);
	
	insert into myerp.tblpedido_item(fldId, fldProduto_Id, fldPedido_Id, fldValor, fldValor_Compra,
		fldDesconto, fldQuantidade, fldDescricao, fldExcluido, fldEntregueData, fldExibicaoOrdem)
	values(new.id, new.productId, @saleId, new.price, new.buyingPrice, new.discount, new.amount,
		new.description, new.disabled, new.deliveredDate, new.displayOrder);

	set @permissao = (SELECT fldEstoque_Controle FROM myerp.tblproduto WHERE fldId = new.productId);
	IF @permissao = 1 OR @permissao = 3 THEN
		insert into myerp.tblproduto_estoque_movimento(fldData, fldHora, fldProduto_Id, 
		fldSaida, fldTipo_Id, fldReferencia_Id, fldItem_Id, fldUsuario_Id, fldEstoque_Id)
		values(NOW(), NOW(), new.productId, new.amount, '1', @saleId, new.id, '1', '1');
	END IF;
end;
//
delimiter ;
*/