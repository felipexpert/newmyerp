use myerp;
drop trigger if exists OLD_syncEmployeeUpdate;
delimiter //
create trigger OLD_syncEmployeeUpdate after update on myerp.tblfuncionario
for each row
begin
  update newmyerp.Employee set newmyerp.Employee.name = new.fldNome where newmyerp.Employee.id = old.fldId;
end;
//
delimiter ;