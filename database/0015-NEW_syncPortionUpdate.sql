use newmyerp;
drop trigger if exists NEW_syncPortionUpdate;
delimiter //
create trigger NEW_syncPortionUpdate after update on newmyerp.Portion
for each row
begin
	set @validUntil		= if(new.validUntil is null, NOW(), new.validUntil);
	set @payerId 		= (SELECT id FROM newmyerp.Payer WHERE newmyerp.Payer.portionBucketId = new.portionBucketId);
	update myerp.tblpedido_parcela set
		fldVencimento 	= @validUntil,
		fldValor 		= new.total
	where fldId = old.id;
	CALL NEW_syncPayer(@payerId);
end;
//
delimiter ;
