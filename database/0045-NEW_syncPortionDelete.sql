use newmyerp;
drop trigger if exists NEW_syncPortionDelete;
delimiter //
create trigger NEW_syncPortionDelete after delete on newmyerp.Portion
for each row
begin
	update myerp.tblpedido_parcela set
		fldExcluido 	= '1'
	where fldId = old.id;
end;
//
delimiter ;