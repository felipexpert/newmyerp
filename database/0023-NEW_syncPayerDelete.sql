use newmyerp;
drop trigger if exists NEW_syncPayerDelete;
delimiter //
create trigger NEW_syncPayerDelete after delete on newmyerp.Payer
for each row
begin
	set @portionId = (SELECT id FROM newmyerp.Portion WHERE portionBucketId = old.portionBucketId);
	update myerp.tblpedido_parcela set fldExcluido = '1' where fldId = @portionId;
end;
//
delimiter ;