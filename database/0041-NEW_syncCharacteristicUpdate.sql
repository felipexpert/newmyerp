use newmyerp;
drop trigger if exists NEW_syncCharacteristicUpdate;
delimiter //
create trigger NEW_syncCharacteristicUpdate after update on newmyerp.Characteristic
for each row
begin
	DECLARE done INT DEFAULT FALSE;
  DECLARE ids INT;
  DECLARE cur CURSOR FOR SELECT DISTINCT productInstanceId 
  	FROM ProductCharacteristic 
  	LEFT JOIN CharacteristicInstance ON CharacteristicInstance.characteristicId = new.id
  	WHERE CharacteristicInstance.characteristicId = new.id;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	
	OPEN cur;
      ins_loop: LOOP
          FETCH cur INTO ids;
          IF done THEN
              LEAVE ins_loop;
          END IF;
          CALL NEW_syncProduct(ids);
      END LOOP;
  CLOSE cur;
end;
//
delimiter ;