use newmyerp;
drop trigger if exists NEW_syncProductCharacteristicInsert;
delimiter //
create trigger NEW_syncProductCharacteristicInsert after insert on newmyerp.ProductCharacteristic
for each row
begin
  CALL NEW_syncProduct(new.productInstanceId);
end;
//
delimiter ;