drop trigger if exists NEW_ProductFlowUpdate;
delimiter //
create trigger NEW_ProductFlowUpdate after update on newmyerp.ProductFlow
for each row
begin
  insert into Logger (message) 
    values ("ProductFlow update started");
  set @hasProductEstoqueMovimento = (
    select count(pem.fldId) 
    from myerp.tblproduto_estoque_movimento pem
    where pem.fldProductFlow = new.id
  );
  insert into Logger (message)
  values ("Has product movement? " + @hasProductEstoqueMovimento);
  if @hasProductEstoqueMovimento > 0 then
      set @productEstoqueMovimento = (
      select pem.fldId
      from myerp.tblproduto_estoque_movimento pem
      where pem.fldProductFlow = new.id
      limit 1
    );
    set @hasEntrada = (
      select pem.fldEntrada
      from myerp.tblproduto_estoque_movimento pem
      where pem.fldId = @productEstoqueMovimento
      limit 1
    );
    set @hasSaida = (
      select pem.fldSaida
      from myerp.tblproduto_estoque_movimento pem
      where pem.fldId = @productEstoqueMovimento
      limit 1
    );
    set @entrada = if(@hasEntrada != 0, new.amount, 0);
    set @saida = if(@hasSaida != 0, new.amount, 0);
    update myerp.tblproduto_estoque_movimento pem set
      pem.fldEntrada = @entrada, pem.fldSaida = @saida
      where pem.fldId = @productEstoqueMovimento;
    if new.disabled = 1 then
      insert into Logger (message) 
        values ("will delete movement related to productFlow id " + new.id);
      delete from myerp.tblproduto_estoque_movimento
        where fldProductFlow = new.id;
    end if;
  end if;
  insert into Logger (message) 
    values ("ProductFlow update ended");
end;
//
delimiter ;
