use newmyerp;
drop trigger if exists NEW_syncPayerUpdate;
delimiter //
create trigger NEW_syncPayerUpdate after update on newmyerp.Payer
for each row
begin
	CALL NEW_syncPayer(new.id);
end;
//
delimiter ;