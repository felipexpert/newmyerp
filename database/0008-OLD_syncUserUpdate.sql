use myerp;
drop trigger if exists OLD_syncUserUpdate;
delimiter //
create trigger OLD_syncUserUpdate after update on myerp.tblusuario
for each row
begin
  update newmyerp.User set
  newmyerp.User.login = new.fldUsuario, 
  newmyerp.User.passwd = new.fldSenha, 
  newmyerp.User.employeeId = new.fldFuncionario_Id
  where newmyerp.User.id = old.fldId;
end;
//
delimiter ;