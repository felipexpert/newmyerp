use newmyerp;
drop trigger if exists NEW_syncPaymentInsert;
delimiter //
create trigger NEW_syncPaymentInsert after insert on newmyerp.Payment
for each row
begin
        insert into newmyerp.Logger (message)
          values ("comeco");
	set @verificarParcela = (SELECT count(myerp.tblpedido_parcela_baixa.fldId) FROM myerp.tblpedido_parcela_baixa WHERE myerp.tblpedido_parcela_baixa.fldId = new.id);
	IF @verificarParcela = 0 THEN
                set @isDebito = (select count(co.id) 

                                  from newmyerp.Payment pay

                                  inner join newmyerp.Portion por on por.id = pay.portionId
                                  inner join newmyerp.PortionBucket pb on pb.id = por.portionBucketId
                                  inner join newmyerp.CashOut co on co.portionBucket = pb.id

                                  where pay.id = new.id);
		set @parcelaValor = (SELECT total FROM newmyerp.Portion WHERE id = new.portionId);
		set @valor = IF(new.amount > @parcelaValor, @parcelaValor, new.amount);
		set @excluido = IF(new.reversalId > 0, 1, 0);

		insert into myerp.tblpedido_parcela_baixa(fldId, fldParcela_Id, fldDataCadastro, fldValor, fldDataRecebido, fldExcluido, fldPayment)
	 	values (new.id, new.portionId, new.creation, @valor, new.creation, @excluido, new.id);

		set @contaId 		= (SELECT fldValor FROM myerp.tblsistema 
						RIGHT JOIN myerp.tblpagamento_tipo ON myerp.tblpagamento_tipo.fldId = new.paymentTypeId
						WHERE fldParametro = CONCAT('pedido_parcela_acao_', myerp.tblpagamento_tipo.fldSigla));
		set @caixaData	= (SELECT fldData_Caixa FROM myerp.tblfinanceiro_conta WHERE fldId = @contaId);
                set @movimentoTipo = (
               select 
  coalesce
  ( if(isNull(s.id), null, 3)
  , if(isNull(coe.id), null, 10)
  , if(isNull(cie.id), null, 10) ) 

from Payment pay

-- Portion bucket
inner join Portion por on por.id = pay.portionId
inner join PortionBucket pb on pb.id = por.portionBucketId

-- Sale
left join ( 
  Payer p 
    inner join Sale s on s.id = p.saleId 
) on p.portionBucketId = pb.id

-- Exchange Out
left join (
  CashOut cashOut
    inner join Exchange coe on coe.cashOut = cashOut.id
) on cashOut.portionBucket = pb.id

-- Exchange In
left join (
  CashIn cashIn
    inner join Exchange cie on cie.cashIn = cashIn.id
) on cashIn.portionBucket = pb.id
where pay.id = new.id
                );

		insert into myerp.tblfinanceiro_conta_fluxo(fldData, fldDataInserida, fldHora, fldPagamento_Tipo_id,
		fldReferencia_Id, fldCredito, fldDebito, fldConta_Id, fldMovimento_Tipo)
		values(coalesce(@caixaData, NOW()), new.creation, new.creation, new.paymentTypeId, new.id, if(@isDebito = 0, @valor, 0), if(@isDebito > 0, @valor, 0), @contaId, @movimentoTipo);
    
    update myerp.tblpedido_parcela set myerp.tblpedido_parcela.fldPagamento_Id = new.paymentTypeId WHERE myerp.tblpedido_parcela.fldId = new.portionId;
	END IF;
        insert into newmyerp.Logger (message)
          values ("fim");
end;
//
delimiter ;
