use newmyerp;
drop trigger if exists NEW_syncPortionInsert;
delimiter //
create trigger NEW_syncPortionInsert after insert on newmyerp.Portion
for each row
begin
	set @portionNumber = (SELECT COUNT(id) FROM newmyerp.Portion 
				WHERE newmyerp.Portion.portionBucketId = new.portionBucketId) + 1;
	set @validUntil		= if(new.validUntil is null, NOW(), new.validUntil);
	set @payerId 		= (SELECT id FROM newmyerp.Payer WHERE newmyerp.Payer.portionBucketId = new.portionBucketId);
	insert into myerp.tblpedido_parcela(fldId, fldParcela, fldVencimento, fldValor, fldPagamento_Id, fldStatus)
	values(new.id, @portionNumber, @validUntil, new.total, '1', '1');
	CALL NEW_syncPayer(@payerId);
end;
//
delimiter ;
