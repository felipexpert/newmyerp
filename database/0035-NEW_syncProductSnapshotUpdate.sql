use newmyerp;
drop trigger if exists NEW_syncProductSnapshotUpdate;
delimiter //
create trigger NEW_syncProductSnapshotUpdate after update on newmyerp.ProductSnapshot
for each row
begin
  set @orderItemId = (SELECT newmyerp.OrderItem.id FROM newmyerp.OrderItem WHERE newmyerp.OrderItem.productSnapshotId = old.id LIMIT 1);
  CALL NEW_syncItem(@orderItemId);
end;
//
delimiter ;
