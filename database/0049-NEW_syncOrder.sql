use newmyerp;
drop trigger if exists NEW_syncOrderUpdate;
delimiter //
create trigger NEW_syncOrderUpdate after update on newmyerp.`Order`
for each row
begin
  set @consignmentId = (select s.consignmentId from newmyerp.Sale s
    inner join newmyerp.`Order` o on o.id = s.orderId
    where o.id = new.id);
  insert into newmyerp.Logger(message)
    values("Going to decide whether it will delete offsets");
  if (old.disabled = 0 and new.disabled != 0 and !isNull(@consignmentId)) then
    insert into newmyerp.Logger(message)
      values("Going to delete offsets");
    delete from myerp.tblproduto_estoque_movimento where fldConsignmentId = @consignmentId;
    insert into newmyerp.Logger(message)
      values(concat("Offsets with fldConsignmentId = ", @consignmentId, " has been deleted"));
  end if;
  insert into newmyerp.Logger(message)
    values("OrderUpdate ended");
end;
//
delimiter ;
