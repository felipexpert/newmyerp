use newmyerp;
drop trigger if exists NEW_syncProductInstanceUpdate;
delimiter //
create trigger NEW_syncProductInstanceUpdate after update on newmyerp.ProductInstance
for each row
begin
  CALL NEW_syncProduct(new.id);
end;
//
delimiter ;