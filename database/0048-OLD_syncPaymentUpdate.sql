use myerp;
drop trigger if exists OLD_syncPaymentUpdate;
delimiter //
create trigger OLD_syncPaymentUpdate after update on myerp.tblpedido_parcela_baixa
for each row
begin
  insert into newmyerp.Logger(message) values('parcela_baixa update start');
  if (!isNull(new.fldPayment) and old.fldExcluido = 0 and new.fldExcluido = 1) then
    insert into newmyerp.Reversal(`date`, `close`, `obs`) values (now(), now(), '');
    SET @lastId = (select id from newmyerp.Reversal order by id desc limit 1);
    update newmyerp.Payment set newmyerp.Payment.reversalId = @lastId 
      where newmyerp.Payment.id = new.fldPayment;
  end if;
  insert into newmyerp.Logger(message) values('parcela_baixa update end');
end;
//
delimiter ;
