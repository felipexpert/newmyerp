use newmyerp;
drop trigger if exists NEW_syncBPaymentInsert;
delimiter //
create trigger NEW_syncBPaymentInsert before insert on newmyerp.Payment
for each row
begin
	SET @antigoId = (SELECT myerp.tblpedido_parcela_baixa.fldId FROM myerp.tblpedido_parcela_baixa 
					ORDER BY myerp.tblpedido_parcela_baixa.fldId DESC LIMIT 1);
	SET @novoId		= ifnull((SELECT newmyerp.Payment.id FROM newmyerp.Payment
					ORDER BY newmyerp.Payment.id DESC LIMIT 1), 0);
	IF @antigoId > @novoId THEN
		SET new.id = @antigoId + 1;
	END IF;
end;
//
delimiter ;
