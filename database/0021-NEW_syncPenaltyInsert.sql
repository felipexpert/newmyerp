use newmyerp;
drop trigger if exists NEW_syncPenaltyInsert;
delimiter //
create trigger NEW_syncPenaltyInsert after insert on newmyerp.Penalty
for each row
begin
	update myerp.tblpedido_parcela_baixa set fldMulta = new.amount where fldId = new.paymentId;
end;
//
delimiter ;