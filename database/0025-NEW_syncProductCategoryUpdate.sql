use newmyerp;
drop trigger if exists NEW_syncProductCategoryUpdate;
delimiter //
create trigger NEW_syncProductCategoryUpdate after update on newmyerp.ProductCategory
for each row
begin
  update myerp.tblcategoria set myerp.tblcategoria.fldNome = new.name where myerp.tblcategoria.fldId = old.id;
end;
//
delimiter ;