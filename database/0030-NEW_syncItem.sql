use newmyerp;
drop procedure if exists NEW_syncItem;
delimiter //
create procedure NEW_syncItem(orderItemId INT(20))
begin
        insert into newmyerp.Logger (message)
          values ("comeco");
	SET @orderItemId = orderItemId;
	SET @productFlow = (SELECT max(newmyerp.OrderItemProductFlow.productFlowId) FROM newmyerp.OrderItemProductFlow 
		WHERE newmyerp.OrderItemProductFlow.orderItemId = @orderItemId);
	SET @flowType = (SELECT newmyerp.ProductFlow.flowType FROM newmyerp.ProductFlow where newmyerp.ProductFlow.id = @productFlow);
	SET @insertEstoque = 1;
	SET @amount_orig = (SELECT newmyerp.ProductFlow.amount FROM newmyerp.ProductFlow where newmyerp.ProductFlow.id = @productFlow);
	SET @amount = ABS(@amount_orig);

	IF @flowType = 1 THEN
		-- checkar se pedido_item existe, se existir, update, else, insert
		SET @quantidade = (SELECT SUM(newmyerp.ProductFlow.amount) 
			FROM newmyerp.OrderItemProductFlow 
			LEFT JOIN newmyerp.ProductFlow ON newmyerp.ProductFlow.id = newmyerp.OrderItemProductFlow.productFlowId				
			WHERE newmyerp.OrderItemProductFlow.orderItemId = @orderItemId
			AND newmyerp.ProductFlow.flowType = @flowType);
		SET @quantidadeOld = IFNULL((SELECT myerp.tblpedido_item.fldQuantidade 
			FROM myerp.tblpedido_item WHERE myerp.tblpedido_item.fldId = orderItemId), 0);
		SET @valor 	= (SELECT newmyerp.ProductSnapshot.price FROM newmyerp.OrderItem
				LEFT JOIN newmyerp.ProductSnapshot ON newmyerp.ProductSnapshot.id = newmyerp.OrderItem.productSnapshotId
				WHERE newmyerp.OrderItem.id = @orderItemId);
		SET @desconto = (SELECT ((newmyerp.ProductSnapshot.discount * 100) / newmyerp.ProductSnapshot.price)  FROM newmyerp.OrderItem
			LEFT JOIN newmyerp.ProductSnapshot ON newmyerp.ProductSnapshot.id = newmyerp.OrderItem.productSnapshotId
			WHERE newmyerp.OrderItem.id = @orderItemId) / @quantidade;
		SET @exist = (select count(fldId) from myerp.tblpedido_item where myerp.tblpedido_item.fldId = @orderItemId);
		IF @exist > 0 THEN
			update myerp.tblpedido_item set myerp.tblpedido_item.fldQuantidade = @quantidade,
			myerp.tblpedido_item.fldValor = @valor,
			myerp.tblpedido_item.fldQuantidade = @quantidade,
			myerp.tblpedido_item.fldDesconto = @desconto
			where myerp.tblpedido_item.fldId = @orderItemId limit 1;
		ELSE
			insert into myerp.tblpedido_item (fldId, fldProduto_Id, fldPedido_Id, fldValor,
			fldValor_Compra, fldDesconto, fldQuantidade, fldDescricao, fldExcluido)
			(select newmyerp.OrderItem.id, newmyerp.ProductInstance.productId, newmyerp.Sale.id, 
			@valor, newmyerp.ProductSnapshot.buyingPrice, @desconto, @quantidade, newmyerp.ProductSnapshot.name, 
			'0' from newmyerp.OrderItem
			left join newmyerp.Sale on newmyerp.Sale.orderId = newmyerp.OrderItem.orderId
			left join newmyerp.OrderItemProductFlow on newmyerp.OrderItemProductFlow.orderItemId = newmyerp.OrderItem.id 
			left join newmyerp.ProductFlow on newmyerp.OrderItemProductFlow.productFlowId = newmyerp.ProductFlow.id
			left join newmyerp.ProductSnapshot on newmyerp.ProductSnapshot.id = newmyerp.OrderItem.productSnapshotId
			left join newmyerp.ProductInstance on newmyerp.ProductInstance.id = newmyerp.ProductSnapshot.productInstanceId
			where newmyerp.OrderItem.id = @orderItemId limit 1);
		END IF;

		IF @quantidade != @quantidadeOld THEN
			IF @amount_orig < 0 THEN
				SET @tipo = '2';
				SET @entrada = @amount;
				SET @saida = 0;
			ELSE
				SET @tipo = '1';
				SET @entrada = 0;
				SET @saida = @amount;
			END IF;
		ELSE
			SET @entrada = 0;
			SET @saida = 0;
		END IF;
	ELSEIF @flowType = 101 OR @flowType = 103 THEN
		SET @tipo = '15';
		SET @entrada = 0;
		SET @saida = @amount;
	ELSEIF @flowType = 102 OR @flowType = 104 THEN
		SET @tipo = '14';
		SET @entrada = @amount;
		SET @saida = 0;
	ELSEIF @flowType = 201 THEN
		SET @tipo = '16';
		SET @entrada = 0;
		SET @saida = @amount;
	ELSEIF @flowType = 202 THEN
		SET @tipo = '17';
		SET @entrada = @amount;
		SET @saida = 0;
	END IF;
                SET @produtoId = (SELECT newmyerp.ProductInstance.id FROM newmyerp.OrderItem
                                                left join newmyerp.ProductSnapshot on newmyerp.ProductSnapshot.id = newmyerp.OrderItem.productSnapshotId
                                                left join newmyerp.ProductInstance on newmyerp.ProductInstance.id = newmyerp.ProductSnapshot.productInstanceId
                                                where newmyerp.OrderItem.id = @orderItemId limit 1);
                INSERT INTO myerp.tblproduto_estoque_movimento(fldData, fldHora, fldProduto_Id, fldEntrada, fldSaida, fldTipo_Id, fldReferencia_Id, fldItem_Id, fldEstoque_Id, fldProductFlow)
                VALUES (NOW(), NOW(), @produtoId, @entrada, @saida, @tipo, @orderItemId, @orderItemId, '1', @productFlow);
        insert into newmyerp.Logger (message)
          values ("fim");
end;
//
delimiter ;
