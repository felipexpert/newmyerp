use newmyerp;
drop trigger if exists NEW_syncBPortionInsert;
delimiter //
create trigger NEW_syncBPortionInsert before insert on newmyerp.Portion
for each row
begin
	SET @antigoId = (SELECT myerp.tblpedido_parcela.fldId FROM myerp.tblpedido_parcela 
					ORDER BY myerp.tblpedido_parcela.fldId DESC LIMIT 1);
	SET @novoId		= ifnull((SELECT newmyerp.Portion.id FROM newmyerp.Portion
					ORDER BY newmyerp.Portion.id DESC LIMIT 1), 0);
	IF @antigoId > @novoId THEN
		SET new.id = @antigoId + 1;
	END IF;
end;
//
delimiter ;
