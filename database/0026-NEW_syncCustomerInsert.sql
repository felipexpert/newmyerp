use newmyerp;
drop trigger if exists NEW_syncCustomerInsert;
delimiter //
create trigger NEW_syncCustomerInsert after insert on newmyerp.Customer
for each row
begin
  insert into myerp.tblcliente(fldId, fldNome, fldCPF_CNPJ, fldRG_IE, fldEndereco, fldTelefone1, fldStatus_Id)
	select new.id, newmyerp.PersonAttr.name, newmyerp.PersonAttr.cpf, newmyerp.PersonAttr.rg, newmyerp.PersonAttr.address, 
	newmyerp.PersonAttr.fone, '4' from newmyerp.PersonAttr where newmyerp.PersonAttr.id = new.personAttrId;
end;
//
delimiter ;
