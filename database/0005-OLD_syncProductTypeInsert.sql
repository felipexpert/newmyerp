use myerp;
drop trigger if exists OLD_syncProductTypeInsert;
delimiter //
create trigger OLD_syncProductTypeInsert after insert on myerp.tblproduto_tipo
for each row
begin
  insert into newmyerp.ProductType(id, name, disabled)
  values(new.fldId, new.fldTipo, new.fldInativo);
end;
//
delimiter ;