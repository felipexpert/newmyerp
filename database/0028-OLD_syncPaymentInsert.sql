use myerp;
drop trigger if exists OLD_syncPaymentInsert;
delimiter //
create trigger OLD_syncPaymentInsert after insert on myerp.tblpedido_parcela_baixa
for each row
begin
	set @verificarPagamento = (SELECT count(newmyerp.Payment.id) FROM newmyerp.Payment WHERE newmyerp.Payment.id = new.fldId);
	IF @verificarPagamento = 0 THEN
		set @verificarParcela = (SELECT count(newmyerp.Portion.id) FROM newmyerp.Portion WHERE newmyerp.Portion.id = new.fldParcela_Id);
		IF @verificarParcela = 1 THEN
			insert into newmyerp.Payment (id, amount, date, creation, paymentTypeId, portionId, reversalId)
			values (new.fldId, new.fldValor, null, new.fldDataRecebido, null, new.fldParcela_Id, null);
		END IF;
	END IF;
end;
//
delimiter ;