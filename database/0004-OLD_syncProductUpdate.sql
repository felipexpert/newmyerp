use myerp;
drop trigger if exists OLD_syncProductUpdate;
/*delimiter //
create trigger OLD_syncProductUpdate after update on myerp.tblproduto
for each row
begin
	set @categoriaId = if(new.fldCategoria_Id > 0, new.fldCategoria_Id, null);
	set @produtoTipo = if(new.fldTipo_Id > 0, new.fldTipo_Id, null);
	update newmyerp.Product set
	newmyerp.Product.name = new.fldNome, 
	newmyerp.Product.productCategoryId = @categoriaId, 
	newmyerp.Product.productTypeId = @produtoTipo
	where newmyerp.Product.id = old.fldId;

	update newmyerp.ProductInstance set
	newmyerp.ProductInstance.code = new.fldCodigo,
	newmyerp.ProductInstance.price = new.fldValorVenda,
	newmyerp.ProductInstance.buyingPrice = new.fldValorVenda,
	newmyerp.ProductInstance.disabled = '0'
	where newmyerp.ProductInstance.code = old.fldCodigo
	and newmyerp.ProductInstance.productId = old.fldId;
end;
//
delimiter ;*/