use newmyerp;
drop trigger if exists NEW_syncCustomerUpdate;
delimiter //
create trigger NEW_syncCustomerUpdate after update on newmyerp.PersonAttr
for each row
begin
  update myerp.tblcliente set 
	myerp.tblcliente.fldNome = new.name, 
	myerp.tblcliente.fldCPF_CNPJ = new.cpf, 
	myerp.tblcliente.fldRG_IE = new.rg, 
	myerp.tblcliente.fldEndereco = new.address, 
	myerp.tblcliente.fldTelefone1 = new.fone
  where myerp.tblcliente.fldId = (SELECT id FROM newmyerp.Customer WHERE newmyerp.Customer.personAttrId = new.id);
end;
//
delimiter ;
