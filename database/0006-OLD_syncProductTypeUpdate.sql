use myerp;
drop trigger if exists OLD_syncProductTypeUpdate;
delimiter //
create trigger OLD_syncProductTypeUpdate after update on myerp.tblproduto_tipo
for each row
begin
  update newmyerp.ProductType set
  newmyerp.ProductType.name = new.fldTipo, 
  newmyerp.ProductType.disabled = new.fldInativo
  where id = old.fldId;
end;
//
delimiter ;