use newmyerp;
drop trigger if exists NEW_syncOrderItemProductFlowInsert;
delimiter //
create trigger NEW_syncOrderItemProductFlowInsert after insert on newmyerp.OrderItemProductFlow
for each row
begin
  declare consignId int(20);
  declare pfId int(20);
  declare pfAmount double;
  declare pfProductInstanceId int(20);
  -- update ProductFlow set OrderItem = new.orderItemId where id = new.productFlowId;
  CALL NEW_syncItem(new.orderItemId);

  insert into newmyerp.Logger (message)
    values ("syncProductFlowInsert - beginning");
  select s.consignmentId, pf.id, pf.amount, pf.productInstanceId
    into consignId, pfId, pfAmount, pfProductInstanceId
    from ProductFlow pf
    inner join OrderItemProductFlow oipf on oipf.productFlowId = pf.id
    inner join OrderItem oi on oi.id = oipf.orderItemId
    inner join Sale s on s.orderId = oi.orderId
  where pf.id = new.productFlowId;
  insert into newmyerp.Logger (message)
    values (concat("syncProductFlowInsert - before if, pfAmount ", coalesce(pfAmount, "null")));
  if(!isNull(consignId)) then
    insert into newmyerp.Logger (message)
      values ("syncProductFlowInsert - inside if start");
    set @positive = if(pfAmount >= 0, pfAmount, 0);
    set @negative = if(pfAmount < 0, pfAmount, 0);

    INSERT INTO myerp.tblproduto_estoque_movimento(fldData, fldHora, fldProduto_Id, 
    fldEntrada, fldSaida, fldTipo_Id, fldReferencia_Id, fldItem_Id, fldEstoque_Id, 
    fldConsignmentId)
    VALUES (NOW(), NOW(), pfProductInstanceId, @positive, @negative, '6', '', 
            '', '1', consignId);
    insert into newmyerp.Logger (message)
      values ("syncProductFlowInsert - inside if end");
  end if; 
  insert into newmyerp.Logger (message)
    values ("syncProductFlowInsert - after if");
  insert into newmyerp.Logger (message)
    values ("syncProductFlowInsert - end");

end;
//
delimiter ;
