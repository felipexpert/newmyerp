use newmyerp;
drop procedure if exists balancearEstoque;
delimiter //
create procedure balancearEstoque(itemId INT(20))
begin
        insert into newmyerp.Logger(message)
          values(concat('Going to balance orderItem (', itemId, ')'));
	SET @saidas = (SELECT SUM(newmyerp.ProductFlow.amount) as saidas FROM newmyerp.OrderItemProductFlow
	inner JOIN newmyerp.ProductFlow on newmyerp.ProductFlow.id = newmyerp.OrderItemProductFlow.productFlowId
	WHERE newmyerp.OrderItemProductFlow.orderItemId = itemId 
	and newmyerp.ProductFlow.flowType IN (101, 103)
        and newmyerp.ProductFlow.disabled = 0);
	SET @entradas = (SELECT SUM(newmyerp.ProductFlow.amount) as entradas FROM newmyerp.OrderItemProductFlow
	inner JOIN newmyerp.ProductFlow on newmyerp.ProductFlow.id = newmyerp.OrderItemProductFlow.productFlowId
	WHERE newmyerp.OrderItemProductFlow.orderItemId = itemId 
	and newmyerp.ProductFlow.flowType IN (102, 104)
        and newmyerp.ProductFlow.disabled = 0);
	SET @quantidade = coalesce(@saidas,0) - coalesce(@entradas,0);
        if @quantidade != 0 then
          SET @produtoId = (SELECT newmyerp.ProductInstance.productId FROM newmyerp.OrderItem
                                  left join newmyerp.ProductSnapshot on newmyerp.ProductSnapshot.id = newmyerp.OrderItem.productSnapshotId
                                  left join newmyerp.ProductInstance on newmyerp.ProductInstance.id = newmyerp.ProductSnapshot.productInstanceId
                                  where newmyerp.OrderItem.id = itemId limit 1);
          
          insert into newmyerp.Logger(message) 
            values(concat('quantity to going to apply <<', @quantidade, '>>'));
          set @consignmentId = (
            select c.id as consignmentId
              from newmyerp.OrderItem oi
              inner join newmyerp.`Order` o on o.id = oi.orderId
              inner join newmyerp.Consignment c on c.orderId = o.id
            where oi.id = itemId
          );
          INSERT INTO myerp.tblproduto_estoque_movimento(fldData, fldHora, fldProduto_Id, fldEntrada, fldSaida, fldTipo_Id, fldReferencia_Id, fldItem_Id, fldEstoque_Id, fldConsignmentId)
          VALUES (NOW(), NOW(), @produtoId, @quantidade, '0', '6', itemId, itemId, '1', @consignmentId);
        end if;
end;
//
delimiter ;
