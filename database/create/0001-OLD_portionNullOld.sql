use myerp;
ALTER TABLE `tblpedido_parcela` CHANGE `fldPagamento_Id` `fldPagamento_Id` TINYINT( 4 ) NULL;
ALTER TABLE  `tblproduto` CHANGE  `fldCategoria_Id`  `fldCategoria_Id` INT( 11 ) NULL DEFAULT NULL ,
CHANGE  `fldMarca_Id`  `fldMarca_Id` INT( 11 ) NULL DEFAULT NULL ,
CHANGE  `fldUN_Medida_Id`  `fldUN_Medida_Id` MEDIUMINT( 9 ) NULL DEFAULT NULL ,
CHANGE  `fldTipo_Id`  `fldTipo_Id` MEDIUMINT( 9 ) NULL DEFAULT NULL;
ALTER TABLE  `tblproduto` CHANGE  `fldCategoria_Id`  `fldCategoria_Id` INT( 11 ) NULL DEFAULT NULL ,
CHANGE  `fldSubCategoria_Id`  `fldSubCategoria_Id` BIGINT( 20 ) NULL DEFAULT NULL ,
CHANGE  `fldFornecedor_Id`  `fldFornecedor_Id` INT( 11 ) NULL DEFAULT NULL ,
CHANGE  `fldTipo_Id`  `fldTipo_Id` MEDIUMINT( 9 ) NULL DEFAULT NULL;
ALTER TABLE  `tblpedido` ADD  `fldFuncionario_Id` BIGINT( 20 ) NULL DEFAULT NULL;