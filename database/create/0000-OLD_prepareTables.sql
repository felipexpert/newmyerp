use myerp;
ALTER TABLE `tblproduto_estoque_movimento` CHANGE `fldEntrada` `fldEntrada` FLOAT( 10, 6 ) NULL ,
CHANGE `fldSaida` `fldSaida` FLOAT( 10, 6 ) NULL ;

update tblproduto set fldRefCodigo = fldId;

ALTER TABLE `tblpedido` ADD `fromNew` TINYINT(1);

-- --------------------- exchange adatpations/ corrections START -------------------------------------
alter table `tblproduto_estoque_movimento` add `fldProductFlow` int(11) NULL;

-- This is to ajust after sale being deactived
alter table `tblproduto_estoque_movimento` add `fldConsignmentId` int(11) NULL;

alter table `tblpedido_parcela_baixa` add `fldPayment` int(11) NULL;

INSERT INTO tblfinanceiro_conta_fluxo_tipo (fldId, fldTipo, fldReferencia_Id)
VALUES(10, 'Troca', 0);

INSERT INTO tblproduto_estoque_movimento_tipo
(fldId, fldDescricao, fldCor)
VALUES (16, 'Troca Entrada', '#900')
     , (17, 'Troca Saída', '#C60');

-- --------------------- exchange adatpations/ corrections END -------------------------------------
