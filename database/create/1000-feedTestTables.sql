use newmyerp;
-- Feeding tables with data from the Old System

-- Feeding Category table 
insert into `ProductCategory`(id, name)
select fldId, fldNome from myerp.tblcategoria;

-- Feeding ProductType table
insert into `ProductType` (id, name, disabled)
select fldId, fldTipo, fldInativo from myerp.tblproduto_tipo;

-- Feeding Product table
set FOREIGN_KEY_CHECKS = 0;
insert into `Product` (`id`, `productCategoryId`, `name`, `productTypeId`, `disabled`, codePrefix, defaultPrice, defaultBuyingPrice)
select fldId, nullif(fldCategoria_Id, 0), fldNome, nullif(fldTipo_Id, 0), fldExcluido, '', 0, 0 from myerp.tblproduto;
set FOREIGN_KEY_CHECKS = 1;

-- Feeding ProductInstance table
set FOREIGN_KEY_CHECKS = 0;
insert into ProductInstance (code, productId, price, buyingPrice, disabled)
select fldCodigo, fldId, fldValorVenda, fldValorCompra, 0 from myerp.tblproduto group by fldCodigo;
set FOREIGN_KEY_CHECKS = 1;

-- Feeding Employee table
insert into `Employee` (id, name)
select fldId, fldNome from myerp.tblfuncionario;

-- Feeding User table
set FOREIGN_KEY_CHECKS = 0;
insert into `User` (id, employeeId, `login`, `passwd`)
select fldId, nullIf(fldFuncionario_Id, 0), fldUsuario, fldSenha from myerp.tblusuario; 
set FOREIGN_KEY_CHECKS = 1;

-- Feeding PaymentType table
insert into PaymentType (id, name, acronym, displayOrder, disabled)
select fldId, fldTipo, fldSigla, fldId, fldExcluido from myerp.tblpagamento_tipo;
