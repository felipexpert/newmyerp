use newmyerp;
-- Feeding tables with data from the Old System

-- Feeding Category table 
insert into `ProductCategory`(id, name)
select fldId, fldNome from myerp.tblcategoria;

-- Feeding Brand table 
insert into `ProductBrand`(id, name)
select fldId, fldNome from myerp.tblmarca;

-- Feeding Tax table 
insert into `ProductTaxRate`(id, description, value)
select fldId, fldDescricao, fldEcf_Valor from myerp.tblecf_aliquota;

-- Feeding ProductType table
insert into `ProductType` (id, name, disabled)
select fldId, fldTipo, fldInativo from myerp.tblproduto_tipo;

-- Feeding ProductType table
insert into `Characteristic` (id, name, displayOrder, disabled)
values ('1', 'Padrão', '1', '0');

-- Feeding ProductType table
insert into `CharacteristicInstance` (id, name, disabled, characteristicId)
values ('1', '', '0', '1');

-- Feeding Product table
set FOREIGN_KEY_CHECKS = 0;
insert into `Product` (`id`, `codePrefix`, `defaultPrice`, `defaultBuyingPrice`,
	`name`, `disabled`, `productCategoryId`, `productBrandId`, `productTaxRateId`, `productTypeId`)
select fldId, '', 0, 0, fldNome, fldExcluido, nullif(fldCategoria_Id, 0), nullif(fldMarca_Id, 0), 
nullif(fldEcf_Aliquota_Id, 0), nullif(fldTipo_Id, 0) from myerp.tblproduto
where fldExcluido = 0 group by fldCodigo;
set FOREIGN_KEY_CHECKS = 1;

update Product set productCategoryId = null where productCategoryId = 0;

-- Feeding ProductInstance table
set FOREIGN_KEY_CHECKS = 0;
insert into `ProductInstance` (`id`, `code`, `price`, `buyingPrice`, `proportional`, `disabled`, `productId`)
select fldId, fldCodigo, fldValorVenda, fldValorCompra, '0', fldExcluido, fldId from myerp.tblproduto 
where fldExcluido = 0 group by fldCodigo;
set FOREIGN_KEY_CHECKS = 1;

-- Feeding ProductCharacteristic table
set FOREIGN_KEY_CHECKS = 0;
insert into `ProductCharacteristic` (`productInstanceId`, `characteristicInstanceId`)
select fldId, '1' from myerp.tblproduto where fldExcluido = 0 group by fldCodigo;
set FOREIGN_KEY_CHECKS = 1;

-- Feeding Employee table
insert into `Employee` (id, name)
select fldId, fldNome from myerp.tblfuncionario;

-- Feeding User table
set FOREIGN_KEY_CHECKS = 0;
insert into `User` (id, employeeId, `login`, `passwd`)
select fldId, nullIf(fldFuncionario_Id, 0), fldUsuario, fldSenha from myerp.tblusuario; 
set FOREIGN_KEY_CHECKS = 1;

-- Feeding PaymentType table
insert into PaymentType (id, name, acronym, displayOrder, disabled)
select fldId, fldTipo, fldSigla, fldId, fldExcluido from myerp.tblpagamento_tipo;

-- Feeding Sale table
-- insert into `Sale`(id, orderNumber, sellingType, closing)
-- select fldId, fldComanda_Numero, fldTipo_Id, fldComanda_Fechamento from myerp.tblpedido;

-- Feeding Order table
-- Create a temporary column
-- Update Sale table
-- Drop the temporary column
-- ALTER TABLE  `Order` ADD  `tempSaleId` BIGINT( 20 ) NOT NULL;
-- insert into `Order`(obs, creation, disabled, userId, tempSaleId)
-- select fldObservacao, CONCAT(fldCadastroData, ' ', fldCadastroHora), 
-- fldExcluido, nullIf(fldUsuario_Id, 0), fldId from myerp.tblpedido;
-- update `Sale` set `orderId` = (select `id` from `Order` where `tempSaleId` = Sale.id);

-- Feeding OrderItem
-- insert into `OrderItem`(id, price, buyingPrice, discount, description, disabled, 
-- deliveredDate, displayOrder, productId, orderId)
-- select fldId, fldValor, fldValor_Compra, fldDesconto, fldDescricao,
-- fldExcluido, fldEntregueData, fldExibicaoOrdem, fldProduto_Id, 
-- (select id from `Order` where `tempSaleId` = fldPedido_Id) from myerp.tblpedido_item;
-- ALTER TABLE `Order` DROP tempSaleId;

-- insert into `ProductFlow`(flowType, amount, creation, orderItemId)
-- (select '1', fldQuantidade, NOW(), fldId from myerp.tblpedido_item);

ALTER TABLE `PersonAttr` ADD `tempCustomerId` BIGINT( 20 ) NOT NULL;
insert into `PersonAttr` (name, rg, cpf, address, fone, creation, tempCustomerId)
select fldNome, fldRG_IE, fldCPF_CNPJ, fldEndereco, fldTelefone1, NOW(), fldId
FROM myerp.tblcliente;

insert into `Customer` (id, creation, disabled, personAttrId)
select fldId, NOW(), '0', 
(select id from `PersonAttr` where `tempCustomerId` = fldId)
from myerp.tblcliente;
ALTER TABLE `PersonAttr` DROP `tempCustomerId`;
