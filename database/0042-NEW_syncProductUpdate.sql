use newmyerp;
drop trigger if exists NEW_syncProductUpdate;
delimiter //
create trigger NEW_syncProductUpdate after update on newmyerp.Product
for each row
begin
	DECLARE done INT DEFAULT FALSE;
  DECLARE ids INT;
  DECLARE cur CURSOR FOR SELECT DISTINCT id FROM ProductInstance WHERE ProductInstance.productId = new.id;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	
	OPEN cur;
      ins_loop: LOOP
          FETCH cur INTO ids;
          IF done THEN
              LEAVE ins_loop;
          END IF;
          CALL NEW_syncProduct(ids);
      END LOOP;
  CLOSE cur;
end;
//
delimiter ;