use myerp;
drop trigger if exists OLD_syncUserInsert;
delimiter //
create trigger OLD_syncUserInsert after insert on myerp.tblusuario
for each row
begin
  insert into newmyerp.User(id, login, passwd, employeeId)
  values(new.fldId, new.fldUsuario, new.fldSenha, new.fldFuncionario_Id);
end;
//
delimiter ;