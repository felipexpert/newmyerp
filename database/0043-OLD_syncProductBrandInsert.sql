use myerp;
drop trigger if exists OLD_syncProductBrandInsert;
delimiter //
create trigger OLD_syncProductBrandInsert after insert on myerp.tblmarca
for each row
begin
  insert into newmyerp.ProductBrand(id, name, disabled)
  values(new.fldId, new.fldNome, '0');
end;
//
delimiter ;