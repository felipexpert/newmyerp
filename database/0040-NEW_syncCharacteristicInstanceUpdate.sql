use newmyerp;
drop trigger if exists NEW_syncCharacteristicInstanceUpdate;
delimiter //
create trigger NEW_syncCharacteristicInstanceUpdate after update on newmyerp.CharacteristicInstance
for each row
begin
	set @productId = (SELECT productInstanceId FROM newmyerp.ProductCharacteristic 
									WHERE newmyerp.ProductCharacteristic.CharacteristicInstanceId = new.id);
  CALL NEW_syncProduct(@productId);
end;
//
delimiter ;