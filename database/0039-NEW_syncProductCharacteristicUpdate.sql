use newmyerp;
drop trigger if exists NEW_syncProductCharacteristicUpdate;
delimiter //
create trigger NEW_syncProductCharacteristicUpdate after update on newmyerp.ProductCharacteristic
for each row
begin
  CALL NEW_syncProduct(new.productInstanceId);
end;
//
delimiter ;