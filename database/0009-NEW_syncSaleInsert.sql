use newmyerp;
drop trigger if exists NEW_syncSaleInsert;
delimiter //
create trigger NEW_syncSaleInsert after insert on newmyerp.Sale
for each row
begin
	DECLARE done INT DEFAULT FALSE;
  DECLARE ids INT;
  DECLARE cur CURSOR FOR (
    select oi.id 
      from newmyerp.OrderItem oi
      inner join newmyerp.Consignment c on c.orderId = oi.orderId
    where c.id = new.consignmentId
  );
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	-- set @consignado = (SELECT COUNT(id) FROM newmyerp.Consignment WHERE newmyerp.Consignment.orderId = new.orderId);
        insert into newmyerp.Logger(message) values('veio0 START');
        insert into newmyerp.Logger(message)
          values(concat('saleInsert veio1 consignmentId <<', if(isNULL(new.consignmentId),'null', new.consignmentId), '>>'));
 	IF (!isNull(new.consignmentId)) THEN
              insert into newmyerp.Logger(message)
                values('saleInsert veio2');
		OPEN cur;
	      ins_loop: LOOP
	          FETCH cur INTO ids;
	          IF done THEN
	              LEAVE ins_loop;
	          END IF;
                  insert into newmyerp.Logger(message)
                    values(concat('saleInsert veio3 <<', ids, '>>'));
	          CALL balancearEstoque(ids);
	      END LOOP;
	  CLOSE cur;
 	END IF;
        insert into newmyerp.Logger(message)
        values('saleInsert veio4 end');

  insert into myerp.tblpedido(fldId, fromNew, fldCliente_Id, fldComanda_Numero, fldComanda_Fechamento, fldTipo_Id,
 	fldObservacao, fldCadastroData, fldPedidoData, fldCadastroHora, fldExcluido, fldUsuario_Id, fldStatus)
 	select new.id, '1', '0', new.orderNumber, new.closing, new.sellingType, newmyerp.Order.obs, 
 	newmyerp.Order.creation, newmyerp.Order.creation, newmyerp.Order.creation, newmyerp.Order.disabled, newmyerp.Order.userId,
 	'4' as status from newmyerp.Order where newmyerp.Order.id = new.orderId;
end;
//
delimiter ;
