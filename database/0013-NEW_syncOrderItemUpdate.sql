use newmyerp;
drop trigger if exists NEW_syncOrderItemUpdate;
delimiter //
create trigger NEW_syncOrderItemUpdate after update on newmyerp.OrderItem
for each row
  begin
    update myerp.tblpedido_item set fldExcluido = new.disabled
    where fldId = old.id;
  end;
  //
  delimiter ;
