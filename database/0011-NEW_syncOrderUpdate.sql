use newmyerp;
drop trigger if exists NEW_syncOrderUpdate;
delimiter //
create trigger NEW_syncOrderUpdate after update on newmyerp.Order
for each row
begin
  set @saleId = (SELECT id FROM newmyerp.Sale WHERE newmyerp.Sale.orderId = old.id);
  IF @saleId != '' THEN
  	update myerp.tblpedido set 
  		fldObservacao = new.obs,
  		fldCadastroData = new.creation,
  		fldCadastroHora = new.creation,
  		fldExcluido = new.disabled,
  		fldUsuario_Id = new.userId,
      fldDescontoReais = new.discount
  	where fldId = @saleId;
  END IF;
end;
//
delimiter ;
