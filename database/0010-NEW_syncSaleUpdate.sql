use newmyerp;
drop trigger if exists NEW_syncSaleUpdate;
delimiter //
create trigger NEW_syncSaleUpdate after update on newmyerp.Sale
for each row
begin
  update myerp.tblpedido set
  	fldComanda_Numero = new.orderNumber,
  	fldComanda_Fechamento = new.closing,
  	fldTipo_id = new.sellingType
  where fldId = old.id;
end;
//
delimiter ;
